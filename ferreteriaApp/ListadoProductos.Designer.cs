﻿
namespace ferreteriaApp
{
    partial class ListadoProductos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListadoProductos));
            this.panel7 = new System.Windows.Forms.Panel();
            this.listProductos = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txt_mensaje = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_codigo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_producto = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Btn_buscar = new System.Windows.Forms.Button();
            this.Btn_seleccionar = new System.Windows.Forms.Button();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listProductos)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.listProductos);
            this.panel7.Location = new System.Drawing.Point(2, 165);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(690, 273);
            this.panel7.TabIndex = 33;
            // 
            // listProductos
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listProductos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.listProductos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.listProductos.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.listProductos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.listProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listProductos.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.listProductos.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.listProductos.Location = new System.Drawing.Point(4, 10);
            this.listProductos.Name = "listProductos";
            this.listProductos.ReadOnly = true;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listProductos.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.listProductos.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.listProductos.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Silver;
            this.listProductos.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.listProductos.Size = new System.Drawing.Size(683, 260);
            this.listProductos.TabIndex = 4;
            this.listProductos.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.listProductos_CellContentDoubleClick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txt_mensaje);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txt_codigo);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txt_producto);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.Btn_buscar);
            this.panel1.Location = new System.Drawing.Point(2, 23);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(690, 136);
            this.panel1.TabIndex = 34;
            // 
            // txt_mensaje
            // 
            this.txt_mensaje.AutoSize = true;
            this.txt_mensaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_mensaje.ForeColor = System.Drawing.Color.Red;
            this.txt_mensaje.Location = new System.Drawing.Point(23, 107);
            this.txt_mensaje.Name = "txt_mensaje";
            this.txt_mensaje.Size = new System.Drawing.Size(0, 16);
            this.txt_mensaje.TabIndex = 6;
            this.txt_mensaje.UseMnemonic = false;
            this.txt_mensaje.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(264, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(186, 24);
            this.label3.TabIndex = 5;
            this.label3.Text = "Datos del producto";
            // 
            // txt_codigo
            // 
            this.txt_codigo.Location = new System.Drawing.Point(477, 63);
            this.txt_codigo.Name = "txt_codigo";
            this.txt_codigo.Size = new System.Drawing.Size(210, 20);
            this.txt_codigo.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(474, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Código interno de producto";
            // 
            // txt_producto
            // 
            this.txt_producto.Location = new System.Drawing.Point(10, 63);
            this.txt_producto.Name = "txt_producto";
            this.txt_producto.Size = new System.Drawing.Size(461, 20);
            this.txt_producto.TabIndex = 1;
            this.txt_producto.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_producto_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nombre del producto";
            // 
            // Btn_buscar
            // 
            this.Btn_buscar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Btn_buscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btn_buscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_buscar.Image = global::ferreteriaApp.Properties.Resources.search;
            this.Btn_buscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Btn_buscar.Location = new System.Drawing.Point(606, 89);
            this.Btn_buscar.Name = "Btn_buscar";
            this.Btn_buscar.Size = new System.Drawing.Size(81, 32);
            this.Btn_buscar.TabIndex = 3;
            this.Btn_buscar.Text = "Buscar";
            this.Btn_buscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Btn_buscar.UseVisualStyleBackColor = true;
            this.Btn_buscar.Click += new System.EventHandler(this.button1_Click);
            // 
            // Btn_seleccionar
            // 
            this.Btn_seleccionar.BackColor = System.Drawing.Color.Transparent;
            this.Btn_seleccionar.FlatAppearance.BorderColor = System.Drawing.Color.Navy;
            this.Btn_seleccionar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btn_seleccionar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_seleccionar.ForeColor = System.Drawing.Color.SteelBlue;
            this.Btn_seleccionar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Btn_seleccionar.Location = new System.Drawing.Point(535, 444);
            this.Btn_seleccionar.Name = "Btn_seleccionar";
            this.Btn_seleccionar.Size = new System.Drawing.Size(154, 36);
            this.Btn_seleccionar.TabIndex = 5;
            this.Btn_seleccionar.Text = "Seleccionar producto";
            this.Btn_seleccionar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Btn_seleccionar.UseVisualStyleBackColor = false;
            this.Btn_seleccionar.Click += new System.EventHandler(this.btn_seleccionar_Click);
            // 
            // ListadoProductos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(704, 503);
            this.Controls.Add(this.Btn_seleccionar);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel7);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ListadoProductos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Listado de Productos";
            this.Activated += new System.EventHandler(this.ListadoProductos_Activated);
            this.Load += new System.EventHandler(this.ListadoProductos_Load);
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listProductos)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.DataGridView listProductos;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txt_producto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Btn_buscar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_codigo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label txt_mensaje;
        private System.Windows.Forms.Button Btn_seleccionar;
    }
}