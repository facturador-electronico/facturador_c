﻿using ferreteriaApp.CEN;
using ferreteriaApp.DAO;
using ferreteriaApp.HELPERS;
using System;
using System.Collections.Generic;

namespace ferreteriaApp.CLN
{
    public class CLNProductos
    {


        public Boolean insertarProductos(CenProducto producto)
        {
            try
            {
                DAOProductos dao = new DAOProductos();
                return dao.insertarProductos(producto);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }


        public Boolean updateProducto(CenProducto producto,int _id)
        {
            try
            {
                DAOProductos dao = new DAOProductos();
                return dao.updateProducto(producto, _id);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public Boolean eliminarProducto(int _id, DateTime _fecha_transaccion)
        {
            try
            {
                DAOProductos dao = new DAOProductos();
                return dao.eliminarProducto( _id, _fecha_transaccion);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }
        public List<CENListadoProducto> buscarProducto(String _descripcion, String _codigo_interno, int _id)
        {
            try
            {
                DAOProductos dao = new DAOProductos();
                return dao.buscarProducto(_descripcion, _codigo_interno, _id);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }


        //agregado codigo sunat producto

        public List<CENListadoCodSntProducto> buscarCodSntPdt(int _flag, int _cod_buscar, int _cod_clase, String _descripcion)
        {
            try
            {
                DAOProductos dao = new DAOProductos();
                return dao.buscarCodSntPdt(_flag,  _cod_buscar,  _cod_clase,  _descripcion);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }
    }

}
