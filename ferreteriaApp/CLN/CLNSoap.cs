﻿using ferreteriaApp.CEN;
using ferreteriaApp.DAO;
using System;
using System.Collections.Generic;

namespace ferreteriaApp.CLN
{
    public class CLNSoap
    {
        public CENEmpresa searchSoap()
        {
            try
            {
                DAOSoap dao = new DAOSoap();
                return dao.searchSoap();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

    }

}
