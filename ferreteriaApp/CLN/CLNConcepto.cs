﻿using ferreteriaApp.CEN;
using ferreteriaApp.DAO;
using System;
using System.Collections.Generic;

namespace ferreteriaApp.CLN
{
    public class CLNConcepto
    {
        public List<CENTipoDocumento> listaTipoDocumentos(int flag)
        {
            try
            {
                DAOConceptos dao = new DAOConceptos();
                return dao.documentos(flag);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }


        public  string ObtenerValorParametro(int flag)
        {
            DAOConceptos db = new DAOConceptos();
            return db.ObtenerValorParametro(flag);
        }
    }

}
