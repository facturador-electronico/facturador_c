﻿using System;

using System.Windows.Forms;

namespace ferreteriaApp
{
    public partial class aviso : Form
    {
        public aviso(String tipo, String titulo, String texto, Boolean Cancelar)
        {
            InitializeComponent();

            if (Cancelar)
            {
                this.btnCancelar.Visible = true;
            }
            else
            {
                this.btnCancelar.Visible = false;
            }
         
            if (tipo == "warning")
            {
                this.pictureBox1.Image = ferreteriaApp.Properties.Resources.warning_100px;
            }
            if (tipo == "success")
            {
                this.pictureBox1.Image = ferreteriaApp.Properties.Resources.success_100px;
            }


            this.lblTitulo.Text = titulo;
            this.lblTexto.Text = texto;

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }

   
    }
}
