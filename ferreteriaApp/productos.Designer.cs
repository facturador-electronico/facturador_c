﻿
namespace ferreteriaApp
{
    partial class productos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(productos));
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_buscar = new System.Windows.Forms.Button();
            this.lblOblCodSunat = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_codigo_interno = new System.Windows.Forms.TextBox();
            this.chkIgv = new System.Windows.Forms.CheckBox();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.txtPrecioVenta = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCodigoSunat = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbUnidad = new System.Windows.Forms.ComboBox();
            this.cmbTipoAfectacionVenta = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbMoneda = new System.Windows.Forms.ComboBox();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_buscar_codigo_interno = new System.Windows.Forms.TextBox();
            this.txt_buscar_descripcion = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.listaToExcel = new System.Windows.Forms.DataGridView();
            this.detalleLista = new System.Windows.Forms.DataGridView();
            this.lblError = new System.Windows.Forms.Label();
            this.panelPaginador = new System.Windows.Forms.Panel();
            this.bnt_next = new System.Windows.Forms.Button();
            this.btn_preview = new System.Windows.Forms.Button();
            this.lbl_totalPaginas = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lbl_pagina = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnEditar = new System.Windows.Forms.Button();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listaToExcel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detalleLista)).BeginInit();
            this.panelPaginador.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.AutoSize = true;
            this.panel3.BackColor = System.Drawing.Color.GhostWhite;
            this.panel3.Controls.Add(this.btn_buscar);
            this.panel3.Controls.Add(this.lblOblCodSunat);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.label28);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.txt_codigo_interno);
            this.panel3.Controls.Add(this.chkIgv);
            this.panel3.Controls.Add(this.btnGuardar);
            this.panel3.Controls.Add(this.txtPrecioVenta);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.txtCodigoSunat);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.cmbUnidad);
            this.panel3.Controls.Add(this.cmbTipoAfectacionVenta);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.cmbMoneda);
            this.panel3.Controls.Add(this.txtDescripcion);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.label24);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1090, 170);
            this.panel3.TabIndex = 47;
            // 
            // btn_buscar
            // 
            this.btn_buscar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btn_buscar.FlatAppearance.BorderSize = 0;
            this.btn_buscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_buscar.Image = global::ferreteriaApp.Properties.Resources.search;
            this.btn_buscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_buscar.Location = new System.Drawing.Point(1061, 90);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(27, 32);
            this.btn_buscar.TabIndex = 9;
            this.btn_buscar.UseVisualStyleBackColor = true;
            this.btn_buscar.Click += new System.EventHandler(this.btn_buscar_Click);
            // 
            // lblOblCodSunat
            // 
            this.lblOblCodSunat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOblCodSunat.ForeColor = System.Drawing.Color.Red;
            this.lblOblCodSunat.Location = new System.Drawing.Point(902, 100);
            this.lblOblCodSunat.Name = "lblOblCodSunat";
            this.lblOblCodSunat.Size = new System.Drawing.Size(10, 15);
            this.lblOblCodSunat.TabIndex = 75;
            this.lblOblCodSunat.Text = "*";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(130, 12);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(119, 13);
            this.label13.TabIndex = 72;
            this.label13.Text = "| Campos obligatorios (*)";
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = global::ferreteriaApp.Properties.Resources.icons8_remove_tag_32px;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(861, 133);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(102, 33);
            this.button1.TabIndex = 11;
            this.button1.Text = "Cancelar";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(588, 97);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(18, 13);
            this.label10.TabIndex = 73;
            this.label10.Text = "*";
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Red;
            this.label28.Location = new System.Drawing.Point(429, 57);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(18, 13);
            this.label28.TabIndex = 72;
            this.label28.Text = "*";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.DimGray;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(447, 52);
            this.label8.MaximumSize = new System.Drawing.Size(140, 20);
            this.label8.MinimumSize = new System.Drawing.Size(140, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(140, 20);
            this.label8.TabIndex = 71;
            this.label8.Text = "Código Producto";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_codigo_interno
            // 
            this.txt_codigo_interno.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txt_codigo_interno.Location = new System.Drawing.Point(599, 50);
            this.txt_codigo_interno.MaxLength = 255;
            this.txt_codigo_interno.Name = "txt_codigo_interno";
            this.txt_codigo_interno.Size = new System.Drawing.Size(150, 20);
            this.txt_codigo_interno.TabIndex = 2;
            // 
            // chkIgv
            // 
            this.chkIgv.AutoSize = true;
            this.chkIgv.Checked = true;
            this.chkIgv.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIgv.Location = new System.Drawing.Point(606, 99);
            this.chkIgv.Name = "chkIgv";
            this.chkIgv.Size = new System.Drawing.Size(64, 17);
            this.chkIgv.TabIndex = 5;
            this.chkIgv.Text = "inc. IGV";
            this.chkIgv.UseVisualStyleBackColor = true;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.Image")));
            this.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardar.Location = new System.Drawing.Point(969, 134);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(90, 33);
            this.btnGuardar.TabIndex = 10;
            this.btnGuardar.Text = "Grabar";
            this.btnGuardar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // txtPrecioVenta
            // 
            this.txtPrecioVenta.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtPrecioVenta.Location = new System.Drawing.Point(670, 97);
            this.txtPrecioVenta.MaxLength = 13;
            this.txtPrecioVenta.Name = "txtPrecioVenta";
            this.txtPrecioVenta.Size = new System.Drawing.Size(80, 20);
            this.txtPrecioVenta.TabIndex = 6;
            this.txtPrecioVenta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrecioVenta_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.DimGray;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(447, 94);
            this.label7.MaximumSize = new System.Drawing.Size(140, 20);
            this.label7.MinimumSize = new System.Drawing.Size(140, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(140, 20);
            this.label7.TabIndex = 58;
            this.label7.Text = "Precio Venta";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCodigoSunat
            // 
            this.txtCodigoSunat.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtCodigoSunat.Location = new System.Drawing.Point(922, 95);
            this.txtCodigoSunat.MaxLength = 8;
            this.txtCodigoSunat.Name = "txtCodigoSunat";
            this.txtCodigoSunat.Size = new System.Drawing.Size(137, 20);
            this.txtCodigoSunat.TabIndex = 8;
            this.txtCodigoSunat.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodigoSunat_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.DimGray;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(758, 97);
            this.label3.MaximumSize = new System.Drawing.Size(140, 20);
            this.label3.MinimumSize = new System.Drawing.Size(140, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 20);
            this.label3.TabIndex = 54;
            this.label3.Text = "Código Sunat";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbUnidad
            // 
            this.cmbUnidad.FormattingEnabled = true;
            this.cmbUnidad.Location = new System.Drawing.Point(922, 51);
            this.cmbUnidad.Name = "cmbUnidad";
            this.cmbUnidad.Size = new System.Drawing.Size(137, 21);
            this.cmbUnidad.TabIndex = 3;
            // 
            // cmbTipoAfectacionVenta
            // 
            this.cmbTipoAfectacionVenta.FormattingEnabled = true;
            this.cmbTipoAfectacionVenta.Location = new System.Drawing.Point(169, 135);
            this.cmbTipoAfectacionVenta.Name = "cmbTipoAfectacionVenta";
            this.cmbTipoAfectacionVenta.Size = new System.Drawing.Size(260, 21);
            this.cmbTipoAfectacionVenta.TabIndex = 7;
            this.cmbTipoAfectacionVenta.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.DimGray;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(15, 136);
            this.label2.MaximumSize = new System.Drawing.Size(150, 20);
            this.label2.MinimumSize = new System.Drawing.Size(150, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(150, 20);
            this.label2.TabIndex = 51;
            this.label2.Text = "T. Afectación Venta";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.Visible = false;
            // 
            // cmbMoneda
            // 
            this.cmbMoneda.FormattingEnabled = true;
            this.cmbMoneda.Location = new System.Drawing.Point(169, 93);
            this.cmbMoneda.Name = "cmbMoneda";
            this.cmbMoneda.Size = new System.Drawing.Size(260, 21);
            this.cmbMoneda.TabIndex = 4;
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtDescripcion.Location = new System.Drawing.Point(169, 50);
            this.txtDescripcion.MaxLength = 255;
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(260, 20);
            this.txtDescripcion.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.DimGray;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(15, 52);
            this.label1.MaximumSize = new System.Drawing.Size(150, 20);
            this.label1.MinimumSize = new System.Drawing.Size(150, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 20);
            this.label1.TabIndex = 48;
            this.label1.Text = "Descripción";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.DimGray;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(15, 96);
            this.label5.MaximumSize = new System.Drawing.Size(150, 20);
            this.label5.MinimumSize = new System.Drawing.Size(150, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(150, 20);
            this.label5.TabIndex = 43;
            this.label5.Text = "Moneda";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.Image = global::ferreteriaApp.Properties.Resources.icon_edit_note_321;
            this.label22.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label22.Location = new System.Drawing.Point(3, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(37, 31);
            this.label22.TabIndex = 20;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.MediumBlue;
            this.label24.Location = new System.Drawing.Point(46, 9);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(78, 16);
            this.label24.TabIndex = 0;
            this.label24.Text = "Productos";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.DimGray;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(758, 52);
            this.label11.MaximumSize = new System.Drawing.Size(160, 20);
            this.label11.MinimumSize = new System.Drawing.Size(160, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(160, 20);
            this.label11.TabIndex = 28;
            this.label11.Text = "Unidad Medida Sunat";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.DimGray;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(634, 59);
            this.label6.MaximumSize = new System.Drawing.Size(140, 20);
            this.label6.MinimumSize = new System.Drawing.Size(140, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(140, 20);
            this.label6.TabIndex = 44;
            this.label6.Text = "Código Producto";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_buscar_codigo_interno
            // 
            this.txt_buscar_codigo_interno.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txt_buscar_codigo_interno.Location = new System.Drawing.Point(780, 60);
            this.txt_buscar_codigo_interno.MaxLength = 255;
            this.txt_buscar_codigo_interno.Name = "txt_buscar_codigo_interno";
            this.txt_buscar_codigo_interno.Size = new System.Drawing.Size(159, 20);
            this.txt_buscar_codigo_interno.TabIndex = 6;
            // 
            // txt_buscar_descripcion
            // 
            this.txt_buscar_descripcion.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txt_buscar_descripcion.Location = new System.Drawing.Point(317, 59);
            this.txt_buscar_descripcion.MaxLength = 255;
            this.txt_buscar_descripcion.Name = "txt_buscar_descripcion";
            this.txt_buscar_descripcion.Size = new System.Drawing.Size(300, 20);
            this.txt_buscar_descripcion.TabIndex = 68;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.DimGray;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(130, 58);
            this.label4.MaximumSize = new System.Drawing.Size(175, 20);
            this.label4.MinimumSize = new System.Drawing.Size(175, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(175, 20);
            this.label4.TabIndex = 67;
            this.label4.Text = "Descripción";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.BackColor = System.Drawing.Color.GhostWhite;
            this.panel1.Controls.Add(this.listaToExcel);
            this.panel1.Controls.Add(this.detalleLista);
            this.panel1.Controls.Add(this.lblError);
            this.panel1.Controls.Add(this.panelPaginador);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.btnEliminar);
            this.panel1.Controls.Add(this.btnEditar);
            this.panel1.Controls.Add(this.btnBuscar);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.txt_buscar_descripcion);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txt_buscar_codigo_interno);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 170);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1090, 543);
            this.panel1.TabIndex = 70;
            // 
            // listaToExcel
            // 
            this.listaToExcel.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.listaToExcel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listaToExcel.Location = new System.Drawing.Point(18, 410);
            this.listaToExcel.Name = "listaToExcel";
            this.listaToExcel.ReadOnly = true;
            this.listaToExcel.Size = new System.Drawing.Size(69, 49);
            this.listaToExcel.TabIndex = 74;
            this.listaToExcel.Visible = false;
            // 
            // detalleLista
            // 
            this.detalleLista.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.detalleLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.detalleLista.Location = new System.Drawing.Point(15, 90);
            this.detalleLista.Name = "detalleLista";
            this.detalleLista.ReadOnly = true;
            this.detalleLista.Size = new System.Drawing.Size(965, 275);
            this.detalleLista.TabIndex = 73;
            this.detalleLista.EnabledChanged += new System.EventHandler(this.detalleLista_EnabledChanged);
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(15, 372);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(34, 13);
            this.lblError.TabIndex = 72;
            this.lblError.Text = "Error";
            // 
            // panelPaginador
            // 
            this.panelPaginador.Controls.Add(this.bnt_next);
            this.panelPaginador.Controls.Add(this.btn_preview);
            this.panelPaginador.Controls.Add(this.lbl_totalPaginas);
            this.panelPaginador.Controls.Add(this.label9);
            this.panelPaginador.Controls.Add(this.lbl_pagina);
            this.panelPaginador.Location = new System.Drawing.Point(756, 371);
            this.panelPaginador.Name = "panelPaginador";
            this.panelPaginador.Size = new System.Drawing.Size(224, 43);
            this.panelPaginador.TabIndex = 71;
            // 
            // bnt_next
            // 
            this.bnt_next.AutoEllipsis = true;
            this.bnt_next.BackColor = System.Drawing.Color.DodgerBlue;
            this.bnt_next.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bnt_next.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_next.ForeColor = System.Drawing.Color.White;
            this.bnt_next.Location = new System.Drawing.Point(191, 8);
            this.bnt_next.Name = "bnt_next";
            this.bnt_next.Size = new System.Drawing.Size(26, 25);
            this.bnt_next.TabIndex = 54;
            this.bnt_next.Text = ">";
            this.bnt_next.UseVisualStyleBackColor = false;
            this.bnt_next.Click += new System.EventHandler(this.bnt_next_Click);
            // 
            // btn_preview
            // 
            this.btn_preview.AutoEllipsis = true;
            this.btn_preview.BackColor = System.Drawing.Color.DodgerBlue;
            this.btn_preview.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_preview.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_preview.ForeColor = System.Drawing.Color.White;
            this.btn_preview.Location = new System.Drawing.Point(56, 9);
            this.btn_preview.Name = "btn_preview";
            this.btn_preview.Size = new System.Drawing.Size(26, 25);
            this.btn_preview.TabIndex = 53;
            this.btn_preview.Text = "<";
            this.btn_preview.UseVisualStyleBackColor = false;
            this.btn_preview.Click += new System.EventHandler(this.btn_preview_Click);
            // 
            // lbl_totalPaginas
            // 
            this.lbl_totalPaginas.AutoSize = true;
            this.lbl_totalPaginas.Location = new System.Drawing.Point(161, 15);
            this.lbl_totalPaginas.Name = "lbl_totalPaginas";
            this.lbl_totalPaginas.Size = new System.Drawing.Size(13, 13);
            this.lbl_totalPaginas.TabIndex = 52;
            this.lbl_totalPaginas.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(135, 15);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(12, 13);
            this.label9.TabIndex = 51;
            this.label9.Text = "/";
            // 
            // lbl_pagina
            // 
            this.lbl_pagina.AutoSize = true;
            this.lbl_pagina.Location = new System.Drawing.Point(108, 15);
            this.lbl_pagina.Name = "lbl_pagina";
            this.lbl_pagina.Size = new System.Drawing.Size(13, 13);
            this.lbl_pagina.TabIndex = 50;
            this.lbl_pagina.Text = "0";
            // 
            // label16
            // 
            this.label16.Image = global::ferreteriaApp.Properties.Resources.icon_edit_note_321;
            this.label16.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label16.Location = new System.Drawing.Point(3, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(37, 31);
            this.label16.TabIndex = 20;
            // 
            // btnEliminar
            // 
            this.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminar.Image = global::ferreteriaApp.Properties.Resources.icons8_delete_bin_32px_11;
            this.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminar.Location = new System.Drawing.Point(983, 272);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(102, 33);
            this.btnEliminar.TabIndex = 64;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditar.Image = global::ferreteriaApp.Properties.Resources.icons8_edit_property_32px;
            this.btnEditar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEditar.Location = new System.Drawing.Point(983, 311);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(102, 33);
            this.btnEditar.TabIndex = 63;
            this.btnEditar.Text = "Editar";
            this.btnEditar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatAppearance.BorderSize = 0;
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscar.Image = global::ferreteriaApp.Properties.Resources.search_32;
            this.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBuscar.Location = new System.Drawing.Point(940, 53);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(47, 33);
            this.btnBuscar.TabIndex = 69;
            this.btnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.MediumBlue;
            this.label17.Location = new System.Drawing.Point(46, 9);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(87, 16);
            this.label17.TabIndex = 0;
            this.label17.Text = "Resultados";
            // 
            // button4
            // 
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Image = global::ferreteriaApp.Properties.Resources.excel_32;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(15, 51);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(106, 33);
            this.button4.TabIndex = 66;
            this.button4.Text = "Exportar";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // productos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1090, 713);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "productos";
            this.Text = "productos";
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listaToExcel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detalleLista)).EndInit();
            this.panelPaginador.ResumeLayout(false);
            this.panelPaginador.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtPrecioVenta;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtCodigoSunat;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbUnidad;
        private System.Windows.Forms.ComboBox cmbTipoAfectacionVenta;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbMoneda;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txt_buscar_codigo_interno;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.CheckBox chkIgv;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txt_buscar_descripcion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_codigo_interno;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panelPaginador;
        private System.Windows.Forms.Button bnt_next;
        private System.Windows.Forms.Button btn_preview;
        private System.Windows.Forms.Label lbl_totalPaginas;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbl_pagina;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.DataGridView detalleLista;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView listaToExcel;
        private System.Windows.Forms.Label lblOblCodSunat;
        private System.Windows.Forms.Button btn_buscar;
    }
}