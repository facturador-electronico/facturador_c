﻿using System;

using System.Windows.Forms;

namespace ferreteriaApp
{
    public partial class principal : Form
    {
        public principal()
        {
            InitializeComponent();

            this.WindowState = FormWindowState.Normal;

        }

        private void callForm(object objeto)
        {
            this.panelForm.Controls.Clear();
            Form fh = objeto as Form;
            fh.TopLevel = false;
            this.panelForm.Controls.Add(fh);
            panelForm.Tag = fh;
            fh.Dock = DockStyle.Fill;
            fh.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            callForm(new dashboard());
        }

        private void button2_Click(object sender, EventArgs e)
        {

            callForm(new searchSaleNote());
        }

        private void button3_Click(object sender, EventArgs e)
        {
      

            callForm(new listadoComprobantes());
        }

      

        private void button4_Click(object sender, EventArgs e)
        {
            callForm(new productos());
        }

     
    }
}
