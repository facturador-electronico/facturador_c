﻿using System;


namespace ferreteriaApp.CEN
{
    class CENDocumento
    {

        public string num_ruc { get; set; }
        public string tip_docu { get; set; }
        public string num_docu { get; set; }

        public DateTime fec_carg { get; set; }
        public DateTime fec_gene { get; set; }
        public DateTime fec_envi { get; set; }
        public string des_obse { get; set; }
        public string nom_arch { get; set; }
        public string ind_situ { get; set; }
        public string tip_arch { get; set; }
        public string firm_digital { get; set; }

    }
}
