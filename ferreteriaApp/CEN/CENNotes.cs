﻿using System;

namespace ferreteriaApp.CEN
{
    public class CenNotes
    {
        public int document_id { get; set; }
        public String note_type { get; set; }
        public String note_credit_type_id { get; set; }
        public String note_debit_type_id { get; set; }
        public String note_description { get; set; }
        public String affected_document_id { get; set; }

        public DateTime fecha_transaccion { get; set; }
        public DateTime fecha_proceso { get; set; }
        public DateTime hora_proceso { get; set; }
        public decimal importe { get; set; }






    }
}
