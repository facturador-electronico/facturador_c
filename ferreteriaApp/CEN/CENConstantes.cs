﻿using System;


namespace ferreteriaApp.CEN
{
    public static class CENConstantes
    {
        
        public const String USER = "vendedor";

        public const String SOLES = "SOLES";
        public const String PEN = "PEN";

        public const int CONST_2 = 2;
        public const int CONST_10 = 10;


        //correlativo de comprobantes
        public const string F001 = "F001";
        public const string B001 = "B001";
        public const string BOLETA = "03";
        public const string FACTURA = "01";

        public const string CODIGO_DNI = "1";
        public const string CODIGO_RUC = "6";

        public const string FACTURA_ELECTRONICA = "FACTURA ELECTRÓNICA";
        public const string BOLETA_ELECTRONICA = "BOLETA DE VENTA ELECTRÓNICA";

        //estados del comprobante

        public const string ID_ESTADO = "05"; //ACEPTADO

        
    }

  
}
