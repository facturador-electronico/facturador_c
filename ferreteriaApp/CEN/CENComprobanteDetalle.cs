﻿using System;
using System.Collections.Generic;

namespace ferreteriaApp.CEN
{
    public class CENComprobanteDetalle
    {
        public decimal cantidad { get; set; }
        public String unidadMedida { get; set; }
        public String codigo { get; set; }
        public String codigo_sunat { get; set; }
        public String descripcion { get; set; }
        public decimal precioUnitario { get; set; }
        public decimal descuentoUnitario { get; set; }
        public decimal valorItem { get; set; }
        public decimal icbper { get; set; }
        public List<CENComprobanteDetalle> detalle { get; set; }

    }

}
