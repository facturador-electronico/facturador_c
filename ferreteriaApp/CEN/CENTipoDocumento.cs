﻿using System;

namespace ferreteriaApp.CEN
{
    public class CENTipoDocumento
    {
        public String nombreDocumento { get; set; }
        public int codigoDocumento { get; set; }
    }
}
