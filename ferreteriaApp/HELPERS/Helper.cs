﻿using ferreteriaApp.CEN;
using ferreteriaApp.DAO;
using ferreteriaApp.HELPERS;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;



namespace WebVentas.util
{
    public static class Helper
    {

        public static string ObtenerValorParametro(int flag)
        {
            DAOConceptos db = new DAOConceptos();
            return db.ObtenerValorParametro(flag);
        }

        public static void GenerarArchivoPlanoFacturaBoletaCabecera(CENEstructuraComprobante comprobante, string tipocomprobante, string serie, string numero)
        {

            String datosEmpresa = ObtenerValorParametro(CENConstantes.CONST_2);

            string RUC = datosEmpresa.Split('|')[0];
            string Ruta = datosEmpresa.Split('|')[1];



            string Tipocomprobante = tipocomprobante;
            string Serie = serie;
            string Numero = numero;
            string Extension = ".CAB";

            //cambios ruta
            String Rutapass= Ruta+@"DATA\";
            string ArchPlano = Rutapass + RUC + "-" + Tipocomprobante.PadLeft(2, '0') + "-" + Serie + "-" + Numero.PadLeft(8, '0') + Extension;

            //Crear el archivo plano cabecera
            StreamWriter file = new StreamWriter(ArchPlano, false);
      
            List<string> lista  = new List<string>();
            try
            {

                lista.Add(comprobante.tipoOperacion);
                lista.Add(comprobante.Cab_Fac_Fecha);
                lista.Add(comprobante.HoraEmision);
                lista.Add(comprobante.FechaVencimiento);
                lista.Add(comprobante.CodDomicilioFiscal);
                lista.Add(comprobante.TipoDocEmpresa);
                lista.Add(comprobante.NumDocEmpresa);
                lista.Add(comprobante.NombEmpresa);
                lista.Add(comprobante.tipo_moneda);
                lista.Add(Math.Round(comprobante.SumTotalTributos,2).ToString());
                lista.Add(Math.Round(comprobante.TotalValorVenta,2).ToString());
                lista.Add(Math.Round(comprobante.TotalPrecioVenta,2).ToString());
                lista.Add(Math.Round(comprobante.TotalDescuentos,2).ToString());
                lista.Add(Math.Round(comprobante.otrosCargos, 2).ToString());
                lista.Add(Math.Round(comprobante.TotalAnticipos,2).ToString());
                lista.Add(Math.Round(comprobante.ImporteTotalVenta,2).ToString());
                lista.Add(comprobante.VersionUBL);
                lista.Add(comprobante.Customizacion);


                string linea = string.Join("|", lista);
                file.WriteLine(linea);

                file.Close();
                //Generar el archivo de detalles
                GenerarArchivoPlanoFacturaBoletaDetalle(comprobante.listaDetalle, Tipocomprobante, Serie, Numero, RUC, Ruta);

            }
            catch (Exception ex)
            {
                file.Close();
                Console.WriteLine(ex);
            }
        }


        public static void GenerarArchivoPlanoFacturaBoletaDetalle(List<CENEstructuraDetalleComprobante> DtComprobante, string TipoComprobante, string Serie, string Numero, string RUC, string Ruta)
        {
            if (DtComprobante.Count > 0)
            {
                String Rutapass = Ruta + @"DATA\";
                string ArchPlano = Rutapass + RUC + "-" + TipoComprobante.PadLeft(2, '0') + "-" + Serie + "-" + Numero.PadLeft(8, '0') + ".DET";
                StreamWriter file = new StreamWriter(ArchPlano, false);
                try
                {
                    List<string> lista = new List<string>();

                    foreach (CENEstructuraDetalleComprobante det in DtComprobante)
                    {
                        lista.Add(String.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}|{12}|{13}|{14}|{15}|{16}|{17}|{18}|{19}|{20}|{21}|{22}|{23}|{24}|{25}|{26}|{27}|{28}|{29}|{30}|{31}|{32}|{33}|{34}|{35}|", 
                                  det.UnidadMedida
                                , det.cantidad.ToString()
                                , det.codigoProducto
                                , det.CodigoProdSunat
                                , det.Producto
                                //, det.ValorUnitario.ToString()
                                , det.precioBaseItem.ToString()

                                , det.SumTribxItem.ToString()
                                , det.CodTribIGV.ToString()
                                , det.MontoIGVItem.ToString()
                                , det.baseImponible.ToString()
                                , det.nombreTributo
                                , det.codAfecIGV
                                , det.tributoAfectacion
                                , det.tributoPorcentaje.ToString()
                                , det.codigTributoISC
                                , det.mtoISCxitem.ToString()
                                , det.BaseImpISC.ToString()
                                ,det.NomTribxItemISC
                                , det.CodTiposTributoISC
                                , det.TipoSistemaISC
                                , det.PorcImoISC
                                , det.CodTipoTribOtros
                                , det.MtoTribOTrosxItem.ToString()
                                , det.BaseImpOtroxItem.ToString()
                                , det.NomTribOtroxItem
                                , det.CodTipTribOtroxItem
                                , det.PorTribOtroXItem.ToString()
                                , det.codTriIcbper
                                , det.mtoTriIcbperItem
                                , det.ctdBolsasTriIcbperItem
                                , det.nomTributoIcbperItem
                                , det.codTipTributoIcbperItem
                                , det.mtoTriIcbperUnidad
                                , det.PrecioVtaUnitario.ToString()
                                , det.valor_item.ToString()
                                , det.ValorRefUnitario_Gratuito.ToString()));
                    }

                    string linea = String.Join("\n", lista.ToArray());
                    file.WriteLine(linea);
                    file.Close();

                }
                catch (Exception ex)
                {
                    file.Close();
                    Console.WriteLine(ex);
                }
            }
        }

        public static void GenerarArchivoTRI(string codTipoTributo, string Destiptributo,
                                                                      string codTipoTribUNEC,
                                                                      String BaseImponible, string TotalIGV,
                                                                      string TipoComprobante, string serie, string numero)
        {

            String datosEmpresa = ObtenerValorParametro(CENConstantes.CONST_2);

            string RUC = datosEmpresa.Split('|')[0];
            string Ruta = datosEmpresa.Split('|')[1];

            //cambios ruta
            String Rutapass = Ruta + @"DATA\";


            string ArchPlano = Rutapass + RUC + "-" + TipoComprobante.PadLeft(2, '0') + "-" + serie + "-" + numero + ".TRI";
            StreamWriter file = new StreamWriter(ArchPlano, false);

            try
            {
                string linea = codTipoTributo + "|" + Destiptributo + "|" + codTipoTribUNEC + "|" + BaseImponible.ToString() + "|" + TotalIGV.ToString();
                file.Write(linea);
                file.Close();
            }
            catch (Exception ex)
            {
                file.Close();
                Console.WriteLine(ex);
            }
        }



        public static void crearArchivoLey(string number, string TipoComprobante, string Serie, string Numero) {

            ConvertNumberToLetter InLetter = new ConvertNumberToLetter();
            string cantidadTexto =  InLetter.InLetter(number);
            string datosEmpresa = ObtenerValorParametro(CENConstantes.CONST_2);

            string RUC = datosEmpresa.Split('|')[0];
            string Ruta = datosEmpresa.Split('|')[1];

            String Rutapass = Ruta + @"DATA\";
            string ArchPlano = Rutapass + RUC + "-" + TipoComprobante.PadLeft(2, '0') + "-" + Serie + "-" + Numero.PadLeft(8, '0') + ".LEY";
            
            StreamWriter file = new StreamWriter(ArchPlano, false);

            List<string> lista = new List<string>();

            try {

                lista.Add(number.ToString(CultureInfo.CreateSpecificCulture("en-GB")));
                lista.Add(cantidadTexto);
                string linea = String.Join("|", lista.ToArray());
                file.WriteLine(linea);
                file.Close();

            }
            catch (Exception ex) { 
            
                file.Close();
                Console.WriteLine(ex);
            }
        }



        //AGREGADO

        public static void GenerarArchivoPlanoNotasCabecera(CENEstructuraComprobante comprobante, string tipocomprobante, string serie, string numero, String codigo_tipo_nota, String descripcion_nota)
        {
            //jc
            String identity_document_customer;
            if (comprobante.docCustomer.Length == 11)
            {
                identity_document_customer = "6";
            }
            else
            {
                identity_document_customer = "1";
            }
            //end

            String datosEmpresa = ObtenerValorParametro(CENConstantes.CONST_2);

            string RUC = datosEmpresa.Split('|')[0];
            string Ruta = datosEmpresa.Split('|')[1];



            string Tipocomprobante = tipocomprobante;
            string Serie = serie;
            string Numero = numero;
            string Extension = ".NOT";

            String Rutapass = Ruta + @"DATA\";
            string ArchPlano = Rutapass + RUC + "-" + Tipocomprobante.PadLeft(2, '0') + "-" + Serie + "-" + Numero.PadLeft(8, '0') + Extension;

            //Crear el archivo plano cabecera
            StreamWriter file = new StreamWriter(ArchPlano, false);

            List<string> lista = new List<string>();
            try
            {

                lista.Add(comprobante.tipoOperacion); //tipo de operacion
                lista.Add(comprobante.Cab_Fac_Fecha); // fecha de emision
                lista.Add(comprobante.HoraEmision); //hora de emision
                lista.Add(comprobante.CodDomicilioFiscal);
                lista.Add(identity_document_customer); // tipo de documento de identidad de usuario
                lista.Add(comprobante.docCustomer); //numero de documento de cliente
                lista.Add(comprobante.nomCustomer); // nombres de clientes
                lista.Add(comprobante.tipo_moneda);

                lista.Add(codigo_tipo_nota); // codigo tipo nota
                lista.Add(descripcion_nota); // descripcion tipo de nota
                //luego cambiar typeDocCustomer x lo que deberia ir
                Console.WriteLine("id. " + comprobante.typeDocCustomer);
                Console.WriteLine("string. " + comprobante.typeDocCustomer.ToString());
                Console.WriteLine("serie documento . " + comprobante.serieDocumento.ToString());
                lista.Add(comprobante.tipo_comprobante); // tipo de documento q se modifica (01,03,12)
                lista.Add(comprobante.serieDocumento); // serie y numero que se modifica 

                float sumatoriaOtrosCargos = 0; // cambiar luego


                lista.Add(Math.Round(comprobante.SumTotalTributos, 2).ToString(CultureInfo.CreateSpecificCulture("en-GB")));
                lista.Add(Math.Round(comprobante.TotalValorVenta, 2).ToString(CultureInfo.CreateSpecificCulture("en-GB")));
                lista.Add(Math.Round(comprobante.TotalPrecioVenta, 2).ToString(CultureInfo.CreateSpecificCulture("en-GB")));
                lista.Add(Math.Round(comprobante.TotalDescuentos, 2).ToString(CultureInfo.CreateSpecificCulture("en-GB")));
                lista.Add(Math.Round(sumatoriaOtrosCargos, 2).ToString(CultureInfo.CreateSpecificCulture("en-GB")));
                lista.Add(Math.Round(comprobante.TotalAnticipos, 2).ToString(CultureInfo.CreateSpecificCulture("en-GB")));
                lista.Add(Math.Round(comprobante.ImporteTotalVenta, 2).ToString(CultureInfo.CreateSpecificCulture("en-GB")));
                lista.Add(comprobante.VersionUBL);
                lista.Add(comprobante.Customizacion);


                string linea = string.Join("|", lista);
                file.WriteLine(linea);

                file.Close();
                //Generar el archivo de detalles
                GenerarArchivoPlanoFacturaBoletaDetalle(comprobante.listaDetalle, Tipocomprobante, Serie, Numero, RUC, Ruta);

            }
            catch (Exception ex)
            {
                file.Close();
                Console.WriteLine(ex);
            }
        }



        public static void crearArchivoPag( string TipoComprobante, string Serie, string Numero)
        {

            ConvertNumberToLetter InLetter = new ConvertNumberToLetter();
        
            string datosEmpresa = ObtenerValorParametro(CENConstantes.CONST_2);

            string RUC = datosEmpresa.Split('|')[0];
            string Ruta = datosEmpresa.Split('|')[1];

           
            String Rutapass = Ruta + @"DATA\";
            string ArchPlano = Rutapass + RUC + "-" + TipoComprobante.PadLeft(2, '0') + "-" + Serie + "-" + Numero.PadLeft(8, '0') + ".PAG";

            StreamWriter file = new StreamWriter(ArchPlano, false);
            try
            {
                string linea = "Contado|0.00|PEN";
                file.WriteLine(linea);
                file.Close();
            }
            catch (Exception ex)
            {
                file.Close();
                Console.WriteLine(ex);
            }
        }



    }

}



