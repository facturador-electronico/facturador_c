﻿
using ferreteriaApp.CEN;
using ferreteriaApp.DAO;
using System;

using System.IO;
using System.Xml;

using System.Threading.Tasks;

using ferreteriaApp.CLN;


namespace ferreteriaApp.HELPERS
{
    public static class funciones
    {


        public static string ObtenerValorParametro(int flag)
        {
            DAOConceptos db = new DAOConceptos();
            return db.ObtenerValorParametro(flag);
        }
        public static bool Get_XML_Text(string tipocomprobante, string serie, string numero, int id)
        {
          
            try
            {
                String datosEmpresa = ObtenerValorParametro(CENConstantes.CONST_2);

                string RUC = datosEmpresa.Split('|')[0];
                string Ruta = datosEmpresa.Split('|')[1];



                string Tipocomprobante = tipocomprobante;
                string Serie = serie;
                string Numero = numero;
                string Extension = ".xml";

                //cambios ruta
                String Rutapass = Ruta + @"FIRMA\";
                string ArchPlano = Rutapass + RUC + "-" + Tipocomprobante.PadLeft(2, '0') + "-" + Serie + "-" + Numero.PadLeft(8, '0') + Extension;


                if (File.Exists(ArchPlano))
                {

                    XmlTextReader xtr = new XmlTextReader(ArchPlano);
                    string hash = "";
                    while (xtr.Read())
                    {

                        if (xtr.NodeType == XmlNodeType.Element && xtr.Name == "ds:DigestValue")
                        {
                            hash = xtr.ReadElementContentAsString();
                        }
                    }

                    xtr.Close();
                 
                    CLNDocumentTypes clnDT = new CLNDocumentTypes();
                    clnDT.updateHash(id, hash);
                    return true;
                }
                else
                {

                    String  ArchPlanoB = Rutapass + @"\" + RUC+@"\" + RUC + "-" + Tipocomprobante.PadLeft(2, '0') + "-" + Serie + "-" + Numero.PadLeft(8, '0') + Extension;
                    if (File.Exists(ArchPlanoB))
                    {

                        XmlTextReader xtr = new XmlTextReader(ArchPlanoB);
                        string hash = "";
                        while (xtr.Read())
                        {

                            if (xtr.NodeType == XmlNodeType.Element && xtr.Name == "ds:DigestValue")
                            {
                                hash = xtr.ReadElementContentAsString();
                            }
                        }

                        xtr.Close();
                       
                        CLNDocumentTypes clnDT = new CLNDocumentTypes();
                        clnDT.updateHash(id, hash);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

               
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

       

  


    }
}
