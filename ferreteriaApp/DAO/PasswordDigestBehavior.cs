/****************************************************************************************************************************************************************************************
 PROGRAMA: PasswordDigestBehavior.cs
 VERSION : 1.0
 OBJETIVO: Clase password para envio en soap
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace ferreteriaApp.DAO
{
    public class PasswordDigestBehavior: IEndpointBehavior
    { 
    
        public string Usuario {get;set;}        // USUARIO
        public string Password  {get;set;}      // PASSWORD
   

    public PasswordDigestBehavior(string username, string password__1)
    {
        // DESCRIPCION: CONSTRUCTOR DE CLASE
        Usuario = username;
        Password = password__1;
    }

    public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
    {
         // DESCRIPCION: CONTRUYENTO PARAMETROS
        return;
    }

    public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
    {
         // DESCRIPCION: COMPORTAMIENTO DE CLIENTE
        clientRuntime.ClientMessageInspectors.Add(new PasswordDigestMessageInspector(Usuario, Password));
    }

    public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
    {
          // DESCRIPCION: COMPORTAMIENTO DE SALIDA
        return;
    }

    public void Validate(ServiceEndpoint endpoint)
    {
          // DESCRIPCION: VALIDACION
        return;
    }
        
    }
}