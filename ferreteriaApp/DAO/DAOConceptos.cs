﻿using ferreteriaApp.CEN;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace ferreteriaApp.DAO
{
    public class DAOConceptos
    {
        public List<CENTipoDocumento> documentos(int flag)
        {
            List<CENTipoDocumento> lista = new List<CENTipoDocumento>();
            CENTipoDocumento cen;
            try
            {
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_listar_conceptos", false);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@flag", flag);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    cen = new CENTipoDocumento();
                    cen.codigoDocumento = dr.GetInt16(0);
                    cen.nombreDocumento = dr.GetString(1).ToUpper();
                    lista.Add(cen);
                }

                dr.Close();
                con.cerrarSession();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }

            return lista;
        }


        public string ObtenerValorParametro(int flag)
        {
            
            try
            {
                String response = "";
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_listar_conceptos", false);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@flag", flag);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    response = dr.GetString(0);
                }
                dr.Close();
                con.cerrarSession();
                return response;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
      
        }
    }
}
