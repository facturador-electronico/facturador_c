﻿using ferreteriaApp.CEN;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace ferreteriaApp.DAO
{
    public class DAOSoap
    {
        public CENEmpresa searchSoap()
        {
  
            CENEmpresa cen = new CENEmpresa();
            try
            {
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_search_soap", false);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    
                    cen.usuario_soap = dr.GetString(0);
                    cen.password_soap = dr.GetString(1);
                    
                }

                dr.Close();
                con.cerrarSession();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }

            return cen;
        }


       
    }
}
