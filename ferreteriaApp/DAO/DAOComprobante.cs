﻿using ferreteriaApp.CEN;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using System.Text;

namespace ferreteriaApp.DAO
{
    public class DAOComprobante
    {
        private static Conexion con;

        private static string sql = @"INSERT INTO `comprobante` (`id`, `fecha_emision`, `fecha_vencimiento`, `codigo`, `num_documento`,
                                    `tipo_documento`, `tipo_moneda`, `dir_cliente`, `nom_cliente`, `igv`, `isc`, `otros_descuentos`, 
                                    `descuento_global`, `subtotal`, `descuento_total`, `anticipo`, `importe`, `empresa_id`,`tipo_comprobante`,
                                    `tipo_operacion`,`id_estado`,`hash`,`fecha_transaccion`,`usuario`) 
                                    VALUES  (0, ?fecha_emision,?fecha_vencimiento,
                                    ?codigo,?num_documento,?tipo_documento,
                                    ?tipo_moneda,?dir_cliente,?nom_cliente,?igv,?isc,?otros_descuentos,?descuento_global,
                                    ?subtotal,?descuento_total,?anticipo,?importe,?empresa_id,
                                    ?tipo_comprobante,?tipo_operacion,?id_estado,?hash,?fecha_transaccion,?usuario)";

        private static string sqlDetalle = @"INSERT INTO `detalle_comprobante` (`id`, `cantidad`, `unidad_medida`, `codigo_producto`, `producto`, `precio_unitario`, `descuento_unitario`, `valor_item`, `icbper`, `comprobante_id`,`fecha_transaccion`,`usuario`, `codigo_sunat`) VALUES  ";

        private static string codigoFactura = "generar_codigo_facturas";

        


        public DAOComprobante()
        {
            con = new Conexion();
        }

        public int guardarBoleta(CENComprobanteCabecera cabecera, List<CENComprobanteDetalle> detalle)
        {

                int id = 0 ;
            try
            {

                MySqlCommand cmd, cmd2;
                con.transaction().Commit();
                try
                {
                    cmd = con.prepararConsulta(sql, true); // el parámetro true activa las transacciones


                    //agregadojc
                    Decimal igv, subtotal, importe;
                    if (cabecera.tipoComprobante == "07" || cabecera.tipoComprobante == "08")
                    {
                        igv = -Convert.ToDecimal(cabecera.igv);
                        subtotal = -Convert.ToDecimal(cabecera.subtotal);
                        importe = -Convert.ToDecimal(cabecera.importeTotal.ToString());
                    }
                    else
                    {
                        igv = Convert.ToDecimal(cabecera.igv);
                        subtotal = Convert.ToDecimal(cabecera.subtotal);
                        importe = Convert.ToDecimal(cabecera.importeTotal.ToString());

                    }
                    //end
                    cmd.Parameters.AddWithValue("?fecha_emision", cabecera.fechaEmision);
                    cmd.Parameters.AddWithValue("?fecha_vencimiento", cabecera.fechaVencimiento);
                    cmd.Parameters.AddWithValue("?codigo", cabecera.codigo);
                    cmd.Parameters.AddWithValue("?num_documento", cabecera.documentoCliente);
                    cmd.Parameters.AddWithValue("?tipo_documento", cabecera.tipoDocumento);
                    cmd.Parameters.AddWithValue("?tipo_moneda", cabecera.tipoMoneda);
                    cmd.Parameters.AddWithValue("?dir_cliente", cabecera.direccionCliente);
                    cmd.Parameters.AddWithValue("?nom_cliente", cabecera.nombreCliente);
                    cmd.Parameters.AddWithValue("?igv", igv);

                    cmd.Parameters.AddWithValue("?isc", cabecera.isc.ToString());
                    cmd.Parameters.AddWithValue("?otros_descuentos", cabecera.otros.ToString());
                    cmd.Parameters.AddWithValue("?descuento_global", Convert.ToDecimal(cabecera.descuentoGlobal.ToString()));
                    cmd.Parameters.AddWithValue("?descuento_total", Convert.ToDecimal(cabecera.descuentoTotal.ToString()));
                    cmd.Parameters.AddWithValue("?subtotal", subtotal);

                    cmd.Parameters.AddWithValue("?anticipo", cabecera.anticipos.ToString());
                    cmd.Parameters.AddWithValue("?importe", importe);

                    cmd.Parameters.AddWithValue("?empresa_id", cabecera.empresa);
                    cmd.Parameters.AddWithValue("?tipo_comprobante", cabecera.tipoComprobante);
                    cmd.Parameters.AddWithValue("?tipo_operacion", cabecera.tipoOperacion);

                    cmd.Parameters.AddWithValue("?id_estado", "01");
                    cmd.Parameters.AddWithValue("?hash", null);
                    cmd.Parameters.AddWithValue("?fecha_transaccion", cabecera.fechaEmision.ToString("yyyy-MM-dd"));
                    cmd.Parameters.AddWithValue("?usuario",CENConstantes.USER);
                    cmd.ExecuteNonQuery();
                    id = Convert.ToInt32(cmd.LastInsertedId.ToString());
                    

                    //insertando datos de detalle


                    StringBuilder sb = new StringBuilder(sqlDetalle);
                    List<object> rows = new List<object>();
                    //agregadojc
                   
                    if (cabecera.tipoComprobante == "07" || cabecera.tipoComprobante == "08")
                    {
                        foreach (CENComprobanteDetalle item in detalle)
                        {
                            
                            decimal precio_unitario_item = -item.precioUnitario;
                            decimal valor_item = -item.valorItem;

                            rows.Add(string.Format("({0},{1},'{2}','{3}','{4}',{5},{6},{7},{8},{9},'{10}','{11}','{12}')", 0, -item.cantidad, item.unidadMedida, item.codigo, item.descripcion, precio_unitario_item.ToString(), item.descuentoUnitario.ToString(), valor_item.ToString(), item.icbper, id,cabecera.fechaEmision.ToString("yyyy-MM-dd"), CENConstantes.USER, item.codigo_sunat));
                        }
                    }
                    else
                    {
                        foreach (CENComprobanteDetalle item in detalle)
                        {

                            rows.Add(string.Format("({0},{1},'{2}','{3}','{4}',{5},{6},{7},{8},{9},'{10}','{11}','{12}')", 0, item.cantidad, item.unidadMedida, item.codigo, item.descripcion, item.precioUnitario.ToString(), item.descuentoUnitario.ToString(), item.valorItem.ToString(), item.icbper, id, cabecera.fechaEmision.ToString("yyyy-MM-dd"), CENConstantes.USER, item.codigo_sunat));
                        }

                    }
                    //end
                 

                    sb.Append(string.Join(",", rows));
                    sb.Append(";");
                    Console.WriteLine(sb.ToString());
                    cmd2 = con.prepararConsulta(sb.ToString(), false); // el parámetro true activa las transacciones
                    cmd2.CommandType = System.Data.CommandType.Text;
                    cmd2.ExecuteNonQuery();
                    con.transaction().Commit();
            


                }
                catch (Exception ex)
                {
                    con.transaction().Rollback();
                    Console.WriteLine(ex);
                    throw;
                }

            
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex);
                throw;
            }
            con.cerrarSession();
            return id;
        }

        public List<CENComprobanteCabecera> All()
        {

            List<CENComprobanteCabecera> lista = new List<CENComprobanteCabecera>();

            return lista;
        }

        public string CodigoBoleta(string Tipo)
        {
            MySqlCommand cmd;
            String rsp = "";
            try
            {
                cmd = con.prepararConsulta(codigoFactura, false); // el parámetro true activa las transacciones
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@_tipo_comprobante", Tipo);

                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    rsp = dr.GetString(0);
                }

              
                dr.Close();
                con.cerrarSession();
            }
            catch (Exception ex)
            {
                con.cerrarSession();
                Console.WriteLine(ex);
                throw;
            }

            return rsp;
        }

        public string CodigoNotas(string Tipo,string letra)
        {
            MySqlCommand cmd;
            String rsp = "";
            try
            {
                cmd = con.prepararConsulta(codigoFactura, false); // el parámetro true activa las transacciones
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@_tipo_comprobante", Tipo);
                cmd.Parameters.AddWithValue("@_inicial", letra);

                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    rsp = dr.GetString(0);
                }

                dr.Close();
                con.cerrarSession();
            }
            catch (Exception ex)
            {
                con.cerrarSession();
                Console.WriteLine(ex);
                throw;
            }
            return rsp;
        }


        public CENEstructuraComprobante Find(long id)
        {

            CENEstructuraComprobante cen = null;
            try
            {

                MySqlCommand cmd = con.prepararConsulta("pa_listar_data_comprobante", false);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@_id", Convert.ToInt32(id));
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {

                    cen = new CENEstructuraComprobante();

                    cen.tipoOperacion = dr.GetString(0);
                    cen.Cab_Fac_Fecha = dr.GetString(1);
                    cen.HoraEmision = dr.GetString(2);
                    cen.FechaVencimiento = dr.GetString(3);
                    cen.CodDomicilioFiscal = dr.GetString(4);
                    cen.TipoDocEmpresa = dr.GetString(5);
                    //cambios

                    cen.NumDocEmpresa = dr.GetString(7);
                    cen.NombEmpresa = dr.GetString(6);
                    //end
             
                    cen.tipo_moneda = dr.GetString(8);
                    cen.SumTotalTributos = dr.GetDecimal(9);
                    cen.TotalValorVenta = dr.GetDecimal(10);
                    cen.TotalPrecioVenta = dr.GetDecimal(11);
                    cen.TotalDescuentos = dr.GetDecimal(12);
                    cen.otrosCargos = dr.GetDecimal(13); 
                    cen.TotalAnticipos = dr.GetDecimal(14);
                    cen.ImporteTotalVenta = dr.GetDecimal(15);
                    cen.VersionUBL = dr.GetString(16);
                    cen.Customizacion = dr.GetString(17);

                    ////agregado
                    cen.nomCustomer = dr.GetString(6);
                    cen.docCustomer = dr.GetString(7);
                    cen.typeDocCustomer = dr.GetString(5);
                    cen.serieDocumento = dr.GetString(18);
                    cen.direccionCustomer = dr.GetString(19);
                    cen.tipo_comprobante = dr.GetString(20);

                    List<CENEstructuraDetalleComprobante> detalle = new List<CENEstructuraDetalleComprobante>();
                    
                    if (dr.NextResult())
                    {
                        CENEstructuraDetalleComprobante cenDetalle;

                        while (dr.Read())
                        {
                            cenDetalle = new CENEstructuraDetalleComprobante();
                            /*0 - string*/   cenDetalle.UnidadMedida = dr.GetString(0);
                            /*1 - string*/   cenDetalle.cantidad = dr.GetDecimal(1);
                            /*2 - string*/   cenDetalle.codigoProducto = dr.GetString(2);
                            /*3 - string*/   cenDetalle.CodigoProdSunat = dr.GetString(3);
                            /*4 - string*/   cenDetalle.Producto = dr.GetString(4);
                            /*5 - decimal*/  cenDetalle.ValorUnitario = dr.GetDecimal(5);
                            /*6 - decimal*/  cenDetalle.SumTribxItem = dr.GetDecimal(6);
                            /*7 - string*/   cenDetalle.CodTribIGV = dr.GetString(7);
                            /*8 - decimal*/  cenDetalle.MontoIGVItem = dr.GetDecimal(8);
                            /*9 - decimal*/  cenDetalle.baseImponible = dr.GetDecimal(9);
                            /*10 - string*/  cenDetalle.nombreTributo = dr.GetString(10);
                            /*11 - string*/  cenDetalle.codAfecIGV = dr.GetString(11);
                            /*12 - string*/  cenDetalle.tributoAfectacion = dr.GetString(12);
                            /*13 - decimal*/ cenDetalle.tributoPorcentaje = dr.GetDecimal(13);
                            /*14 - string*/  cenDetalle.codigTributoISC = dr.GetString(14);
                            /*15 - decimal*/ cenDetalle.mtoISCxitem = dr.GetDecimal(15);
                            /*16 - decimal*/ cenDetalle.BaseImpISC = dr.GetDecimal(16);
                            /*17 - string*/  cenDetalle.NomTribxItemISC = dr.GetString(17);
                            /*18 - string*/  cenDetalle.CodTiposTributoISC = dr.GetString(18);
                            /*19 - string*/  cenDetalle.TipoSistemaISC = dr.GetString(19);
                            /*20 - string*/  cenDetalle.PorcImoISC = dr.GetString(20);
                            /*21 - string*/  cenDetalle.CodTipoTribOtros = dr.GetString(21);
                            /*22 - decimal*/ cenDetalle.MtoTribOTrosxItem = dr.GetDecimal(22);
                            /*23 - decimal*/ cenDetalle.BaseImpOtroxItem = dr.GetDecimal(23);
                            /*24 - string*/  cenDetalle.NomTribOtroxItem = dr.GetString(24);
                            /*25 - string*/  cenDetalle.CodTipTribOtroxItem = dr.GetString(25);
                            /*26 - decimal*/ cenDetalle.PorTribOtroXItem = dr.GetDecimal(26);
                            /*27 - string*/  cenDetalle.codTriIcbper = dr.GetString(27);
                            /*28 - string*/  cenDetalle.mtoTriIcbperItem = dr.GetString(28);
                            /*29 - string*/  cenDetalle.ctdBolsasTriIcbperItem = dr.GetString(29);
                            /*30 - string*/  cenDetalle.nomTributoIcbperItem = dr.GetString(30);
                            /*31 - string*/  cenDetalle.codTipTributoIcbperItem = dr.GetString(31);
                            /*32 - string*/  cenDetalle.mtoTriIcbperUnidad = dr.GetString(32);
                            /*33 - decimal*/ cenDetalle.PrecioVtaUnitario = dr.GetDecimal(33);
                            /*34 - decimal*/ cenDetalle.valor_item = dr.GetDecimal(34);
                            /*35 - decimal*/ cenDetalle.ValorRefUnitario_Gratuito = dr.GetDecimal(35);

                            //agregadoJC

                            /*36 - decimal*/ cenDetalle.precioBaseItem = dr.GetDecimal(36);
                                             
                                            cenDetalle.unidadMedidaNota = dr.GetString(37);
                                            cenDetalle.codigoProductoNota = dr.GetString(38);

                            detalle.Add(cenDetalle);
                        }

                    }

                    cen.listaDetalle = detalle;
                }

                dr.Close();
                con.cerrarSession();


            }
            catch (Exception ex)
            {
                con.cerrarSession();
                Console.WriteLine(ex);
                throw;
            }

            return cen;
        }
    }
}
