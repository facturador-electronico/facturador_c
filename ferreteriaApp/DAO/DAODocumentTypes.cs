﻿using ferreteriaApp.CEN;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace ferreteriaApp.DAO
{
    public class DAODocumentTypes
    {

        public List<CENDocumentTypes> getTipoDocumentosString(int flag)
        {
            List<CENDocumentTypes> lista = new List<CENDocumentTypes>();
            CENDocumentTypes cen;
            try
            {
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_listar_conceptos", false);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@flag", flag);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    cen = new CENDocumentTypes();
                    cen.id = dr.GetString(0);
                    cen.description = dr.GetString(1).ToUpper();
             
                    lista.Add(cen);
                }
                dr.Close();
                con.cerrarSession();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }

            return lista;
        }

        public string getNumberSeriesNotes(int flag)
        {
            String response = "";
            try
            {
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_listar_conceptos", false);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@flag", flag);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {

                    response = dr.GetString(0);

                }
                dr.Close();
                con.cerrarSession();
                return response;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }

        }

        public string getSerieNota(string Tipo, string letra)
        {

            String response = "";
            try
            {
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_generar_codigo_notas", false);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@_tipo_comprobante", Tipo);
                cmd.Parameters.AddWithValue("@_inicial", letra);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    response = dr.GetString(0);
                }


                dr.Close();
                con.cerrarSession();
           
                return response;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }

        }

      
      
     

        public List<CENBuscarVentasNotas> search_comprobantes(int codigo_venta, String series_number, String dni_ruc, String name_cliente)
        {
            List<CENBuscarVentasNotas> listaSC = new List<CENBuscarVentasNotas>();
            CENBuscarVentasNotas censc;
            try
            {
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_search_comprobantes", false);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@codigo_venta", codigo_venta);
                cmd.Parameters.AddWithValue("@series_number", series_number);
                cmd.Parameters.AddWithValue("@dni_ruc", dni_ruc);
                cmd.Parameters.AddWithValue("@name_cliente", name_cliente);
                
                MySqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    censc = new CENBuscarVentasNotas();
                    censc.VENTA = dr.GetInt16(0);
                    censc.DOCUMENTO = dr.GetString(1);
                    censc.FECHA = dr.GetDateTime(2);
                    censc.CLIENTE = dr.GetString(3);
                    listaSC.Add(censc);

                }
                dr.Close();
                con.cerrarSession();
                return listaSC;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
            
        }

        public List<CENBuscarVentasNotas> search_comprobantes_fechas(int codigo_venta, String series_number, String fecha_inicio, String fecha_fin, String dni_ruc, String name_cliente)
        {
            List<CENBuscarVentasNotas> listaSC = new List<CENBuscarVentasNotas>();
            CENBuscarVentasNotas censc;
            try
            {
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_search_comprobantes_fechas", false);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@fecha_inicio", fecha_inicio);
                cmd.Parameters.AddWithValue("@fecha_fin", fecha_fin);
                cmd.Parameters.AddWithValue("@dni_ruc", dni_ruc);
                cmd.Parameters.AddWithValue("@name_cliente", name_cliente);
                cmd.Parameters.AddWithValue("@codigo_venta", codigo_venta);
                cmd.Parameters.AddWithValue("@series_number", series_number);
                MySqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    censc = new CENBuscarVentasNotas();

                    censc.VENTA = dr.GetInt16(0);
                    censc.DOCUMENTO = dr.GetString(1);
                    censc.FECHA = dr.GetDateTime(2);
                    censc.CLIENTE = dr.GetString(3);
                    listaSC.Add(censc);

                }
                dr.Close();
                con.cerrarSession();
                return listaSC;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        
        }


        public void insertNotes(CenNotes notes)
        {

           

            try
            {
                String marca_baja = "0";
                String usuario = "";
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_insert_notes", false);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@document_id", notes.document_id);
                cmd.Parameters.AddWithValue("@note_type", notes.note_type);
                cmd.Parameters.AddWithValue("@note_credit_type_id", notes.note_credit_type_id);
                cmd.Parameters.AddWithValue("@note_debit_type_id", notes.note_debit_type_id);
                cmd.Parameters.AddWithValue("@note_description", notes.note_description);
                cmd.Parameters.AddWithValue("@affected_document_id", notes.affected_document_id);
                cmd.Parameters.AddWithValue("@marca_baja", marca_baja);
                cmd.Parameters.AddWithValue("@fecha_transaccion", notes.fecha_transaccion);
                cmd.Parameters.AddWithValue("@fecha_proceso", notes.fecha_proceso);
                cmd.Parameters.AddWithValue("@hora_proceso", notes.hora_proceso);
                cmd.Parameters.AddWithValue("@usuario", usuario);
                cmd.Parameters.AddWithValue("@importe", notes.importe);

                MySqlDataReader dr = cmd.ExecuteReader();

                dr.Close();
                con.cerrarSession();

               

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        
        }


        public string getSeries(String tipo_comprobante)
        {


            String response = "";
            try
            {
                //     
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("generar_codigo_facturas", false);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@_tipo_comprobante", tipo_comprobante);


                MySqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    response = dr.GetString(0);
                }
                dr.Close();
                con.cerrarSession();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
    
            return response;



        }

        //para get series de un comprobante factura y boletas de jeffrey
        public string getSeriesDocuments(String tipo_comprobante)
        {


            String response = "";
            try
            {
                //     
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_get_series", false);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@id_comprobante", tipo_comprobante);


                MySqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    response = dr.GetString(0);
                }

                dr.Close();
                con.cerrarSession();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
      
            return response;



        }



        public void updateHash(int id, String hash)
        {

            try
            {
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_update_hash", false);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@_id", id);
                cmd.Parameters.AddWithValue("@_hash", hash);
                MySqlDataReader dr = cmd.ExecuteReader();
                dr.Close();
                con.cerrarSession();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
            


        }


        public string getHash(int id)
        {


            String response = "";
            try
            {
                //     
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_get_hash", false);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@_id", id);


                MySqlDataReader dr = cmd.ExecuteReader();
              
                if (dr.Read() && !dr.IsDBNull(0))
                {
                   response = dr.GetString(0);
                                

                }


                dr.Close();
                con.cerrarSession();

                return response;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
                   



        }

        //bnuscar para listado de comprobantes

        public List<CENListadoComprobantes> search_listado_comprobantes(CenSearchComprobantes search)
        {
            List<CENListadoComprobantes> listaSC = new List<CENListadoComprobantes>();
            CENListadoComprobantes censc;
            try
            {
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_search_listado_comprobantes", false);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@codigo_venta", search.codigo_venta);
                cmd.Parameters.AddWithValue("@series_number", search.series_number);
                cmd.Parameters.AddWithValue("@dni_ruc", search.dni_ruc);
                cmd.Parameters.AddWithValue("@name_cliente", search.name_cliente);
                cmd.Parameters.AddWithValue("@_tipo_comprobante", search.tipo_comprobante);
                cmd.Parameters.AddWithValue("@_id_estado", search._id_estado);

                MySqlDataReader dr = cmd.ExecuteReader();


                while (dr.Read())
                {
                    censc = new CENListadoComprobantes();
                    censc.VENTA = dr.GetInt16(0);
                    censc.DOCUMENTO = dr.GetString(1);
                    censc.FECHA = dr.GetDateTime(2);
                    censc.CLIENTE = dr.GetString(3);
                    censc.ESTADO = dr.GetString(4);
                    censc.MONEDA = dr.GetString(5);
                    censc.GRAVADO = dr.GetDecimal(6);
                    censc.IGV = dr.GetDecimal(7);
                    censc.TOTAL = dr.GetDecimal(8);
                    censc.TIPO = dr.GetString(9);
                    censc.ID_TC = dr.GetString(10);


                    listaSC.Add(censc);

                }
                dr.Close();
                con.cerrarSession();
                return listaSC;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }

        }

        public List<CENListadoComprobantes> search_listado_comprobantes_fechas(CenSearchComprobantes search)
        {
            List<CENListadoComprobantes> listaSC = new List<CENListadoComprobantes>();
            CENListadoComprobantes censc;
            try
            {
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_search_listado_comprobantes_fechas", false);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@fecha_inicio", search.fecha_inicio);
                cmd.Parameters.AddWithValue("@fecha_fin", search.fecha_fin);
                cmd.Parameters.AddWithValue("@dni_ruc", search.dni_ruc);
                cmd.Parameters.AddWithValue("@name_cliente", search.name_cliente);
                cmd.Parameters.AddWithValue("@_tipo_comprobante", search.tipo_comprobante);
                cmd.Parameters.AddWithValue("@series_number", search.series_number);
                cmd.Parameters.AddWithValue("@codigo_venta", search.codigo_venta);
                cmd.Parameters.AddWithValue("@_id_estado", search._id_estado);




                MySqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    censc = new CENListadoComprobantes();

                    censc.VENTA = dr.GetInt16(0);
                    censc.DOCUMENTO = dr.GetString(1);
                    censc.FECHA = dr.GetDateTime(2);
                    censc.CLIENTE = dr.GetString(3);
                    censc.ESTADO = dr.GetString(4);
                    censc.MONEDA = dr.GetString(5);
                    censc.GRAVADO = dr.GetDecimal(6);
                    censc.IGV = dr.GetDecimal(7);
                    censc.TOTAL = dr.GetDecimal(8);
                    censc.TIPO = dr.GetString(9);
                    censc.ID_TC = dr.GetString(10);
                    listaSC.Add(censc);

                }
                dr.Close();
                con.cerrarSession();
                return listaSC;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }

        }

        public List<CENDocumentTypes> getDocumentTypes()
        {
            List<CENDocumentTypes> lista = new List<CENDocumentTypes>();
            CENDocumentTypes cendt;

            try
            {

        
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_listar_conceptos", false);
                cmd.Parameters.AddWithValue("@flag", 12);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                MySqlDataReader dr = cmd.ExecuteReader();
                cendt = new CENDocumentTypes();

                cendt.id = "0";
                cendt.description = "SELECCIONAR";
                lista.Add(cendt);
                while (dr.Read())
                {
                    cendt = new CENDocumentTypes();

                    cendt.id = dr.GetString(0);
                    cendt.description = dr.GetString(1);
                    lista.Add(cendt);

                }
                dr.Close();
                con.cerrarSession();
                return lista;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }

        }


        public CENDocAfectadoNota getDocAfectadoNota(int id)
        {
            
            CENDocAfectadoNota cendt;

            try
            {
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_buscar_documento_afectado_nota", false);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@_id", id);
                MySqlDataReader dr = cmd.ExecuteReader();
                cendt = new CENDocAfectadoNota();

               

                while (dr.Read())
                {
                    cendt = new CENDocAfectadoNota();
                    

                    cendt.codigo = dr.GetString(0);
                    cendt.reason_credit = dr.GetString(1);
                    cendt.reason_debit= dr.GetString(2);
           

                }
                dr.Close();
                con.cerrarSession();
                return cendt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }


        public void setEstadoDocumento(int _id, String _id_estado)
        {
            try
            {
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_set_estado_comprobante", false);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@_id", _id);
                cmd.Parameters.AddWithValue("@_id_estado", _id_estado);
                MySqlDataReader dr = cmd.ExecuteReader();
                dr.Close();
                con.cerrarSession();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public List<CENDocumentTypes> getCmbUnidadString(int flag)
        {
            List<CENDocumentTypes> lista = new List<CENDocumentTypes>();
            CENDocumentTypes cen;
            try
            {
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_listar_conceptos", false);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@flag", flag);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    cen = new CENDocumentTypes();
                    cen.id = dr.GetString(0);
                    cen.description = dr.GetString(1).ToUpper() + " - " + dr.GetString(0).ToUpper();

                    lista.Add(cen);
                }
                dr.Close();
                con.cerrarSession();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }

            return lista;
        }


    }
}
