﻿using System;

using System.Windows.Forms;

namespace ferreteriaApp
{
    public partial class progress : Form
    {
        int last;
        public progress(String titulo, int segundos)
        {
            InitializeComponent();
            this.lblTitulo.Text = titulo;
            this.progressBar1.Maximum = segundos;
            this.progressBar1.Minimum = 0;
            last = segundos;

          




        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            System.Threading.Thread.Sleep(1000);

            if (this.progressBar1.Value<last)
            {
                this.progressBar1.Value++;
            }

            if (this.progressBar1.Value ==last)
            {
                this.timer1.Enabled = false;
                this.Close();
            }
        }
    }
}
