﻿using ferreteriaApp.CEN;
using ferreteriaApp.CLN;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Windows.Forms;
using System.Xml;

namespace ferreteriaApp
{
    public partial class listadoComprobantes : Form
    {
        List<CENListadoComprobantes> listaRN = new List<CENListadoComprobantes>();
        int pagina = 0;
        int maximo_paginas = 0;
        decimal items_por_paginas = 10;
        decimal total_datos = 0;

        String dni_ruc = "";
        String name_cliente = "";
        int codigo_venta;

        String series_number;
        public listadoComprobantes()
        {
            InitializeComponent();
            this.placeHolder();
            this.btn_preview.Enabled = false;
            this.bnt_next.Enabled = false;
            this.LlenarComboTipoCmp();
            this.lblError.Text = "";

        }
        private void LlenarComboTipoCmp()
        {
            List<CENDocumentTypes> lista;
            this.cmbTipoComprobante.Items.Clear();
            try
            {
                CLNDocumentTypes cln = new CLNDocumentTypes();

                lista = cln.getDocumentTypes();

                this.cmbTipoComprobante.DataSource = lista;
                this.cmbTipoComprobante.DisplayMember = "description";
                this.cmbTipoComprobante.ValueMember = "id";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CENDocumentTypes cen = new CENDocumentTypes();
                cen.id = "0";
                cen.description = "No se encontraron documentos";
                lista = null;
                this.cmbTipoComprobante.DataSource = lista;
                this.cmbTipoComprobante.DisplayMember = "description";
                this.cmbTipoComprobante.ValueMember = "id";
            }
        }
        private string agregarCeros(int Values)
        {
            int conteos = Values.ToString().Length;
            int decimalLength = Values.ToString("D").Length + (8 - conteos);

            return Values.ToString("D" + decimalLength.ToString());
        }
        public void placeHolder()
        {
            this.txt_codigo_venta.Text = "CODIGO";
            this.txt_codigo_venta.ForeColor = Color.Silver;

            this.txt_serie.Text = "SERIE";
            this.txt_serie.ForeColor = Color.Silver;

            this.txt_number.Text = "NUMERO";
            this.txt_number.ForeColor = Color.Silver;

            this.txt_cliente.Text = "NUMERO DE DOCUMENTO / NOMBRE";
            this.txt_serie.ForeColor = Color.Silver;

        }
        private void btn_buscar_sale_note_Click(object sender, EventArgs e)
        {
            Int64 documento;

            if (this.txt_codigo_venta.Text != "CODIGO" && !Int64.TryParse(this.txt_codigo_venta.Text, NumberStyles.AllowThousands, CultureInfo.CreateSpecificCulture("es-ES"), out documento))
            {
                
                 
                    aviso aviso_form = new aviso("warning", "Error", "Error codigo solo debe tener números", false);
                    aviso_form.ShowDialog();

                    this.txt_codigo_venta.Focus();
                    return;
                
            }


            if (this.txt_serie.Text != "SERIE" && this.txt_serie.Text.Trim().Length < 4)
            {
               aviso aviso_form = new aviso("warning", "Error", "Serie debe tener 4 dígitos", false);
               aviso_form.ShowDialog();


                this.txt_serie.Focus();
                return;

            }

            if (this.txt_serie.Text != "SERIE" && this.txt_number.Text == "NUMERO")
            {
   
                aviso aviso_form = new aviso("warning", "Error", "Escriba el número de la serie del documento", false);
                aviso_form.ShowDialog();
                this.txt_number.Focus();
                return;
            }

            if (this.txt_number.Text != "NUMERO" && this.txt_serie.Text == "SERIE")
            {
               
                aviso aviso_form = new aviso("warning", "Error", "Escriba la serie del documento", false);
                aviso_form.ShowDialog();
                this.txt_number.Focus();
                return;
            }




            if (this.txt_codigo_venta.Text == "CODIGO")
            {

                codigo_venta = 0;
            }
            else
            {
                if (this.txt_codigo_venta.Text.Length > 11)
                {
                  
                    aviso aviso_form = new aviso("warning", "Error", "11 como máximo de carácteres", false);
                    aviso_form.ShowDialog();
                    this.txt_number.Focus();
                    return;
                }
                else
                {
                    codigo_venta = (int)Convert.ToInt64(this.txt_codigo_venta.Text);
                    this.cmbTipoComprobante.SelectedIndex = 0;

                }

            }


            if (this.txt_number.Text.Contains(".") || this.txt_number.Text.Contains(","))
            {
                
                aviso aviso_form = new aviso("warning", "Error", "El campo número de documento, solo debe tener números", false);
                aviso_form.ShowDialog();
                this.txt_number.Focus();
                return;
            }
            else
            {
                if (this.txt_serie.Text == "SERIE" && this.txt_number.Text == "NUMERO")
                {
                    series_number = "";
                }
                else
                {
                    series_number = this.txt_serie.Text + '-' + this.agregarCeros(Convert.ToInt32(this.txt_number.Text));
                    this.cmbTipoComprobante.SelectedIndex = 0;
                }
            }

            //verificar campo dni ruc 
            if (this.txt_cliente.Text == "NUMERO DE DOCUMENTO / NOMBRE")
            {
                dni_ruc = "";
                name_cliente = "";
            }
            else
            {
                if (Char.IsNumber(this.txt_cliente.Text, 1))
                {
                    dni_ruc = this.txt_cliente.Text;
                    name_cliente = "";
                }
                else
                {
                    name_cliente = this.txt_cliente.Text;
                    dni_ruc = "";
                }
            }

            this.generar_busqueda();

        }

        public void generar_busqueda()
        {
            CenSearchComprobantes search = new CenSearchComprobantes
            {
                fecha_inicio = this.txt_fecha_inicio.Value.ToString("yyyy-MM-dd"),
                fecha_fin = this.txt_fecha_fin.Value.ToString("yyyy-MM-dd"),
                dni_ruc = dni_ruc,
                name_cliente = name_cliente,
                tipo_comprobante = this.cmbTipoComprobante.SelectedValue.ToString(),
                series_number = series_number,
                codigo_venta = codigo_venta,
                _id_estado = ""
            };
            if (this.chkFechas.Checked)
            {

                try
                {
                    CLNDocumentTypes cln2 = new CLNDocumentTypes();
                    
               
                    listaRN = cln2.search_listado_comprobantes_fechas(search);
                    this.cargar_datos(listaRN);
                    total_datos = listaRN.Count;

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                   
                    aviso aviso_form = new aviso("warning", "ERROR EN LA SOLICITUD", ex.Message, false);
                    aviso_form.ShowDialog();
              
                }
            }
            else
            {

                try
                {
                    pagina = 0;
                    CLNDocumentTypes cln2 = new CLNDocumentTypes();
                    listaRN = cln2.search_listado_comprobantes(search);
                    this.cargar_datos(listaRN);
                    total_datos = listaRN.Count;


                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    aviso aviso_form = new aviso("warning", "ERROR EN LA SOLICITUD", ex.Message, false);
                    aviso_form.ShowDialog();
                   
                }

            }
        }
        private void gestionaResaltados(DataGridView visor)
        {

            for (int i = 0; i < visor.Rows.Count; i++)
            {
                string compare = (string)visor.Rows[i].Cells[5].Value;
                if (compare == "ACEPTADO")
                {
                    visor.Rows[i].Cells[5].Style.BackColor = System.Drawing.ColorTranslator.FromHtml("#28a745");
                    visor.Rows[i].Cells[5].Style.ForeColor = Color.White;
                    visor.Rows[i].Cells[5].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                    DataGridViewTextBoxCell oEmptyTextCell = new DataGridViewTextBoxCell();
                    oEmptyTextCell.Value = String.Empty;
                    visor.Rows[i].Cells[11] = oEmptyTextCell;
                }
                if (compare == "REGISTRADO")
                {
                    visor.Rows[i].Cells[5].Style.BackColor = Color.Gray;
                    visor.Rows[i].Cells[5].Style.ForeColor = Color.White;
                    visor.Rows[i].Cells[5].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                }
                if (compare == "XML GENERADO")
                {
                    visor.Rows[i].Cells[5].Style.BackColor = System.Drawing.ColorTranslator.FromHtml("#0088cc");
                    visor.Rows[i].Cells[5].Style.ForeColor = Color.White;
                    visor.Rows[i].Cells[5].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                }

                if (compare == "RECHAZADO")
                {
                    visor.Rows[i].Cells[5].Style.BackColor = System.Drawing.ColorTranslator.FromHtml("#343a40");
                    visor.Rows[i].Cells[5].Style.ForeColor = Color.White;
                    visor.Rows[i].Cells[5].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
                if (compare == "OBSERVADO")
                {
                    visor.Rows[i].Cells[5].Style.BackColor = Color.Orange;
                    visor.Rows[i].Cells[5].Style.ForeColor = Color.White;
                    visor.Rows[i].Cells[5].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                    DataGridViewTextBoxCell oEmptyTextCell = new DataGridViewTextBoxCell();
                    oEmptyTextCell.Value = String.Empty;
                    visor.Rows[i].Cells[11] = oEmptyTextCell;
                }

                visor.Rows[i].Cells[5].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                visor.Rows[i].Cells[6].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                visor.Rows[i].Cells[7].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                visor.Rows[i].Cells[8].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                visor.Rows[i].Cells[9].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            }

        }
        public void cargar_datos(List<CENListadoComprobantes> lista)
        {
            total_datos = lista.Count;
            maximo_paginas = Convert.ToInt32(Math.Ceiling(total_datos / items_por_paginas));
            this.lbl_totalPaginas.Text = maximo_paginas.ToString();

            if (total_datos > 0)
            {
                this.lbl_pagina.Text = (pagina + 1).ToString();
            }

            detalleLista.DataSource = null;
            detalleLista.Columns.Clear();
            detalleLista.DataSource = lista.Skip((int)items_por_paginas * pagina).Take((int)items_por_paginas).ToList();

            DataGridViewButtonColumn reenviarCol = new DataGridViewButtonColumn();
            reenviarCol.HeaderText = "SUNAT";
            reenviarCol.Text = "REENVIAR";
            reenviarCol.Name = "colReenviar";
            reenviarCol.UseColumnTextForButtonValue = true;
            detalleLista.Columns.Add(reenviarCol);

            DataGridViewButtonColumn bcol = new DataGridViewButtonColumn();
            bcol.HeaderText = "ACCION";
            bcol.Text = "PDF";
            bcol.Name = "colAccion";
            bcol.UseColumnTextForButtonValue = true;
            detalleLista.Columns.Add(bcol);
            this.detalleLista.Columns["ID_TC"].Visible = false;

            this.gestionaResaltados(detalleLista);


            this.habilitar_botones();


            if (total_datos == 0)
            {
                this.lblError.Text = "NO SE ENCONTRARON RESULTADOS";
            }
            else
            {
                this.lblError.Text = "";
            }
           


        }
        public void habilitar_botones()
        {


            if (pagina == 0)
            {
                this.btn_preview.Enabled = false;
            }
            else
            {
                this.btn_preview.Enabled = true;
            }

            if (pagina == (maximo_paginas - 1) || total_datos <= 0)
            {
                this.bnt_next.Enabled = false;
            }
            else
            {
                this.bnt_next.Enabled = true;
            }

        }

        private void txt_codigo_venta_Enter(object sender, EventArgs e)
        {
            if (this.txt_codigo_venta.Text == "CODIGO")
            {
                this.txt_codigo_venta.Text = "";
                this.txt_codigo_venta.ForeColor = Color.Black;
               
            }


        }

        private void txt_codigo_venta_Leave(object sender, EventArgs e)
        {
            if (this.txt_codigo_venta.Text == "")
            {
                this.txt_codigo_venta.Text = "CODIGO";
                this.txt_codigo_venta.ForeColor = Color.Silver;
            }
        }

        private void txt_codigo_venta_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
             (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') >= -1))
            {
                e.Handled = true;
            }


        }



        private void txt_serie_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') >= -1))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') >= -1))
            {
                e.Handled = true;
            }
        }

        private void txt_serie_Enter(object sender, EventArgs e)
        {
            if (this.txt_serie.Text == "SERIE")
            {
                this.txt_serie.Text = "";
                this.txt_serie.ForeColor = Color.Black;
            }
        }

        private void txt_serie_Leave(object sender, EventArgs e)
        {
            if (this.txt_serie.Text == "")
            {
                this.txt_serie.Text = "SERIE";
                this.txt_serie.ForeColor = Color.Silver;
            }
        }

        private void txt_number_Enter(object sender, EventArgs e)
        {
            if (this.txt_number.Text == "NUMERO")
            {
                this.txt_number.Text = "";
                this.txt_number.ForeColor = Color.Black;
            }
        }

        private void txt_number_Leave(object sender, EventArgs e)
        {
            if (this.txt_number.Text == "")
            {
                this.txt_number.Text = "NUMERO";
                this.txt_number.ForeColor = Color.Silver;
            }
        }

        private void txt_cliente_Enter(object sender, EventArgs e)
        {
            if (this.txt_cliente.Text == "NUMERO DE DOCUMENTO / NOMBRE")
            {
                this.txt_cliente.Text = "";
                this.txt_cliente.ForeColor = Color.Black;
            }

        }

        private void txt_cliente_Leave(object sender, EventArgs e)
        {
            if (this.txt_cliente.Text == "")
            {
                this.txt_cliente.Text = "NUMERO DE DOCUMENTO / NOMBRE";
                this.txt_cliente.ForeColor = Color.Silver;
            }
        }


        private void txt_number_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
            (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') >= -1))
            {
                e.Handled = true;
            }
        }

        private void bnt_next_Click(object sender, EventArgs e)
        {

            pagina += 1;

            this.cargar_datos(listaRN);


        }

        private void btn_preview_Click(object sender, EventArgs e)
        {
            pagina -= 1;

            this.cargar_datos(listaRN);
        }

        private void cmbTipoComprobante_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (this.cmbTipoComprobante.SelectedValue.ToString() != "0") {
                if (this.txt_codigo_venta.Text != "CODIGO")
                {
                    this.txt_codigo_venta.Text = "CODIGO";
                    this.txt_codigo_venta.ForeColor = Color.Silver;
                }

                if (this.txt_serie.Text != "SERIE")
                {
                    this.txt_serie.Text = "SERIE";
                    this.txt_serie.ForeColor = Color.Silver;
                }
                if (this.txt_number.Text != "SERIE")
                {
                    this.txt_number.Text = "NUMERO";
                    this.txt_number.ForeColor = Color.Silver;
                }


            }




        }
        public static IDictionary ReadDictionaryFile(string fileName)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            foreach (string line in File.ReadAllLines(fileName))
            {
                if ((!string.IsNullOrEmpty(line)) &&
                    (!line.StartsWith(";")) &&
                    (!line.StartsWith("#")) &&
                    (!line.StartsWith("'")) &&
                    (line.Contains('=')))
                {
                    int index = line.IndexOf('=');
                    string key = line.Substring(0, index).Trim();
                    string value = line.Substring(index + 1).Trim();

                    if ((value.StartsWith("\"") && value.EndsWith("\"")) ||
                        (value.StartsWith("'") && value.EndsWith("'")))
                    {
                        value = value.Substring(1, value.Length - 2);
                    }
                    dictionary.Add(key, value);
                }
            }

            return dictionary;
        }
        private void detalleLista_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }


        private void CreateIfMissing(string path) {
            try
            {
                bool folderExists = Directory.Exists(path);
                if (!folderExists) Directory.CreateDirectory(path);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                aviso aviso_form = new aviso("warning", "Error mover archivos", ex.Message, false);
                aviso_form.ShowDialog();
            
            }

        }
        private void moveArchivo(string rutaA, string rutaB)
        {
            if (File.Exists(rutaA))
            {
                File.Move(rutaA, rutaB);
            }
        }

        private void deleteBdSFS(String Ruta, String RUC, String t_d, String series, int id_pass)
        {
            try
            {

                //seteamos a 03 en bd de factura
            
                CLNDocumento daoD = new CLNDocumento();

                try
                {
                    var respSFS = daoD.Delete(series);

                    if (respSFS)
                    {
                        //eliminamos en bd sfs y pasamos la data en carpeta
                        String extDelete = "", rutaPlanos, cabD, leyD, detD, triD, rutaNewPlanos, rutaXml, rutaNewXml, rutaEnvio, rutaNewEnvio, fileXml, fileZip
                                            , fileRPTA, rutaRPTA, rutaNewRPTA;
                        if (t_d == "01" || t_d == "03")
                        {
                            extDelete = ".CAB";
                        }
                        if (t_d == "07" || t_d == "08")
                        {
                            extDelete = ".NOT";
                        }
                        rutaPlanos = Ruta + @"DATA\";
                        cabD = RUC + "-" + t_d + "-" + series + extDelete;
                        leyD = RUC + "-" + t_d + "-" + series + ".LEY";
                        detD = RUC + "-" + t_d + "-" + series + ".DET";
                        triD = RUC + "-" + t_d + "-" + series + ".TRI";

                        rutaNewPlanos = rutaPlanos + RUC;

                        CreateIfMissing(rutaNewPlanos);

                        moveArchivo(rutaPlanos + cabD, rutaNewPlanos + @"\" + cabD);
                        moveArchivo(rutaPlanos + leyD, rutaNewPlanos + @"\" + leyD);
                        moveArchivo(rutaPlanos + detD, rutaNewPlanos + @"\" + detD);
                        moveArchivo(rutaPlanos + triD, rutaNewPlanos + @"\" + triD);

                        //antes de mover xml revisamos si existe hash en el comprobante, de no existir, grabamos

                        CLNDocumentTypes clnDT = new CLNDocumentTypes();
                        string get_hash = clnDT.getHash(id_pass);

                        Console.WriteLine("get_hash " + get_hash);
                        if (get_hash.ToString() == "" || get_hash.ToString() == null)
                        {
                            try
                            {
                                bool xml_hash;
                                string serie, numero;
                                serie = series.Split('-')[0];
                                numero = series.Split('-')[1];
                                xml_hash = clnDT.getHashXML(t_d, serie, numero, id_pass);

                                if (xml_hash)
                                {

                                    rutaXml = Ruta + @"FIRMA\";
                                    rutaNewXml = rutaXml + RUC;
                                    fileXml = RUC + "-" + t_d + "-" + series + ".xml"; 
                                    CreateIfMissing(rutaNewXml);
                                    moveArchivo(rutaXml + fileXml, rutaNewXml + @"\" + fileXml);
                                }


                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex);
                                
                                aviso aviso_sfs = new aviso("warning", "ERROR AL GRABAR CODIGO HASH DE XML", ex.ToString(), false);
                                aviso_sfs.ShowDialog();

                            }
                        }


                        rutaEnvio = Ruta + @"ENVIO\";
                        rutaNewEnvio = rutaEnvio + RUC;
                        fileZip = RUC + "-" + t_d + "-" + series + ".zip"; ;
                        CreateIfMissing(rutaNewEnvio);
                        moveArchivo(rutaEnvio + fileZip, rutaNewEnvio + @"\" + fileZip);

                        rutaRPTA = Ruta + @"RPTA\";
                        rutaNewRPTA = rutaRPTA + RUC;
                        fileRPTA = "R" + RUC + "-" + t_d + "-" + series + ".zip"; 
                        CreateIfMissing(rutaNewRPTA);
                        moveArchivo(rutaRPTA + fileRPTA, rutaNewRPTA + @"\" + fileRPTA);

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    
                    aviso aviso_sfs = new aviso("warning", "ERROR", ex.ToString(), false);
                    aviso_sfs.ShowDialog();
                    return;
                }

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex);
                aviso aviso_sfs = new aviso("warning", "ERROR", ex.ToString(), false);
                aviso_sfs.ShowDialog();
            
            }
        }


       

        private void button1_Click(object sender, EventArgs e)
        {

          
            Cursor.Current = Cursors.WaitCursor;

            this.actEstCmp();
            this.sendCmpSync();


           
            aviso aviso_form_e = new aviso("success", "SUCCESS", "Sincronización realizado con éxito.", false);
            aviso_form_e.ShowDialog();
            Cursor.Current = Cursors.Default;
        }

        private void sendCmpSync()
        {
            List<CENListadoComprobantes> listaDocumentos;
            CenSearchComprobantes search01 = new CenSearchComprobantes
            {
                fecha_inicio = "",
                fecha_fin = "",
                dni_ruc = "",
                name_cliente ="",
                tipo_comprobante = "0",
                series_number = "",
                codigo_venta = 0,
                _id_estado = "01"
            };
          
            CLNDocumentTypes cln0 = new CLNDocumentTypes();
            listaDocumentos = cln0.search_listado_comprobantes(search01);

            Cursor.Current = Cursors.WaitCursor;
            foreach (var item in listaDocumentos)
            {

                sendCmpIndividual(item.VENTA, item.ID_TC, item.DOCUMENTO, false);
            }
            CenSearchComprobantes search02 = new CenSearchComprobantes
            {
                fecha_inicio = "",
                fecha_fin = "",
                dni_ruc = "",
                name_cliente = "",
                tipo_comprobante = "0",
                series_number = "",
                codigo_venta = 0,
                _id_estado = "02"
            };
            listaDocumentos = cln0.search_listado_comprobantes(search02);
            foreach (var item in listaDocumentos)
            {

                sendCmpIndividual(item.VENTA, item.ID_TC, item.DOCUMENTO, false);
            }
            Cursor.Current = Cursors.Default;

        }

        private void sendCmpIndividual( int id_pass, String t_d, String series,Boolean showMsg)
        {
            string datosEmpresa = WebVentas.util.Helper.ObtenerValorParametro(CENConstantes.CONST_2);
            string Ruta = datosEmpresa.Split('|')[1];
            string RUC = datosEmpresa.Split('|')[0];
            String fileZip, fileXml, ArchivoXMLzip, ArchivoCDR;
            Byte[] byteArray;

            fileZip = RUC + "-" + t_d + "-" + series + ".zip";
            fileXml = RUC + "-" + t_d + "-" + series + ".xml";
            ArchivoXMLzip = Ruta + @"ENVIO\" + fileZip;
            ArchivoCDR = Ruta + @"RPTA\R" + fileZip;


            if (File.Exists(ArchivoCDR))
            {

                if (showMsg)
                {
                  
                    aviso aviso_form00 = new aviso("success", "SUCCESS", "El documento ya ha sido enviado anteriormente.", false);
                    aviso_form00.ShowDialog();
                }

            

                String rsDsc = "";
                String rsCode = "";
                List<string> rsObs = new List<string>();
                try
                {
                    using (ZipArchive zfile = ZipFile.Open(ArchivoCDR, ZipArchiveMode.Read))
                    {
                        ZipArchiveEntry zentry = zfile.GetEntry("R-" + fileXml);
                        XmlDocument xDoc = new XmlDocument();
                        Stream stream;
                        stream = zentry.Open();
                        xDoc.Load(stream);
                        XmlNodeList xmlCode = xDoc.GetElementsByTagName("cbc:ResponseCode");
                        foreach (XmlElement item in xmlCode)
                        {
                            rsCode = item.InnerText;
                        }
                        XmlNodeList xmlDsc = xDoc.GetElementsByTagName("cbc:Description");
                        foreach (XmlElement item in xmlDsc)
                        {
                            rsDsc = item.InnerText;
                        }

                        XmlNodeList xmlObs = xDoc.GetElementsByTagName("cbc:Note");

                        foreach (XmlElement item in xmlObs)
                        {
                            rsObs.Add(item.InnerText);

                        }


                        if (showMsg)
                        {
                            
                            aviso aviso_form = new aviso("success", "SUCCESS", rsDsc, false);
                            aviso_form.ShowDialog();
                        }


                

                    }

                    if (rsCode == "0")
                    {
                        //revisar si tiene observaciones
                        try
                        {
                            String estado = "";
                            if (rsObs.Count == 0) estado = "05"; else estado = "07";

                            CLNDocumentTypes clndt = new CLNDocumentTypes();
                            clndt.setEstadoDocumento(id_pass, estado);
                            this.generar_busqueda();

                            if (rsObs.Count == 0)
                            {
                                deleteBdSFS(Ruta, RUC, t_d, series, id_pass);
                            }
                            else
                            {
                                CENDocumento doc = new CENDocumento();
                                CLNDocumento daoD = new CLNDocumento();
                                doc.fec_envi = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                                doc.des_obse = rsObs.Count + " - Observacion(es)";
                                doc.ind_situ = "04";
                                doc.num_docu = series;
                                try
                                {
                                  daoD.Update(doc);
                                 

                                }
                                catch (Exception ex)
                                {

                                   
                                    aviso aviso_form = new aviso("warning", "ERROR", ex.Message.ToString(), false);
                                    aviso_form.ShowDialog();
                                    return;
                                }
                              
                            }

                            Cursor.Current = Cursors.Default;
                            return;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                            aviso aviso_form2 = new aviso("warnint", "ERROR", ex.Message, false);
                            aviso_form2.ShowDialog();
                            Cursor.Current = Cursors.Default;
                            return;
                        }

                    }
                }
                catch (Exception ex)
                {

                    //Del 0100 al 1999 Excepciones
                    //Del 2000 al 3999 Errores que generan rechazo
                    //Del 4000 en adelante Observaciones
                    //if(ex.InnerException.Message.Contains("No se puede resolver el nombre remoto") == true)
                    if (ex.InnerException != null)
                    {
                       
                        aviso aviso_form = new aviso("warning", "Error - revise su conexión a internet", ex.Message, false);
                        aviso_form.ShowDialog();
                        Cursor.Current = Cursors.Default;
                        return;
                    }
                    else
                    {
                        String codeRes0 = (String)((System.ServiceModel.FaultException)ex).Code.Name.ToString();
                        int codeRes = (int)Convert.ToInt32(codeRes0.Split('.')[1]);
                        if (codeRes < 2000)
                        {
                            //Excepciones
                            
                            aviso aviso_form = new aviso("warning", "Error al invocar el servicio de SUNAT", ex.Message, false);
                            aviso_form.ShowDialog();
                            Cursor.Current = Cursors.Default;
                            return;
                        }
                        else if (codeRes < 4000)
                        {
                            //Rechazo
                            CLNDocumentTypes clndt = new CLNDocumentTypes();
                            clndt.setEstadoDocumento(id_pass, "09");
                            this.generar_busqueda();

                            CENDocumento doc = new CENDocumento();
                            CLNDocumento daoD = new CLNDocumento();

                            String obs = ex.Message.Split('-')[0];


                            doc.fec_envi = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                            doc.des_obse = codeRes + " - " + obs;
                            doc.ind_situ = "05";
                            doc.num_docu = series;

                            daoD.Update(doc);

                        }
                        else
                        {

                            //Observaciones
                            CLNDocumentTypes clndt = new CLNDocumentTypes();
                            clndt.setEstadoDocumento(id_pass, "07");
                            this.generar_busqueda();
                            String obs = ex.Message.Split('-')[0];
                            CENDocumento doc = new CENDocumento();
                            CLNDocumento daoD = new CLNDocumento();
                            doc.fec_envi = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                            doc.des_obse = codeRes + " - " + obs;
                            doc.ind_situ = "04";
                            doc.num_docu = series;

                            daoD.Update(doc);
                        }

                    }
                }
                Cursor.Current = Cursors.Default;
                return;

            }
            else
            {
                if (File.Exists(ArchivoXMLzip))
                {
                    try
                    {
                        // obteniendo usuario y password de empresa
                        CENEmpresa cenEmp = new CENEmpresa();

                        CLNSoap clnsoap = new CLNSoap();

                        cenEmp = clnsoap.searchSoap();




                        //obteniendo endpoint
                        String RutaEndPoint = Ruta + @"VALI\constantes.properties";

                        var dataEP = new Dictionary<string, string>();

                        dataEP = (Dictionary<string, string>)ReadDictionaryFile(RutaEndPoint);

                        byteArray = File.ReadAllBytes(ArchivoXMLzip);
                        var wsSunatGeneral = new wsSunatBeta.billServiceClient("BillServicePort1", dataEP["RUTA_SERV_CDP"]);
                        ////var wsSunatGeneral = new wsSunatBeta.billServiceClient("BillServicePort1", "https://e-beta.sunat.gob.pe/ol-ti-itcpfegem-beta/billServiceW");

                        wsSunatGeneral.ClientCredentials.UserName.UserName = cenEmp.usuario_soap;
                        wsSunatGeneral.ClientCredentials.UserName.Password = cenEmp.password_soap;
                        var behavior = new ferreteriaApp.DAO.PasswordDigestBehavior(cenEmp.usuario_soap, cenEmp.password_soap);

                        wsSunatGeneral.Endpoint.EndpointBehaviors.Add(behavior);

                        wsSunatGeneral.Open();
                        Byte[] returnByte;

                        returnByte = wsSunatGeneral.sendBill(fileZip, byteArray, "");
                        wsSunatGeneral.Close();

                        FileStream fs = new FileStream(Ruta + @"RPTA\" + "R" + fileZip, FileMode.Create);
                        fs.Write(returnByte, 0, returnByte.Length);
                        fs.Close();
                    


                        String rsDsc = "";
                        String rsCode = "";
                        List<string> rsObs = new List<string>();
                        try
                        {

                            using (ZipArchive zfile = ZipFile.Open(ArchivoCDR, ZipArchiveMode.Read))
                            {
                                ZipArchiveEntry zentry = zfile.GetEntry("R-" + fileXml);
                                XmlDocument xDoc = new XmlDocument();
                                Stream stream;
                                stream = zentry.Open();
                                xDoc.Load(stream);
                                XmlNodeList xmlCode = xDoc.GetElementsByTagName("cbc:ResponseCode");
                                foreach (XmlElement item in xmlCode)
                                {
                                    rsCode = item.InnerText;
                                }
                                XmlNodeList xmlDsc = xDoc.GetElementsByTagName("cbc:Description");
                                foreach (XmlElement item in xmlDsc)
                                {
                                    rsDsc = item.InnerText;
                                }



                                XmlNodeList xmlObs = xDoc.GetElementsByTagName("cbc:Note");

                                foreach (XmlElement item in xmlObs)
                                {
                                    rsObs.Add(item.InnerText);

                                }

                                if (showMsg == true)
                                {
                                  
                                    aviso aviso_form = new aviso("success", "SUCCESS", rsDsc, false);
                                    aviso_form.ShowDialog();
                                }
                             
                            }


                            if (rsCode == "0")
                            {

                                //revisar si tiene observaciones


                                try
                                {
                                    String estado = "";
                                    if (rsObs.Count == 0) estado = "05"; else estado = "07";

                                    CLNDocumentTypes clndt = new CLNDocumentTypes();
                                    clndt.setEstadoDocumento(id_pass, estado);
                                    this.generar_busqueda();

                                    if (rsObs.Count == 0)
                                    {
                                        deleteBdSFS(Ruta, RUC, t_d, series, id_pass);
                                    }
                                    else
                                    {
                                        CENDocumento doc = new CENDocumento();
                                        CLNDocumento daoD = new CLNDocumento();
                                        doc.fec_envi = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                                        doc.des_obse = rsObs.Count + " - Observacion(es)";
                                        doc.ind_situ = "04";
                                        doc.num_docu = series;

                                        daoD.Update(doc);
                                     
                                    }

                                    Cursor.Current = Cursors.Default;
                                    return;
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex);
                                    aviso aviso_form2 = new aviso("warning", "ERROR", ex.Message, false);
                                    aviso_form2.ShowDialog();

                                    Cursor.Current = Cursors.Default;
                                    return;

                                }
                                //cambiamos el estado a 05 en comprobante
                            }
                        }
                        catch (Exception ex)
                        {

                            Console.WriteLine(ex);
                            aviso aviso_form = new aviso("warning", "ERROR", ex.Message, false);
                            aviso_form.ShowDialog();

                            Cursor.Current = Cursors.Default;
                            return;

                        }


                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        //Del 0100 al 1999 Excepciones
                        //Del 2000 al 3999 Errores que generan rechazo
                        //Del 4000 en adelante Observaciones
                        //if(ex.InnerException.Message.Contains("No se puede resolver el nombre remoto") == true)
                        if (ex.InnerException != null)
                        {
                            Console.WriteLine(ex);
                            aviso aviso_form = new aviso("warning", "Error - revise su conexión a internet", ex.Message, false);
                            aviso_form.ShowDialog();
                            Cursor.Current = Cursors.Default;
                            return;
                        }
                        else
                        {
                            String codeRes0 = (String)((System.ServiceModel.FaultException)ex).Code.Name.ToString();
                            int codeRes = (int)Convert.ToInt32(codeRes0.Split('.')[1]);
                            if (codeRes < 2000)
                            {
                                //Excepciones
                                Console.WriteLine(ex);
                                aviso aviso_form = new aviso("warning", "Error al invocar el servicio de SUNAT", ex.Message, false);
                                aviso_form.ShowDialog();
                                Cursor.Current = Cursors.Default;
                                return;
                            }
                            else if (codeRes < 4000)
                            {
                                //Rechazo
                                CLNDocumentTypes clndt = new CLNDocumentTypes();
                                clndt.setEstadoDocumento(id_pass, "09");
                                this.generar_busqueda();

                                CENDocumento doc = new CENDocumento();
                                CLNDocumento daoD = new CLNDocumento();
                                String obs = ex.Message.Split('-')[0];
                                doc.fec_envi = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                                doc.des_obse = codeRes + " - " + obs;
                                doc.ind_situ = "05";
                                doc.num_docu = series;
                                daoD.Update(doc);
                            }
                            else
                            {

                                //Observaciones
                                CLNDocumentTypes clndt = new CLNDocumentTypes();
                                clndt.setEstadoDocumento(id_pass, "07");
                                this.generar_busqueda();
                                String obs = ex.Message.Split('-')[0];
                                CENDocumento doc = new CENDocumento();
                                CLNDocumento daoD = new CLNDocumento();
                                doc.fec_envi = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                                doc.des_obse = codeRes + " - " + obs;
                                doc.ind_situ = "04";
                                doc.num_docu = series;

                                daoD.Update(doc);
                            }

                        }

                    }



                }
                else
                {
                    if (showMsg)
                    {
                        
                        aviso aviso_form = new aviso("warning", "Error", "Genere el XML y vuelva a intentar.", false);
                        aviso_form.ShowDialog();
                        Cursor.Current = Cursors.Default;
                        return;
                    }
               
                }



                Cursor.Current = Cursors.Default;
            }

        }
        private void actEstCmp()
        {
            List<CENListadoComprobantes> listaDocumentos;
            CenSearchComprobantes search01 = new CenSearchComprobantes
            {
                fecha_inicio = "",
                fecha_fin = "",
                dni_ruc = "",
                name_cliente = "",
                tipo_comprobante = "0",
                series_number = "",
                codigo_venta = 0,
                _id_estado = "01"
            };

            CLNDocumentTypes cln0 = new CLNDocumentTypes();
            listaDocumentos = cln0.search_listado_comprobantes(search01);

            Cursor.Current = Cursors.WaitCursor;

            foreach (var item in listaDocumentos)
            {

                int id_pass = (int)Convert.ToInt64(item.VENTA);

                String t_d = item.ID_TC;
                String series = item.DOCUMENTO;

                string datosEmpresa = WebVentas.util.Helper.ObtenerValorParametro(CENConstantes.CONST_2);
                string Ruta = datosEmpresa.Split('|')[1];
                string RUC = datosEmpresa.Split('|')[0];

                String  fileZip, fileXml, ArchivoCDR, ArchivoCDR_A, rutaCdr="";
             
                Boolean has_ruta;

                fileZip = RUC + "-" + t_d + "-" + series + ".zip";
                fileXml = RUC + "-" + t_d + "-" + series + ".xml";
                ArchivoCDR = Ruta + @"RPTA\R" + fileZip;
                ArchivoCDR_A = Ruta + @"RPTA\" + RUC + @"\R" + fileZip;


                CLNDocumentTypes clndt = new CLNDocumentTypes();
                try
                {
                    if (File.Exists(ArchivoCDR))
                    {
                        rutaCdr = ArchivoCDR;
                        has_ruta = true;
                    }
                    else
                    {
                        if (File.Exists(ArchivoCDR_A))
                        {
                            rutaCdr = ArchivoCDR_A;
                            has_ruta = true;
                        }
                        else
                        {
                            has_ruta = false;

                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    aviso aviso_form0 = new aviso("warnint", "ERROR EN ARCHIVOS CDR", ex.Message, false);
                    aviso_form0.ShowDialog();           
                    return;
                }
              

                if (has_ruta)
                {

                    String rsCode = "";
                    List<string> rsObs = new List<string>();
                    try
                    {
                        using (ZipArchive zfile = ZipFile.Open(rutaCdr, ZipArchiveMode.Read))
                        {
                            ZipArchiveEntry zentry = zfile.GetEntry("R-" + fileXml);
                            XmlDocument xDoc = new XmlDocument();
                            Stream stream;
                            stream = zentry.Open();
                            xDoc.Load(stream);
                            XmlNodeList xmlCode = xDoc.GetElementsByTagName("cbc:ResponseCode");
                            foreach (XmlElement itemE in xmlCode)
                            {
                                rsCode = itemE.InnerText;
                            }
                            XmlNodeList xmlObs = xDoc.GetElementsByTagName("cbc:Note");
                            foreach (XmlElement itemObs in xmlObs)
                            {
                                rsObs.Add(itemObs.InnerText);
                            }
                        }

                        if (rsCode == "0")
                        {
                            try
                            {

                                String estado = "";
                                if (rsObs.Count == 0) estado = "05"; else estado = "07";
                                clndt.setEstadoDocumento(id_pass, estado);

                                if (rsObs.Count == 0)
                                {
                                    deleteBdSFS(Ruta, RUC, t_d, series, id_pass);
                                }
                                this.generar_busqueda();

                                Cursor.Current = Cursors.Default;
                                return;

                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex);
                                aviso aviso_form2 = new aviso("warnint", "ERROR", ex.Message, false);
                                aviso_form2.ShowDialog();
                                return;
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        aviso aviso_form = new aviso("warning", "ERROR", ex.Message, false);
                        aviso_form.ShowDialog();
                        return;
                    }

                }
                else
                {

                    String fileZipXml = RUC + "-" + t_d + "-" + series + ".zip";
                    String ArchivoZipXml = Ruta + @"ENVIO\" + fileZipXml;

                    if (File.Exists(ArchivoZipXml) == true)
                    {
                        try
                        {
                            clndt.setEstadoDocumento(id_pass, "02");
                            this.generar_busqueda();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                            aviso aviso_form = new aviso("warning", "ERROR", ex.Message, false);
                            aviso_form.ShowDialog();
                            return;
                        }

                    }
                }
            }
            return;
        }

        private void detalleLista_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (this.detalleLista.Columns[e.ColumnIndex].Name == "colAccion")
            {


                int valor = e.RowIndex;
                int id_pass = Convert.ToInt32(this.detalleLista.Rows[valor].Cells[0].Value);

                CLNComprobante clncmp = new CLNComprobante();

                try
                {
                    Boolean resPDf = clncmp.recrearDocumento(id_pass);
                    if (!resPDf)
                    {

                        DialogResult diagPDF;
                        aviso aviso_form_error_hash = new aviso("warning", "ERROR AL OBTENER CODIGO HASH", "REVISE QUE FACTURADOR SUNAT GENERE XML  \n"
                                                      + "¿Desea volver a generar?", true);
                        diagPDF = aviso_form_error_hash.ShowDialog();

                        if (diagPDF == DialogResult.OK)
                        {
                            Boolean resPDf2 = clncmp.recrearDocumento(id_pass);

                            if (!resPDf)
                            {
                               
                                aviso aviso_form_error_hash2 = new aviso("warning", "ERROR", "CONTACTESE CON SERVICIO TECNICO", true);
                                aviso_form_error_hash2.ShowDialog();
                                return;

                            }


                        }
                        else
                        {
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {


                    aviso aviso_form = new aviso("warning", "ERROR EN LA SOLICITUD", ex.Message, false);
                    aviso_form.ShowDialog();
                    return;
                }



            }


            if (this.detalleLista.Columns[e.ColumnIndex].Name == "colReenviar")
            {
                int valor = e.RowIndex;
                int id_pass = Convert.ToInt32(this.detalleLista.Rows[valor].Cells[0].Value);
                String t_d = this.detalleLista.Rows[valor].Cells["ID_TC"].Value.ToString();
                String series = this.detalleLista.Rows[valor].Cells["DOCUMENTO"].Value.ToString();

                Cursor.Current = Cursors.WaitCursor;
                sendCmpIndividual(id_pass, t_d, series, true);
                Cursor.Current = Cursors.Default;
            }
        }
    }
}
