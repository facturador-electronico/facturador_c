﻿using ferreteriaApp.CEN;
using ferreteriaApp.CLN;
using ferreteriaApp.HELPERS;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

namespace ferreteriaApp
{
    public partial class dashboard : Form
    {

        CLNComprobante cln = new CLNComprobante();
        CLNConcepto clnConcepto = new CLNConcepto();


        // encapsulacion de datos para el llenado desde el formulario de producto
        #region

        private TextBox _textPrdo;
        private TextBox _textcodigo;
        private TextBox _textPrecio;
        private TextBox _textunidad;
        private CheckBox _cambiarPrecio;
        private Label _mensajeCheck;
        public string _tipoMoneda;

        public TextBox TextPrdo { get => _textPrdo; set => _textPrdo = value; }
        public TextBox Textcodigo { get => _textcodigo; set => _textcodigo = value; }
        public TextBox TextPrecio { get => _textPrecio; set => _textPrecio = value; }
        public TextBox Textunidad { get => _textunidad; set => _textunidad = value; }
        public CheckBox CambiarPrecio { get => _cambiarPrecio; set => _cambiarPrecio = value; }
        public Label MensajeCheck { get => _mensajeCheck; set => _mensajeCheck = value; }
       
        private void init() {
            _textPrdo = txt_producto;
            _textcodigo = txt_codigo;
            _textPrecio = txt_precio;
            _textunidad = txt_unidadMedida;
            _cambiarPrecio = chk_editar_precio;
            _mensajeCheck = lbl_modificar;
        }

        #endregion

        public dashboard()
        {
            InitializeComponent();
        }

        private void llenarCodigoComprobante() {
            string cp;

            if (comboBox1.SelectedValue.ToString() == "1")
            {

                cp = cln.codigoBoleta(CENConstantes.FACTURA);
                txt_documento.MaxLength = 11;
                mensaje_documento.Text = "Cantidad de números 11";
                txt_codigoBoleta.Text = clnConcepto.ObtenerValorParametro(3) + "-" + cp;
            }
            else
            {
                cp = cln.codigoBoleta(CENConstantes.BOLETA);
                txt_documento.MaxLength = 8;
                mensaje_documento.Text = "Cantidad de números 8";
                txt_codigoBoleta.Text = clnConcepto.ObtenerValorParametro(4) + "-" + cp;

            }

            comboBox1.SelectedValue = 1;
            txt_codigo_comprobante.Text = cp;


        }
        private void LlenarCombo()
        {
            List<CENTipoDocumento> lista;
            this.comboBox1.Items.Clear();
            try
            {
                CLNConcepto cln = new CLNConcepto();

                lista = cln.listaTipoDocumentos(1);

                this.comboBox1.DataSource = lista;
                this.comboBox1.DisplayMember = "nombreDocumento";
                this.comboBox1.ValueMember = "codigoDocumento";
            }
            catch
            {
                CENTipoDocumento cen = new CENTipoDocumento();
                cen.codigoDocumento = 0;
                cen.nombreDocumento = "No se encontraron documentos";
                lista = null;
                this.comboBox1.DataSource = lista;
                this.comboBox1.DisplayMember = "nombreDocumento";
                this.comboBox1.ValueMember = "codigoDocumento";
            }


        }


        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
       
        private void dashboard_Load(object sender, EventArgs e)
        {
            init();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void detalleProductTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void comporobant1_Load(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_agragr_producto_Click(object sender, EventArgs e)
        {
            decimal subtotal = 0;
            decimal total = 0;
            decimal igv = 0;
            decimal valor_item = 0;

            String producto = txt_producto.Text;
            String unidadMedida = txt_unidadMedida.Text;
            String codigo = txt_codigo.Text;

            Boolean estado = true;
            int cantidad = 0;
            decimal precio = 0,
                descuento = 0;

            if (txt_producto.Text == null || txt_producto.Text == "")
            {
                lbl_mensaje.Text = "";
                lbl_mensaje.Text = "Ingrese el nombre del producto";
                estado = false;
                txt_producto.Focus();
                return;
            }

            if (txt_precio.Text.Trim() == null || txt_precio.Text.Trim() == "") {
                estado = false;
                lbl_mensaje.Text = "";
                txt_precio.Focus();
                lbl_mensaje.Text = "Ingrese el precio del producto";
                return;
            }
            else
            {
              

                try
                {
                    precio = Convert.ToDecimal(txt_precio.Text);
                    if (precio <= 0)
                    {
                        estado = false;
                        lbl_mensaje.Text = "";
                        txt_precio.Text = "";
                        txt_precio.Focus();
                        lbl_mensaje.Text = "El precio debe ser mayor a cero, ";
                        return;
                    }
                }
                catch
                {
                    lbl_mensaje.Text = "";
                    txt_precio.Text = "0";
                    estado = false;
                    txt_precio.Text = "";
                    lbl_mensaje.Text = "El precio debe ser expresado en números";
                    txt_precio.Focus();
                    return;
                }

            }

            if (txt_cantidad.Text.Trim() == null || txt_cantidad.Text.Trim() == "")
            {
                estado = false;
                lbl_mensaje.Text = "";
                txt_cantidad.Focus();
                lbl_mensaje.Text = "Ingrese la cantidad del producto";
                return;
            }
            else {

                try
                {
                    cantidad = Convert.ToInt32(txt_cantidad.Text);

                    if (cantidad <= 0)
                    {
                        lbl_mensaje.Text = "";
                        estado = false;
                        txt_cantidad.Text = "";
                        txt_cantidad.Focus();
                        lbl_mensaje.Text = "La cantidad debe ser mayor a cero";
                        return;
                    }
                }
                catch
                {
                    estado = false;
                    lbl_mensaje.Text = "";
                    txt_cantidad.Text = "";
                    txt_cantidad.Focus();
                    lbl_mensaje.Text = "La cantidad debe ser expresada en números";
                    return;
                }

            }

            try
            {
                descuento = Convert.ToDecimal(txt_descuento.Text);
                if (descuento < 0)
                {
                    lbl_mensaje.Text = "";
                    estado = false;
                    txt_descuento.Text = "";
                    txt_descuento.Focus();
                    lbl_mensaje.Text = "El descuento no puede contener signos y debe ser mato o igual a cero";
                    return;
                }
            }
            catch
            {
                lbl_mensaje.Text = "";
                txt_descuento.Text = "0";
                estado = false;
                lbl_mensaje.Text = "El descuento debe ser expresado en números";
                txt_descuento.Focus();

                return;

            }

           

            if (estado)
            {
                lbl_mensaje.Text = "";
                int cantidad_prod = detalleProductTable.Rows.Count;

                if (cantidad_prod < 15)
                {
                    cantidad_productos.Text = "| Cantidad de productos: " + cantidad_prod;

                    int table = detalleProductTable.Rows.Add();

                    detalleProductTable.Rows[table].Cells[0].Value = codigo;
                    detalleProductTable.Rows[table].Cells[1].Value = producto;
                    detalleProductTable.Rows[table].Cells[2].Value = cantidad;
                    detalleProductTable.Rows[table].Cells[3].Value = unidadMedida;
                    detalleProductTable.Rows[table].Cells[4].Value = precio;
                    detalleProductTable.Rows[table].Cells[5].Value = descuento;
                    detalleProductTable.Rows[table].Cells[6].Value = decimal.Round(((cantidad * precio) / Convert.ToDecimal(1.18) * Convert.ToDecimal(0.18)),2) ;

                    valor_item = Convert.ToDecimal((precio * cantidad) - descuento) / Convert.ToDecimal(1.18);

                    detalleProductTable.Rows[table].Cells[7].Value = decimal.Round(valor_item, 2);
                    detalleProductTable.Rows[table].DefaultCellStyle.ForeColor = Color.Black;

                    for (var i = 0; i < detalleProductTable.Rows.Count - 1; i++)
                    {
                        subtotal = subtotal + Convert.ToDecimal(detalleProductTable.Rows[i].Cells[7].Value.ToString());
                        total = total + Convert.ToInt32(detalleProductTable.Rows[i].Cells[2].Value.ToString()) * Convert.ToDecimal(detalleProductTable.Rows[i].Cells[4].Value.ToString());
                    }

                    igv = decimal.Round((total / Convert.ToDecimal(1.18) * Convert.ToDecimal(0.18)), 2); ;


                    txt_subtotal.Text = subtotal.ToString();
                    txt_igv.Text = igv.ToString();
                    txt_total.Text = total.ToString();



                    txt_producto.Text = "";
                    txt_codigo.Text = "";
                    txt_producto.Text = "";
                    txt_precio.Text = "";
                    txt_cantidad.Text = "";
                    txt_descuento.Text = "0";
                    txt_unidadMedida.Text = "UNIDAD";
                    txt_mensaje.Text = "";
                    txt_producto.Focus();

                    lbl_modificar.Visible = false;
                    chk_editar_precio.Visible = false;
                }
                else {
                    MessageBox.Show("Solo puede agregar 14 productos como máximo");
                }



            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
    
                Int64 documento;
                int descuentoTotal = 0;

                CLNComprobante cln = new CLNComprobante();

                if (txtCliente.Text.Trim().Length == 0)
                {
                    MessageBox.Show("Debe ingresar el nombre del cliente");
                    txtCliente.Focus();
                }
                else if (txt_documento.Text.Trim().Length == 0)
                {
                    MessageBox.Show("Debe ingresar documento del cliente");
                    txt_documento.Focus();

                }
                else if (detalleProductTable.Rows.Count == 1)
                {
                    MessageBox.Show("Debe ingresar un producto para guardar la venta");

                }
                else if (comboBox1.SelectedValue.ToString() == "2" && txt_documento.Text.Trim().Length < 8)
                {
                    txt_documento.Focus();
                    MessageBox.Show("El dni debe tener 8 digitos");
                }
                else if (comboBox1.SelectedValue.ToString() == "1" && txt_documento.Text.Trim().Length < 11)
                {
                    txt_documento.Focus();
                    MessageBox.Show("El ruc debe tener 11 digitos");


                }

                else if (!Int64.TryParse(txt_documento.Text, NumberStyles.AllowThousands, CultureInfo.CreateSpecificCulture("es-ES"), out documento))
                {

                    MessageBox.Show("Error el documento solo debe tener números");
                    txt_documento.Text = "";
                    txt_documento.Focus();
                }
                else
                {

               
                DialogResult dialog = MessageBox.Show("¿ Está seguro que desea guardar la venta ?", "Confirmación",
                MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                if (dialog == DialogResult.Yes)
                {



                    //datos del cliente

                    String direccionCliente = txtDireccionCliente.Text;
                    String cliente = txtCliente.Text;

                    decimal anticipo = 0;

                    decimal isc = 0;
                    decimal otrosImpuestos = 0;
                    decimal descuentoGlobal = 0;
                    CENComprobanteDetalle det;
                    CENComprobanteCabecera cab = new CENComprobanteCabecera();
                    List<CENComprobanteDetalle> listDet = new List<CENComprobanteDetalle>();

                    for (int i = 0; i < detalleProductTable.Rows.Count - 1; i++)
                    {

                        det = new CENComprobanteDetalle();

                        det.cantidad = Int16.Parse(detalleProductTable.Rows[i].Cells[2].Value.ToString());
                        det.unidadMedida = detalleProductTable.Rows[i].Cells[3].Value.ToString();
                        det.codigo = detalleProductTable.Rows[i].Cells[0].Value.ToString(); ;
                        det.descripcion = detalleProductTable.Rows[i].Cells[1].Value.ToString();
                        det.precioUnitario = decimal.Parse(detalleProductTable.Rows[i].Cells[4].Value.ToString());
                        det.descuentoUnitario = decimal.Parse(detalleProductTable.Rows[i].Cells[5].Value.ToString());
                        det.valorItem = decimal.Parse(detalleProductTable.Rows[i].Cells[7].Value.ToString());
                        det.icbper = 0;

                        listDet.Add(det);

                    }


                    string serie;

                    if (comboBox1.SelectedValue.ToString() == "1")
                    {

                        cab.tipoComprobante = CENConstantes.FACTURA;
                        cab.tipoDocumento = CENConstantes.CODIGO_RUC;
                        serie = clnConcepto.ObtenerValorParametro(3);
                    }
                    else
                    {
                        cab.tipoComprobante = CENConstantes.BOLETA;
                        cab.tipoDocumento = CENConstantes.CODIGO_DNI;
                        serie = clnConcepto.ObtenerValorParametro(4);

                    }

                    cab.tipoOperacion = 1;
                    cab.empresa = 1;
                    cab.codigo = txt_codigoBoleta.Text;
                    cab.fechaEmision = txt_fecha.Value.Date;
                    cab.anticipos = anticipo;
                    cab.descuentoGlobal = descuentoGlobal;
                    cab.descuentoTotal = descuentoTotal;
                    cab.direccionCliente = direccionCliente;
                    cab.nombreCliente = cliente;
                    cab.documentoCliente = documento.ToString();
                    cab.tipoMoneda = _tipoMoneda;
                    cab.igv = Convert.ToDecimal(txt_igv.Text);
                    cab.isc = isc;
                    cab.otros = otrosImpuestos;
                    cab.importeTotal = Convert.ToDecimal(txt_total.Text);
                    cab.subtotal = Convert.ToDecimal(txt_subtotal.Text); ;
                    cab.estado_comprobante = CENConstantes.ID_ESTADO;

                    Console.Write(cab.fechaEmision.Date);

                    int id_comprobante = cln.GuardarBoleta(cab, listDet);

                    if (id_comprobante >= 1)
                    {
                        txtCliente.Text = "";
                        txt_documento.Text = "";
                        txtDireccionCliente.Text = "";
                        cantidad_productos.Text = "| Cantidad de productos : 0";

                        detalleProductTable.Rows.Clear();
                        txt_subtotal.Text = "";
                        txt_igv.Text = "";
                        txt_total.Text = "";
                        //llenarCodigoComprobante();

                        CENEstructuraComprobante estructura = cln.Find(id_comprobante);
                        try
                        {

                            cln.CrearArchivos(estructura, cab.tipoComprobante, serie, txt_codigo_comprobante.Text);
                            cln.GenerarArchivoTRI("1000", "IGV", "VAT", cab.subtotal.ToString(), cab.igv.ToString(), cab.tipoComprobante, serie, txt_codigo_comprobante.Text);
                            cln.crearArchivoLey("1000", cab.tipoComprobante, serie, txt_codigo_comprobante.Text);


                            DialogResult resultadoD = new DialogResult();
                            aviso aviso_form = new aviso("success", "Comprobante con código " + id_comprobante + " generado con éxito", " ¿Desea generar impresión del comprobante?", true);
                            resultadoD = aviso_form.ShowDialog();

                            if (resultadoD == DialogResult.OK)
                            {

                                DialogResult resultadoD2 = new DialogResult();
                                progress progressBar = new progress("GENERANDO PDF DE COMPROBANTE", 7);
                                resultadoD2 = progressBar.ShowDialog();

                                List<string> cabecera = new List<string>();
                                cabecera.Add(string.Format("Fecha Emisión|{0}", cab.fechaEmision));
                                cabecera.Add(string.Format("Señor(es)|{0}", cab.nombreCliente));
                                cabecera.Add(string.Format("R.U.C|{0}", cab.documentoCliente));
                                cabecera.Add(string.Format("Dirección Cliente|{0}", cab.direccionCliente));
                                cabecera.Add(string.Format("Tipo Moneda|SOLES"));
                                cabecera.Add(string.Format("N° Transacción: |{0}", id_comprobante));


                                List<string> impuestos = new List<string>();

                                impuestos.Add(string.Format("IGV: |{0}", cab.igv));
                                impuestos.Add("ISC: | 0.00");
                                impuestos.Add("OTROS: | 0.00");
                                impuestos.Add("DESC.GLOBAL: | 0.00");

                                ConvertNumberToLetter InLetter = new ConvertNumberToLetter();
                                string cantidadEnLetras = InLetter.InLetter(cab.importeTotal.ToString());

                                List<string> subtotal = new List<string>();
                                subtotal.Add(string.Format("Total Valor de venta: |{0}", cab.subtotal));
                                subtotal.Add(string.Format("Descuento Total: | 0.00"));
                                subtotal.Add("Anticipos: | 0.00");
                                subtotal.Add(string.Format("Importe Total: | {0}", cab.importeTotal));

                                List<string> body = new List<string>();

                                foreach (CENComprobanteDetalle item in listDet)
                                {
                                    body.Add(string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|", item.cantidad.ToString(),
                                    item.unidadMedida.ToString(),
                                    item.codigo.ToString()
                                    , item.descripcion.ToString()
                                    , item.precioUnitario.ToString()
                                    , item.descuentoUnitario.ToString()
                                    , item.valorItem.ToString()));
                                }

                                string nombreComprobante = CENConstantes.FACTURA_ELECTRONICA;

                                string[] NombHeaderTable = { "Cant.", "Uni. Medida", "Código", "Descripción", "P. Unitario", "Desc", "V. Item" };
                                float[] TamCeldas = { 5, 10, 10, 40, 15, 10, 10 };

                                CLNDocumentTypes cnotes = new CLNDocumentTypes();

                                cnotes.getHashXML(cab.tipoComprobante, serie, txt_codigo_comprobante.Text, id_comprobante);

                                List<string> datosHash = new List<string>();
                                datosHash.Add(Convert.ToString(cab.igv));
                                datosHash.Add(Convert.ToString(cab.subtotal));
                                datosHash.Add(Convert.ToString(cab.fechaEmision.ToString("yyyy-MM-dd")));
                                datosHash.Add(Convert.ToString(cab.tipoDocumento));
                                datosHash.Add(Convert.ToString(cab.documentoCliente));

                               Boolean estado = cln.CrearPDF(cabecera, NombHeaderTable, TamCeldas,
                                        body, impuestos, cab.tipoComprobante, serie, txt_codigo_comprobante.Text, nombreComprobante,
                                        cantidadEnLetras, subtotal, datosHash, id_comprobante
                                    );

                                if (!estado) {
                                    MessageBox.Show("NO SE PUDO IMPRIMIR COMPROBANTE, VERIFIQUE SI EL ENVÍO  DEL COMPROBANTE A SUNAT RE REALIZÓ DE MANERA CORRECTA");
                                }

                                llenarCodigoComprobante();




                            }
                            else
                            {
                                llenarCodigoComprobante();
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error al crear los archivos del comprobante");
                            Console.Write("error" + ex);
                            llenarCodigoComprobante();
                        }


                    }
                    else
                    {
                        MessageBox.Show("No se pudo guardar la factura");
                    }
                }
                
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

            DialogResult dialog = MessageBox.Show("¿ Está seguro que desea cancelar la venta ?","Confirmación",
                MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

            if (dialog == DialogResult.Yes)
            {
                //datos del cliente
                txtCliente.Text = "";
                txtDireccionCliente.Text = "";
                txt_documento.Text = "";

                //datos del producto
                txt_producto.Text = "";
                txt_codigo.Text = "";
                txt_cantidad.Text = "";
                txt_precio.Text = "";

                txt_subtotal.Text = "";
                txt_igv.Text = "";
                txt_total.Text = "";

                detalleProductTable.Rows.Clear();
            }
                
            

            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            string cp="";

            txt_codigoBoleta.Text = CENConstantes.F001 + "-" + cp;

                txt_documento.Text = "";

            if (comboBox1.SelectedValue.ToString() == "1")
            {
                cp = cln.codigoBoleta(CENConstantes.FACTURA);
                txt_documento.Enabled = true;
                txt_codigoBoleta.Text = CENConstantes.F001 + "-" + cp;
                txt_documento.MaxLength = 11;
                mensaje_documento.Text = "Cantidad de números 11";

            }
            else
            {
                cp = cln.codigoBoleta(CENConstantes.BOLETA);
                txt_documento.MaxLength = 8;
                txt_documento.Enabled = true;
                txt_codigoBoleta.Text = CENConstantes.B001 + "-" + cp;
                mensaje_documento.Text = "Cantidad de números 8";
               

            }
            txt_codigo_comprobante.Text = cp;
            txt_titulo_comprobante.Text = comboBox1.Text;
        }

        private void groupBox1_Enter_1(object sender, EventArgs e)
        {

        }



    

        private void detalleProductTable_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

            
        }

        private void detalleProductTable_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
          
        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {

        
            try {
                int indice = detalleProductTable.CurrentCell.RowIndex;


                detalleProductTable.Rows.RemoveAt(indice);

                int cantidad_prod = detalleProductTable.Rows.Count - 1;
                cantidad_productos.Text = "| Cantidad de productos: " + cantidad_prod;
                decimal subtotal = 0;
                decimal total = 0;
                decimal igv = 0;

                for (var i = 0; i < detalleProductTable.Rows.Count - 1; i++)
                {
                    subtotal = subtotal + Convert.ToDecimal(detalleProductTable.Rows[i].Cells[7].Value.ToString());
                    total = total + Convert.ToInt32(detalleProductTable.Rows[i].Cells[2].Value.ToString()) * Convert.ToDecimal(detalleProductTable.Rows[i].Cells[4].Value.ToString());
                }

                igv = decimal.Round((total / Convert.ToDecimal(1.18) * Convert.ToDecimal(0.18)), 2); ;


                txt_subtotal.Text = subtotal.ToString();
                txt_igv.Text = igv.ToString();
                txt_total.Text = total.ToString();


                txt_cantidad.Text = "";
                txt_unidadMedida.Text = "UNIDAD";
                txt_codigo.Text = "";
                txt_producto.Text = "";
                txt_precio.Text = "";
                txt_descuento.Text = "0";
                txt_cantidad.Text = "";
                txt_id_row.Text = "";
            }
            catch(Exception ex){

                MessageBox.Show("La fila seleccionada no contiene un producto");

            }

        }

        private void txtCliente_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void txtCliente_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
          
        }

        private void label22_Click(object sender, EventArgs e)
        {
          
        }

        private void panel7_Paint(object sender, PaintEventArgs e)
        {
        
        }

        private void label23_Click(object sender, EventArgs e)
        {
     
        }

        private void button4_Click(object sender, EventArgs e)
        {
      
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
        
        }

        private void button1_Click_3(object sender, EventArgs e)
        {
            
        
        
        }

        private void button1_Click_4(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lbl_descripcion_obligatorios_Click(object sender, EventArgs e)
        {

        }

        private void txt_documento_KeyDown(object sender, KeyEventArgs e)
        {
            
           
        }

        private void txt_documento_KeyUp(object sender, KeyEventArgs e)
        {
            if (txt_documento.Text.Trim().Length != 0)
            {
                mensaje_documento.Text = Convert.ToString(txt_documento.Text.Trim().Length);
            }
        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txt_codigo_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void txt_subtotal_TextChanged(object sender, EventArgs e)
        {

        }

        private void lbl_mensaje_Click(object sender, EventArgs e)
        {

        }

        private void dashboard_Shown(object sender, EventArgs e)
        {
            LlenarCombo();


            lbl_modificar.Visible = false;
            chk_editar_precio.Visible = false;

            string CodigoComprobante = cln.codigoBoleta(CENConstantes.FACTURA);
            txt_codigo_comprobante.Text = CodigoComprobante;
            txt_codigoBoleta.Text = clnConcepto.ObtenerValorParametro(3) + "-" + CodigoComprobante;

            txt_documento.MaxLength = 11;
            txt_producto.MaxLength = 40;
            txt_codigo.MaxLength = 10;
            txtDireccionCliente.MaxLength = 100;
            mensaje_documento.Text = "Cantidad de números 11";
            txt_descuento.Text = "0";

            txtCliente.Focus();
            txt_titulo_comprobante.Text = CENConstantes.FACTURA_ELECTRONICA;
            detalleProductTable.DataSource = null;

            detalleProductTable.ColumnCount = 8;
            detalleProductTable.ColumnHeadersDefaultCellStyle.BackColor = Color.Navy;
            detalleProductTable.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            detalleProductTable.ColumnHeadersDefaultCellStyle.Font =
                new Font(detalleProductTable.Font, FontStyle.Bold);

            detalleProductTable.Name = "detalleProductTable";
            detalleProductTable.Location = new Point(8, 8);
            detalleProductTable.Size = new Size(950, 250);
            detalleProductTable.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            detalleProductTable.ColumnHeadersBorderStyle =DataGridViewHeaderBorderStyle.Single;
            detalleProductTable.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            detalleProductTable.GridColor = Color.Black;
            detalleProductTable.RowHeadersVisible = false;

            detalleProductTable.Columns[0].Name = "Cod. Prod";
            detalleProductTable.Columns[1].Name = "Descripción";
            detalleProductTable.Columns[2].Name = "Cantidad";
            detalleProductTable.Columns[3].Name = "Un. Medida";
            detalleProductTable.Columns[4].Name = "Prec. Unitario";
            detalleProductTable.Columns[5].Name = "Desc.";
            detalleProductTable.Columns[6].Name = "IGV";
            detalleProductTable.Columns[7].Name = "Valor Item";
            detalleProductTable.Columns[7].DefaultCellStyle.Font = new Font(detalleProductTable.DefaultCellStyle.Font, FontStyle.Italic);
            detalleProductTable.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            detalleProductTable.MultiSelect = false;
            detalleProductTable.Dock = DockStyle.Fill;

            foreach (DataGridViewColumn Col in detalleProductTable.Columns)
            {
                Col.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
           


        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
           
        }

        private void btn_buscar_Click_1(object sender, EventArgs e)
        {
            

        }

        private void btn_link_products_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ListadoProductos list = new ListadoProductos();
            AddOwnedForm(list);
            list.Show();
        }

        private void chk_editar_precio_CheckedChanged(object sender, EventArgs e)
        {


            if (this.chk_editar_precio.Checked)
            {
                txt_precio.ReadOnly = false;
                txt_precio.Focus();
            }
            else {
                txt_precio.ReadOnly = true;
                txt_cantidad.Focus();
            }
        }

        //txt_producto.Text = prod.DESCRIPCION;
        //            txt_codigo.Text = prod.CODIGO_PRODUCTO;
        //            txt_precio.Text = prod.PRECIO.ToString();
        //            txt_unidadMedida.Text = prod.UNIDAD_MEDIDA == null ? "UNIDAD":prod.UNIDAD_MEDIDA;
        //            txt_precio.ReadOnly = true;
        //            lbl_modificar.Visible = true;
        //            chk_editar_precio.Visible = true;
    }
}
