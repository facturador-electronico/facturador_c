﻿using ferreteriaApp.CEN;
using ferreteriaApp.CLN;
using ferreteriaApp.DAO;
using ferreteriaApp.HELPERS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Xml;

namespace WebVentas.util
{
    public static class Helper
    {

        public static string ObtenerValorParametro(int flag)
        {
            try
            {
                DAOConceptos db = new DAOConceptos();
                return db.ObtenerValorParametro(flag);

            }
            catch (Exception ex)
            {

                throw ex;
            }
           
        }

        public static void GenerarArchivoPlanoFacturaBoletaCabecera(CENEstructuraComprobante comprobante, string tipocomprobante, string serie, string numero)
        {

            String datosEmpresa = ObtenerValorParametro(CENConstantes.CONST_2);

            string RUC = datosEmpresa.Split('|')[0];
            string Ruta = datosEmpresa.Split('|')[1];



            string Tipocomprobante = tipocomprobante;
            string Serie = serie;
            string Numero = numero;
            string Extension = ".CAB";

            //cambios ruta
            String Rutapass= Ruta+@"DATA\";
            string ArchPlano = Rutapass + RUC + "-" + Tipocomprobante.PadLeft(2, '0') + "-" + Serie + "-" + Numero.PadLeft(8, '0') + Extension;

            //Crear el archivo plano cabecera
            StreamWriter file = new StreamWriter(ArchPlano, false);
      
            List<string> lista  = new List<string>();
            decimal sumaImporteVenta = 0;

            //if(comprobante.TotalDescuentos>0)
            //{
            //    sumaImporteVenta = Math.Round(comprobante.TotalPrecioVenta - comprobante.TotalDescuentos, 2);
            //}
            //else
            //{
            //    sumaImporteVenta= Math.Round(comprobante.ImporteTotalVenta, 2);

            //}

            sumaImporteVenta = Math.Round(comprobante.ImporteTotalVenta, 2);

            try
            {

                lista.Add(comprobante.tipoOperacion);
                lista.Add(comprobante.Cab_Fac_Fecha);
                lista.Add(comprobante.HoraEmision);
                lista.Add(comprobante.FechaVencimiento);
                lista.Add(comprobante.CodDomicilioFiscal);
                lista.Add(comprobante.TipoDocEmpresa);
                lista.Add(comprobante.NumDocEmpresa);
                lista.Add(comprobante.NombEmpresa);
                lista.Add(comprobante.tipo_moneda);
                lista.Add(Math.Round(comprobante.SumTotalTributos,2).ToString());
                lista.Add(Math.Round(comprobante.TotalValorVenta,2).ToString());
                lista.Add(Math.Round(comprobante.TotalPrecioVenta,2).ToString());
                //lista.Add(Math.Round(comprobante.TotalDescuentos,2).ToString()); // descuentos q afectan a la base imponible
                lista.Add(Math.Round(Convert.ToDecimal(0), 2).ToString());// descuentos q noafectan a la base imponible
                lista.Add(Math.Round(comprobante.otrosCargos, 2).ToString());
                lista.Add(Math.Round(comprobante.TotalAnticipos,2).ToString());

                lista.Add(Math.Round(comprobante.ImporteTotalVenta, 2).ToString()); // cambio si existe descuento 
                //lista.Add(sumaImporteVenta.ToString());

                lista.Add(comprobante.VersionUBL);
                lista.Add(comprobante.Customizacion);


                string linea = string.Join("|", lista);
                file.WriteLine(linea);

                file.Close();


                ////Generar el archivo de detalles
                GenerarArchivoPlanoFacturaBoletaDetalle(comprobante.listaDetalle, Tipocomprobante, Serie, Numero, RUC, Ruta);




            }
            catch (Exception ex)
            {
                file.Close();
                throw ex;
            }
        }


        public static void GenerarArchivoPlanoFacturaBoletaDetalle(List<CENEstructuraDetalleComprobante> DtComprobante, string TipoComprobante, string Serie, string Numero, string RUC, string Ruta)
        {
            if (DtComprobante.Count > 0)
            {
                String Rutapass = Ruta + @"DATA\";
                string ArchPlano = Rutapass + RUC + "-" + TipoComprobante.PadLeft(2, '0') + "-" + Serie + "-" + Numero.PadLeft(8, '0') + ".DET";
                StreamWriter file = new StreamWriter(ArchPlano, false);
                try
                {
                    List<string> lista = new List<string>();

                    foreach (CENEstructuraDetalleComprobante det in DtComprobante)
                    {
                        lista.Add(String.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}|{12}|{13}|{14}|{15}|{16}|{17}|{18}|{19}|{20}|{21}|{22}|{23}|{24}|{25}|{26}|{27}|{28}|{29}|{30}|{31}|{32}|{33}|{34}|{35}|", 
                                  det.UnidadMedida
                                , det.cantidad.ToString()
                                , det.codigoProducto
                                , det.CodigoProdSunat
                                , det.Producto
                                //, det.ValorUnitario.ToString()
                                , det.precioBaseItem.ToString()

                                , det.SumTribxItem.ToString()
                                , det.CodTribIGV.ToString()
                                , det.MontoIGVItem.ToString()
                                , Math.Round(Convert.ToDecimal(det.baseImponible.ToString()), 2).ToString()  //10
                                , det.nombreTributo
                                , det.codAfecIGV
                                , det.tributoAfectacion
                                , det.tributoPorcentaje.ToString()
                                , det.codigTributoISC
                                , det.mtoISCxitem.ToString()
                                , det.BaseImpISC.ToString()
                                ,det.NomTribxItemISC
                                , det.CodTiposTributoISC
                                , det.TipoSistemaISC
                                , det.PorcImoISC
                                , det.CodTipoTribOtros
                                , det.MtoTribOTrosxItem.ToString()
                                , det.BaseImpOtroxItem.ToString()
                                , det.NomTribOtroxItem
                                , det.CodTipTribOtroxItem
                                , det.PorTribOtroXItem.ToString()
                                , det.codTriIcbper
                                , det.mtoTriIcbperItem
                                , det.ctdBolsasTriIcbperItem
                                , det.nomTributoIcbperItem
                                , det.codTipTributoIcbperItem
                                , det.mtoTriIcbperUnidad
                                , det.PrecioVtaUnitario.ToString()
                                , Math.Round(Convert.ToDecimal(det.valor_item.ToString()), 2).ToString()
                                , det.ValorRefUnitario_Gratuito.ToString()));
                    }

                    string linea = String.Join("\n", lista.ToArray());
                    file.WriteLine(linea);
                    file.Close();

                }
                catch (Exception ex)
                {
                    file.Close();
                    throw ex;

                }
            }
        }

        public static void GenerarArchivoPlanoACV(CENComprobanteCabecera comprobante, string tipocomprobante, string serie, string numero)
        {
            String datosEmpresa = ObtenerValorParametro(CENConstantes.CONST_2);

            string RUC = datosEmpresa.Split('|')[0];
            string Ruta = datosEmpresa.Split('|')[1];



            string Tipocomprobante = tipocomprobante;
            string Serie = serie;
            string Numero = numero;
            string Extension = ".ACV";
            decimal porcentaje = 0;
            decimal totalSinDsc = 0;
            decimal baseIm = 0;

            //cambios ruta
            String Rutapass = Ruta + @"DATA\";
            string ArchPlano = Rutapass + RUC + "-" + Tipocomprobante.PadLeft(2, '0') + "-" + Serie + "-" + Numero.PadLeft(8, '0') + Extension;

            //Crear el archivo plano cabecera
            StreamWriter file = new StreamWriter(ArchPlano, false);

            List<string> lista = new List<string>();
            try
            {
                totalSinDsc = Convert.ToDecimal(comprobante.importeTotal);

                //baseIm = Math.Round((totalSinDsc / Convert.ToDecimal(1.18)),2);
                //porcentaje = 1 * Convert.ToDecimal(comprobante.descuentoGlobal) / Convert.ToDecimal(comprobante.importeTotal);

                porcentaje = Convert.ToDecimal(comprobante.descuentoGlobal)/totalSinDsc ;

                //archivo plano
                lista.Add("false");                                             //tipo de variable true/false(desc)
                lista.Add(CENConstantes.CODTIPO_DESC_GLOBAL_AFECTABASE);        // codigo tipo de item 
                lista.Add(Math.Round(porcentaje,5).ToString());                               //porcerntaje de item
                lista.Add(CENConstantes.PEN);                                   //moneda de monto del item
                lista.Add(comprobante.descuentoGlobal.ToString("0.00"));              //monto del item
                lista.Add(CENConstantes.PEN);                                  //moneda de base imponible del item
                lista.Add(totalSinDsc.ToString("0.00"));                                         //base imponible del item
              

                string linea = string.Join("|", lista);
                file.WriteLine(linea);

                file.Close();
            

            }
            catch (Exception ex)
            {
                file.Close();
                throw ex;
            }
        }
        

        

        public static void GenerarArchivoTRI(string codTipoTributo, string Destiptributo,
                                                                      string codTipoTribUNEC,
                                                                      String BaseImponible, string TotalIGV,
                                                                      string TipoComprobante, string serie, string numero)
        {

            String datosEmpresa = ObtenerValorParametro(CENConstantes.CONST_2);

            string RUC = datosEmpresa.Split('|')[0];
            string Ruta = datosEmpresa.Split('|')[1];

            //cambios ruta
            String Rutapass = Ruta + @"DATA\";


            string ArchPlano = Rutapass + RUC + "-" + TipoComprobante.PadLeft(2, '0') + "-" + serie + "-" + numero + ".TRI";
            StreamWriter file = new StreamWriter(ArchPlano, false);
            decimal baseI = 0;



            try
            {
                baseI = Math.Round(Convert.ToDecimal(BaseImponible),2);

                //string linea = codTipoTributo + "|" + Destiptributo + "|" + codTipoTribUNEC + "|" + BaseImponible.ToString() + "|" + TotalIGV.ToString();
                string linea = codTipoTributo + "|" + Destiptributo + "|" + codTipoTribUNEC + "|" + baseI.ToString() + "|" + TotalIGV.ToString();
                file.Write(linea);
                file.Close();
            }
            catch (Exception ex)
            {
                file.Close();
                throw ex;
            }
        }



        public static void crearArchivoLey(string number, string TipoComprobante, string Serie, string Numero) {

            ConvertNumberToLetter InLetter = new ConvertNumberToLetter();
            string cantidadTexto =  InLetter.InLetter(number);
            string datosEmpresa = ObtenerValorParametro(CENConstantes.CONST_2);

            string RUC = datosEmpresa.Split('|')[0];
            string Ruta = datosEmpresa.Split('|')[1];

            String Rutapass = Ruta + @"DATA\";
            string ArchPlano = Rutapass + RUC + "-" + TipoComprobante.PadLeft(2, '0') + "-" + Serie + "-" + Numero.PadLeft(8, '0') + ".LEY";
            
            StreamWriter file = new StreamWriter(ArchPlano, false);

            List<string> lista = new List<string>();

            try {

                lista.Add(number.ToString(CultureInfo.CreateSpecificCulture("en-GB")));
                lista.Add(cantidadTexto);
                string linea = String.Join("|", lista.ToArray());
                file.WriteLine(linea);
                file.Close();

            }
            catch (Exception ex) { 
            
                file.Close();
                throw ex;
            }
        }



        //AGREGADO

        public static void GenerarArchivoPlanoNotasCabecera(CENEstructuraComprobante comprobante, string tipocomprobante, string serie, string numero, String codigo_tipo_nota, String descripcion_nota)
        {
            //jc
            String identity_document_customer;
            if (comprobante.docCustomer.Length == 11)
            {
                identity_document_customer = "6";
            }
            else
            {
                identity_document_customer = "1";
            }
            //end

            String datosEmpresa = ObtenerValorParametro(CENConstantes.CONST_2);

            string RUC = datosEmpresa.Split('|')[0];
            string Ruta = datosEmpresa.Split('|')[1];



            string Tipocomprobante = tipocomprobante;
            string Serie = serie;
            string Numero = numero;
            string Extension = ".NOT";

            String Rutapass = Ruta + @"DATA\";
            string ArchPlano = Rutapass + RUC + "-" + Tipocomprobante.PadLeft(2, '0') + "-" + Serie + "-" + Numero.PadLeft(8, '0') + Extension;

            //Crear el archivo plano cabecera
            StreamWriter file = new StreamWriter(ArchPlano, false);

            List<string> lista = new List<string>();
            try
            {

                lista.Add(comprobante.tipoOperacion); //tipo de operacion
                lista.Add(comprobante.Cab_Fac_Fecha); // fecha de emision
                lista.Add(comprobante.HoraEmision); //hora de emision
                lista.Add(comprobante.CodDomicilioFiscal);
                lista.Add(identity_document_customer); // tipo de documento de identidad de usuario
                lista.Add(comprobante.docCustomer); //numero de documento de cliente
                lista.Add(comprobante.nomCustomer); // nombres de clientes
                lista.Add(comprobante.tipo_moneda);

                lista.Add(codigo_tipo_nota); // codigo tipo nota
                lista.Add(descripcion_nota); // descripcion tipo de nota
                //luego cambiar typeDocCustomer x lo que deberia ir
                lista.Add(comprobante.tipo_comprobante); // tipo de documento q se modifica (01,03,12)
                lista.Add(comprobante.serieDocumento); // serie y numero que se modifica 

                float sumatoriaOtrosCargos = 0; // cambiar luego


                lista.Add(Math.Round(comprobante.SumTotalTributos, 2).ToString(CultureInfo.CreateSpecificCulture("en-GB")));
                lista.Add(Math.Round(comprobante.TotalValorVenta, 2).ToString(CultureInfo.CreateSpecificCulture("en-GB")));
                lista.Add(Math.Round(comprobante.TotalPrecioVenta, 2).ToString(CultureInfo.CreateSpecificCulture("en-GB")));
                lista.Add(Math.Round(comprobante.TotalDescuentos, 2).ToString(CultureInfo.CreateSpecificCulture("en-GB")));
                lista.Add(Math.Round(sumatoriaOtrosCargos, 2).ToString(CultureInfo.CreateSpecificCulture("en-GB")));
                lista.Add(Math.Round(comprobante.TotalAnticipos, 2).ToString(CultureInfo.CreateSpecificCulture("en-GB")));
                lista.Add(Math.Round(comprobante.ImporteTotalVenta, 2).ToString(CultureInfo.CreateSpecificCulture("en-GB")));
                lista.Add(comprobante.VersionUBL);
                lista.Add(comprobante.Customizacion);


                string linea = string.Join("|", lista);
                file.WriteLine(linea);

                file.Close();
                ////Generar el archivo de detalles
                //GenerarArchivoPlanoFacturaBoletaDetalle(comprobante.listaDetalle, Tipocomprobante, Serie, Numero, RUC, Ruta);

                //descuento global
                if (comprobante.descuento_global > 0)
                {
                    CENEstructuraComprobante comprobanteConDsc = null;

                    comprobanteConDsc = new CENEstructuraComprobante();

                    comprobanteConDsc.tipoOperacion = comprobante.tipoOperacion;
                    comprobanteConDsc.Cab_Fac_Fecha = comprobante.Cab_Fac_Fecha;
                    comprobanteConDsc.HoraEmision = comprobante.HoraEmision;
                    comprobanteConDsc.FechaVencimiento = comprobante.FechaVencimiento;
                    comprobanteConDsc.CodDomicilioFiscal = comprobante.CodDomicilioFiscal;
                    comprobanteConDsc.TipoDocEmpresa = comprobante.TipoDocEmpresa;
                    //cambios

                    comprobanteConDsc.NumDocEmpresa = comprobante.NumDocEmpresa;
                    comprobanteConDsc.NombEmpresa = comprobante.NombEmpresa;
                    //end

                    comprobanteConDsc.tipo_moneda = comprobante.tipo_moneda;
                    comprobanteConDsc.SumTotalTributos = comprobante.SumTotalTributos;
                    comprobanteConDsc.TotalValorVenta = comprobante.TotalValorVenta;
                    comprobanteConDsc.TotalPrecioVenta = comprobante.TotalPrecioVenta;
                    comprobanteConDsc.TotalDescuentos = comprobante.TotalDescuentos;
                    comprobanteConDsc.otrosCargos = comprobante.otrosCargos;
                    comprobanteConDsc.TotalAnticipos = comprobante.TotalAnticipos;
                    comprobanteConDsc.ImporteTotalVenta = comprobante.ImporteTotalVenta;
                    comprobanteConDsc.VersionUBL = comprobante.VersionUBL;
                    comprobanteConDsc.Customizacion = comprobante.Customizacion;

                    ////agregado
                    comprobanteConDsc.nomCustomer = comprobante.nomCustomer;
                    comprobanteConDsc.docCustomer = comprobante.docCustomer;
                    comprobanteConDsc.typeDocCustomer = comprobante.typeDocCustomer;
                    comprobanteConDsc.serieDocumento = comprobante.serieDocumento;
                    comprobanteConDsc.direccionCustomer = comprobante.direccionCustomer;
                    comprobanteConDsc.tipo_comprobante = comprobante.tipo_comprobante;

                    comprobanteConDsc.descuento_global = comprobante.descuento_global;
                    comprobanteConDsc.descuento_total = comprobante.descuento_total;


                    List<CENEstructuraDetalleComprobante> detalle = new List<CENEstructuraDetalleComprobante>();
                    CENEstructuraDetalleComprobante cenDetalle;

                  
                    int conteoItems = comprobante.listaDetalle.Count();
                    decimal descuentoTotal= Math.Round((comprobante.descuento_global * Convert.ToDecimal(1.18)), 2);
                    decimal porcentajeDct = Math.Round(((comprobante.ImporteTotalVenta / (comprobante.ImporteTotalVenta + descuentoTotal))), 6);

                    foreach (var dr in comprobante.listaDetalle)
                    {

                        decimal precioVtaUnitario = 0, precioBaseItem = 0, montoIgvItem = 0 , sumaTributosItem=0 , valorUnitario=0, valorItem=0,baseImponible=0;
                        precioVtaUnitario = Math.Round((dr.PrecioVtaUnitario * porcentajeDct), 6); //  2 DECIMALES
                        precioBaseItem = Math.Round((precioVtaUnitario / Convert.ToDecimal(1.18)), 6); //

                        montoIgvItem = Math.Round(((precioVtaUnitario * dr.cantidad) / Convert.ToDecimal(1.18) * Convert.ToDecimal(0.18)),6); // 2 DECIMALES
                        sumaTributosItem = Math.Round(((precioVtaUnitario * dr.cantidad) / Convert.ToDecimal(1.18) * Convert.ToDecimal(0.18)), 6); // 2 DECIMALES
                         
                        valorUnitario = Math.Round((precioVtaUnitario * dr.cantidad / Convert.ToDecimal(1.18)), 6); //
                        valorItem = Math.Round((precioVtaUnitario * dr.cantidad / Convert.ToDecimal(1.18)), 6);
                        baseImponible = Math.Round((precioVtaUnitario * dr.cantidad / Convert.ToDecimal(1.18)), 6); //



                        cenDetalle = new CENEstructuraDetalleComprobante();

                        cenDetalle.UnidadMedida = dr.UnidadMedida;
                        cenDetalle.cantidad = dr.cantidad;
                        cenDetalle.codigoProducto = dr.codigoProducto;
                        cenDetalle.CodigoProdSunat = dr.CodigoProdSunat;
                        cenDetalle.Producto = dr.Producto;
                        //cenDetalle.ValorUnitario = dr.ValorUnitario; //
                        cenDetalle.ValorUnitario = valorUnitario; 

                        //cenDetalle.SumTribxItem = dr.SumTribxItem; //
                        cenDetalle.SumTribxItem = Math.Round(sumaTributosItem,2); //

                        cenDetalle.CodTribIGV = dr.CodTribIGV;  

                        //cenDetalle.MontoIGVItem = dr.MontoIGVItem; //
                        cenDetalle.MontoIGVItem = Math.Round(montoIgvItem, 2) ; //
                        

                        //cenDetalle.baseImponible = dr.baseImponible; //
                        cenDetalle.baseImponible = baseImponible; //
                        

                        cenDetalle.nombreTributo = dr.nombreTributo;
                        cenDetalle.codAfecIGV = dr.codAfecIGV;
                        cenDetalle.tributoAfectacion = dr.tributoAfectacion;
                        cenDetalle.tributoPorcentaje = dr.tributoPorcentaje;
                        cenDetalle.codigTributoISC = dr.codigTributoISC;
                        cenDetalle.mtoISCxitem = dr.mtoISCxitem;
                        cenDetalle.BaseImpISC = dr.BaseImpISC;
                        cenDetalle.NomTribxItemISC = dr.NomTribxItemISC;
                        cenDetalle.CodTiposTributoISC = dr.CodTiposTributoISC;
                        cenDetalle.TipoSistemaISC = dr.TipoSistemaISC;
                        cenDetalle.PorcImoISC = dr.PorcImoISC;
                        cenDetalle.CodTipoTribOtros = dr.CodTipoTribOtros;
                        cenDetalle.MtoTribOTrosxItem = dr.MtoTribOTrosxItem;
                        cenDetalle.BaseImpOtroxItem = dr.BaseImpOtroxItem;
                        cenDetalle.NomTribOtroxItem = dr.NomTribOtroxItem;
                        cenDetalle.CodTipTribOtroxItem = dr.CodTipTribOtroxItem;
                        cenDetalle.PorTribOtroXItem = dr.PorTribOtroXItem;
                        cenDetalle.codTriIcbper = dr.codTriIcbper;
                        cenDetalle.mtoTriIcbperItem = dr.mtoTriIcbperItem;
                        cenDetalle.ctdBolsasTriIcbperItem = dr.ctdBolsasTriIcbperItem;
                        cenDetalle.nomTributoIcbperItem = dr.nomTributoIcbperItem;
                        cenDetalle.codTipTributoIcbperItem = dr.codTipTributoIcbperItem;
                        cenDetalle.mtoTriIcbperUnidad = dr.mtoTriIcbperUnidad;

                        //cenDetalle.PrecioVtaUnitario = dr.PrecioVtaUnitario;
                        cenDetalle.PrecioVtaUnitario = Math.Round(precioVtaUnitario, 2) ;
                        

                        //cenDetalle.valor_item = dr.valor_item;
                        cenDetalle.valor_item = valorItem;


                        cenDetalle.ValorRefUnitario_Gratuito = dr.ValorRefUnitario_Gratuito;

                        //cenDetalle.precioBaseItem = dr.precioBaseItem;
                        cenDetalle.precioBaseItem = precioBaseItem;
                        

                        cenDetalle.unidadMedidaNota = dr.unidadMedidaNota;
                        cenDetalle.codigoProductoNota = dr.codigoProductoNota;

                        detalle.Add(cenDetalle);
                    }

                    comprobanteConDsc.listaDetalle = detalle;

                    //Generar el archivo de detalles
                    GenerarArchivoPlanoFacturaBoletaDetalle(comprobanteConDsc.listaDetalle, Tipocomprobante, Serie, Numero, RUC, Ruta);
                }
                else
                {
                    //Generar el archivo de detalles
                    GenerarArchivoPlanoFacturaBoletaDetalle(comprobante.listaDetalle, Tipocomprobante, Serie, Numero, RUC, Ruta);
                }

            }
            catch (Exception ex)
            {
                file.Close();
                throw ex;
            }
        }



        public static void crearArchivoPag( string TipoComprobante, string Serie, string Numero, decimal total)
        {

            ConvertNumberToLetter InLetter = new ConvertNumberToLetter();
        
            string datosEmpresa = ObtenerValorParametro(CENConstantes.CONST_2);

            string RUC = datosEmpresa.Split('|')[0];
            string Ruta = datosEmpresa.Split('|')[1];

           
            String Rutapass = Ruta + @"DATA\";
            string ArchPlano = Rutapass + RUC + "-" + TipoComprobante.PadLeft(2, '0') + "-" + Serie + "-" + Numero.PadLeft(8, '0') + ".PAG";

            StreamWriter file = new StreamWriter(ArchPlano, false);
            try
            {
                string linea = "Contado|"+ total + "|PEN";
                file.WriteLine(linea);
                file.Close();
            }
            catch (Exception ex)
            {
                file.Close();
                throw ex;
            }
        }


        //ARCHIVOS PLANOS PARA RESUMEN DIARIO DE 1 BOLETA

        public static int GenerarArchivoPlanoResumenDiarioCabecera(CENEstructuraComprobante comprobante, string tipocomprobante, string serie, string numero, int id)
        {

            String datosEmpresa = ObtenerValorParametro(CENConstantes.CONST_2);

            string RUC = datosEmpresa.Split('|')[0];
            string Ruta = datosEmpresa.Split('|')[1];
          
            string Extension = ".RDI";
            string ExtensionTRD = ".TRD";
            //GUARDAMOS EL RESUMEN EN BD

            CLNComprobante clnComp = new CLNComprobante();

            int contadorIdent= clnComp.buscarResumenCorrelativo(DateTime.Now.ToString("yyyy-MM-dd"));
            int correlativo = contadorIdent + 1;

            string nombre_archivo = RUC + "-RC-" + DateTime.Now.ToString("yyyyMMdd") + "-" + string.Format("{0:000}", correlativo);
            CENResumen dataResumen = new CENResumen();

            dataResumen.estado_id = CENConstantes.ID_ESTADO_REGISTRADO;
            dataResumen.tipo_estado = "1";  //1 adicionar, 2 modificar, 3 anular
            dataResumen.fecha_emision = DateTime.Now.ToString("yyyy-MM-dd");
            dataResumen.fecha_referencia = comprobante.Cab_Fac_Fecha;
            dataResumen.identificador = string.Format("{0:000}", correlativo);
            dataResumen.nombre_archivo = nombre_archivo;
            dataResumen.ticket = "";


            CENDetalleResumen det;
            List<CENDetalleResumen> listDet = new List<CENDetalleResumen>();
            det = new CENDetalleResumen();

            det.documento_id = id;
            listDet.Add(det);

            int idResumen = clnComp.insertarResumenDiario(dataResumen, listDet);


            //cambios ruta
            String Rutapass = Ruta + @"DATA\";
            string ArchPlano = Rutapass + nombre_archivo + Extension;

            //Crear el archivo plano cabecera
            StreamWriter file = new StreamWriter(ArchPlano, false);

            string ArchPlanoD = Rutapass + nombre_archivo + ExtensionTRD;
            StreamWriter fileD = new StreamWriter(ArchPlanoD, false);

            List<string> lista = new List<string>();

            decimal TotalValorVenta = 0;
            decimal TotalPrecioVenta = 0;
            decimal SumTotalTributos = 0;

            string _15 = "";
            string _16 = "";
            string _17 = "";
            CENDocAfectadoNota listaDocAfN;
            

            try
            {

                if(tipocomprobante == "07")
                {
                    TotalValorVenta  = comprobante.TotalValorVenta * -1 ;
                    TotalPrecioVenta = comprobante.TotalPrecioVenta * -1;
                    SumTotalTributos = comprobante.SumTotalTributos * -1;

                    CLNDocumentTypes clnDT = new CLNDocumentTypes();
                    listaDocAfN = clnDT.getDocAfectadoNota(id);
                    string[] series = listaDocAfN.codigo.Split('-');

                    _15 = "03";
                    _16 = series[0];
                    _17 = series[1];
                }
                else
                {
                    TotalValorVenta = comprobante.TotalValorVenta;
                    TotalPrecioVenta = comprobante.TotalPrecioVenta;
                    SumTotalTributos = comprobante.SumTotalTributos;

                    _15 = "";
                    _16 = "";
                    _17 = "";
                }

               

                lista.Add(dataResumen.fecha_referencia);
                lista.Add(dataResumen.fecha_emision);
              
                lista.Add(tipocomprobante);
                lista.Add(serie+"-"+numero);
                lista.Add(comprobante.typeDocCustomer);
                lista.Add(comprobante.docCustomer);
                lista.Add(comprobante.tipo_moneda);
                lista.Add(Math.Round(TotalValorVenta, 2).ToString());
                lista.Add("0.00");
                lista.Add("0.00");
                lista.Add("0.00");
                lista.Add("0.00");
                lista.Add("0.00");
                lista.Add(Math.Round(TotalPrecioVenta, 2).ToString());
                lista.Add(_15); //7 TIPO DOC
                lista.Add(_16); //7 SERIE
                lista.Add(_17); //7 NUMERO
                lista.Add("");
                lista.Add("");
                lista.Add("0");
                lista.Add("0");
                lista.Add("0");
            
                lista.Add(dataResumen.tipo_estado);

                lista.Add("");
                string linea = string.Join("|", lista);
                file.WriteLine(linea);

                file.Close();


                //Generar el archivo de detalles
             
                List<string> listaD = new List<string>();
                listaD.Add("1");
                listaD.Add("1000");
                listaD.Add("IGV");
                listaD.Add("VAT");
                listaD.Add(Math.Round(TotalValorVenta, 2).ToString());
                listaD.Add(Math.Round(SumTotalTributos, 2).ToString());
                lista.Add("");

                string lineaD = string.Join("|", listaD);
                fileD.WriteLine(lineaD);


                fileD.Close();



                //GenerarArchivoPlanoFacturaBoletaDetalle(comprobante.listaDetalle, Tipocomprobante, Serie, Numero, RUC, Ruta);

                return idResumen;

            }
            catch (Exception ex)
            {
                file.Close();
                fileD.Close();
                throw ex;
            }
        }


        public static IDictionary ReadDictionaryFile(string fileName)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            foreach (string line in File.ReadAllLines(fileName))
            {
                if ((!string.IsNullOrEmpty(line)) &&
                    (!line.StartsWith(";")) &&
                    (!line.StartsWith("#")) &&
                    (!line.StartsWith("'")) &&
                    (line.Contains('=')))
                {
                    int index = line.IndexOf('=');
                    string key = line.Substring(0, index).Trim();
                    string value = line.Substring(index + 1).Trim();

                    if ((value.StartsWith("\"") && value.EndsWith("\"")) ||
                        (value.StartsWith("'") && value.EndsWith("'")))
                    {
                        value = value.Substring(1, value.Length - 2);
                    }
                    dictionary.Add(key, value);
                }
            }

            return dictionary;
        }


        public static CENResponseResumen verificarCdrResumenDiario(string ArchivoCDR, string fileXml)
        {
            CENResponseResumen response = new CENResponseResumen();
            response.code = "";
            response.descripcion = "";
            response.observacion = new List<string>();
            //var rsCode, rsDsc, rsObs;
            using (ZipArchive zfile = ZipFile.Open(ArchivoCDR, ZipArchiveMode.Read))
            {
                ZipArchiveEntry zentry = zfile.GetEntry("R-"+fileXml);
                XmlDocument xDoc = new XmlDocument();
                Stream stream;
                stream = zentry.Open();
                xDoc.Load(stream);
                XmlNodeList xmlCode = xDoc.GetElementsByTagName("cbc:ResponseCode");
                foreach (XmlElement item in xmlCode)
                {
                    response.code = item.InnerText;
                }
                XmlNodeList xmlDsc = xDoc.GetElementsByTagName("cbc:Description");
                foreach (XmlElement item in xmlDsc)
                {
                    response.descripcion = item.InnerText;
                }



                XmlNodeList xmlObs = xDoc.GetElementsByTagName("cbc:Note");

                foreach (XmlElement item in xmlObs)
                {
                    response.observacion.Add(item.InnerText);

                }


                return response;

            }
        }

        public static void CreateIfMissing(string path)
        {
            try
            {
                bool folderExists = Directory.Exists(path);
                if (!folderExists) Directory.CreateDirectory(path);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
             
                throw ex;

            }

        }
        public static void moveArchivo(string rutaA, string rutaB)
        {
            try
            {
                if (File.Exists(rutaA))
                {
                    File.Move(rutaA, rutaB);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public static void eliminarArchivo(string rutaA)
        {
            try
            {
                if (File.Exists(rutaA))
                {
                    File.Delete(rutaA);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }



    }

}



