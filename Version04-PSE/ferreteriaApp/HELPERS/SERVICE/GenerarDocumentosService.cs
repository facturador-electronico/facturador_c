﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using crypto;



//using ferreteriaApp.HELPERS.SERVICE.MODEL;

//namespace ferreteriaApp.HELPERS.SERVICE
//{
//    public class GenerarDocumentosService : IGenerarDocumentoService 
//    {

//    //    public virtual string validaCertificado(string input, string numRuc, string passPrivateKey)
//    //    {
//    //        string validacion = "", alias = "", cadena = "";
//    //        string cadenaValidar = numRuc;
//    //        bool? certificadoCorrecto = false;
//    //        Certificate cf = null;
//    //        FileStream fis = new FileStream(input, FileMode.Open, FileAccess.Read);
//    //        KeyStore ks = KeyStore.getInstance("PKCS12");
//    //        ks.load(fis, passPrivateKey.ToCharArray());
//    //        for (IEnumerator<string> e = ks.aliases(); e.MoveNext();)
//    //        {
//    //            if (!certificadoCorrecto.Value)
//    //            {
//    //                alias = (string)e.Current;
//    //                if (ks.isKeyEntry(alias))
//    //                {
//    //                    cf = ks.getCertificate(alias);
//    //                    cadena = cf.ToString();

//    //                    if (cadena.IndexOf(cadenaValidar, StringComparison.Ordinal) > 0)
//    //                    {
//    //                        certificadoCorrecto = true;
//    //                    }
//    //                }
//    //            }
//    //            else
//    //            {
//    //                break;
//    //            }
//    //        }

//    //        /*
//			 // String cadenaValidar = numRuc;
//			 // String cadena = cf.toString();
//			 //*/

//    //        /*if(cadena.indexOf(cadenaValidar) == -1 )*/
//    //        if (!certificadoCorrecto.Value)
//    //        {
//    //            validacion = "El propietario del certificado no es el RUC " + numRuc;
//    //        }
//    //        else
//    //        {
//    //            validacion = "[ALIAS]:" + alias;
//    //        }

//    //        return validacion;
//    //    }




//        public virtual void formatoPlantillaXml(string tipoDocumento, string[] archivos, string nombreArchivo)
//        {
//            IDictionary<string, object> root = null;
//            string plantillaSeleccionada = "";

//            // Formato de Resumen de Bajas
//            //if (CONSTANTE_TIPO_DOCUMENTO_RBAJAS.Equals(tipoDocumento))
//            //{
//            //	root = formatoResumenBajas(archivos[0], nombreArchivo);
//            //	plantillaSeleccionada = "ConvertirRBajasXML.ftl";
//            //}

//            // Formato de Facturas
//            if (Constante.CONSTANTE_TIPO_DOCUMENTO_FACTURA.Equals(tipoDocumento) || Constante.CONSTANTE_TIPO_DOCUMENTO_BOLETA.Equals(tipoDocumento))
//            {
//                root = formatoFactura(archivos[0], archivos[1], archivos[2], archivos[3], archivos[4], archivos[5], nombreArchivo);
//                plantillaSeleccionada = "ConvertirFacturaXML.ftl";
//            }

//            //if (CONSTANTE_TIPO_DOCUMENTO_NCREDITO.Equals(tipoDocumento))
//            //{
//            //	root = formatoNotaCredito(archivos[0], archivos[1], archivos[2], archivos[3], archivos[4], archivos[5], nombreArchivo);
//            //	plantillaSeleccionada = "ConvertirNCreditoXML.ftl";
//            //}
//            //if (CONSTANTE_TIPO_DOCUMENTO_NDEBITO.Equals(tipoDocumento))
//            //{
//            //	root = formatoNotaDebito(archivos[0], archivos[1], archivos[2], archivos[3], archivos[4], archivos[5], nombreArchivo);
//            //	plantillaSeleccionada = "ConvertirNDebitoXML.ftl";
//            //}


//            File archivoFTL = new File(comunesService.obtenerRutaTrabajo(Constante.CONSTANTE_FORMATO), plantillaSeleccionada);

//            if (!archivoFTL.exists())
//            {
//                throw new Exception("No existe la plantilla para el tipo documento a generar XML (Archivo FTL).");
//            }


//            Configuration cfg = new Configuration();
//            cfg.DirectoryForTemplateLoading = new File(comunesService.obtenerRutaTrabajo(Constante.CONSTANTE_FORMATO));
//            cfg.DefaultEncoding = "ISO8859_1";
//            cfg.Locale = Locale.US;
//            cfg.TemplateExceptionHandler = TemplateExceptionHandler.RETHROW_HANDLER;

//            Template temp = cfg.getTemplate(plantillaSeleccionada);
//            StringBuilder rutaSalida = new StringBuilder();
//            rutaSalida.Length = 0;
//            rutaSalida.Append(comunesService.obtenerRutaTrabajo(Constante.CONSTANTE_TEMP)).Append(nombreArchivo).Append(".xml");
//            Stream outputStream = new FileStream(rutaSalida.ToString(), FileMode.Create, FileAccess.Write);
//            Writer @out = new StreamWriter(outputStream);
//            temp.process(root, @out);
//            outputStream.close();

//        }



//        private IDictionary<string, object> formatoFactura(string archivoCabecera, string archivoDetalle, string archivoRelacionado, string archivoAdiCabecera, string archivoAdiDetalle, string archivoLeyendas, string nombreArchivo)
//        {
//            string cadena = "", nombreComercial = "", ubigeo = "", direccion = "", param = "", numRuc = "", razonSocial = "";
//            int? activarCabecera = 0, activarDetalle = 0;
//            string[] registro;
//            /* Cargando PArametros de Firma */
//            string identificadorFirmaSwf = "SIGN";
//            Random calcularRnd = new Random();
//            int? codigoFacturadorSwf = (int)(calcularRnd.NextDouble() * 1000000);

//            /* Cargando Parametros del Facturador */
//            TxxxyBean paramtroBean = new TxxxyBean();
//            paramtroBean = new TxxxyBean();
//            paramtroBean.Id_para = "PARASIST";
//            IList<TxxxyBean> listaParametros = txxxyDAO.consultarParametroById(paramtroBean);
//            if (listaParametros.Count > 0)
//            {
//                foreach (TxxxyBean parametro in listaParametros)
//                {
//                    param = parametro.Val_para;
//                    if ("NUMRUC".Equals(parametro.Cod_para))
//                    {
//                        numRuc = param;
//                    }
//                    if ("RAZON".Equals(parametro.Cod_para))
//                    {
//                        razonSocial = param;
//                    }
//                    if ("NOMCOM".Equals(parametro.Cod_para))
//                    {
//                        nombreComercial = param;
//                    }
//                    if ("UBIGEO".Equals(parametro.Cod_para))
//                    {
//                        ubigeo = param;
//                    }
//                    if ("DIRECC".Equals(parametro.Cod_para))
//                    {
//                        direccion = param;
//                    }
//                }
//            }

//            /* Validar que Archivos serán procesados */
//            File fileArchivoCabecera = new File(archivoCabecera);
//            if (fileArchivoCabecera.exists())
//            {
//                activarCabecera = 1;
//            }

//            File fileArchivoDetalle = new File(archivoDetalle);
//            if (fileArchivoDetalle.exists())
//            {
//                activarDetalle = 1;
//            }

//            /* Obtener Datos de Archivo */
//            string[] datosArchivo = nombreArchivo.Split('-');





//            /* Leyendo Archivo de Cabecera del Comprobante */
//            IDictionary<string, object> factura = null;
//            if (activarCabecera == 1)
//            {

//                StreamReader fArchivoCabecera = new StreamReader(archivoCabecera);
//                StreamReader bArchivoCabereca = new StreamReader(fArchivoCabecera);

//                while ((cadena = bArchivoCabereca.ReadLine()) != null)
//                {
//                    registro = cadena.Split('|');

//                    if (registro.Length != 17)
//                    {
//                        bArchivoCabereca.close();
//                        throw new Exception("El archivo cabecera no continene la cantidad de datos esperada (17 columnas).");
//                    }
//                    else
//                    {
//                        // Desde Archivo Txt
//                        factura = new Dictionary<string, object>();
//                        factura["tipoOperacion"] = registro[0]; // NUEVO
//                        factura["fechaEmision"] = registro[1];
//                        factura["direccionUsuario"] = registro[2]; // NUEVO
//                        factura["tipoDocumento"] = registro[3];
//                        factura["nroDocumento"] = registro[4];
//                        factura["razonSocialUsuario"] = registro[5];
//                        factura["moneda"] = registro[6];
//                        factura["descuentoGlobal"] = registro[7]; // CAMBIO
//                        factura["sumaOtrosCargos"] = registro[8];
//                        factura["totalDescuento"] = registro[9]; // CAMBIO
//                        factura["montoOperGravadas"] = registro[10];
//                        factura["montoOperInafectas"] = registro[11];
//                        factura["montoOperExoneradas"] = registro[12];
//                        factura["sumaIgv"] = registro[13];
//                        factura["sumaIsc"] = registro[14];
//                        factura["sumaOtros"] = registro[15];
//                        factura["sumaImporteVenta"] = registro[16];

//                        // Valores Automaticos
//                        factura["ublVersionIdSwf"] = Constante.CONSTANTE_UBL_VERSION;
//                        factura["CustomizationIdSwf"] = Constante.CONSTANTE_CUSTOMIZATION_ID;
//                        factura["nroCdpSwf"] = datosArchivo[2] + "-" + datosArchivo[3]; // NUEVO
//                        factura["tipCdpSwf"] = datosArchivo[1]; // NUEVO
//                        factura["nroRucEmisorSwf"] = numRuc;
//                        factura["tipDocuEmisorSwf"] = Constante.CONSTANTE_TIPO_DOCU_EMISOR;
//                        factura["nombreComercialSwf"] = nombreComercial;
//                        factura["razonSocialSwf"] = razonSocial;
//                        factura["ubigeoDomFiscalSwf"] = ubigeo;
//                        factura["direccionDomFiscalSwf"] = direccion;
//                        factura["paisDomFiscalSwf"] = Constante.CONSTANTE_CODIGO_PAIS;
//                        factura["codigoMontoDescuentosSwf"] = Constante.CONSTANTE_CODIGO_MONTO_DSCTO;
//                        factura["codigoMontoOperGravadasSwf"] = Constante.CONSTANTE_CODIGO_OPER_GRAVADA;
//                        factura["codigoMontoOperInafectasSwf"] = Constante.CONSTANTE_CODIGO_OPER_INAFECTA;
//                        factura["codigoMontoOperExoneradasSwf"] = Constante.CONSTANTE_CODIGO_OPER_EXONERADA;
//                        factura["idIgv"] = Constante.CONSTANTE_ID_IVG;
//                        factura["codIgv"] = Constante.CONSTANTE_COD_IVG;
//                        factura["codExtIgv"] = Constante.CONSTANTE_COD_EXT_IVG;
//                        factura["idIsc"] = Constante.CONSTANTE_ID_ISC;
//                        factura["codIsc"] = Constante.CONSTANTE_COD_ISC;
//                        factura["codExtIsc"] = Constante.CONSTANTE_COD_EXT_ISC;
//                        factura["idOtr"] = Constante.CONSTANTE_ID_OTR;
//                        factura["codOtr"] = Constante.CONSTANTE_COD_OTR;
//                        factura["codExtOtr"] = Constante.CONSTANTE_COD_EXT_OTR;
//                        factura["tipoCodigoMonedaSwf"] = Constante.CONSTANTE_TIPO_CODIGO_MONEDA_ONEROSO;
//                        factura["identificadorFacturadorSwf"] = Constante.CONSTANTE_INFO_SFS_SUNAT + Constante.CONSTANTE_VERSION_SFS;
//                        factura["codigoFacturadorSwf"] = codigoFacturadorSwf.ToString();
//                        factura["identificadorFirmaSwf"] = identificadorFirmaSwf;

//                    }
//                }
//                bArchivoCabereca.Close();

//            }
//            else
//            {

//                throw new Exception("No Existe el formato de la cabecera del Comprobante de Pago.");

//            }



//            /* Leyendo Archivo de Detalle del Comprobante */
//            IList<IDictionary<string, object>> listaDetalle = new List<IDictionary<string, object>>();
//            IDictionary<string, object> detalle = null;

//            if (activarDetalle == 1)
//            {
//                int? linea = 0;
//                StreamReader fArchivoDetalle = new StreamReader(archivoDetalle);
//                StreamReader bArchivoDetalle = new StreamReader(fArchivoDetalle);

//                /* Abrir Archivo de Detalle en caso exista*/
//                while ((cadena = bArchivoDetalle.ReadLine()) != null)
//                {
//                    registro = cadena.Split("\\|");
//                    if (registro.Length != 13)
//                    {
//                        bArchivoDetalle.Close();
//                        throw new Exception("El archivo detalle no continene la cantidad de datos esperada (13 columnas).");
//                    }
//                    else
//                    {
//                        // Desde Archivo Txt
//                        linea = linea + 1;
//                        detalle = new Dictionary<string, object>();
//                        detalle["unidadMedida"] = registro[0];
//                        detalle["cantItem"] = registro[1];
//                        detalle["codiProducto"] = registro[2];
//                        detalle["codiSunat"] = registro[3];
//                        detalle["desItem"] = registro[4];
//                        detalle["valorUnitario"] = registro[5];
//                        detalle["descuentoItem"] = registro[6];
//                        detalle["montoIgvItem"] = registro[7];
//                        detalle["afectaIgvItem"] = registro[8];
//                        detalle["montoIscItem"] = registro[9];
//                        detalle["tipoSistemaIsc"] = registro[10];
//                        detalle["precioVentaUnitarioItem"] = registro[11];
//                        detalle["valorVentaItem"] = registro[12];
//                        // Valores Automaticos
//                        detalle["lineaSwf"] = linea;
//                        detalle["tipoCodiMoneGratiSwf"] = Constante.CONSTANTE_TIPO_CODIGO_MONEDA_GRATUITO;

//                        listaDetalle.Add(detalle);
//                    }
//                }
//                bArchivoDetalle.close();
//                factura.put("listaDetalle", listaDetalle);

//                /* Agregar informacion adicional */
//                formatoComunes(factura, archivoRelacionado, archivoAdiCabecera, archivoAdiDetalle, archivoLeyendas);


//            }
//            else
//            {

//                throw new Exception("No Existe el formato del detalle del Comprobante de Pago.");

//            }

//            return factura;







//        }




//    }
//}
