﻿
using ferreteriaApp.CEN;
using ferreteriaApp.DAO;
using System;

using System.IO;
using System.Xml;
using ferreteriaApp.CLN;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;
using WebVentas.util;
using System.Linq;
using System.Web;
using System.Globalization;

namespace ferreteriaApp.HELPERS
{
    public static class funciones
    {


        public static string ObtenerValorParametro(int flag)
        {
            try
            {
                DAOConceptos db = new DAOConceptos();
                return db.ObtenerValorParametro(flag);
            }
            catch (Exception ex)
            {

                throw ex;
            }
      
        }
        public static bool Get_XML_Text(string tipocomprobante, string serie, string numero, int id)
        {
          
            try
            {
                String datosEmpresa = ObtenerValorParametro(CENConstantes.CONST_2);

                string RUC = datosEmpresa.Split('|')[0];
                string Ruta = datosEmpresa.Split('|')[1];



                string Tipocomprobante = tipocomprobante;
                string Serie = serie;
                string Numero = numero;
                string Extension = ".xml";

                //cambios ruta
                String Rutapass = Ruta + @"FIRMA\";
                string ArchPlano = Rutapass + RUC + "-" + Tipocomprobante.PadLeft(2, '0') + "-" + Serie + "-" + Numero.PadLeft(8, '0') + Extension;


                if (File.Exists(ArchPlano))
                {

                    XmlTextReader xtr = new XmlTextReader(ArchPlano);
                    string hash = "";
                    while (xtr.Read())
                    {

                        if (xtr.NodeType == XmlNodeType.Element && xtr.Name == "ds:DigestValue")
                        {
                            hash = xtr.ReadElementContentAsString();
                        }
                    }

                    xtr.Close();
                 
                    CLNDocumentTypes clnDT = new CLNDocumentTypes();
                    clnDT.updateHash(id, hash);
                    return true;
                }
                else
                {

                    String  ArchPlanoB = Rutapass + @"\" + RUC+@"\" + RUC + "-" + Tipocomprobante.PadLeft(2, '0') + "-" + Serie + "-" + Numero.PadLeft(8, '0') + Extension;
                    if (File.Exists(ArchPlanoB))
                    {

                        XmlTextReader xtr = new XmlTextReader(ArchPlanoB);
                        string hash = "";
                        while (xtr.Read())
                        {

                            if (xtr.NodeType == XmlNodeType.Element && xtr.Name == "ds:DigestValue")
                            {
                                hash = xtr.ReadElementContentAsString();
                            }
                        }

                        xtr.Close();
                       
                        CLNDocumentTypes clnDT = new CLNDocumentTypes();
                        clnDT.updateHash(id, hash);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

               
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public static bool Get_XML_Text_Resumen(string tipocomprobante, string nombre_archivo, int id)
        {

            try
            {
                String datosEmpresa = ObtenerValorParametro(CENConstantes.CONST_2);

                string RUC = datosEmpresa.Split('|')[0];
                string Ruta = datosEmpresa.Split('|')[1];



                string Tipocomprobante = tipocomprobante;
            
                string Extension = ".xml";

                //cambios ruta
                String Rutapass = Ruta + @"FIRMA\";


                string ArchPlano = Rutapass + nombre_archivo + Extension;


                if (File.Exists(ArchPlano))
                {

                    XmlTextReader xtr = new XmlTextReader(ArchPlano);
                    string hash = "";
                    while (xtr.Read())
                    {

                        if (xtr.NodeType == XmlNodeType.Element && xtr.Name == "ds:DigestValue")
                        {
                            hash = xtr.ReadElementContentAsString();
                        }
                    }

                    xtr.Close();

                    CLNDocumentTypes clnDT = new CLNDocumentTypes();
                    clnDT.updateHash(id, hash);
                    return true;
                }
                else
                {

                    String ArchPlanoB = Rutapass + @"\" + RUC + @"\" + nombre_archivo + Extension;
                    if (File.Exists(ArchPlanoB))
                    {

                        XmlTextReader xtr = new XmlTextReader(ArchPlanoB);
                        string hash = "";
                        while (xtr.Read())
                        {

                            if (xtr.NodeType == XmlNodeType.Element && xtr.Name == "ds:DigestValue")
                            {
                                hash = xtr.ReadElementContentAsString();
                            }
                        }

                        xtr.Close();

                        CLNDocumentTypes clnDT = new CLNDocumentTypes();
                        clnDT.updateHash(id, hash);
                        return true;
                    }
                    else
                    {
                        return false;
                    }


                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }



        public static void llenarComboUnidad(System.Windows.Forms.ComboBox cmbUnidad)
        {
            List<CENDocumentTypes> lista;
            cmbUnidad.Items.Clear();
            try
            {
                CLNDocumentTypes cln = new CLNDocumentTypes();
                lista = cln.getCmbUnidadString(14);
                cmbUnidad.DataSource = lista;
                cmbUnidad.DisplayMember = "description";
                cmbUnidad.ValueMember = "id";
                
            }
            catch (Exception ex)
            {
                aviso aviso_form = new aviso("warning", "ERROR", ex.Message, false);
                aviso_form.ShowDialog();

                CENDocumentTypes cen = new CENDocumentTypes();
                cen.id = "0";
                cen.description = "No se encontraron resultados";
                lista = null;
                cmbUnidad.DataSource = lista;
                cmbUnidad.DisplayMember = "description";
                cmbUnidad.ValueMember = "id";
            }
        }

        // EXTRACCION DE DNI 
        private static string ExtraerContenidoEntreNombreString(string cadena, int posicion, string nombreInicio, string nombreFin, StringComparison reglaComparacion = StringComparison.OrdinalIgnoreCase)
        {
            string respuesta = "";
            int posicionInicio = cadena.IndexOf(nombreInicio, posicion, reglaComparacion);
            if (posicionInicio > -1)
            {
                posicionInicio += nombreInicio.Length;
                int posicionFin = cadena.IndexOf(nombreFin, posicionInicio, reglaComparacion);
                if (posicionFin > -1)
                    respuesta = cadena.Substring(posicionInicio, posicionFin - posicionInicio);
            }

            return respuesta;
        }
        private static string[] ExtraerContenidoEntreNombre(string cadena, int posicion, string nombreInicio, string nombreFin, StringComparison reglaComparacion = StringComparison.OrdinalIgnoreCase)
        {
            string[] arrRespuesta = null;
            int posicionInicio = cadena.IndexOf(nombreInicio, posicion, reglaComparacion);
            if (posicionInicio > -1)
            {
                posicionInicio += nombreInicio.Length;
                int posicionFin = cadena.IndexOf(nombreFin, posicionInicio, reglaComparacion);
                if (posicionFin > -1)
                {
                    posicion = posicionFin + nombreFin.Length;
                    arrRespuesta = new string[2];
                    arrRespuesta[0] = posicion.ToString();
                    arrRespuesta[1] = cadena.Substring(posicionInicio, posicionFin - posicionInicio);
                }
            }

            return arrRespuesta;
        }
        private class DatoSolicitudDNI
        {
            public string _token { get; set; }
            public string dni { get; set; }
        }

        public static async System.Threading.Tasks.Task<CENDataCliente> BuscarDniAsync(string par_dni)
        {
            int tipoRespuesta = 2;
            string mensajeRespuesta = "";

            string nombres_completos = "";
            CENDataCliente dataCliente = new CENDataCliente();
            dataCliente.success = false;
            dataCliente.nombresCompletos = "";
            dataCliente.direccion = "";
            dataCliente.mensaje = "";



            string numeroDNI = par_dni;
            if (string.IsNullOrWhiteSpace(numeroDNI))
                return dataCliente;

            Stopwatch oCronometro = new Stopwatch();
            oCronometro.Start();

            //btnConsultarDNIMediantePaginaExterna.Enabled = false;

            CookieContainer cookies = new CookieContainer();
            HttpClientHandler controladorMensaje = new HttpClientHandler();
            controladorMensaje.CookieContainer = cookies;
            controladorMensaje.UseCookies = true;
            using (System.Net.Http.HttpClient cliente = new HttpClient(controladorMensaje))
            {
                cliente.DefaultRequestHeaders.Add("Host", "eldni.com");
                cliente.DefaultRequestHeaders.Add("sec-ch-ua", "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\", \"Google Chrome\";v=\"90\"");

                cliente.DefaultRequestHeaders.Add("sec-ch-ua-mobile", "?0");
                cliente.DefaultRequestHeaders.Add("Sec-Fetch-Dest", "document");
                cliente.DefaultRequestHeaders.Add("Sec-Fetch-Mode", "navigate");
                cliente.DefaultRequestHeaders.Add("Sec-Fetch-Site", "none");
                cliente.DefaultRequestHeaders.Add("Sec-Fetch-User", "?1");
                cliente.DefaultRequestHeaders.Add("Upgrade-Insecure-Requests", "1");
                cliente.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36");

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |
                                                       SecurityProtocolType.Tls12;

                string url = "https://eldni.com/pe/buscar-por-dni";
                using (HttpResponseMessage resultadoConsultaToken = await cliente.GetAsync(new Uri(url)))
                {
                    if (resultadoConsultaToken.IsSuccessStatusCode)
                    {
                        mensajeRespuesta = await resultadoConsultaToken.Content.ReadAsStringAsync();

                        string token = ExtraerContenidoEntreNombreString(mensajeRespuesta, 0, "name=\"_token\" value=\"", "\">");

                        cliente.DefaultRequestHeaders.Remove("Sec-Fetch-Site");

                        cliente.DefaultRequestHeaders.Add("Origin", "https://eldni.com");
                        cliente.DefaultRequestHeaders.Add("Referer", "https://eldni.com/pe/buscar-por-dni");
                        cliente.DefaultRequestHeaders.Add("Sec-Fetch-Site", "same-origin");

                        DatoSolicitudDNI oDatoSolicitudDNI = new DatoSolicitudDNI();
                        oDatoSolicitudDNI._token = token;
                        oDatoSolicitudDNI.dni = numeroDNI;

                        using (HttpResponseMessage resultadoConsultaDatos = 
                            await cliente.PostAsJsonAsync(url, oDatoSolicitudDNI ))
                        {
                            if (resultadoConsultaDatos.IsSuccessStatusCode)
                            {
                                string contenidoHTML = await resultadoConsultaDatos.Content.ReadAsStringAsync();
                                string nombreInicio = "<table class=\"table table-striped table-scroll\">";
                                string nombreFin = "</table>";
                                string contenidoDNI = ExtraerContenidoEntreNombreString(contenidoHTML, 0, nombreInicio, nombreFin);
                                if (contenidoDNI == "")
                                {
                                    nombreInicio = "<h3 class=\"text-error\">";
                                    nombreFin = "</h3>";
                                    mensajeRespuesta = ExtraerContenidoEntreNombreString(contenidoHTML, 0, nombreInicio, nombreFin);
                                    mensajeRespuesta = mensajeRespuesta == ""
                                        ? string.Format(
                                            "No se pudo realizar la consulta del número de DNI {0}.", numeroDNI)
                                        : string.Format(
                                            "No se pudo realizar la consulta del número de DNI {0}.\r\nDetalle: {1}",
                                            numeroDNI,
                                            mensajeRespuesta);
                                }
                                else
                                {
                                    nombreInicio = "<td>";
                                    nombreFin = "</td>";
                                    string[] arrResultado = ExtraerContenidoEntreNombre(contenidoDNI, 0,
                                        nombreInicio,
                                        nombreFin);
                                    if (arrResultado != null)
                                    {
                                        // Nombres
                                        arrResultado = ExtraerContenidoEntreNombre(contenidoDNI,
                                            Convert.ToInt32(arrResultado[0]),
                                            nombreInicio, nombreFin);
                                        if (arrResultado != null)
                                        {
                                            nombres_completos = arrResultado[1];

                                            // Apellido Paterno
                                            arrResultado = ExtraerContenidoEntreNombre(contenidoDNI,
                                                Convert.ToInt32(arrResultado[0]),
                                                nombreInicio, nombreFin);

                                            if (arrResultado != null)
                                            {
                                                nombres_completos = nombres_completos + " "+ arrResultado[1];

                                                // Apellido Materno
                                                arrResultado = ExtraerContenidoEntreNombre(contenidoDNI,
                                                    Convert.ToInt32(arrResultado[0]),
                                                    nombreInicio, nombreFin);

                                                if (arrResultado != null)
                                                {
                                                    nombres_completos = nombres_completos + " " + arrResultado[1];
                                                    tipoRespuesta = 1;
                                                    mensajeRespuesta = string.Format("Se realizó exitosamente la consulta del número de DNI {0}",
                                                                numeroDNI);
                                                }
                                            }
                                        }
                                    }
                                    dataCliente.success = true;
                                }

                               
                            }
                            else
                            {
                                dataCliente.success = false;
                                mensajeRespuesta = await resultadoConsultaDatos.Content.ReadAsStringAsync();
                                mensajeRespuesta =
                                    string.Format(
                                        "Ocurrió un inconveniente al consultar los datos del DNI {0}.\r\nDetalle:{1}",
                                        numeroDNI, mensajeRespuesta);
                            }
                        }
                    }
                    else
                    {
                        mensajeRespuesta = await resultadoConsultaToken.Content.ReadAsStringAsync();
                        mensajeRespuesta =
                            string.Format(
                                "Ocurrió un inconveniente al consultar el número de DNI {0}.\r\nDetalle:{1}",
                                numeroDNI, mensajeRespuesta);
                    }
                }
            }

            oCronometro.Stop();
         
            dataCliente.nombresCompletos = nombres_completos;
            dataCliente.mensaje = mensajeRespuesta;


            return dataCliente;
        }

        //BUSCAR RUC

        public static string ExtraerContenidoEntreTagString(string cadena, int posicion, string nombreInicio, string nombreFin, StringComparison reglaComparacion = StringComparison.Ordinal)
        {
            string respuesta = "";
            int posicionInicio = cadena.IndexOf(nombreInicio, posicion, reglaComparacion);
            if (posicionInicio > -1)
            {
                posicionInicio += nombreInicio.Length;
                if (nombreFin == null || nombreFin == "")
                    respuesta = cadena.Substring(posicionInicio, cadena.Length - posicionInicio);
                else
                {
                    int posicionFin = cadena.IndexOf(nombreFin, posicionInicio, reglaComparacion);
                    if (posicionFin > -1)
                        respuesta = cadena.Substring(posicionInicio, posicionFin - posicionInicio);
                }

            }

            return respuesta;
        }

        public static string[] ExtraerContenidoEntreTag(string cadena, int posicion, string nombreInicio, string nombreFin, StringComparison reglaComparacion = StringComparison.Ordinal)
        {
            string[] arrRespuesta = null;
            int posicionInicio = cadena.IndexOf(nombreInicio, posicion, reglaComparacion);
            if (posicionInicio > -1)
            {
                posicionInicio += nombreInicio.Length;
                if (nombreFin == null || nombreFin == "")
                {
                    arrRespuesta = new string[2];
                    arrRespuesta[0] = cadena.Length.ToString();
                    arrRespuesta[1] = cadena.Substring(posicionInicio, cadena.Length - posicionInicio);
                }
                else
                {
                    int posicionFin = cadena.IndexOf(nombreFin, posicionInicio, reglaComparacion);
                    if (posicionFin > -1)
                    {
                        posicion = posicionFin + nombreFin.Length;
                        arrRespuesta = new string[2];
                        arrRespuesta[0] = posicion.ToString();
                        arrRespuesta[1] = cadena.Substring(posicionInicio, posicionFin - posicionInicio);
                    }
                }
            }

            return arrRespuesta;
        }

        private static CENSunat ObtenerDatosRuc(string contenidoHTML, bool is_ruc10)
        {

            CENSunat oEnSUNAT = new CENSunat();
            string nombreInicio = "<HEAD><TITLE>";
            string nombreFin = "</TITLE></HEAD>";
            string contenidoBusqueda = ExtraerContenidoEntreTagString(contenidoHTML, 0, nombreInicio, nombreFin);
            if (contenidoBusqueda == ".:: Pagina de Mensajes ::.")
            {
                nombreInicio = "<p class=\"error\">";
                nombreFin = "</p>";
                oEnSUNAT.TipoRespuesta = 2;
                oEnSUNAT.MensajeRespuesta = ExtraerContenidoEntreTagString(contenidoHTML, 0, nombreInicio, nombreFin);
            }
            else if (contenidoBusqueda == ".:: Pagina de Error ::.")
            {
                nombreInicio = "<p class=\"error\">";
                nombreFin = "</p>";
                oEnSUNAT.TipoRespuesta = 3;
                oEnSUNAT.MensajeRespuesta = ExtraerContenidoEntreTagString(contenidoHTML, 0, nombreInicio, nombreFin);
            }
            else
            {
                oEnSUNAT.TipoRespuesta = 2;
                nombreInicio = "<div class=\"list-group\">";
                nombreFin = "<div class=\"panel-footer text-center\">";
                contenidoBusqueda = ExtraerContenidoEntreTagString(contenidoHTML, 0, nombreInicio, nombreFin);
                if (contenidoBusqueda == "")
                {
                    nombreInicio = "<strong>";
                    nombreFin = "</strong>";
                    oEnSUNAT.MensajeRespuesta = ExtraerContenidoEntreTagString(contenidoHTML, 0, nombreInicio, nombreFin);
                    if (oEnSUNAT.MensajeRespuesta == "")
                        oEnSUNAT.MensajeRespuesta = "No se encuentra las cabeceras principales del contenido HTML";
                }
                else
                {
                    contenidoHTML = contenidoBusqueda;
                    oEnSUNAT.MensajeRespuesta = "Mensaje del inconveniente no especificado";
                    nombreInicio = "<h4 class=\"list-group-item-heading\">";
                    nombreFin = "</h4>";
                    int resultadoBusqueda = contenidoHTML.IndexOf(nombreInicio, 0, StringComparison.OrdinalIgnoreCase);
                    if (resultadoBusqueda > -1)
                    {
                        // Modificar cuando el estado del Contribuyente es "BAJA DE OFICIO", porque se agrega un elemento con clase "list-group-item"
                        resultadoBusqueda += nombreInicio.Length;
                        string[] arrResultado = ExtraerContenidoEntreTag(contenidoHTML, resultadoBusqueda,
                            nombreInicio, nombreFin);
                        if (arrResultado != null)
                        {
                            oEnSUNAT.RazonSocial = arrResultado[1];

                            // Tipo Contribuyente
                            nombreInicio = "<p class=\"list-group-item-text\">";
                            nombreFin = "</p>";
                            arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                nombreInicio, nombreFin);
                            if (arrResultado != null)
                            {
                                oEnSUNAT.TipoContribuyente = arrResultado[1];
                                // si es ruc 10 se agrega tipo de documento
                                if(is_ruc10)
                                {
                                    // Tipo de Documento
                                    arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                        nombreInicio, nombreFin);

                                    if (arrResultado != null)
                                    {
                                        oEnSUNAT.TipoDocumento = arrResultado[1].Replace("\r\n", "").Replace("\t", "").Trim();
                                    }
                                }



                                // Nombre Comercial
                                arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                    nombreInicio, nombreFin);
                                if (arrResultado != null)
                                {
                                    oEnSUNAT.NombreComercial = arrResultado[1].Replace("\r\n", "").Replace("\t", "").Trim();

                                    // Fecha de Inscripción
                                    arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                        nombreInicio, nombreFin);
                                    if (arrResultado != null)
                                    {
                                        oEnSUNAT.FechaInscripcion = arrResultado[1];

                                        // Fecha de Inicio de Actividades: 
                                        arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                            nombreInicio, nombreFin);
                                        if (arrResultado != null)
                                        {
                                            oEnSUNAT.FechaInicioActividades = arrResultado[1];

                                            // Estado del Contribuyente
                                            arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                            nombreInicio, nombreFin);
                                            if (arrResultado != null)
                                            {
                                                oEnSUNAT.EstadoContribuyente = arrResultado[1].Trim();

                                                // Condición del Contribuyente
                                                arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                                    nombreInicio, nombreFin);
                                                if (arrResultado != null)
                                                {
                                                    oEnSUNAT.CondicionContribuyente = arrResultado[1].Trim();

                                                    // Domicilio Fiscal
                                                    arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                                        nombreInicio, nombreFin);
                                                    if (arrResultado != null)
                                                    {
                                                        oEnSUNAT.DomicilioFiscal = arrResultado[1].Trim();

                                                        // Actividad(es) Económica(s)
                                                        nombreInicio = "<tbody>";
                                                        nombreFin = "</tbody>";
                                                        arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                                            nombreInicio, nombreFin);
                                                        if (arrResultado != null)
                                                        {
                                                            oEnSUNAT.ActividadesEconomicas = arrResultado[1].Replace("\r\n", "").Replace("\t", "").Trim();

                                                            // Comprobantes de Pago c/aut. de impresión (F. 806 u 816)
                                                            arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                                                nombreInicio, nombreFin);
                                                            if (arrResultado != null)
                                                            {
                                                                oEnSUNAT.ComprobantesPago = arrResultado[1].Replace("\r\n", "").Replace("\t", "").Trim();

                                                                // Sistema de Emisión Electrónica
                                                                arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                                                    nombreInicio, nombreFin);
                                                                if (arrResultado != null)
                                                                {
                                                                    oEnSUNAT.SistemaEmisionComprobante = arrResultado[1].Replace("\r\n", "").Replace("\t", "").Trim();

                                                                    // Afiliado al PLE desde
                                                                    nombreInicio = "<p class=\"list-group-item-text\">";
                                                                    nombreFin = "</p>";
                                                                    arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                                                        nombreInicio, nombreFin);
                                                                    if (arrResultado != null)
                                                                    {
                                                                        oEnSUNAT.AfiliadoPLEDesde = arrResultado[1];

                                                                        // Padrones 
                                                                        nombreInicio = "<tbody>";
                                                                        nombreFin = "</tbody>";
                                                                        arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                                                            nombreInicio, nombreFin);
                                                                        if (arrResultado != null)
                                                                        {
                                                                            oEnSUNAT.Padrones = arrResultado[1].Replace("\r\n", "").Replace("\t", "").Trim();
                                                                        }
                                                                    }

                                                                    oEnSUNAT.TipoRespuesta = 1;
                                                                    oEnSUNAT.MensajeRespuesta = "Ok";
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return oEnSUNAT;
        }

        public static async System.Threading.Tasks.Task<CENSunat>  ConsultarRUC(string pa_ruc)
        {

            int tipoRespuesta = 2;
            string mensajeRespuesta = "";

            CENSunat oEnSUNAT = new CENSunat();
            bool is_ruc_10 = false;
            string min_ruc = "";
            min_ruc = pa_ruc.Substring(0, 2);
            if (min_ruc == "10")
            {
                is_ruc_10 = true;
            }
            else
            {
                is_ruc_10 = false;
            }

            string ruc = pa_ruc;
            if (string.IsNullOrWhiteSpace(ruc))
                return oEnSUNAT;


          

            try
            {
                Stopwatch oCronometro = new Stopwatch();
                oCronometro.Start();

                //btnConsultarRUCMedianteNumeroRandom.Enabled = false;
                CookieContainer cookies = new CookieContainer();
                HttpClientHandler controladorMensaje = new HttpClientHandler();
                controladorMensaje.CookieContainer = cookies;
                controladorMensaje.UseCookies = true;
                using (HttpClient cliente = new HttpClient(controladorMensaje))
                {
                    cliente.DefaultRequestHeaders.Add("Host", "e-consultaruc.sunat.gob.pe");
                    cliente.DefaultRequestHeaders.Add("sec-ch-ua",
                        " \" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\", \"Google Chrome\";v=\"90\"");
                    cliente.DefaultRequestHeaders.Add("sec-ch-ua-mobile", "?0");
                    cliente.DefaultRequestHeaders.Add("Sec-Fetch-Dest", "document");
                    cliente.DefaultRequestHeaders.Add("Sec-Fetch-Mode", "navigate");
                    cliente.DefaultRequestHeaders.Add("Sec-Fetch-Site", "none");
                    cliente.DefaultRequestHeaders.Add("Sec-Fetch-User", "?1");
                    cliente.DefaultRequestHeaders.Add("Upgrade-Insecure-Requests", "1");
                    cliente.DefaultRequestHeaders.Add("User-Agent",
                        "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36");
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |
                                                           SecurityProtocolType.Tls12;
                    await Task.Delay(100);

                    string url =
                        "https://e-consultaruc.sunat.gob.pe/cl-ti-itmrconsruc/jcrS00Alias";
                    using (HttpResponseMessage resultadoConsulta = await cliente.GetAsync(new Uri(url)))
                    {
                        if (resultadoConsulta.IsSuccessStatusCode)
                        {
                            await Task.Delay(100);
                            cliente.DefaultRequestHeaders.Remove("Sec-Fetch-Site");

                            cliente.DefaultRequestHeaders.Add("Origin", "https://e-consultaruc.sunat.gob.pe");
                            cliente.DefaultRequestHeaders.Add("Referer", url);
                            cliente.DefaultRequestHeaders.Add("Sec-Fetch-Site", "same-origin");

                            string numeroDNI = "12345678"; // cualquier número DNI pero que exista en SUNAT.
                            var lClaveValor = new List<KeyValuePair<string, string>>
                        {
                            new KeyValuePair<string, string>("accion", "consPorTipdoc"),
                            new KeyValuePair<string, string>("razSoc", ""),
                            new KeyValuePair<string, string>("nroRuc", ""),
                            new KeyValuePair<string, string>("nrodoc", numeroDNI),
                            new KeyValuePair<string, string>("contexto", "ti-it"),
                            new KeyValuePair<string, string>("modo", "1"),
                            new KeyValuePair<string, string>("search1", ""),
                            new KeyValuePair<string, string>("rbtnTipo", "2"),
                            new KeyValuePair<string, string>("tipdoc", "1"),
                            new KeyValuePair<string, string>("search2", numeroDNI),
                            new KeyValuePair<string, string>("search3", ""),
                            new KeyValuePair<string, string>("codigo", ""),
                        };
                            FormUrlEncodedContent contenido = new FormUrlEncodedContent(lClaveValor);

                            url = "https://e-consultaruc.sunat.gob.pe/cl-ti-itmrconsruc/jcrS00Alias";
                            using (HttpResponseMessage resultadoConsultaRandom = await cliente.PostAsync(url, contenido))
                            {
                                if (resultadoConsultaRandom.IsSuccessStatusCode)
                                {
                                    await Task.Delay(100);
                                    string contenidoHTML = await resultadoConsultaRandom.Content.ReadAsStringAsync();
                                    string numeroRandom = ExtraerContenidoEntreTagString(contenidoHTML, 0, "name=\"numRnd\" value=\"", "\">");

                                    lClaveValor = new List<KeyValuePair<string, string>>
                                {
                                    new KeyValuePair<string, string>("accion", "consPorRuc"),
                                    new KeyValuePair<string, string>("actReturn", "1"),
                                    new KeyValuePair<string, string>("nroRuc", ruc),
                                    new KeyValuePair<string, string>("numRnd", numeroRandom),
                                    new KeyValuePair<string, string>("modo", "1")
                                };

                                    int cConsulta = 0;
                                    int nConsulta = 3;
                                    HttpStatusCode codigoEstado = HttpStatusCode.Unauthorized;
                                    while (cConsulta < nConsulta && codigoEstado == HttpStatusCode.Unauthorized)
                                    {
                                        contenido = new FormUrlEncodedContent(lClaveValor);
                                        using (HttpResponseMessage resultadoConsultaDatos =
                                        await cliente.PostAsync(url, contenido))
                                        {
                                            codigoEstado = resultadoConsultaDatos.StatusCode;
                                            if (resultadoConsultaDatos.IsSuccessStatusCode)
                                            {
                                                contenidoHTML = await resultadoConsultaDatos.Content.ReadAsStringAsync();
                                                contenidoHTML = WebUtility.HtmlDecode(contenidoHTML);

                                                #region Obtener los datos del RUC
                                                oEnSUNAT = ObtenerDatosRuc(contenidoHTML, is_ruc_10);
                                                if (oEnSUNAT.TipoRespuesta == 1)
                                                {
                                                    string[] nom_com;
                                                    string direccion_com;
                                                    nom_com = oEnSUNAT.RazonSocial.Split('-');
                                                    direccion_com = oEnSUNAT.DomicilioFiscal;
                                                    oEnSUNAT.RazonSocial = nom_com[1].Trim();
                                                    oEnSUNAT.DomicilioFiscal= GetDireccion(direccion_com);
                                                    //txtRUC.Text = oEnSUNAT.RazonSocial;
                                                    //txtTipoContribuyente.Text = oEnSUNAT.TipoContribuyente;
                                                    //txtNombreComercial.Text = oEnSUNAT.NombreComercial;
                                                    //txtFechaInscripcion.Text = oEnSUNAT.FechaInscripcion;
                                                    //txtFechaInicioActividades.Text = oEnSUNAT.FechaInicioActividades;
                                                    //txtEstadoContribuyente.Text = oEnSUNAT.EstadoContribuyente;
                                                    //txtCondicionContribuyente.Text = oEnSUNAT.CondicionContribuyente;
                                                    //txtDomicilioFiscal.Text = oEnSUNAT.DomicilioFiscal;
                                                    //txtSistemaEmisionComprobante.Text = oEnSUNAT.SistemaEmisionComprobante;
                                                    //txtActividadesEconomicas.Text = oEnSUNAT.ActividadesEconomicas;
                                                    //txtComprobantesPago.Text = oEnSUNAT.ComprobantesPago;
                                                    //txtAfiliadoPLE.Text = oEnSUNAT.AfiliadoPLEDesde;
                                                    //txtPadrones.Text = oEnSUNAT.Padrones;

                                                    tipoRespuesta = 1;
                                                    mensajeRespuesta =
                                                        string.Format("Se realizó exitosamente la consulta del número de RUC {0}",
                                                            ruc);
                                                }
                                                else
                                                {
                                                    tipoRespuesta = oEnSUNAT.TipoRespuesta;
                                                    mensajeRespuesta = string.Format(
                                                        "No se pudo realizar la consulta del número de RUC {0}.\r\nDetalle: {1}",
                                                        ruc,
                                                        oEnSUNAT.MensajeRespuesta);
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                mensajeRespuesta = await resultadoConsultaDatos.Content.ReadAsStringAsync();
                                                mensajeRespuesta =
                                                    string.Format(
                                                        "Ocurrió un inconveniente al consultar los datos del RUC {0}.\r\nDetalle:{1}",
                                                        ruc, mensajeRespuesta);
                                            }
                                        }

                                        cConsulta++;
                                    }



                                }
                                else
                                {
                                    mensajeRespuesta = await resultadoConsultaRandom.Content.ReadAsStringAsync();
                                    mensajeRespuesta =
                                        string.Format(
                                            "Ocurrió un inconveniente al consultar el número random del RUC {0}.\r\nDetalle:{1}",
                                            ruc, mensajeRespuesta);
                                }
                            }
                        }
                        else
                        {
                            mensajeRespuesta = await resultadoConsulta.Content.ReadAsStringAsync();
                            mensajeRespuesta =
                                string.Format(
                                    "Ocurrió un inconveniente al consultar la página principal {0}.\r\nDetalle:{1}",
                                    ruc, mensajeRespuesta);
                        }
                    }
                }

                oEnSUNAT.MensajeRespuesta = mensajeRespuesta;
                return oEnSUNAT;

                oCronometro.Stop();
            }
            catch (Exception ex)
            {

                throw ex;
            }

           


        }

        private static string GetDireccion(string direccion)
        {
            string[] dire;
            string newDir = "";
            dire = direccion.Split('-');
            int count = 0;
            foreach (string der in dire)
            {
                if (count == 0)
                {

                    newDir = der.Trim();
                }
                else
                {
                    newDir = newDir + " - " + der.Trim();
                }
                count++;

            }
            return newDir;

        }


        //CONSULTA DE DNI POR APIPERU

        public static CENDataCliente Consultar(string DNI)
        {
            var Result = new CENDataCliente();
            try
            {

                string urlPar = "", tokenPar = "";
                string html = string.Empty;

                CENParametro parFlagRegrear;
                CLNConcepto clncconcepto;

                clncconcepto = new CLNConcepto();
                parFlagRegrear = new CENParametro();
                parFlagRegrear = clncconcepto.buscarParametro(1000);
                urlPar = parFlagRegrear.parametro;
                tokenPar = parFlagRegrear.parametro2;

                string url = $"{urlPar}{DNI}";

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Headers.Add("Authorization", "Bearer " + tokenPar);
                //request.Headers.Add("Authorization", "Bearer 8dddb831c0f5145ddc683e1f42b2fb5d788ff2997a210c9b544f202ddd2f2b1a"); //token de prueba
                //request.Headers.Add("Authorization", "Bearer 26d9db9ff8972239c3e19a967ee2dc337fd19e3b444be2b6a6fe680c14e21c95"); // token expirado


                request.AutomaticDecompression = DecompressionMethods.GZip;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    html = reader.ReadToEnd();
                }
                var serializerSettings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
                var objE = JsonConvert.DeserializeObject<CENTransactionApidni>(html, serializerSettings);

                if (objE.success)
                {
                    Result.success = objE.success;
                    Result.nombresCompletos = objE.data.apellido_paterno + " " + objE.data.apellido_materno + " " + objE.data.nombres;
                    Result.direccion = "";
                }
                else
                {
                    Result.success = objE.success;
                    Result.mensaje = "Si el problema persiste revisar Token de " + urlPar+
                        " \n Comunicarse con el área de sistemas";
                }
            

           }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }


        // PSE - INCIAR SESSION JWT 
        public static CENResponsePse sendWebApi(string urlWA, string dataSerializado)
        {
            // DESCRIPCION: FUNCION PARA COMONICARSE CON OTRA WEB API


            try
            {

                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                string responseBody;                                    //respuesta
                var url = $"{urlWA}";                                   //url
                var request = (HttpWebRequest)WebRequest.Create(url);   // request
                string json = dataSerializado;                          // json
                request.Method = "POST";
                request.ContentType = "application/json";
                
                request.Accept = "application/json";

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(json);
                }

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream strReader = response.GetResponseStream())
                    {
                        using (StreamReader objReader = new StreamReader(strReader))
                        {
                            responseBody = objReader.ReadToEnd();

                            CENResponsePse rpt = (CENResponsePse)JsonConvert.DeserializeObject(responseBody, typeof(CENResponsePse));
                            objReader.Dispose();
                            strReader.Dispose();
                            response.Dispose();
                            return rpt;
                        }
                    }

                }

            }
            catch (Exception ex)
            {

                //error 400- error en data
                
                //No se puede resolver el nombre remoto --contiene--- mensaje de no hay internet

                if (ex.Message.Contains("No se puede resolver el nombre remoto"))
                {
                    throw new Exception("REVISE SU CONEXION DE INTERNET \n" + ex.Message.Trim().ToString());
                }

                //No es posible conectar con el servidor remoto -- mensaje si el servidor  esta apagado
                if (ex.Message.Trim().ToString()== "No es posible conectar con el servidor remoto")
                {
                    throw new Exception("ERROR AL CONECTAR CON EL SERVIDOR: " + urlWA + " - " + ex.Message.Trim().ToString());
                }
                else
                {
                    throw ex;
                }
                
            }

            

        }

        public static object sendWebApiWithToken(string urlWA, string dataSerializado, Type tipoResponse)
        {
            var Result = new CENDataCliente();
            dynamic objE = null;
            CENParametro parametro;
            CLNConcepto clncconcepto;

            try
            {

                string tokenPar = "";
                string html = string.Empty;

   

                clncconcepto = new CLNConcepto();
                parametro = new CENParametro();
                parametro = clncconcepto.buscarParametro(2002);
                tokenPar = parametro.parametro.Trim();



                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlWA);
                request.Headers.Add("Authorization", "Bearer " + tokenPar);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Accept = "application/json";

                string json = dataSerializado;                          // json

                request.AutomaticDecompression = DecompressionMethods.GZip;

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(json);
                }

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    html = reader.ReadToEnd();
                }

                if(tipoResponse == typeof(CENResponsePse))
                {
                    objE = JsonConvert.DeserializeObject <CENResponsePse> (html);
                }
                if (tipoResponse == typeof(CENResponseSyncCpe))
                {
                    objE = JsonConvert.DeserializeObject<List<CENResponseSyncCpe>>(html);
                }





            }
            catch (Exception ex)
            {
               
                throw ex;
            }
            return objE;
        }

        public static CENResponsePse IniciarSessionPse()
        {
            //DESCRIPCION: FUNCION PARA INICIAR SESION EN WEBAPI PSE Y GRABAR EL TOKEN EN LA TABLA PARAMETRO

            string url_ws ;
            string api_iniciar_session ;

            CENSessionPse sessionPse = new CENSessionPse();
            CENResponsePse response = new CENResponsePse();
            CENParametro parametro;
            CLNConcepto clncconcepto;

            try
            {
              

                clncconcepto = new CLNConcepto();
                parametro = new CENParametro();
                parametro = clncconcepto.buscarParametro(2000);
                sessionPse.usuario = parametro.parametro;

                clncconcepto = new CLNConcepto();
                parametro = new CENParametro();
                parametro = clncconcepto.buscarParametro(2001);
                sessionPse.password = parametro.parametro;

                clncconcepto = new CLNConcepto();
                parametro = new CENParametro();
                parametro = clncconcepto.buscarParametro(2003);
                url_ws = parametro.parametro.Trim();


                clncconcepto = new CLNConcepto();
                parametro = new CENParametro();
                parametro = clncconcepto.buscarParametro(2004);
                api_iniciar_session = parametro.parametro.Trim();

                var urlWA = url_ws + api_iniciar_session ;
                string dataSerializado = JsonConvert.SerializeObject(sessionPse);

                response = sendWebApi(urlWA, dataSerializado);

                if(response.rptaRegistroCpeDoc.flagVerificacion)
                {
                    //actualizamos el token en parametro 2002
                    clncconcepto.ActualizarTokenPse(response);

                }


                return response;

            }
            catch (Exception ex)
            {

                throw ex;
            }


           
        }

        public static CENResponsePse EnviarTramaPse(CENDocumentoPse docPse)
        {
            //DESCRIPCION: FUNCION PARA INICIAR SESION EN WEBAPI PSE Y GRABAR EL TOKEN EN LA TABLA PARAMETRO

            string url_ws, urlWA="", dataSerializado ="";
            string api_generar_comprobante;

            dynamic response = new CENResponsePse();

            try
            {
                CENParametro parametro;
                CLNConcepto clncconcepto;

                clncconcepto = new CLNConcepto();
                parametro = new CENParametro();
                parametro = clncconcepto.buscarParametro(2003);
                url_ws = parametro.parametro.Trim();


                clncconcepto = new CLNConcepto();
                parametro = new CENParametro();
                parametro = clncconcepto.buscarParametro(2005);
                api_generar_comprobante = parametro.parametro.Trim();

                urlWA = url_ws + api_generar_comprobante;

                var serializerSettings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
                dataSerializado = JsonConvert.SerializeObject(docPse, Newtonsoft.Json.Formatting.Indented, serializerSettings);

                response = (CENResponsePse)sendWebApiWithToken(urlWA, dataSerializado, typeof(CENResponsePse));

                if(response.rptaRegistroCpeDoc.flagVerificacion)
                {
                    return response;
                }
                else
                {
                    if(response.rptaRegistroCpeDoc.codigo==4000)
                    {
                        throw new Exception("ERROR AL REGISTRAR COMPROBANTE EN PSE - " + response.rptaRegistroCpeDoc.descripcionResp  + " - "
                            +response.errorWebService.descripcionErr);
                    }
                    else
                    {
                        throw new Exception("ERROR AL REGISTRAR COMPROBANTE EN PSE - " + response.rptaRegistroCpeDoc.descripcionResp);
                    }
                    
                }

            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("401"))
                {
                    CENResponsePse responseIS = new CENResponsePse();
                    responseIS = funciones.IniciarSessionPse();
                    if(responseIS.rptaRegistroCpeDoc.flagVerificacion)
                    {
                        response = (CENResponsePse)sendWebApiWithToken(urlWA, dataSerializado, typeof(CENResponsePse));
                        return response;
                    }
                    else
                    {
                        throw new Exception("ERROR AL INICIAR SESSION Y OBTENER TOKEN");
                    }
                }
                throw ex;
            }



        }

       

        public static CENDocumentoPse CrearJsonPSE(CENEstructuraComprobante comprobante, CENComprobanteCabecera cab, string serie, string numero)
        {
            CENDocumentoPse docPse = new CENDocumentoPse();
            List<ListaDetalleDocumento> detalle = new List<ListaDetalleDocumento>();
            ListaDetalleDocumento detalleDoc;
            CENEmpresa cenEmp = new CENEmpresa();
            CLNSoap clnsoap = new CLNSoap();

            String datosEmpresa;
            string RUC, Ruta;


            try
            {
                
                datosEmpresa = ObtenerValorParametro(CENConstantes.CONST_2);
                RUC = datosEmpresa.Split('|')[0];
                Ruta = datosEmpresa.Split('|')[1];

                // obteniendo usuario y password de empresa
                cenEmp = clnsoap.searchSoap();

                docPse.usuario = cenEmp.usuario_soap;
                docPse.clave = cenEmp.password_soap;
                docPse.serie = serie;
                docPse.numero = numero;
                docPse.idtipodocumento = cab.tipoComprobante;
                docPse.tipoOperacion = comprobante.tipoOperacion;
                docPse.tipoNotaCredNotaDeb = "";
                docPse.tipodoccliente = comprobante.TipoDocEmpresa;
                docPse.nrodoccliente = comprobante.NumDocEmpresa;
                docPse.nombrecliente = comprobante.NombEmpresa;
                docPse.direccioncliente = cab.direccionCliente;
                docPse.emailcliente = "";
                docPse.fecha = comprobante.Cab_Fac_Fecha;
                //docPse.hora = comprobante.HoraEmision;
                docPse.hora = getDateHourNowLima(); 
        

        
                



                docPse.fechavencimiento = null;
                docPse.glosa = "PRODUCCION";
                docPse.moneda = comprobante.tipo_moneda;
                docPse.tipocambio = "0";

                if (cab.descuentoGlobal>0)
                {
                    decimal totalSinDsc = Convert.ToDecimal(comprobante.ImporteTotalVenta);
                    decimal porcentaje = Convert.ToDecimal(cab.descuentoGlobal) * 100 / totalSinDsc;

                    docPse.descuento = porcentaje.ToString();
                    docPse.Descuentoafectabase = "SI";
                }

                docPse.igvventa = Math.Round(comprobante.SumTotalTributos, 2).ToString();
                docPse.iscventa = "0";
                docPse.otrosTributos = "0";
                docPse.otrosCargos= Math.Round(comprobante.otrosCargos, 2).ToString();
                docPse.importeventa = Math.Round(comprobante.TotalValorVenta, 2).ToString();
                docPse.regimenpercep = "";
                docPse.porcentpercep = "0";
                docPse.importepercep = "0";
                docPse.porcentdetrac = "0";
                docPse.importedetrac = "0";
                docPse.importerefdetrac = "0";

                docPse.importefinal = Math.Round(comprobante.ImporteTotalVenta, 2).ToString();
                docPse.importeanticipo = Math.Round(comprobante.TotalAnticipos, 2).ToString();
                docPse.indicaAnticipo = "";
                docPse.estado = "N";
                docPse.situacion = "P";
                docPse.estransgratuita = "NO";
                docPse.montoFise = "0";
                docPse.formapago = "Contado";
                docPse.hasRetencionIgv = "NO";
                docPse.porcRetencionIgv = "0";
                docPse.impRetencionIgv = "0";
                docPse.impOperacionRetencionIgv = "0";


           
                int lineId = 1;
                foreach (CENEstructuraDetalleComprobante det in comprobante.listaDetalle)
                {

                     detalleDoc = new ListaDetalleDocumento();
                    detalleDoc.lineid = lineId.ToString();
                    detalleDoc.codigo = (det.codigoProducto=="-")?"": det.codigoProducto;
                    detalleDoc.codigoSunat = (det.CodigoProdSunat == "-") ? "" : det.CodigoProdSunat;
                    detalleDoc.producto = det.Producto;
                    detalleDoc.afectoIgv = "SI";
                    detalleDoc.afectoIsc = "NO";
                    detalleDoc.sistemaISC = "";
                    detalleDoc.afectacionigv = det.tributoAfectacion;
                    detalleDoc.unidad = det.UnidadMedida;
                    detalleDoc.cantidad=det.cantidad.ToString();
                    detalleDoc.preciounitario = det.PrecioVtaUnitario.ToString();
                    detalleDoc.precioreferencial = det.PrecioVtaUnitario.ToString();
                    detalleDoc.valorunitario = det.precioBaseItem.ToString();
                    detalleDoc.valorbruto = Math.Round(Convert.ToDecimal(det.baseImponible.ToString()), 2).ToString();
                    detalleDoc.valordscto = "0";
                    detalleDoc.valorcargo = "0";
                    detalleDoc.valorventa = Math.Round(Convert.ToDecimal(det.valor_item.ToString()), 2).ToString();
                    detalleDoc.isc = "0";
                    detalleDoc.igv = det.MontoIGVItem.ToString();
                    detalleDoc.total= Math.Round(det.PrecioVtaUnitario * det.cantidad, 2).ToString();
                    detalleDoc.factorIgv = det.tributoPorcentaje.ToString();
                    detalleDoc.factorIsc = "0";

                    lineId ++;
                    detalle.Add(detalleDoc);

                }
                docPse.listaDetalleDocumento = detalle;

                return docPse;

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public static CENDocumentoPse CrearJsonNotesPSE(CENEstructuraComprobante comprobante, CENComprobanteCabecera cab, string serie, string numero, string codigo_tipo_nota, string descripcion_nota)
        {
            CENDocumentoPse docPse = new CENDocumentoPse();
            List<ListaDetalleDocumento> detalleFinal = new List<ListaDetalleDocumento>();
            ListaDetalleDocumento detalleDoc;

            List<ListaDocumentoReferencia> ListadetalleReferencia  = new List<ListaDocumentoReferencia>();

            ListaDocumentoReferencia detalleReferencia;

            CENEmpresa cenEmp = new CENEmpresa();
            CLNSoap clnsoap = new CLNSoap();

            try
            {

                CENEstructuraComprobante comprobanteFinal = null;
                
                if (comprobante.descuento_global > 0)
                {
                
                    comprobanteFinal = new CENEstructuraComprobante();
                    comprobanteFinal.tipoOperacion = comprobante.tipoOperacion;
                    comprobanteFinal.Cab_Fac_Fecha = comprobante.Cab_Fac_Fecha;
                    //comprobanteFinal.HoraEmision = comprobante.HoraEmision;
                    comprobanteFinal.HoraEmision = getDateHourNowLima(); 
                    
                    comprobanteFinal.FechaVencimiento = comprobante.FechaVencimiento;
                    comprobanteFinal.CodDomicilioFiscal = comprobante.CodDomicilioFiscal;
                    comprobanteFinal.TipoDocEmpresa = comprobante.TipoDocEmpresa;
                    comprobanteFinal.NumDocEmpresa = comprobante.NumDocEmpresa;
                    comprobanteFinal.NombEmpresa = comprobante.NombEmpresa;
                    comprobanteFinal.tipo_moneda = comprobante.tipo_moneda;
                    comprobanteFinal.SumTotalTributos = comprobante.SumTotalTributos;
                    comprobanteFinal.TotalValorVenta = comprobante.TotalValorVenta;
                    comprobanteFinal.TotalPrecioVenta = comprobante.TotalPrecioVenta;
                    comprobanteFinal.TotalDescuentos = comprobante.TotalDescuentos;
                    comprobanteFinal.otrosCargos = comprobante.otrosCargos;
                    comprobanteFinal.TotalAnticipos = comprobante.TotalAnticipos;
                    comprobanteFinal.ImporteTotalVenta = comprobante.ImporteTotalVenta;
                    comprobanteFinal.VersionUBL = comprobante.VersionUBL;
                    comprobanteFinal.Customizacion = comprobante.Customizacion;
                    comprobanteFinal.nomCustomer = comprobante.nomCustomer;
                    comprobanteFinal.docCustomer = comprobante.docCustomer;
                    comprobanteFinal.typeDocCustomer = comprobante.typeDocCustomer;
                    comprobanteFinal.serieDocumento = comprobante.serieDocumento;
                    comprobanteFinal.direccionCustomer = comprobante.direccionCustomer;
                    comprobanteFinal.tipo_comprobante = comprobante.tipo_comprobante;
                    comprobanteFinal.descuento_global = comprobante.descuento_global;
                    comprobanteFinal.descuento_total = comprobante.descuento_total;

                    List<CENEstructuraDetalleComprobante> detalle = new List<CENEstructuraDetalleComprobante>();
                    CENEstructuraDetalleComprobante cenDetalle;

                    int conteoItems = comprobante.listaDetalle.Count();
                    decimal descuentoTotal = Math.Round((comprobante.descuento_global * Convert.ToDecimal(1.18)), 2);
                    decimal porcentajeDct = Math.Round(((comprobante.ImporteTotalVenta / (comprobante.ImporteTotalVenta + descuentoTotal))), 6);

                    foreach (var dr in comprobante.listaDetalle)
                    {

                        decimal precioVtaUnitario = 0, precioBaseItem = 0, montoIgvItem = 0, sumaTributosItem = 0, valorUnitario = 0, valorItem = 0, baseImponible = 0;
                        precioVtaUnitario = Math.Round((dr.PrecioVtaUnitario * porcentajeDct), 6); //  2 DECIMALES
                        precioBaseItem = Math.Round((precioVtaUnitario / Convert.ToDecimal(1.18)), 6); //

                        montoIgvItem = Math.Round(((precioVtaUnitario * dr.cantidad) / Convert.ToDecimal(1.18) * Convert.ToDecimal(0.18)), 6); // 2 DECIMALES
                        sumaTributosItem = Math.Round(((precioVtaUnitario * dr.cantidad) / Convert.ToDecimal(1.18) * Convert.ToDecimal(0.18)), 6); // 2 DECIMALES

                        valorUnitario = Math.Round((precioVtaUnitario * dr.cantidad / Convert.ToDecimal(1.18)), 6); //
                        valorItem = Math.Round((precioVtaUnitario * dr.cantidad / Convert.ToDecimal(1.18)), 6);
                        baseImponible = Math.Round((precioVtaUnitario * dr.cantidad / Convert.ToDecimal(1.18)), 6); //

                        cenDetalle = new CENEstructuraDetalleComprobante();

                        cenDetalle.UnidadMedida = dr.UnidadMedida;
                        cenDetalle.cantidad = dr.cantidad;
                        cenDetalle.codigoProducto = dr.codigoProducto;
                        cenDetalle.CodigoProdSunat = dr.CodigoProdSunat;
                        cenDetalle.Producto = dr.Producto;
                        cenDetalle.ValorUnitario = valorUnitario;
                        cenDetalle.SumTribxItem = Math.Round(sumaTributosItem, 2); 
                        cenDetalle.CodTribIGV = dr.CodTribIGV;
                        cenDetalle.MontoIGVItem = Math.Round(montoIgvItem, 2); 
                        cenDetalle.baseImponible = baseImponible; 
                        cenDetalle.nombreTributo = dr.nombreTributo;
                        cenDetalle.codAfecIGV = dr.codAfecIGV;
                        cenDetalle.tributoAfectacion = dr.tributoAfectacion;
                        cenDetalle.tributoPorcentaje = dr.tributoPorcentaje;
                        cenDetalle.codigTributoISC = dr.codigTributoISC;
                        cenDetalle.mtoISCxitem = dr.mtoISCxitem;
                        cenDetalle.BaseImpISC = dr.BaseImpISC;
                        cenDetalle.NomTribxItemISC = dr.NomTribxItemISC;
                        cenDetalle.CodTiposTributoISC = dr.CodTiposTributoISC;
                        cenDetalle.TipoSistemaISC = dr.TipoSistemaISC;
                        cenDetalle.PorcImoISC = dr.PorcImoISC;
                        cenDetalle.CodTipoTribOtros = dr.CodTipoTribOtros;
                        cenDetalle.MtoTribOTrosxItem = dr.MtoTribOTrosxItem;
                        cenDetalle.BaseImpOtroxItem = dr.BaseImpOtroxItem;
                        cenDetalle.NomTribOtroxItem = dr.NomTribOtroxItem;
                        cenDetalle.CodTipTribOtroxItem = dr.CodTipTribOtroxItem;
                        cenDetalle.PorTribOtroXItem = dr.PorTribOtroXItem;
                        cenDetalle.codTriIcbper = dr.codTriIcbper;
                        cenDetalle.mtoTriIcbperItem = dr.mtoTriIcbperItem;
                        cenDetalle.ctdBolsasTriIcbperItem = dr.ctdBolsasTriIcbperItem;
                        cenDetalle.nomTributoIcbperItem = dr.nomTributoIcbperItem;
                        cenDetalle.codTipTributoIcbperItem = dr.codTipTributoIcbperItem;
                        cenDetalle.mtoTriIcbperUnidad = dr.mtoTriIcbperUnidad;
                        cenDetalle.PrecioVtaUnitario = Math.Round(precioVtaUnitario, 2);
                        cenDetalle.valor_item = valorItem;
                        cenDetalle.ValorRefUnitario_Gratuito = dr.ValorRefUnitario_Gratuito;
                        cenDetalle.precioBaseItem = precioBaseItem;
                        cenDetalle.unidadMedidaNota = dr.unidadMedidaNota;
                        cenDetalle.codigoProductoNota = dr.codigoProductoNota;

                        detalle.Add(cenDetalle);
                    }

                    comprobanteFinal.listaDetalle = detalle;

                }
                else
                {
                    comprobanteFinal = comprobante;
                }

                cenEmp = clnsoap.searchSoap();

                docPse.usuario = cenEmp.usuario_soap;
                docPse.clave = cenEmp.password_soap;
                docPse.serie = serie;
                docPse.numero = numero;
                docPse.idtipodocumento = cab.tipoComprobante;

                docPse.tipoNotaCredNotaDeb = codigo_tipo_nota;
                docPse.idtipodocumentonota = Convert.ToInt32(comprobanteFinal.tipo_comprobante).ToString();
                docPse.tipoOperacion = comprobanteFinal.tipoOperacion;
                
                docPse.tipodoccliente = comprobanteFinal.TipoDocEmpresa;
                docPse.nrodoccliente = comprobanteFinal.NumDocEmpresa;
                docPse.nombrecliente = comprobanteFinal.NombEmpresa;
                docPse.direccioncliente = cab.direccionCliente;
                docPse.emailcliente = "";
                docPse.fecha = comprobanteFinal.Cab_Fac_Fecha;
                docPse.hora = getDateHourNowLima();
                docPse.fechavencimiento = null;
                docPse.glosa = "PRODUCCION";
                docPse.moneda = comprobanteFinal.tipo_moneda;
                docPse.tipocambio = "0";
                docPse.descuento = "0";
                docPse.igvventa = Math.Round(comprobanteFinal.SumTotalTributos, 2).ToString();
                docPse.iscventa = "0";
                docPse.otrosTributos = "0";
                docPse.otrosCargos = Math.Round(comprobanteFinal.otrosCargos, 2).ToString();
                docPse.importeventa = Math.Round(comprobanteFinal.TotalValorVenta, 2).ToString();
                docPse.regimenpercep = "";
                docPse.porcentpercep = "0";
                docPse.importepercep = "0";
                docPse.porcentdetrac = "0";
                docPse.importedetrac = "0";
                docPse.importerefdetrac = "0";

                docPse.importefinal = Math.Round(comprobanteFinal.ImporteTotalVenta, 2).ToString();
                docPse.importeanticipo = Math.Round(comprobanteFinal.TotalAnticipos, 2).ToString();
                docPse.indicaAnticipo = "";
                docPse.estado = "N";
                docPse.situacion = "P";
                docPse.estransgratuita = "NO";
                docPse.montoFise = "0";
                docPse.formapago = "Contado";
                docPse.hasRetencionIgv = "NO";
                docPse.porcRetencionIgv = "0";
                docPse.impRetencionIgv = "0";
                docPse.impOperacionRetencionIgv = "0";

               
                    int lineId = 1;
                foreach (CENEstructuraDetalleComprobante det in comprobanteFinal.listaDetalle)
                {

                    detalleDoc = new ListaDetalleDocumento();
                    detalleDoc.lineid = lineId.ToString();
                    detalleDoc.codigo = (det.codigoProducto == "-") ? "" : det.codigoProducto;
                    detalleDoc.codigoSunat = (det.CodigoProdSunat == "-") ? "" : det.CodigoProdSunat;
                    detalleDoc.producto = det.Producto;
                    detalleDoc.afectoIgv = "SI";
                    detalleDoc.afectoIsc = "NO";
                    detalleDoc.sistemaISC = "";
                    detalleDoc.afectacionigv = det.tributoAfectacion;
                    detalleDoc.unidad = det.UnidadMedida;
                    detalleDoc.cantidad = det.cantidad.ToString();
                    detalleDoc.preciounitario = det.PrecioVtaUnitario.ToString();
                    detalleDoc.precioreferencial = det.PrecioVtaUnitario.ToString();
                    detalleDoc.valorunitario = det.precioBaseItem.ToString();
                    detalleDoc.valorbruto = Math.Round(Convert.ToDecimal(det.baseImponible.ToString()), 2).ToString();
                    detalleDoc.valordscto = "0";
                    detalleDoc.valorcargo = "0";
                    detalleDoc.valorventa = Math.Round(Convert.ToDecimal(det.valor_item.ToString()), 2).ToString();
                    detalleDoc.isc = "0";
                    detalleDoc.igv = det.MontoIGVItem.ToString();
                    detalleDoc.total = Math.Round(det.PrecioVtaUnitario * det.cantidad, 2).ToString();
                    detalleDoc.factorIgv = det.tributoPorcentaje.ToString();
                    detalleDoc.factorIsc = "0";

                    lineId++;
                    detalleFinal.Add(detalleDoc);

                }

                docPse.listaDetalleDocumento = detalleFinal;

                detalleReferencia = new ListaDocumentoReferencia();
                detalleReferencia.iddocumentoref = comprobante.tipo_comprobante + comprobante.serieDocumento;
                detalleReferencia.idtipodocumentoref = comprobante.tipo_comprobante;
                detalleReferencia.numerodocref = comprobante.serieDocumento;

                ListadetalleReferencia.Add(detalleReferencia);

                docPse.listaDocumentoReferencia = ListadetalleReferencia;

                return docPse;
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        public static Boolean GuardarDocsS3(CENResponsePse response , string tipoDoc, ref string rutaArchivoPdfCompleto)
        {
            try
            {
                string datosEmpresa = Helper.ObtenerValorParametro(CENConstantes.CONST_2);

                string Ruta = datosEmpresa.Split('|')[1];
                string RUC = datosEmpresa.Split('|')[0];

                string rutaPdf = Ruta + @"REPO\";
                string rutaXml = Ruta + @"FIRMA\";
                string rutaCdr = Ruta + @"RPTA\";

                Helper.CreateIfMissing(rutaPdf);
                Helper.CreateIfMissing(rutaXml);
                Helper.CreateIfMissing(rutaCdr);

                string nombreArchivo = RUC + "-" + tipoDoc + "-" + response.rptaRegistroCpeDoc.comprobante;


                if (response.rptaRegistroCpeDoc.ruta_pdf.Trim().Length > 0)
                {
                    using (WebClient client = new WebClient())
                    {
                        // Download data.
                        byte[] arr = client.DownloadData(response.rptaRegistroCpeDoc.ruta_pdf);
                        File.WriteAllBytes(rutaPdf + nombreArchivo+ ".PDF", arr);
                    }
                    rutaArchivoPdfCompleto = rutaPdf + nombreArchivo + ".PDF";
                }

                if (response.rptaRegistroCpeDoc.ruta_xml.Trim().Length > 0)
                {
                    using (WebClient client = new WebClient())
                    {
                        // Download data.
                        byte[] arr = client.DownloadData(response.rptaRegistroCpeDoc.ruta_xml);
                        File.WriteAllBytes(rutaXml + nombreArchivo+".XML", arr);
                    }
                }

                if (response.rptaRegistroCpeDoc.ruta_cdr.Trim().Length >0 )
                {
                    using (WebClient client = new WebClient())
                    {
                        // Download data.
                        byte[] arr = client.DownloadData(response.rptaRegistroCpeDoc.ruta_cdr);
                        File.WriteAllBytes(rutaCdr + "R-"+ nombreArchivo+".XML", arr);
                    }
                }

                return true;

            }
            catch (Exception)
            {

                return false;
            }
           


        }

        public static Boolean GuardarResponseDocumento(int id_comprobante, CENResponsePse response)
        {


            try
            {
                CLNComprobante cln = new CLNComprobante();

                cln.insertarResponseDocumento(id_comprobante, response);

                return true;

            }
            catch (Exception)
            {

                return false;
            }
        }

        public static void SyncEstadosCpePSE()
        {

            CENRequestSyncCpe request;
            List<CENRequestSyncCpe> listaRequest = new List<CENRequestSyncCpe>();

            List<CENListadoComprobantes> listaDocumentos;
            CenSearchComprobantes search01;
            try
            {


                search01 = new CenSearchComprobantes
                {
                    fecha_inicio = "",
                    fecha_fin = "",
                    dni_ruc = "",
                    name_cliente = "",
                    tipo_comprobante = "0",
                    series_number = "",
                    codigo_venta = 0,
                    _id_estado = "01,02,03,09"
                };


                CLNDocumentTypes cln0 = new CLNDocumentTypes();
                listaDocumentos = cln0.search_listado_comprobantes(search01);

                foreach (var item in listaDocumentos)
                {
                    string[] docs = item.DOCUMENTO.Split('-');

                    request = new CENRequestSyncCpe();
                    request.tipodocumento_id = item.ID_TC;
                    request.serie = docs[0];
                    request.numero = docs[1];
                    //request.send_s3 = false;
                    request.send_s3 = true; // cambiamos a true para descargar los xml de cdr (respuesta)


                    listaRequest.Add(request);
                }

                funciones.EnviarActualizarConsultaEstados(listaRequest);

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        public static void EnviarActualizarConsultaEstados(List<CENRequestSyncCpe> docPse)
        {
            //DESCRIPCION: FUNCIONENVIAR TRAMA DE CONSULTA DE ESTADOS A WEB API PSE

            string url_ws, urlWA = "", dataSerializado = "";
            string api_consultar_estados;

            List<CENResponseSyncCpe> response = new List<CENResponseSyncCpe>();

            CenSearchComprobantes search01;
            CLNDocumentTypes cln0;
            List<CENListadoComprobantes> listaDocumentos;
            CLNDocumentTypes clndt ;
            try
            {

                string datosEmpresa = Helper.ObtenerValorParametro(CENConstantes.CONST_2);

                string Ruta = datosEmpresa.Split('|')[1];
                string RUC = datosEmpresa.Split('|')[0];

                string rutaPdf = Ruta + @"REPO\";
                string rutaXml = Ruta + @"FIRMA\";
                string rutaCdr = Ruta + @"RPTA\";

                Helper.CreateIfMissing(rutaPdf);
                Helper.CreateIfMissing(rutaXml);
                Helper.CreateIfMissing(rutaCdr);

                CENParametro parametro;
                CLNConcepto clncconcepto;

                clncconcepto = new CLNConcepto();
                parametro = new CENParametro();
                parametro = clncconcepto.buscarParametro(2003);
                url_ws = parametro.parametro.Trim();


                clncconcepto = new CLNConcepto();
                parametro = new CENParametro();
                parametro = clncconcepto.buscarParametro(2006);
                api_consultar_estados = parametro.parametro.Trim();

                urlWA = url_ws + api_consultar_estados;

                var serializerSettings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
                dataSerializado = JsonConvert.SerializeObject(docPse, Newtonsoft.Json.Formatting.Indented, serializerSettings);

                response = (List<CENResponseSyncCpe>)sendWebApiWithToken(urlWA, dataSerializado, typeof(CENResponseSyncCpe));
                
                if(response != null)
                {
                    //actualizamos los estados 

                    foreach(CENResponseSyncCpe item in response.Where(r => r.situacion != "P"))
                    {
                        string estado = CENConstantes.ID_ESTADO_REGISTRADO;
                        bool descarga_cdr = false;

                        if (item.situacion == "A")
                        {
                            estado = CENConstantes.ID_ESTADO;
                            descarga_cdr = true;
                        }

                        if (item.situacion == "O")
                        {
                            estado = CENConstantes.ID_ESTADO_OBSERVADO;
                            descarga_cdr = true;
                        }
                        if (item.situacion == "E" || item.situacion == "R")
                        {
                            estado = CENConstantes.ID_ESTADO_RECHAZADO;
                            descarga_cdr = false;
                        }

                        search01 = new CenSearchComprobantes
                        {
                            fecha_inicio = "",
                            fecha_fin = "",
                            dni_ruc = "",
                            name_cliente = "",
                            tipo_comprobante = "0",
                            series_number = item.serie+"-"+item.numero,
                            codigo_venta = 0,
                            _id_estado = ""
                        };

                        cln0 = new CLNDocumentTypes();
                        listaDocumentos = cln0.search_listado_comprobantes(search01);

                        clndt = new CLNDocumentTypes();
                        clndt.setEstadoDocumento(listaDocumentos[0].VENTA, estado);

                        
                        if(descarga_cdr)
                        {
                            //descargamos el xml cdr


                            if (item.url_cdr.Trim().Length > 0)
                            {
                                using (WebClient client = new WebClient())
                                {
                                    string nombreArchivo = RUC + "-" + item.tipodocumento_id + "-" + item.serie+"-"+item.numero;
                                    // Download data.
                                    byte[] arr = client.DownloadData(item.url_cdr.Trim());
                                    File.WriteAllBytes(rutaCdr + "R-" + nombreArchivo + ".XML", arr);
                                }
                            }
                        }
                    }


                }
                
                
                


            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("401"))
                {
                    CENResponsePse responseIS = new CENResponsePse();
                    responseIS = funciones.IniciarSessionPse();
                    if (responseIS.rptaRegistroCpeDoc.flagVerificacion)
                    {
                        EnviarActualizarConsultaEstados(docPse);
                        return;
                    }
                    else
                    {
                        throw new Exception("ERROR AL INICIAR SESSION Y OBTENER TOKEN");
                    }
                }

                throw ex;
            }



        }


        public static bool ActualizarUrlPdfFechVenc(List<CENRequestSyncCpe> docPse, int id_doc)
        {
            //DESCRIPCION: FUNCIONENVIAR TRAMA DE CONSULTA DE ESTADOS A WEB API PSE

            string url_ws, urlWA, dataSerializado;
            string api_consultar_estados;

            List<CENResponseSyncCpe> response;
            CENTablaResponseDoc tbl_resp_doc = new CENTablaResponseDoc();
            CLNComprobante clnCpe = new CLNComprobante();
            try
            {
                CENParametro parametro;
                CLNConcepto clncconcepto;

                clncconcepto = new CLNConcepto();
                parametro = new CENParametro();
                parametro = clncconcepto.buscarParametro(2003);
                url_ws = parametro.parametro.Trim();


                clncconcepto = new CLNConcepto();
                parametro = new CENParametro();
                parametro = clncconcepto.buscarParametro(2006);
                api_consultar_estados = parametro.parametro.Trim();

                urlWA = url_ws + api_consultar_estados;

                var serializerSettings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
                dataSerializado = JsonConvert.SerializeObject(docPse, Newtonsoft.Json.Formatting.Indented, serializerSettings);

                response = (List<CENResponseSyncCpe>)sendWebApiWithToken(urlWA, dataSerializado, typeof(CENResponseSyncCpe));

                if (response != null)
                {
                    tbl_resp_doc.url_pdf = response[0].url_pdf;
                    tbl_resp_doc.url_xml = response[0].url_xml;
                    tbl_resp_doc.url_cdr = response[0].url_cdr;

                    //actualizamos 
                    if (clnCpe.ActualizarUrlPdfFechas(id_doc, tbl_resp_doc)) return true;
                    else return false;

                }
                else
                {
                    return false;
                }



            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("401"))
                {
                    CENResponsePse responseIS = new CENResponsePse();
                    responseIS = funciones.IniciarSessionPse();
                    if (responseIS.rptaRegistroCpeDoc.flagVerificacion)
                    {
                        ActualizarUrlPdfFechVenc(docPse, id_doc);
                        
                    }
                    else
                    {
                        throw new Exception("ERROR AL INICIAR SESSION Y OBTENER TOKEN");
                    }
                }

                throw ex;
            }

        }


        public static void EnviarUrlPdfWhatsapp(int id, string tipodoc, string series, string numeroWa)
        {
            CLNComprobante cln = new CLNComprobante();
            CENTablaResponseDoc tbl_resp_doc;
            CENRequestSyncCpe request;
            List<CENRequestSyncCpe> listaRequest = new List<CENRequestSyncCpe>();
            string textoTipoDoc = "";
            string textoBody;
            string codif;
            string urlWA_inicial;
            string urlWA_completo;
            string numeroCelular;
            bool actualizaUrl;
            try
            {
                urlWA_inicial = "https://wa.me/";

                tbl_resp_doc = new CENTablaResponseDoc();
                tbl_resp_doc = cln.buscarUrlPdfDoc(id);

                //revisamos el vencimiento del pdf
                DateTime today = DateTime.Today;
                TimeSpan tSpan = today - tbl_resp_doc.fecha_vencimiento_url;
                int dias_pasados = Convert.ToInt32(tSpan.Days);

                if(dias_pasados>-2)
                {
                    //volvemos a hacer la peticion al webapi por la url

                    string[] docs = series.Split('-');

                    request = new CENRequestSyncCpe();
                    request.tipodocumento_id = tipodoc;
                    request.serie = docs[0];
                    request.numero = docs[1];
                    request.send_s3 = true;

                    listaRequest.Add(request);

                    
                    actualizaUrl= ActualizarUrlPdfFechVenc(listaRequest, id);
                    
                    if(actualizaUrl)
                    {
                        tbl_resp_doc = new CENTablaResponseDoc();
                        tbl_resp_doc = cln.buscarUrlPdfDoc(id);

                    }

                }


                if (tbl_resp_doc.url_pdf != "")
                {
                
                    if (tipodoc == CENConstantes.FACTURA)
                    {
                        textoTipoDoc = CENConstantes.FACTURA_ELECTRONICA;
                    }
                    if (tipodoc == CENConstantes.BOLETA)
                    {
                        textoTipoDoc = CENConstantes.BOLETA_ELECTRONICA;
                    }
                    if (tipodoc == CENConstantes.NOTACREDITO)
                    {
                        textoTipoDoc = CENConstantes.NOTACREDITO_ELECTRONICA;
                    }
                    textoBody       = "Puede descargar la " 
                                    + textoTipoDoc + " Nro. " + series 
                                    + " en el siguiente enlace: " 
                                    + tbl_resp_doc.url_pdf;

                    codif           = HttpUtility.UrlEncode(textoBody);
                    numeroCelular   = "+51" + numeroWa;

                    urlWA_completo = urlWA_inicial + numeroCelular + "? text=" + codif;

                    Process.Start(urlWA_completo);
                }
                else
                {
                    throw new Exception("Error a la hora de obtener el url del pdf");
                }

            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }



        public static string getDateNowLima()
        {

            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(CENConstantes.g_cost_nameTimeDefect);

            DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, cstZone);
            return cstTime.ToString(CENConstantes.g_const_formfech, new CultureInfo(CENConstantes.const_CultureInfoPeru));

        }
        public static string getDateHourNowLima()
        {
            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(CENConstantes.g_cost_nameTimeDefect);
            DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, cstZone);
            return cstTime.ToString(CENConstantes.g_const_formhora, new CultureInfo(CENConstantes.const_CultureInfoPeru));
        }

        public static DateTime getDateTimeNowLima()
        {
            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(CENConstantes.g_cost_nameTimeDefect);
            DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, cstZone);
            return cstTime;
        }







    }
}
