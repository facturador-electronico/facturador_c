﻿
namespace ferreteriaApp
{
    partial class listadoComprobantes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel_cabecera = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.btnUpdateEstados = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbTipoComprobante = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_buscar_sale_note = new System.Windows.Forms.Button();
            this.txtVendedor = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.chkFechas = new System.Windows.Forms.CheckBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txt_fecha_fin = new System.Windows.Forms.DateTimePicker();
            this.txt_fecha_inicio = new System.Windows.Forms.DateTimePicker();
            this.txt_codigo_venta = new System.Windows.Forms.TextBox();
            this.txt_number = new System.Windows.Forms.TextBox();
            this.txt_cliente = new System.Windows.Forms.TextBox();
            this.txt_serie = new System.Windows.Forms.TextBox();
            this.detalleLista = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.listaToExcel = new System.Windows.Forms.DataGridView();
            this.panel5 = new System.Windows.Forms.Panel();
            this.bnt_next = new System.Windows.Forms.Button();
            this.lbl_pagina = new System.Windows.Forms.Label();
            this.btn_preview = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_totalPaginas = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.panel_body = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel_cabecera.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.detalleLista)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listaToExcel)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel_body.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_cabecera
            // 
            this.panel_cabecera.AutoSize = true;
            this.panel_cabecera.BackColor = System.Drawing.Color.GhostWhite;
            this.panel_cabecera.Controls.Add(this.button4);
            this.panel_cabecera.Controls.Add(this.btnUpdateEstados);
            this.panel_cabecera.Controls.Add(this.label3);
            this.panel_cabecera.Controls.Add(this.cmbTipoComprobante);
            this.panel_cabecera.Controls.Add(this.label1);
            this.panel_cabecera.Controls.Add(this.btn_buscar_sale_note);
            this.panel_cabecera.Controls.Add(this.txtVendedor);
            this.panel_cabecera.Controls.Add(this.label7);
            this.panel_cabecera.Controls.Add(this.label6);
            this.panel_cabecera.Controls.Add(this.label5);
            this.panel_cabecera.Controls.Add(this.chkFechas);
            this.panel_cabecera.Controls.Add(this.label22);
            this.panel_cabecera.Controls.Add(this.label24);
            this.panel_cabecera.Controls.Add(this.label4);
            this.panel_cabecera.Controls.Add(this.label11);
            this.panel_cabecera.Controls.Add(this.label13);
            this.panel_cabecera.Controls.Add(this.txt_fecha_fin);
            this.panel_cabecera.Controls.Add(this.txt_fecha_inicio);
            this.panel_cabecera.Controls.Add(this.txt_codigo_venta);
            this.panel_cabecera.Controls.Add(this.txt_number);
            this.panel_cabecera.Controls.Add(this.txt_cliente);
            this.panel_cabecera.Controls.Add(this.txt_serie);
            this.panel_cabecera.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_cabecera.Location = new System.Drawing.Point(0, 0);
            this.panel_cabecera.Name = "panel_cabecera";
            this.panel_cabecera.Size = new System.Drawing.Size(1050, 209);
            this.panel_cabecera.TabIndex = 45;
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Image = global::ferreteriaApp.Properties.Resources.excel_32;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(795, 167);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(103, 39);
            this.button4.TabIndex = 67;
            this.button4.Text = "Exportar";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnUpdateEstados
            // 
            this.btnUpdateEstados.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdateEstados.FlatAppearance.BorderSize = 0;
            this.btnUpdateEstados.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdateEstados.Image = global::ferreteriaApp.Properties.Resources.sync_32;
            this.btnUpdateEstados.Location = new System.Drawing.Point(157, 168);
            this.btnUpdateEstados.Name = "btnUpdateEstados";
            this.btnUpdateEstados.Size = new System.Drawing.Size(44, 35);
            this.btnUpdateEstados.TabIndex = 51;
            this.btnUpdateEstados.UseVisualStyleBackColor = true;
            this.btnUpdateEstados.Click += new System.EventHandler(this.button1_Click);
            this.btnUpdateEstados.MouseLeave += new System.EventHandler(this.btnUpdateEstados_MouseLeave);
            this.btnUpdateEstados.MouseHover += new System.EventHandler(this.btnUpdateEstados_MouseHover);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(17, 176);
            this.label3.MaximumSize = new System.Drawing.Size(140, 20);
            this.label3.MinimumSize = new System.Drawing.Size(140, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 20);
            this.label3.TabIndex = 50;
            this.label3.Text = "Sincronizar Estado ";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbTipoComprobante
            // 
            this.cmbTipoComprobante.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbTipoComprobante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipoComprobante.FormattingEnabled = true;
            this.cmbTipoComprobante.Location = new System.Drawing.Point(184, 54);
            this.cmbTipoComprobante.Name = "cmbTipoComprobante";
            this.cmbTipoComprobante.Size = new System.Drawing.Size(319, 21);
            this.cmbTipoComprobante.TabIndex = 49;
            this.cmbTipoComprobante.SelectedIndexChanged += new System.EventHandler(this.cmbTipoComprobante_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.DimGray;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(17, 55);
            this.label1.MaximumSize = new System.Drawing.Size(140, 20);
            this.label1.MinimumSize = new System.Drawing.Size(140, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 20);
            this.label1.TabIndex = 48;
            this.label1.Text = "Tipo Comprobante";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_buscar_sale_note
            // 
            this.btn_buscar_sale_note.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_buscar_sale_note.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btn_buscar_sale_note.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_buscar_sale_note.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_buscar_sale_note.Image = global::ferreteriaApp.Properties.Resources.search_32;
            this.btn_buscar_sale_note.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_buscar_sale_note.Location = new System.Drawing.Point(905, 167);
            this.btn_buscar_sale_note.Name = "btn_buscar_sale_note";
            this.btn_buscar_sale_note.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btn_buscar_sale_note.Size = new System.Drawing.Size(95, 39);
            this.btn_buscar_sale_note.TabIndex = 47;
            this.btn_buscar_sale_note.Text = "Buscar";
            this.btn_buscar_sale_note.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_buscar_sale_note.UseVisualStyleBackColor = true;
            this.btn_buscar_sale_note.Click += new System.EventHandler(this.btn_buscar_sale_note_Click);
            // 
            // txtVendedor
            // 
            this.txtVendedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVendedor.Enabled = false;
            this.txtVendedor.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtVendedor.Location = new System.Drawing.Point(712, 132);
            this.txtVendedor.Name = "txtVendedor";
            this.txtVendedor.Size = new System.Drawing.Size(286, 20);
            this.txtVendedor.TabIndex = 46;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.DimGray;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(545, 132);
            this.label7.MaximumSize = new System.Drawing.Size(140, 20);
            this.label7.MinimumSize = new System.Drawing.Size(140, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(140, 20);
            this.label7.TabIndex = 45;
            this.label7.Text = "Vendedor";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.DimGray;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(545, 89);
            this.label6.MaximumSize = new System.Drawing.Size(140, 20);
            this.label6.MinimumSize = new System.Drawing.Size(140, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(140, 20);
            this.label6.TabIndex = 44;
            this.label6.Text = "Cliente";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.DimGray;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(17, 135);
            this.label5.MaximumSize = new System.Drawing.Size(140, 20);
            this.label5.MinimumSize = new System.Drawing.Size(140, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(140, 20);
            this.label5.TabIndex = 43;
            this.label5.Text = "Fechas";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkFechas
            // 
            this.chkFechas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkFechas.AutoSize = true;
            this.chkFechas.Location = new System.Drawing.Point(165, 138);
            this.chkFechas.Name = "chkFechas";
            this.chkFechas.Size = new System.Drawing.Size(15, 14);
            this.chkFechas.TabIndex = 42;
            this.chkFechas.UseVisualStyleBackColor = true;
            this.chkFechas.CheckedChanged += new System.EventHandler(this.chkFechas_CheckedChanged);
            // 
            // label22
            // 
            this.label22.Image = global::ferreteriaApp.Properties.Resources.icon_edit_note_321;
            this.label22.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label22.Location = new System.Drawing.Point(3, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(37, 31);
            this.label22.TabIndex = 20;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.MediumBlue;
            this.label24.Location = new System.Drawing.Point(46, 9);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(147, 16);
            this.label24.TabIndex = 0;
            this.label24.Text = "Filtros de Busqueda";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(334, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(12, 16);
            this.label4.TabIndex = 40;
            this.label4.Text = "-";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.DimGray;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(17, 97);
            this.label11.MaximumSize = new System.Drawing.Size(140, 20);
            this.label11.MinimumSize = new System.Drawing.Size(140, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(140, 20);
            this.label11.TabIndex = 28;
            this.label11.Text = "Documento";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.DimGray;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(545, 52);
            this.label13.MaximumSize = new System.Drawing.Size(140, 20);
            this.label13.MinimumSize = new System.Drawing.Size(140, 20);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(140, 20);
            this.label13.TabIndex = 26;
            this.label13.Text = "Código Venta";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_fecha_fin
            // 
            this.txt_fecha_fin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_fecha_fin.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.txt_fecha_fin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txt_fecha_fin.Location = new System.Drawing.Point(352, 133);
            this.txt_fecha_fin.Name = "txt_fecha_fin";
            this.txt_fecha_fin.Size = new System.Drawing.Size(151, 20);
            this.txt_fecha_fin.TabIndex = 5;
            // 
            // txt_fecha_inicio
            // 
            this.txt_fecha_inicio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_fecha_inicio.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.txt_fecha_inicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txt_fecha_inicio.Location = new System.Drawing.Point(184, 133);
            this.txt_fecha_inicio.Name = "txt_fecha_inicio";
            this.txt_fecha_inicio.Size = new System.Drawing.Size(144, 20);
            this.txt_fecha_inicio.TabIndex = 4;
            this.txt_fecha_inicio.Tag = "";
            // 
            // txt_codigo_venta
            // 
            this.txt_codigo_venta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_codigo_venta.Location = new System.Drawing.Point(712, 50);
            this.txt_codigo_venta.MaxLength = 11;
            this.txt_codigo_venta.Name = "txt_codigo_venta";
            this.txt_codigo_venta.Size = new System.Drawing.Size(118, 20);
            this.txt_codigo_venta.TabIndex = 1;
            this.txt_codigo_venta.Enter += new System.EventHandler(this.txt_codigo_venta_Enter);
            this.txt_codigo_venta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_codigo_venta_KeyPress);
            this.txt_codigo_venta.Leave += new System.EventHandler(this.txt_codigo_venta_Leave);
            // 
            // txt_number
            // 
            this.txt_number.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_number.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txt_number.Location = new System.Drawing.Point(352, 93);
            this.txt_number.MaxLength = 8;
            this.txt_number.Name = "txt_number";
            this.txt_number.Size = new System.Drawing.Size(151, 20);
            this.txt_number.TabIndex = 3;
            this.txt_number.Enter += new System.EventHandler(this.txt_number_Enter);
            this.txt_number.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_number_KeyPress);
            this.txt_number.Leave += new System.EventHandler(this.txt_number_Leave);
            // 
            // txt_cliente
            // 
            this.txt_cliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_cliente.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txt_cliente.Location = new System.Drawing.Point(712, 89);
            this.txt_cliente.MaxLength = 120;
            this.txt_cliente.Name = "txt_cliente";
            this.txt_cliente.Size = new System.Drawing.Size(286, 20);
            this.txt_cliente.TabIndex = 6;
            this.txt_cliente.Enter += new System.EventHandler(this.txt_cliente_Enter);
            this.txt_cliente.Leave += new System.EventHandler(this.txt_cliente_Leave);
            // 
            // txt_serie
            // 
            this.txt_serie.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_serie.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txt_serie.Location = new System.Drawing.Point(184, 94);
            this.txt_serie.MaxLength = 4;
            this.txt_serie.Name = "txt_serie";
            this.txt_serie.Size = new System.Drawing.Size(144, 20);
            this.txt_serie.TabIndex = 2;
            this.txt_serie.Enter += new System.EventHandler(this.txt_serie_Enter);
            this.txt_serie.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_serie_KeyPress);
            this.txt_serie.Leave += new System.EventHandler(this.txt_serie_Leave);
            // 
            // detalleLista
            // 
            this.detalleLista.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.detalleLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.detalleLista.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detalleLista.Location = new System.Drawing.Point(0, 0);
            this.detalleLista.Name = "detalleLista";
            this.detalleLista.ReadOnly = true;
            this.detalleLista.Size = new System.Drawing.Size(1050, 297);
            this.detalleLista.TabIndex = 46;
            this.detalleLista.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.detalleLista_CellContentClick_1);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.listaToExcel);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 334);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1050, 43);
            this.panel1.TabIndex = 47;
            // 
            // listaToExcel
            // 
            this.listaToExcel.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.listaToExcel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listaToExcel.Location = new System.Drawing.Point(6, 7);
            this.listaToExcel.Name = "listaToExcel";
            this.listaToExcel.ReadOnly = true;
            this.listaToExcel.Size = new System.Drawing.Size(57, 30);
            this.listaToExcel.TabIndex = 75;
            this.listaToExcel.Visible = false;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.bnt_next);
            this.panel5.Controls.Add(this.lbl_pagina);
            this.panel5.Controls.Add(this.btn_preview);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.lbl_totalPaginas);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(815, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(235, 43);
            this.panel5.TabIndex = 55;
            // 
            // bnt_next
            // 
            this.bnt_next.AutoEllipsis = true;
            this.bnt_next.BackColor = System.Drawing.Color.DodgerBlue;
            this.bnt_next.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bnt_next.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_next.ForeColor = System.Drawing.Color.White;
            this.bnt_next.Location = new System.Drawing.Point(170, 6);
            this.bnt_next.Name = "bnt_next";
            this.bnt_next.Size = new System.Drawing.Size(26, 25);
            this.bnt_next.TabIndex = 54;
            this.bnt_next.Text = ">";
            this.bnt_next.UseVisualStyleBackColor = false;
            this.bnt_next.Click += new System.EventHandler(this.bnt_next_Click);
            // 
            // lbl_pagina
            // 
            this.lbl_pagina.AutoSize = true;
            this.lbl_pagina.Location = new System.Drawing.Point(87, 13);
            this.lbl_pagina.Name = "lbl_pagina";
            this.lbl_pagina.Size = new System.Drawing.Size(13, 13);
            this.lbl_pagina.TabIndex = 50;
            this.lbl_pagina.Text = "0";
            // 
            // btn_preview
            // 
            this.btn_preview.AutoEllipsis = true;
            this.btn_preview.BackColor = System.Drawing.Color.DodgerBlue;
            this.btn_preview.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_preview.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_preview.ForeColor = System.Drawing.Color.White;
            this.btn_preview.Location = new System.Drawing.Point(35, 7);
            this.btn_preview.Name = "btn_preview";
            this.btn_preview.Size = new System.Drawing.Size(26, 25);
            this.btn_preview.TabIndex = 53;
            this.btn_preview.Text = "<";
            this.btn_preview.UseVisualStyleBackColor = false;
            this.btn_preview.Click += new System.EventHandler(this.btn_preview_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(114, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(12, 13);
            this.label2.TabIndex = 51;
            this.label2.Text = "/";
            // 
            // lbl_totalPaginas
            // 
            this.lbl_totalPaginas.AutoSize = true;
            this.lbl_totalPaginas.Location = new System.Drawing.Point(140, 13);
            this.lbl_totalPaginas.Name = "lbl_totalPaginas";
            this.lbl_totalPaginas.Size = new System.Drawing.Size(13, 13);
            this.lbl_totalPaginas.TabIndex = 52;
            this.lbl_totalPaginas.Text = "0";
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblError.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(0, 0);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(34, 13);
            this.lblError.TabIndex = 49;
            this.lblError.Text = "Error";
            // 
            // panel_body
            // 
            this.panel_body.Controls.Add(this.panel6);
            this.panel_body.Controls.Add(this.panel4);
            this.panel_body.Controls.Add(this.panel1);
            this.panel_body.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_body.Location = new System.Drawing.Point(0, 209);
            this.panel_body.Name = "panel_body";
            this.panel_body.Size = new System.Drawing.Size(1050, 377);
            this.panel_body.TabIndex = 50;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.detalleLista);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 37);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1050, 297);
            this.panel6.TabIndex = 51;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lblError);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1050, 37);
            this.panel4.TabIndex = 50;
            // 
            // listadoComprobantes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1050, 586);
            this.Controls.Add(this.panel_body);
            this.Controls.Add(this.panel_cabecera);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "listadoComprobantes";
            this.Text = "listadoComprobantes";
            this.panel_cabecera.ResumeLayout(false);
            this.panel_cabecera.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.detalleLista)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listaToExcel)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel_body.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel_cabecera;
        private System.Windows.Forms.Button btn_buscar_sale_note;
        private System.Windows.Forms.TextBox txtVendedor;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkFechas;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker txt_fecha_fin;
        private System.Windows.Forms.DateTimePicker txt_fecha_inicio;
        private System.Windows.Forms.TextBox txt_codigo_venta;
        private System.Windows.Forms.TextBox txt_number;
        private System.Windows.Forms.TextBox txt_cliente;
        private System.Windows.Forms.TextBox txt_serie;
        private System.Windows.Forms.DataGridView detalleLista;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button bnt_next;
        private System.Windows.Forms.Button btn_preview;
        private System.Windows.Forms.Label lbl_totalPaginas;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_pagina;
        private System.Windows.Forms.ComboBox cmbTipoComprobante;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel_body;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnUpdateEstados;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DataGridView listaToExcel;
    }
}