﻿using System;

using System.Windows.Forms;

namespace ferreteriaApp
{
    public partial class numCelular : Form
    {
        public numCelular()
        {
            
            InitializeComponent();



        }
        private bool bandera = false;
        private void enterClick()
        {
            if (this.txt_numero.Text.Trim() == "")
            {
                MessageBox.Show("Debe ingresar un numero");
                this.txt_numero.Focus();
                this.bandera = false;
                return;
            }
       
            else if (this.txt_numero.Text.Trim().Length > 10 || this.txt_numero.Text.Trim().Length < 9)
            {
                MessageBox.Show("Número de celular debe ser de 9 digitos");
                this.txt_numero.Focus();
                this.bandera = false;
                return;
            }
            else
            {
                this.bandera = true;
                listadoComprobantes formPadre = Owner as listadoComprobantes;
                formPadre._numero_celular_whatsapp = this.txt_numero.Text.Trim();
                formPadre._bandera_celular_whatsapp = true;
                this.Close();

            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.enterClick();




        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.bandera = true;
            listadoComprobantes formPadre = Owner as listadoComprobantes;
            formPadre._numero_celular_whatsapp = "";
            formPadre._bandera_celular_whatsapp = false;
            this.Close();
        }

        private void txt_numero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
           (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') >= -1))
            {
                e.Handled = true;
            }
        }

       
        private void txt_numero_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.enterClick();

            }

        
        }

        private void numCelular_FormClosing(object sender, FormClosingEventArgs e)
        {

            if(bandera==false)
            {
                e.Cancel = true;
            }
        }

        private void numCelular_Shown(object sender, EventArgs e)
        {
            this.txt_numero.Focus();
            listadoComprobantes formPadre = Owner as listadoComprobantes;
            formPadre._numero_celular_whatsapp ="";
            formPadre._bandera_celular_whatsapp = false;
        }
    }
}
