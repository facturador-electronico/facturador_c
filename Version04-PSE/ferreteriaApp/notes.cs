﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using ferreteriaApp.CEN;
using ferreteriaApp.CLN;
using ferreteriaApp.HELPERS;

namespace ferreteriaApp
{
    public partial class notes : Form
    {
        public notes()
        {
            InitializeComponent();
            this.defaultComp();
            this.LlenarComboNotes();
            this.callSelectedNotes();
            this.getParamFechas();

            this.txt_descripcion.Focus();



        }


        String  id_type_document,
                doc_client_general,
                direccion_cliente_general, 
                name_cliente_general,
                series_general,
                number_general,
                type_note_general = "credit",
                credit_id_general,
                debit_id_general,
                id_tipo_doc_customer, 
                serie_factura_afectada,
                fecha_emision_comp;
        DateTime fecha_transaccion_general,
                hora_trasaccion_general, fecha_proceso_general;


        decimal _descuento_global = 0;
        decimal _descuento_total = 0;

        private void btn_Generar_Click(object sender, EventArgs e)
        {
            
            if (this.cmb_type_note.SelectedValue.ToString() == "0")
            {
                this.lblError.Text = "Seleccione tipo de Nota";
                return;
            }
            if (this.txt_descripcion.Text.Trim() == "" || this.txt_descripcion.Text.Trim() == null)
            {
                this.lblError.Text = "Escriba una descripción";
                this.txt_descripcion.Focus();
                return;
            }


            //datos del cliente
            CLNComprobante clncmp = new CLNComprobante();
            String direccionCliente = direccion_cliente_general;
            String cliente = name_cliente_general;


            decimal anticipo = 0;

            decimal isc = 0;
            decimal otrosImpuestos = 0;
          
            CENComprobanteDetalle det;
            CENComprobanteCabecera cab = new CENComprobanteCabecera();
            List<CENComprobanteDetalle> listDet = new List<CENComprobanteDetalle>();

            for (int i = 0; i < detalleProductTable.Rows.Count - 1; i++)
            {

                det = new CENComprobanteDetalle();

                det.cantidad = decimal.Parse(detalleProductTable.Rows[i].Cells[0].Value.ToString());
                det.unidadMedida = detalleProductTable.Rows[i].Cells[1].Value.ToString();
                det.codigo = detalleProductTable.Rows[i].Cells[2].Value.ToString();
                det.descripcion = detalleProductTable.Rows[i].Cells[3].Value.ToString();
                det.precioUnitario = decimal.Parse(detalleProductTable.Rows[i].Cells[4].Value.ToString());
                det.descuentoUnitario = decimal.Parse(detalleProductTable.Rows[i].Cells[5].Value.ToString());
                det.valorItem = decimal.Parse(detalleProductTable.Rows[i].Cells[9].Value.ToString());
                det.codigo_sunat = detalleProductTable.Rows[i].Cells[10].Value.ToString();
                det.icbper = 0;

                listDet.Add(det);

            }

            cab.empresa = 1;
            cab.codigo = series_general + "-" + number_general;
            cab.fechaEmision = Convert.ToDateTime(txt_fecha.Value);

            cab.anticipos = anticipo;
            cab.descuentoGlobal = _descuento_global;
            cab.descuentoTotal = _descuento_total;
            cab.direccionCliente = direccionCliente;
            cab.nombreCliente = cliente;
            cab.documentoCliente = "" + doc_client_general;
            cab.tipoMoneda = this.lblTipoMonedaPass.Text;
            cab.igv = Convert.ToDecimal(txt_igv.Text);
            cab.isc = isc;
            cab.otros = otrosImpuestos;
            cab.importeTotal = Convert.ToDecimal(txt_total.Text);
            cab.subtotal = Convert.ToDecimal(txt_subtotal.Text);
            cab.tipoDocumento = id_tipo_doc_customer;
            cab.tipoOperacion = 1;
            cab.empresa = 1;
            cab.tipoComprobante = this.cmb_type_note.SelectedValue.ToString();

            importe_general = Convert.ToDecimal(txt_total.Text);
            fecha_transaccion_general = Convert.ToDateTime(txt_fecha.Value);
            fecha_proceso_general = Convert.ToDateTime(DateTime.Now.ToString("yyy-MM-dd"));
            hora_trasaccion_general = Convert.ToDateTime(DateTime.Now.ToString("HH:mm:ss"));


            int id_comprobante = 0;
            int id_nota = 0;
            try
            {
                DialogResult dialog = MessageBox.Show("¿ Está seguro que desea generar la nota ?", "Confirmación",
               MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                if (dialog == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                
                    id_comprobante = clncmp.GuardarBoleta(cab, listDet);
       

                if (id_comprobante > 1)
                {
                    if (this.cmb_type_note.SelectedValue.ToString() == "07")
                    {
                        credit_id_general = this.cmb_reason_notes.SelectedValue.ToString();
                        debit_id_general = null;
                    }
                    if (this.cmb_type_note.SelectedValue.ToString() == "08")
                    {
                        credit_id_general = null;
                        debit_id_general = this.cmb_reason_notes.SelectedValue.ToString();
                    }

                    CLNDocumentTypes cln = new CLNDocumentTypes();
                    //try
                    //{

                        CenNotes notes = new CenNotes();
                        notes.document_id = id_comprobante;
                        notes.note_type = type_note_general;
                        notes.note_credit_type_id = credit_id_general;
                        notes.note_debit_type_id = debit_id_general;
                        notes.note_description = this.txt_descripcion.Text;
                        notes.affected_document_id = searchSaleNote.id_pass.ToString();
                        notes.fecha_transaccion = fecha_transaccion_general;
                        notes.fecha_proceso = fecha_proceso_general;
                        notes.hora_proceso = hora_trasaccion_general;
                        notes.importe = importe_general;

                       id_nota= cln.insertNotes(notes);


                    //}
                    //catch (Exception ex)
                    //{

                    //    aviso aviso_form = new aviso("warning", "ERROR AL INSERTAR TABLA NOTAS", ex.Message, false);
                    //    aviso_form.ShowDialog();
                    //    return;
                    //}

                    for (int i = 0; i < detalleProductTable.Rows.Count - 1; i++)
                    {
                        detalleProductTable.Rows.RemoveAt(i);
                    }

                    CENEstructuraComprobante estructura = clncmp.Find(searchSaleNote.id_pass);

                    //try
                    //{

                        //try
                        //{
                        //Version 1-3 SFS
                        //clncmp.CrearArchivosNotas(estructura, this.cmb_type_note.SelectedValue.ToString(), series_general, number_general, this.cmb_reason_notes.SelectedValue.ToString(), this.txt_descripcion.Text);
                        //clncmp.GenerarArchivoTRI("1000", "IGV", "VAT", Convert.ToString(cab.subtotal), Convert.ToString(cab.igv), this.cmb_type_note.SelectedValue.ToString(), series_general, number_general);
                        //clncmp.crearArchivoLey("1000", this.cmb_type_note.SelectedValue.ToString(), series_general, number_general);

                             //Version 4 - PSE
                            CENDocumentoPse docPse;
                            docPse = funciones.CrearJsonNotesPSE(estructura, cab, series_general, number_general, this.cmb_reason_notes.SelectedValue.ToString(), this.txt_descripcion.Text);


                            CENResponsePse responsePse = new CENResponsePse();
                            responsePse = funciones.EnviarTramaPse(docPse);

                            if (!responsePse.rptaRegistroCpeDoc.flagVerificacion)
                            {
                                throw new Exception(responsePse.rptaRegistroCpeDoc.descripcionResp + " " + responsePse.errorWebService.descripcionErr);
                            }
                            else
                            {

                                //CAMBIAR ESTADOS
                                string estado = "";
                                string adicionalAceptado = "";

                                if (responsePse.rptaRegistroCpeDoc.estadoComprobante == "A")
                                {
                                    estado = CENConstantes.ID_ESTADO;
                                    adicionalAceptado = ". "+responsePse.rptaRegistroCpeDoc.mensajeResp;
                                }

                                if (responsePse.rptaRegistroCpeDoc.estadoComprobante == "O")
                                {
                                    estado = CENConstantes.ID_ESTADO_OBSERVADO;
                                    adicionalAceptado = ". " + responsePse.rptaRegistroCpeDoc.mensajeResp;
                            }
                                if (responsePse.rptaRegistroCpeDoc.estadoComprobante == "E" || responsePse.rptaRegistroCpeDoc.estadoComprobante == "R")
                                {
                                    estado = CENConstantes.ID_ESTADO_RECHAZADO;
                                }

                                if (responsePse.rptaRegistroCpeDoc.estadoComprobante == "P" || responsePse.rptaRegistroCpeDoc.estadoComprobante.Trim() == "")
                                {
                                    estado = CENConstantes.ID_ESTADO_REGISTRADO;
                                }
                                CLNDocumentTypes clndt = new CLNDocumentTypes();
                                clndt.setEstadoDocumento(id_comprobante, estado);

                            //guardamos pdf del s3

                            string rutaArchivoPdfCompleto = "";
                                Boolean saveDoc = funciones.GuardarDocsS3(responsePse, cab.tipoComprobante, ref rutaArchivoPdfCompleto);
                                Boolean saveResponseDoc = funciones.GuardarResponseDocumento(id_comprobante, responsePse);

                                this.btn_Generar.Enabled = false;

                                String tituloAviso;
                                String textoAviso;
                                String varN, nombre_comprobante;

                                varN = (type_note_general == "credit") ? "Crédito" : "Débito";
                                nombre_comprobante = (type_note_general == "credit") ? "NOTA DE CREDITO" : "NOTA DE DEBITO";
                                tituloAviso = "Nota de " + varN + " Registrada";
                                textoAviso = "Se registró la Nota de " + varN + "  correctamente con código " 
                                + id_comprobante + adicionalAceptado+ " ¿Desea generar impresión de la Nota?";


                                DialogResult resultadoD;
                                aviso aviso_form = new aviso("success", tituloAviso, textoAviso, true);
                                resultadoD = aviso_form.ShowDialog();

                                if (resultadoD == DialogResult.OK)
                                {
                                    ////Version 4 - PSE
                                    if (!saveDoc)
                                    {
                                        MessageBox.Show("ERROR AL OBTENER EL PDF");
                                        this.Close();
                                        return;
                                    }
                                    else
                                    {
                                        System.Diagnostics.Process.Start(rutaArchivoPdfCompleto);
                                    }
                                    this.btn_Generar.Enabled = true;
                                }
                                else
                                {
                                    this.btn_Generar.Enabled = true;
                                    this.Close();
                                    return;

                                }


                            }
                        //}
                        //catch (Exception ex)
                        //{

                        //    Console.WriteLine(ex);
                        //    aviso aviso_form_error_planos = new aviso("warning", "ERROR", "NO SE CREARON ARCHIVOS PLANOS", false);
                        //    aviso_form_error_planos.ShowDialog();

                        //    return;
                        //}






                        //Version 1-3 SFS

                        //this.btn_Generar.Enabled = false;

                        //String tituloAviso;
                        //String textoAviso;
                        //String varN, nombre_comprobante;

                        //varN = (type_note_general == "credit") ? "Crédito" : "Débito";
                        //nombre_comprobante = (type_note_general == "credit") ? "NOTA DE CREDITO" : "NOTA DE DEBITO";
                        //tituloAviso = "Nota de " + varN + " Registrada";
                        //textoAviso = "Se registró la Nota de " + varN + "  correctamente con código " + id_comprobante + " ¿Desea generar impresión de la Nota?";


                        //DialogResult resultadoD;
                        //aviso aviso_form = new aviso("success", tituloAviso, textoAviso, true);
                        //resultadoD = aviso_form.ShowDialog();

                        //if (resultadoD == DialogResult.OK)
                        //{


                        //    progress progressBar = new progress("GENERANDO PDF NOTA", 7);
                        //    progressBar.ShowDialog();

                        //    //ahora agregamos el hash al comprobante
                        //    bool xml_hash;
                        //    try
                        //    {

                        //        xml_hash = cln.getHashXML(this.cmb_type_note.SelectedValue.ToString(), series_general, number_general, id_comprobante);
                        //        if (!xml_hash)
                        //        {
                        //            DialogResult diag_hash = new DialogResult();
                        //            aviso aviso_form_error_hash = new aviso("warning", "ERROR AL OBTENER CODIGO HASH", "REVISE QUE FACTURADOR SUNAT GENERE XML  \n"
                        //                                    + "¿Desea volver a generar?", true);
                        //            diag_hash = aviso_form_error_hash.ShowDialog();
                        //            if (diag_hash == DialogResult.OK)
                        //            {
                        //                xml_hash = cln.getHashXML(this.cmb_type_note.SelectedValue.ToString(), series_general, number_general, id_comprobante);
                        //                if (!xml_hash)
                        //                {

                        //                    aviso aviso_form_error_hash2 = new aviso("warning", "ERROR AL OBTENER CODIGO HASH", "REVISE QUE FACTURADOR SUNAT GENERE XML ", false);
                        //                    aviso_form_error_hash2.ShowDialog();
                        //                    this.Close();
                        //                    return;
                        //                }
                        //            }
                        //            else
                        //            {
                        //                this.Close();
                        //                return;
                        //            }
                        //        }
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        DialogResult resultadoDH;
                        //        aviso aviso_form_error_hash = new aviso("warning", "ERROR AL OBTENER CODIGO HASH", "REVISE QUE FACTURADOR SUNAT GENERE XML \n " + ex.Message + "\n"
                        //                                    + "¿Desea volver a generar?", true);
                        //        resultadoDH = aviso_form_error_hash.ShowDialog();

                        //        if (resultadoDH == DialogResult.OK)
                        //        {
                        //            xml_hash = cln.getHashXML(this.cmb_type_note.SelectedValue.ToString(), series_general, number_general, id_comprobante);
                        //            if (!xml_hash)
                        //            {

                        //                aviso aviso_form_error_hash2 = new aviso("warning", "ERROR AL OBTENER CODIGO HASH", "REVISE QUE FACTURADOR SUNAT GENERE XML ", false);
                        //                aviso_form_error_hash2.ShowDialog();
                        //                this.Close();
                        //                return;
                        //            }
                        //        }
                        //        else
                        //        {

                        //            this.Close();
                        //            return;
                        //        }


                        //    }

                        //    //crearPDF
                        //    List<string> datosHash = new List<string>();
                        //    datosHash.Add(Convert.ToString(cab.igv));
                        //    datosHash.Add(Convert.ToString(cab.subtotal));
                        //    datosHash.Add(Convert.ToString(cab.fechaEmision.ToString("yyyy-MM-dd")));
                        //    datosHash.Add(Convert.ToString(id_tipo_doc_customer));
                        //    datosHash.Add(Convert.ToString(doc_client_general));

                        //    List<string> cabecera = new List<string>();

                        //    cabecera.Add(string.Format("Fecha Emisión|" + cab.fechaEmision.ToString("dd/MM/yyyy") + "||"));


                        //    //cabecera.Add(string.Format("Señor(es)|" + cab.nombreCliente + "||"));
                        //    //cabecera.Add(string.Format("R.U.C|" + cab.documentoCliente + "||"));

                        //    cabecera.Add(string.Format("Señor(es)|{0}" + "||", (cab.documentoCliente == CENConstantes.DNI_DEFAULT) ? "" : cab.nombreCliente));
                        //    cabecera.Add(string.Format((cab.tipoDocumento == "1") ? "D.N.I|{0}" + "||" : "R.U.C|{0}" + "||", (cab.documentoCliente == CENConstantes.DNI_DEFAULT) ? "" : cab.documentoCliente));

                        //    cabecera.Add(string.Format("Dirección Cliente|" + cab.direccionCliente + "||"));
                        //    cabecera.Add(string.Format("Tipo Moneda|SOLES" + "|APLICA|" + this.txtSerieVenta.Text + "-" + this.txtNumeroVenta.Text));
                        //    cabecera.Add(string.Format("N° Transacción|" + id_comprobante + "|MOTIVO|" + this.cmb_reason_notes.Text));


                        //    List<string> impuestos = new List<string>();

                        //    impuestos.Add(string.Format("IGV: |{0}", cab.igv));
                        //    impuestos.Add("ISC: | 0.00");
                        //    impuestos.Add("OTROS: | 0.00");
                        //    impuestos.Add(string.Format("DESC.GLOBAL: | {0}", (Math.Round((cab.descuentoGlobal * Convert.ToDecimal(1.18)), 2)).ToString("0.00")));

                        //    ConvertNumberToLetter InLetter = new ConvertNumberToLetter();
                        //    string cantidadEnLetras = InLetter.InLetter(cab.importeTotal.ToString());

                        //    List<string> subtotal = new List<string>();
                        //    subtotal.Add(string.Format("Total Valor de venta: |{0}", cab.subtotal));
                        //    subtotal.Add(string.Format("Descuento Total: | {0}", (Math.Round((cab.descuentoTotal * Convert.ToDecimal(1.18)), 2)).ToString("0.00")));
                        //    subtotal.Add("Anticipos: | 0.00");
                        //    subtotal.Add(string.Format("Importe Total: | {0}", cab.importeTotal));

                        //    List<string> body = new List<string>();

                        //    foreach (CENComprobanteDetalle item in listDet)
                        //    {
                        //        body.Add(string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|", item.cantidad.ToString(),
                        //        item.unidadMedida.ToString(),
                        //        item.codigo.ToString()
                        //        , item.descripcion.ToString()
                        //        , item.precioUnitario.ToString()
                        //        , item.descuentoUnitario.ToString()
                        //        , item.valorItem.ToString()));
                        //    }

                        //    string[] NombHeaderTable = { "Cant.", "Uni. Medida", "Código", "Descripción", "P. Unitario", "Desc", "V. Item" };
                        //    float[] TamCeldas = { 5, 10, 10, 40, 15, 10, 10 };

                        //    try
                        //    {
                        //        Boolean resPDf = clncmp.CrearPDFNotas(cabecera, NombHeaderTable, TamCeldas,
                        //                                    body, impuestos, cab.tipoComprobante, series_general, number_general, nombre_comprobante,
                        //                                    cantidadEnLetras, subtotal, id_comprobante, datosHash);
                        //        if (!resPDf)
                        //        {

                        //            DialogResult diagPDF;
                        //            aviso aviso_form_error_hash = new aviso("warning", "ERROR AL CREAR PDF", "¿Desea volver a generar? \n" +
                        //                                            "ANTE DE CONTINUAR, REVISE QUE FACTURADOR SUNAT GENERE XML", true);
                        //            diagPDF = aviso_form_error_hash.ShowDialog();

                        //            if (diagPDF == DialogResult.OK)
                        //            {
                        //                try
                        //                {
                        //                    cln.getHashXML(this.cmb_type_note.SelectedValue.ToString(), series_general, number_general, id_comprobante);
                        //                }
                        //                catch (Exception ex)
                        //                {
                        //                    DialogResult resultadoDH2;
                        //                    aviso aviso_form_error_hash2 = new aviso("warning", "ERROR AL OBTENER CODIGO HASH", "REVISE QUE FACTURADOR SUNAT GENERE XML \n " + ex.Message + "\n"
                        //                                                + "¿Desea volver a generar?", true);
                        //                    resultadoDH2 = aviso_form_error_hash2.ShowDialog();

                        //                    if (resultadoDH2 == DialogResult.OK)
                        //                    {
                        //                        cln.getHashXML(this.cmb_type_note.SelectedValue.ToString(), series_general, number_general, id_comprobante);
                        //                    }
                        //                    else
                        //                    {
                        //                        this.Close();
                        //                        return;
                        //                    }


                        //                }

                        //                Boolean resPDf2 = clncmp.CrearPDFNotas(cabecera, NombHeaderTable, TamCeldas,
                        //                                     body, impuestos, cab.tipoComprobante, series_general, number_general, nombre_comprobante,
                        //                                     cantidadEnLetras, subtotal, id_comprobante, datosHash);
                        //                if (!resPDf2)
                        //                {

                        //                    aviso aviso_form_error_hash2 = new aviso("warning", "ERROR AL CREAR PDF", "Contacte soporte técnico", false);
                        //                    aviso_form_error_hash2.ShowDialog();
                        //                }

                        //            }
                        //            else
                        //            {
                        //                this.Close();
                        //                return;
                        //            }

                        //        }
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        MessageBox.Show(ex.Message);
                        //        return;

                        //    }



                        //    searchSaleNote frm_search2 = new searchSaleNote();
                        //    frm_search2.Activate();
                        //    this.Close();

                        //    return;
                        //}
                        //else
                        //{
                        //    try
                        //    {
                        //        cln.getHashXML(this.cmb_type_note.SelectedValue.ToString(), series_general, number_general, id_comprobante);
                        //    }
                        //    catch (Exception ex)
                        //    {

                        //        aviso aviso_form_error_hash = new aviso("warning", "ERROR AL OBTENER CODIGO HASH", "REVISE QUE FACTURADOR SUNAT GENERE XML " + ex.Message, false);
                        //        aviso_form_error_hash.ShowDialog();
                        //        this.btn_Generar.Enabled = true;
                        //        this.Close();
                        //        return;
                        //    }


                        //}


                        //EndInvoke dialogo

                        searchSaleNote frm_search = new searchSaleNote();
                        frm_search.Activate();
                        this.Close();
                    //}
                    //catch (Exception ex)
                    //{
                    //    Console.WriteLine(ex);
                    //    DialogResult resultadoD;
                    //    aviso aviso_form = new aviso("warning", "Error al crear los archivos del comprobante, Facturador Sunat no esta generando XML automatico (revisar)", "¿Desea volver a generar impresión de la Nota?", true);
                    //    resultadoD = aviso_form.ShowDialog();

                    //    if (resultadoD == DialogResult.OK)
                    //    {

                    //        progress progressBar = new progress("GENERANDO PDF NOTA", 7);
                    //        progressBar.ShowDialog();

                    //        cln.getHashXML(this.cmb_type_note.SelectedValue.ToString(), series_general, number_general, id_comprobante);


                    //        //crearPDF
                    //        List<string> datosHash = new List<string>();
                    //        datosHash.Add(Convert.ToString(cab.igv));
                    //        datosHash.Add(Convert.ToString(cab.subtotal));
                    //        datosHash.Add(Convert.ToString(cab.fechaEmision.ToString("yyyy-MM-dd")));
                    //        datosHash.Add(Convert.ToString(id_tipo_doc_customer));
                    //        datosHash.Add(Convert.ToString(doc_client_general));


                    //        List<string> cabecera = new List<string>();

                    //        cabecera.Add(string.Format("Fecha Emisión|" + cab.fechaEmision + "||"));

                    //        //cabecera.Add(string.Format("Señor(es)|" + cab.nombreCliente + "||"));
                    //        //cabecera.Add(string.Format("R.U.C|" + cab.documentoCliente + "||"));

                    //        cabecera.Add(string.Format("Señor(es)|{0}" + "||", (cab.documentoCliente == CENConstantes.DNI_DEFAULT) ? "" : cab.nombreCliente));
                    //        cabecera.Add(string.Format((cab.tipoDocumento == "1") ? "D.N.I|{0}" + "||" : "R.U.C|{0}" + "||", (cab.documentoCliente == CENConstantes.DNI_DEFAULT) ? "" : cab.documentoCliente));

                    //        cabecera.Add(string.Format("Dirección Cliente|" + cab.direccionCliente + "||"));
                    //        cabecera.Add(string.Format("Tipo Moneda|SOLES" + "|APLICA|" + this.txtSerieVenta.Text + "-" + this.txtNumeroVenta.Text));
                    //        cabecera.Add(string.Format("N° Transacción|" + id_comprobante + "|MOTIVO|" + this.cmb_reason_notes.Text));



                    //        List<string> impuestos = new List<string>();

                    //        impuestos.Add(string.Format("IGV: |{0}", cab.igv));
                    //        impuestos.Add("ISC: | 0.00");
                    //        impuestos.Add("OTROS: | 0.00");
                    //        impuestos.Add("DESC.GLOBAL: | 0.00");

                    //        //string cantidadEnLetras, List< string > subtotal
                    //        ConvertNumberToLetter InLetter = new ConvertNumberToLetter();
                    //        string cantidadEnLetras = InLetter.InLetter(cab.importeTotal.ToString());

                    //        List<string> subtotal = new List<string>();
                    //        subtotal.Add(string.Format("Total Valor de venta: |{0}", cab.subtotal));
                    //        subtotal.Add(string.Format("Descuento Total: | {0}", cab.descuentoGlobal));
                    //        subtotal.Add("Anticipos: | 0.00");
                    //        subtotal.Add(string.Format("Importe Total: | {0}", cab.importeTotal));

                    //        List<string> body = new List<string>();

                    //        foreach (CENComprobanteDetalle item in listDet)
                    //        {
                    //            body.Add(string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|", item.cantidad.ToString(),
                    //            item.unidadMedida.ToString(),
                    //            item.codigo.ToString()
                    //            , item.descripcion.ToString()
                    //            , item.precioUnitario.ToString()
                    //            , item.descuentoUnitario.ToString()
                    //            , item.valorItem.ToString()));
                    //        }



                    //        string[] NombHeaderTable = { "Cant.", "Uni. Medida", "Código", "Descripción", "P. Unitario", "Desc", "V. Item" };
                    //        float[] TamCeldas = { 5, 10, 10, 40, 15, 10, 10 };

                    //        String nombre_comprobante;


                    //        nombre_comprobante = (type_note_general == "credit") ? "NOTA DE CREDITO" : "NOTA DE DEBITO";


                    //        clncmp.CrearPDFNotas(cabecera, NombHeaderTable, TamCeldas,
                    //                body, impuestos, cab.tipoComprobante, series_general, number_general, nombre_comprobante,
                    //                cantidadEnLetras, subtotal, id_comprobante, datosHash
                    //            );

                    //        searchSaleNote frm_search = new searchSaleNote();
                    //        frm_search.Activate();
                    //        this.Close();


                    //        return;


                    //    }
                    //}

                }
                else
                {
                    MessageBox.Show("No se pudo guardar la nota");
                }
                this.lblError.Text = "";

                    Cursor.Current = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                if(id_comprobante>0) clncmp.deleteComprobantexError(id_comprobante);
                if (id_comprobante > 0) clncmp.deleteNotaxError(id_comprobante);

                aviso aviso_form = new aviso("warning", "ERROR AL CREAR LA NOTA ", ex.Message, false);
                aviso_form.ShowDialog();
                return;

            }
        }

        private void txt_descripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
        
            if (!char.IsControl(e.KeyChar) 
                && !char.IsDigit(e.KeyChar) 
                && !char.IsLetter(e.KeyChar)
                && !char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            
        }

        private void notes_Activated(object sender, EventArgs e)
        {
            this.txt_descripcion.Focus();
        }

        int dias_antes, dias_despues;


        private void getParamFechas()
        {
            CLNConcepto clnConcepto = new CLNConcepto();

            String param = clnConcepto.ObtenerValorParametro(17);
            dias_antes = Convert.ToInt32(param.Split(',')[0]);
            dias_despues = Convert.ToInt32(param.Split(',')[1]);
        }
        private void txt_fecha_ValueChanged(object sender, EventArgs e)
        {
            DateTime today = DateTime.Today;
            DateTime fecha_doc = Convert.ToDateTime(txt_fecha.Value);
            DateTime fecha_emision = Convert.ToDateTime(fecha_emision_comp);

            TimeSpan tSpan = today - fecha_doc;
       
            int dias_pasados = Convert.ToInt32(tSpan.Days);

          

            if (dias_pasados > dias_antes)
            {
                String tituloAviso = "No es posible generar comprobante";
                String textoAviso = "Fecha (" + fecha_doc.ToString("dd/MM/yyyy") + ") excede a los " + dias_antes + " días permitidos.";
               
                aviso aviso_form = new aviso("warning", tituloAviso, textoAviso, false);
                 aviso_form.ShowDialog();

                this.txt_fecha.Value= DateTime.Today;

                return;
            }
            else if (fecha_doc < fecha_emision)
            {
                    String tituloAviso = "No es posible generar comprobante";
                    String textoAviso = "Fecha de emisión del comprobante ("+ fecha_emision.ToString("dd/MM/yyyy") + ") "
                                 +"es mayor al de nota a generar (" + fecha_doc.ToString("dd/MM/yyyy") + ")";

                    aviso aviso_form = new aviso("warning", tituloAviso, textoAviso, false);
                    aviso_form.ShowDialog();

                    this.txt_fecha.Value = DateTime.Today;

                    return;

             }
            
            if (dias_pasados < 0)
            {
                if ((dias_pasados*-1) > dias_despues)
                {
                    String tituloAviso = "Error";
                    String textoAviso = "Fecha (" + fecha_doc.ToString("dd/MM/yyyy") + ") no puede exceder a  " + dias_despues + " días posteriores permitidos.";
                    
                    aviso aviso_form = new aviso("warning", tituloAviso, textoAviso, false);
                    aviso_form.ShowDialog();

                    this.txt_fecha.Value = DateTime.Today;

                }
            }
          



        }

        Decimal importe_general;

        private void button3_Click(object sender, EventArgs e)
        {
            searchSaleNote frm_search = new searchSaleNote();
            frm_search.Activate();
            this.Close();
        }

        private void defaultComp()
        {
            CENEstructuraComprobante cen;
            try
            {
                CLNComprobante cln = new CLNComprobante();
              
                cen = cln.Find(searchSaleNote.id_pass);
                fecha_emision_comp = cen.Cab_Fac_Fecha;
                string[] arraySeriesVenta = cen.serieDocumento.Split('-');

                this.txtCodigoVenta.Text = searchSaleNote.id_pass.ToString();
                serie_factura_afectada= arraySeriesVenta[0];
                this.txtSerieVenta.Text = arraySeriesVenta[0];
                this.txtNumeroVenta.Text = arraySeriesVenta[1];

                this.txtClienteVenta.Text = cen.docCustomer + " - " + cen.nomCustomer;
            
               
                this.txt_subtotal.Text = cen.TotalValorVenta.ToString("0.00");
                this.txt_igv.Text = cen.SumTotalTributos.ToString("0.00");
                this.txt_total.Text = cen.TotalPrecioVenta.ToString("0.00");

                //---
                
                _descuento_global = cen.descuento_global ;
                _descuento_total = cen.descuento_total ;

                this.txt_descuentoGlobal.Text = Math.Round((cen.descuento_global * Convert.ToDecimal(1.18)), 2).ToString("0.00");

                //---


                this.lblTipoMonedaPass.Text = cen.tipo_moneda;

                doc_client_general = cen.docCustomer;
                direccion_cliente_general = cen.direccionCustomer;
                name_cliente_general = cen.nomCustomer;

                id_type_document = cen.tipo_comprobante;

                id_tipo_doc_customer = cen.typeDocCustomer;

                for (int i = 0; i < this.detalleProductTable.Rows.Count - 1; i++)
                {
                    this.detalleProductTable.Rows.RemoveAt(i);
                }

                foreach (var listR in cen.listaDetalle)
                {
                    int table = this.detalleProductTable.Rows.Add();

                    this.detalleProductTable.Rows[table].Cells[0].Value = listR.cantidad;
                    this.detalleProductTable.Rows[table].Cells[1].Value = listR.unidadMedidaNota;
                    this.detalleProductTable.Rows[table].Cells[2].Value = listR.codigoProductoNota;
                    this.detalleProductTable.Rows[table].Cells[3].Value = listR.Producto;
                    this.detalleProductTable.Rows[table].Cells[4].Value = listR.PrecioVtaUnitario.ToString("0.00");
                    this.detalleProductTable.Rows[table].Cells[5].Value = 0;
                    this.detalleProductTable.Rows[table].Cells[6].Value = listR.SumTribxItem.ToString("0.00");
                    this.detalleProductTable.Rows[table].Cells[7].Value = 0;
                    this.detalleProductTable.Rows[table].Cells[9].Value = listR.ValorUnitario.ToString("0.00");
                    this.detalleProductTable.Rows[table].Cells[10].Value = listR.CodigoProdSunat;

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
               
                aviso aviso_form = new aviso("warning", "ERROR", ex.Message, false);
                aviso_form.ShowDialog();
               
            }
        }

        private void cmb_reason_notes_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void LlenarComboNotes()
        {
        
            List<CENDocumentTypes> lista;
            this.cmb_type_note.Items.Clear();
            try
            {
                
                 CLNDocumentTypes cln = new CLNDocumentTypes();
                lista = cln.getTipoDocumentosString(5);
                

                this.cmb_type_note.DataSource = lista;
                this.cmb_type_note.DisplayMember = "description";
                this.cmb_type_note.ValueMember = "id";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CENDocumentTypes cen = new CENDocumentTypes();
                cen.id = "0";
                cen.description = "No se encontraron documentos";
                lista = null;
                this.cmb_type_note.DataSource = lista;
                this.cmb_type_note.DisplayMember = "description";
                this.cmb_type_note.ValueMember = "id";
            }
        }
            

     
        private void callSelectedNotes()
        {
            CLNDocumentTypes cln = new CLNDocumentTypes();
            if (this.cmb_type_note.SelectedValue.ToString() == "07")
            {
                type_note_general = "credit";

            }
            if (this.cmb_type_note.SelectedValue.ToString() == "08")
            {
                type_note_general = "debit";
            }


            List<CENDocumentTypes> listaRN ;
            CLNDocumentTypes cln2 = new CLNDocumentTypes();
            string series="";
            try
            {

                string number = cln2.getSerieNota(this.cmb_type_note.SelectedValue.ToString(), serie_factura_afectada.Substring(0,1));
                number_general = number;

                this.lbl_numero_nota.Text = number.ToString();

                if(type_note_general == "credit")
                {
                    listaRN = cln.getTipoDocumentosString(11);

                    if (id_type_document == "01")
                    {
                        series = cln.getNumberSeriesNotes(6);
                        this.lbl_serie_note.Text = series;
                        series_general = series;
                    }
                    else
                    {
                        series = cln.getNumberSeriesNotes(7);
                        this.lbl_serie_note.Text = series;
                        series_general = series;

                    }
                }
                else
                {
                    listaRN = cln.getTipoDocumentosString(10);
                }
               

                this.cmb_reason_notes.DataSource = listaRN;
                this.cmb_reason_notes.DisplayMember = "description";
                this.cmb_reason_notes.ValueMember = "id";


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                this.lbl_serie_note.Text = "FC01";
            }

        }
        private void cmb_type_note_SelectedIndexChanged(object sender, EventArgs e)
        {

            this.callSelectedNotes();
        }
        public void setearTextos()
        {
            this.cmb_reason_notes.SelectedValue = null;
            this.cmb_type_note.SelectedValue = null;
            this.LlenarComboNotes();
        }
        private void button2_Click(object sender, EventArgs e)
        {
                
            
        }


   
     
    }
}
