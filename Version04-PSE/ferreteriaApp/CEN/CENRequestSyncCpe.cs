﻿using System.Collections.Generic;

namespace ferreteriaApp.CEN
{
    public class CENRequestSyncCpe
    {
        public string tipodocumento_id { get; set; }        // ID TIPO DE DOCUMENTO
        public string serie { get; set; }                   // SERIE DEL DOCUMENTO
        public string numero { get; set; }                  // NUMERO CORRELATIVO DEL DOCUMENTO
        public bool send_s3 { get; set; }                   // PETICIO DE URL DE S3

    }
}
