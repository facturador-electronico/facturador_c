﻿using System;


namespace ferreteriaApp.CEN
{
    public class CENDocumentTypes
    {
        public String id { get; set; }
        public int active { get; set; }
        public String short_ab { get; set; }
        public String description { get; set; }

        //para series
        public String numberseries { get; set; }

        //para get comprobantes
        public String cliente { get; set; }
        public int idC { get; set; }
        public DateTime fechaEmision { get; set; }

    }
}
