﻿using System;

namespace ferreteriaApp.CEN
{
    public class CenSearchComprobantes
    {


        public String fecha_inicio { get; set; }
        public String fecha_fin { get; set; }
        public String dni_ruc { get; set; }
        public String name_cliente { get; set; }
        public String tipo_comprobante { get; set; }
        public String series_number { get; set; }
        public int codigo_venta { get; set; }
        public String _id_estado { get; set; }



    }
}
