﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ferreteriaApp.CEN
{
   public class CENTransactionApidni
    {
        public bool success { get; set; } = false;
        public string mensaje { get; set; }
        public PersonaEN data { get; set; }
    }

    public class PersonaEN
    {
        public int origen { get; set; }
        public string numero { get; set; }
        public string nombre_completo { get; set; }
        public string nombres { get; set; }
        public string apellido_paterno { get; set; }
        public string apellido_materno { get; set; }
        public int codigo_verificacion { get; set; }
        public string fecha_nacimiento { get; set; }
        public object sexo { get; set; }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"origen: {this.origen}");
            sb.AppendLine($"numero: {this.numero}");
            sb.AppendLine($"nombre_completo: {this.nombre_completo}");
            sb.AppendLine($"nombres: {this.nombres}");
            sb.AppendLine($"apellido_paterno: {this.apellido_paterno}");
            sb.AppendLine($"apellido_materno: {this.apellido_materno}");
            sb.AppendLine($"codigo_verificacion: {this.codigo_verificacion}");
            sb.AppendLine($"fecha_nacimiento: {this.fecha_nacimiento}");
            sb.AppendLine($"sexo: {this.sexo}");
            return sb.ToString();
        }
    }

}
