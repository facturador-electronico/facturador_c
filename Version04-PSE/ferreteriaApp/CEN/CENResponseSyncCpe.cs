﻿using System.Collections.Generic;

namespace ferreteriaApp.CEN
{
    public class CENResponseSyncCpe
    {
        public string id { get; set; }                    // ID DEL COMPROBANTE
        public string serie { get; set; }                 // SERIE DEL DOCUMENTO
        public string numero { get; set; }                // NUMERO CORRELATIVO DEL DOCUMENTO
        public string tipodocumento_id { get; set; }      // ID DE TIPO DE DOCUMENTO
        public string situacion { get; set; }             // SITUACION DEL COMPROBANTE
        public string url_pdf { get; set; }               // URL PDF DEL S3
        public string url_xml { get; set; }               // URL XML DEL S3
        public string url_cdr { get; set; }               // URL XML DEL CDR DEL S3

    }
}
