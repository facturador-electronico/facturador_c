﻿using System;
using System.Collections.Generic;

namespace ferreteriaApp.CEN
{
    public class CENDocumentoPse
    {
        public string idempresa { get; set; }
        public string id { get; set; }
        public string idtipodocumento { get; set; }
        public string idtipodocumentonota { get; set; }
        public string nroDocEmpresa { get; set; }
        public string descripcionEmpresa { get; set; }
        public string tipodocumento { get; set; }
        public string serie { get; set; }
        public string numero { get; set; }
        public string idpuntoventa { get; set; }
        public string tipoOperacion { get; set; }
        public string tipoNotaCredNotaDeb { get; set; }
        public string tipodoccliente { get; set; }
        public string nrodoccliente { get; set; }
        public string nombrecliente { get; set; }
        public string direccioncliente { get; set; }
        public string emailcliente { get; set; }
        public string fecha { get; set; }
        public string hora { get; set; }
        public string fechavencimiento { get; set; }
        public string glosa { get; set; }
        public string formapago { get; set; }
        public string moneda { get; set; }
        public string tipocambio { get; set; }
        public string descuento { get; set; }
        public string Descuentoafectabase { get; set; }
        
        public string igvventa { get; set; }
        public string iscventa { get; set; }
        public string otrosTributos { get; set; }
        public string otrosCargos { get; set; }
        public string importeventa { get; set; }
        public string regimenpercep { get; set; }
        public string descRegimenpercep { get; set; }
        public string porcentpercep { get; set; }
        public string importepercep { get; set; }
        public string porcentdetrac { get; set; }
        public string importedetrac { get; set; }
        public string importerefdetrac { get; set; }
        public string importefinal { get; set; }
        public string importeanticipo { get; set; }
        public string indicaAnticipo { get; set; }
        public string estado { get; set; }
        public string situacion { get; set; }
        public string estransgratuita { get; set; }
        
      
        public string ordenCompra { get; set; }
        public string placaVehiculo { get; set; }
        public string montoFise { get; set; }
        public string tipoRegimen { get; set; }
        public string codigoBBSSSujetoDetrac { get; set; }
        public string numCtaBcoNacion { get; set; }
        public string bienTransfAmazonia { get; set; }
        public string serviTransfAmazonia { get; set; }
        public string contratoConstAmazonia { get; set; }
        public string desctiponotcreddeb { get; set; }
        public string descripciondetrac { get; set; }
        public string usuario { get; set; }
        public string clave { get; set; }
       
        public string hasRetencionIgv { get; set; }
        public string porcRetencionIgv { get; set; }
        public string impRetencionIgv { get; set; }
        public string impOperacionRetencionIgv { get; set; }
        public string usuarioSession { get; set; }



        public List<ListaDetalleDocumento> listaDetalleDocumento { get; set; }
        public List<ListaDocumentoReferencia> listaDocumentoReferencia { get; set; }
        public List<ListaDocumentoAnticipo> listaDocumentoAnticipo { get; set; }
        public List<ListaDocRefeGuia> listaDocRefeGuia { get; set; }
        public List<FormaPagoCredito> formaPagoCredito { get; set; }



    }


    public class ListaDetalleDocumento
    {
        public int idempresa { get; set; }
        public string iddocumento { get; set; }
        public string idtipodocumento { get; set; }
        public string lineid { get; set; }
        public string codigo { get; set; }
        public string codigoSunat { get; set; }
        public string producto { get; set; }
        public string afectoIgv { get; set; }
        public string afectoIsc { get; set; }
        public string afectacionigv { get; set; }
        public string sistemaISC { get; set; }
        public string unidad { get; set; }
        public string cantidad { get; set; }
        public string preciounitario { get; set; }
        public string precioreferencial { get; set; }
        public string valorunitario { get; set; }
        public string valorbruto { get; set; }
        public string valordscto { get; set; }
        public string valorcargo { get; set; }
        public string valorventa { get; set; }
        public string isc { get; set; }
        public string igv { get; set; }
        public string total { get; set; }
        public string factorIsc { get; set; }
        public string factorIgv { get; set; }
      
        public string tbvtPuntoOrigen { get; set; }
        public string tbvtDescripcionOrigen { get; set; }
        public string tbvtPuntoDestino { get; set; }
        public string tbvtDescripcionDestino { get; set; }
        public string tbvtDetalleViaje { get; set; }
        public string tbvtValorRefPreliminar { get; set; }
        public string tbvtValorCargaEfectiva { get; set; }
        public string tbvtValorCargaUtil { get; set; }
        public string tbvtNumRegistroMtc { get; set; }
        public string tbvtConfiguracionVehicular { get; set; }
     
    }

    public class ListaDocumentoReferencia
    {
        public int idempresa { get; set; }
        public string iddocumento { get; set; }
        public string idtipodocumento { get; set; }
        public string iddocumentoref { get; set; }
        public string idtipodocumentoref { get; set; }
        public string numerodocref { get; set; }
        public string descripcionTipoDoc { get; set; }
        public string usuarioSession { get; set; }
        public string cadenaAleatoria { get; set; }
    }

    public class ListaDocumentoAnticipo
    {
        public string documento_id { get; set; }
        public int empresa_id { get; set; }
        public string tipodocumento_id { get; set; }
        public int line_id { get; set; }
        public string tipodocanticipo { get; set; }
        public string desctipodocanticipo { get; set; }
        public string desctipodocemisor { get; set; }
        public string nrodocanticipo { get; set; }
        public int montoanticipo { get; set; }
        public string tipodocemisor { get; set; }
        public string nrodocemisor { get; set; }
        public string usuarioSession { get; set; }
        public string cadenaAleatoria { get; set; }
    }

    
    public class ListaDocRefeGuia
    {
        public string documento_id { get; set; }
        public int empresa_id { get; set; }
        public string tipodocumento_id { get; set; }
        public string tipo_guia { get; set; }
        public string numero_guia { get; set; }
        public string desctipoguia { get; set; }
        public string usuarioSession { get; set; }
        public string cadenaAleatoria { get; set; }
    }

    public class FormaPagoCredito
    {
        public string numeroCuota { get; set; }
        public string montoCuota { get; set; }
        public string fechaCuota { get; set; }
        public string lineid { get; set; }
    }
}
