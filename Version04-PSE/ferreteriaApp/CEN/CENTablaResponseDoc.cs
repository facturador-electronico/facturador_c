﻿using System;
using System.Collections.Generic;

namespace ferreteriaApp.CEN
{
    public class CENTablaResponseDoc
    {

        public int id { get; set; }                             // ID TABLA RESPONSE DOCUMENTO
        public int comprobante_id { get; set; }                 // ID DE COMPROBANTE
        public string response { get; set; }                    // RESPONSE DE WEBAPI PSE
        public string url_pdf { get; set; }                     // URL DEL PDF DEL S3
        public string url_xml { get; set; }                     // URL DEL XML DEL S3
        public string url_cdr { get; set; }                     // URL DEL CDR DEL S3
        public DateTime fecha_creacion_url { get; set; }        // FECHA DE CREACION DE LOS URL
        public DateTime fecha_vencimiento_url { get; set; }     // FECHA DE VENCIMIENTO DE LOS URL
      

    }
}
