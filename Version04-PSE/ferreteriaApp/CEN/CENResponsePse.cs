﻿using System;


namespace ferreteriaApp.CEN
{
    public class CENResponsePse
    {

        public RptaRegistroCpeDoc rptaRegistroCpeDoc { get; set; }
        public ErrorWebService errorWebService { get; set; }


    }

    public class RptaRegistroCpeDoc
    {
        public bool flagVerificacion { get; set; }
        public string descripcionResp { get; set; }
        public string mensajeResp { get; set; }
        public int codigo { get; set; }
        public string comprobante { get; set; }
        public string estadoComprobante { get; set; }
        public string ruta_pdf { get; set; } // ruta pdf
        public string ruta_xml { get; set; } // ruta xml
        public string ruta_cdr { get; set; } // ruta cdr
    }

    public class ErrorWebService
    {
        public int tipoError { get; set; }
        public int codigoError { get; set; }
        public string descripcionErr { get; set; }
    }

}
