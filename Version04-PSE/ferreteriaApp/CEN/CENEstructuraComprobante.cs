﻿using System.Collections.Generic;

namespace ferreteriaApp.CEN
{
    public class CENEstructuraComprobante
    {
        public string tipoOperacion { get; set; }
        public string Cab_Fac_Fecha { get; set; }
        public string HoraEmision { get; set; }
        public string FechaVencimiento { get; set; }
        public string CodDomicilioFiscal { get; set; }
        public string TipoDocEmpresa { get; set; }
        public string NumDocEmpresa { get; set; }
        public string NombEmpresa { get; set; }
        public string tipo_moneda { get; set; }
        public decimal SumTotalTributos { get; set; }
        public decimal TotalValorVenta { get; set; }
        public decimal TotalPrecioVenta { get; set; }
        public decimal TotalDescuentos { get; set; }
        public decimal otrosCargos { get; set; }
        public decimal TotalAnticipos { get; set; }
        public decimal ImporteTotalVenta { get; set; }
        public string VersionUBL { get; set; }
        public string Customizacion { get; set; }

        public decimal descuento_total { get; set; }
        public decimal descuento_global { get; set; }


        //agregadoJC0
        public string nomCustomer { get; set; }
        public string docCustomer { get; set; }
        public string serieDocumento { get; set; }
        public string typeDocCustomer { get; set; }
        public string direccionCustomer { get; set; }

        public string tipo_comprobante { get; set; }



        public List<CENEstructuraDetalleComprobante> listaDetalle { get; set; }
    }

    public class CENEstructuraDetalleComprobante
    {
        public string UnidadMedida { get; set; }
        public decimal cantidad { get; set; }
        public string codigoProducto { get; set; }
        public string CodigoProdSunat { get; set; }
        public string Producto { get; set; }
        public decimal ValorUnitario { get; set; }
        public decimal SumTribxItem { get; set; } 

        public string CodTribIGV { get; set; }
        public decimal MontoIGVItem { get; set; }
        public decimal baseImponible { get; set; }
        public string nombreTributo { get; set; }
        public string codAfecIGV { get; set; }
        public string tributoAfectacion { get; set; }
        public decimal tributoPorcentaje { get; set; }


        public string codigTributoISC { get; set; }
        public decimal mtoISCxitem { get; set; }
        public decimal BaseImpISC { get; set; }
        public string NomTribxItemISC { get; set; }
        public string CodTiposTributoISC { get; set; }
        public string TipoSistemaISC { get; set; }
        public string PorcImoISC { get; set; }


        public string CodTipoTribOtros { get; set; }
        public decimal MtoTribOTrosxItem { get; set; }
        public decimal BaseImpOtroxItem { get; set; }
        public string NomTribOtroxItem { get; set; }
        public string CodTipTribOtroxItem { get; set; }
        public decimal PorTribOtroXItem { get; set; }


        public string codTriIcbper { get; set; }
        public string mtoTriIcbperItem { get; set; }
        public string ctdBolsasTriIcbperItem { get; set; }
        public string nomTributoIcbperItem { get; set; }
        public string codTipTributoIcbperItem { get; set; }
        public string mtoTriIcbperUnidad { get; set; }


        public decimal PrecioVtaUnitario { get; set; }
        public decimal valor_item { get; set; }
        public decimal ValorRefUnitario_Gratuito { get; set; }

        //agregadoJC0
        public decimal precioBaseItem { get; set; }

        public string unidadMedidaNota { get; set; }
        public string codigoProductoNota { get; set; }
    }
}
