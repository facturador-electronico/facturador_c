﻿using ferreteriaApp.CEN;
using MySql.Data.MySqlClient;
using System;
using System.Data.SqlTypes;
using System.IO;

using System.Xml;

using System.Data.SQLite;

namespace ferreteriaApp.DAO
{
    class Conexion
    {

        private static String usuario = getConstante("user");
        private static String pass = getConstante("pass");
        private static String server = getConstante("server");
        private static String port = getConstante("port");
        private static String db = getConstante("db");

        public Conexion() { }


        public static string getConstante(string Variable)
        {
            string currentDir = Path.GetDirectoryName(Environment.CurrentDirectory);
            
            string archivo = currentDir + @"\\ferreteria.conf";
            string valor = "";
            if (File.Exists(archivo))
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(archivo);
                XmlNodeList listaNodos = documento.GetElementsByTagName("configuracion");
                foreach (XmlElement nodos in listaNodos)
                {
                    XmlNodeList nombre = nodos.GetElementsByTagName(Variable);
                    valor = nombre[0].InnerText;
                }
            }
            return valor;
        }
     
        public static MySqlConnection conectar()
        {
            string cadena = "server=" + server + ";port=" + port + ";Uid=" + usuario + ";pwd=" + pass + ";database=" + db;
            try
            {


                MySqlConnection conDB = new MySqlConnection(cadena);
                conDB = new MySqlConnection(cadena);
                conDB.Open();
                return conDB;
                
             
          
            }
            catch (SqlTypeException ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public MySqlCommand prepararConsulta(string sql, Boolean tran = false)
        {

            MySqlCommand cmd;

            if (tran)
            {

                using (MySqlTransaction transaction = conectar().BeginTransaction())
                {
                    cmd = new MySqlCommand(sql, conectar(), transaction);
                }

            }
            else
            {

                cmd = new MySqlCommand(sql, conectar());

            }


            return cmd;
        }



        public void cerrarSession()
        {
            conectar().Close();
        }

        public MySqlTransaction transaction()
        {
            return conectar().BeginTransaction();
        }


        public string returnCadena()
        {
            string cadena = "server=" + server + ";port=" + port + ";Uid=" + usuario + ";pwd=" + pass + ";database=" + db;
            return cadena;
        }


        //conexion bd sfs


        public static SQLiteConnection conectionSQLite()
        {
       
            String rutaBdSFS = GetDirectorioBDsfs();
            string urlBD = rutaBdSFS + "bd/BDFacturador.db";
            string DBName = "BDFacturador.db";
            var dbcon = new SQLiteConnection(
              string.Format("Data Source="+ urlBD + ";Version=3;", DBName)
          );

            dbcon.Open();

            return dbcon;
      

        }

      
        public SQLiteCommand prepararConsultaSqlite(string sql)
        {


            var ctx = conectionSQLite();

            SQLiteCommand cmd = new SQLiteCommand(sql, ctx);

            return cmd;

        }
   
        public static string GetDirectorioBDsfs()
        {
            try
            {
                string datosEmpresa = WebVentas.util.Helper.ObtenerValorParametro(CENConstantes.CONST_2);
                string Ruta = datosEmpresa.Split('|')[1];
                String[] spearator = { "sunat_archivos" };
                Int32 count = 2;
                String[] strlist = Ruta.Split(spearator, count, StringSplitOptions.RemoveEmptyEntries);
                String Rbd = strlist[0];
                return Rbd;
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex);
                throw;
            }
       
        }
    

    }
}
