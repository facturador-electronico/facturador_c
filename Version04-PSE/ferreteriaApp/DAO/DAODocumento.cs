﻿using ferreteriaApp.CEN;
using System;
using System.Collections.Generic;
using System.Data.SQLite;


namespace ferreteriaApp.DAO
{
    class DAODocumento
    {
        readonly Conexion con;

        private const string SELECT = @"SELECT * FROM DOCUMENTO";
      

        public DAODocumento()
        {
            con = new Conexion();
        }

        public List<CENDocumento> lista()
        {
            CENDocumento doc;
            List<CENDocumento> lista = new List<CENDocumento>();

            try
            {
                SQLiteCommand cmd = con.prepararConsultaSqlite(SELECT);
                SQLiteDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    doc = new CENDocumento();
                    doc.num_ruc = dr.GetString(0);
                    doc.tip_docu = dr.GetString(1);
                    doc.num_docu = dr.GetString(2);
                    doc.fec_carg = dr.GetDateTime(3);
                    doc.fec_gene = dr.GetDateTime(4);
                    doc.des_obse = dr.GetString(6);
                    doc.nom_arch = dr.GetString(7);
                    doc.ind_situ = dr.GetString(8);
                    doc.tip_arch = dr.GetString(9);
                    doc.firm_digital = dr.GetString(10);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }
            return lista;
        }


        public void Update(CENDocumento doc)
        {
            try
            {
                SQLiteCommand cmd = con.prepararConsultaSqlite("UPDATE DOCUMENTO SET  DES_OBSE ='" + doc.des_obse + "', FEC_ENVI ='" + doc.fec_envi + "' , IND_SITU ='" + doc.ind_situ + "' WHERE NUM_DOCU ='" + doc.num_docu + "'");
                cmd.ExecuteNonQuery();
                cmd.Dispose();
       
              
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;

            }

        }

        public Boolean Delete(string num_docu)
        {
            try
            {
                SQLiteCommand cmd = con.prepararConsultaSqlite("DELETE FROM DOCUMENTO WHERE NUM_DOCU = '"+ num_docu+"'");
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }
        }


    
    }
}

