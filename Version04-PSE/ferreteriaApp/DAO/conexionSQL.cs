﻿
using MySql.Data.MySqlClient;
using System;
using System.Data.SqlTypes;
using System.IO;

using System.Xml;

namespace ferreteriaApp.DAO
{
    class conexionSQL
    {

        private static String usuario = getConstante("user");
        private static String pass = getConstante("pass");
        private static String server = getConstante("server");
        private static String port = getConstante("port");
        private static String db = getConstante("db");

        public conexionSQL() { }


        public static string getConstante(string Variable)
        {
            string currentDir = Path.GetDirectoryName(Environment.CurrentDirectory);
            
            string archivo = currentDir + @"\\ferreteria.conf";
            string valor = "";
            if (File.Exists(archivo))
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(archivo);
                XmlNodeList listaNodos = documento.GetElementsByTagName("configuracion");
                foreach (XmlElement nodos in listaNodos)
                {
                    XmlNodeList nombre = nodos.GetElementsByTagName(Variable);
                    valor = nombre[0].InnerText;
                }
            }
            return valor;
        }

        public static MySqlConnection conectar()
        {
            string cadena = "server=" + server + ";port=" + port + ";Uid=" + usuario + ";pwd=" + pass + ";database=" + db;
            try
            {

                MySqlConnection conDB = new MySqlConnection(cadena);
                    conDB.Open();
                    return conDB;
              
          
            }
            catch (SqlTypeException ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public MySqlCommand prepararConsulta(string sql, Boolean tran = false)
        {

            MySqlCommand cmd;

            if (tran)
            {

                using (MySqlTransaction transaction = conectar().BeginTransaction())
                {
                    cmd = new MySqlCommand(sql, conectar(), transaction);
                }
           

            }
            else
            {

                cmd = new MySqlCommand(sql, conectar());

            }


            return cmd;
        }



        public void cerrarSession()
        {
            conectar().Close();
        }

        public MySqlTransaction transaction()
        {
            return conectar().BeginTransaction();
        }







    }
}
