﻿using ferreteriaApp.CEN;
using ferreteriaApp.CLN;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ferreteriaApp
{
    public partial class listadoComprobantes : Form
    {
        List<CENListadoComprobantes> listaRN = new List<CENListadoComprobantes>();
        int pagina = 0;
        int maximo_paginas = 0;
        decimal items_por_paginas = 10;
        decimal total_datos = 0;

        String dni_ruc = "";
        String name_cliente = "";
        int codigo_venta;

        String series_number;
        public listadoComprobantes()
        {
            InitializeComponent();
            this.placeHolder();
            this.btn_preview.Enabled = false;
            this.bnt_next.Enabled = false;
            this.LlenarComboTipoCmp();
            this.lblError.Text = "";

        }
        private void LlenarComboTipoCmp()
        {
            List<CENDocumentTypes> lista = new List<CENDocumentTypes>();
            this.cmbTipoComprobante.Items.Clear();
            try
            {
                CLNDocumentTypes cln = new CLNDocumentTypes();

                CENDocumentTypes cendt = new CENDocumentTypes();
             

                lista =cln.getDocumentTypes();

                this.cmbTipoComprobante.DataSource = lista;
                this.cmbTipoComprobante.DisplayMember = "description";
                this.cmbTipoComprobante.ValueMember = "id";
            }
            catch (Exception ex)
            {
                CENDocumentTypes cen = new CENDocumentTypes();
                cen.id = "0";
                cen.description = "No se encontraron documentos";
                lista = null;
                this.cmbTipoComprobante.DataSource = lista;
                this.cmbTipoComprobante.DisplayMember = "description";
                this.cmbTipoComprobante.ValueMember = "id";
            }
        }
        private string agregarCeros(int Values)
        {
            int conteos = Values.ToString().Length;
            int decimalLength = Values.ToString("D").Length + (8 - conteos);

            return Values.ToString("D" + decimalLength.ToString());
        }
        public void placeHolder()
        {
            this.txt_codigo_venta.Text = "CODIGO";
            this.txt_codigo_venta.ForeColor = Color.Silver;

            this.txt_serie.Text = "SERIE";
            this.txt_serie.ForeColor = Color.Silver;

            this.txt_number.Text = "NUMERO";
            this.txt_number.ForeColor = Color.Silver;

            this.txt_cliente.Text = "NUMERO DE DOCUMENTO / NOMBRE";
            this.txt_serie.ForeColor = Color.Silver;

        }
        private void btn_buscar_sale_note_Click(object sender, EventArgs e)
        {
            Int64 documento;

            if (this.txt_codigo_venta.Text != "CODIGO")
            {
                if (!Int64.TryParse(this.txt_codigo_venta.Text, NumberStyles.AllowThousands, CultureInfo.CreateSpecificCulture("es-ES"), out documento))
                {
                    DialogResult resultadoD = new DialogResult();
                    aviso aviso_form = new aviso("warning", "Error", "Error codigo solo debe tener números", false);
                    resultadoD = aviso_form.ShowDialog();


                    this.txt_codigo_venta.Focus();
                    return;
                }
            }


            if (this.txt_serie.Text != "SERIE" && this.txt_serie.Text.Trim().Length < 4)
            {
                DialogResult resultadoD = new DialogResult();
                aviso aviso_form = new aviso("warning", "Error", "Serie debe tener 4 dígitos", false);
                resultadoD = aviso_form.ShowDialog();


                this.txt_serie.Focus();
                return;

            }

            if (this.txt_serie.Text != "SERIE" && this.txt_number.Text == "NUMERO")
            {
                DialogResult resultadoD = new DialogResult();
                aviso aviso_form = new aviso("warning", "Error", "Escriba el número de la serie del documento", false);
                resultadoD = aviso_form.ShowDialog();


                this.txt_number.Focus();
                return;
            }

            if (this.txt_number.Text != "NUMERO" && this.txt_serie.Text == "SERIE")
            {
                DialogResult resultadoD = new DialogResult();
                aviso aviso_form = new aviso("warning", "Error", "Escriba la serie del documento", false);
                resultadoD = aviso_form.ShowDialog();


                this.txt_number.Focus();
                return;
            }




            if (this.txt_codigo_venta.Text == "CODIGO")
            {

                codigo_venta = 0;
            }
            else
            {
                if (this.txt_codigo_venta.Text.Length > 11)
                {
                    DialogResult resultadoD = new DialogResult();
                    aviso aviso_form = new aviso("warning", "Error", "11 como máximo de carácteres", false);
                    resultadoD = aviso_form.ShowDialog();

                    this.txt_number.Focus();
                    return;
                }
                else
                {
                    codigo_venta = (int)Convert.ToInt64(this.txt_codigo_venta.Text);
                    this.cmbTipoComprobante.SelectedIndex = 0;

                }

            }


            if (this.txt_number.Text.Contains(".") || this.txt_number.Text.Contains(","))
            {
                DialogResult resultadoD = new DialogResult();
                aviso aviso_form = new aviso("warning", "Error", "El campo número de documento, solo debe tener números", false);

                resultadoD = aviso_form.ShowDialog();

                this.txt_number.Focus();
                return;
            }
            else
            {
                if (this.txt_serie.Text == "SERIE" && this.txt_number.Text == "NUMERO")
                {
                    series_number = "";
                }
                else
                {
                    series_number = this.txt_serie.Text + '-' + this.agregarCeros(Convert.ToInt32(this.txt_number.Text));
                    this.cmbTipoComprobante.SelectedIndex = 0;
                }
            }


            //verificar campo dni ruc 
            if (this.txt_cliente.Text == "NUMERO DE DOCUMENTO / NOMBRE")
            {
                dni_ruc = "";
                name_cliente = "";
            }
            else
            {
                if (Char.IsNumber(this.txt_cliente.Text, 1) == true)
                {
                    //MessageBox.Show(this.txt_cliente.Text);
                    dni_ruc = this.txt_cliente.Text;
                    name_cliente = "";
                }
                else
                {
                    //MessageBox.Show("son letras");
                    name_cliente = this.txt_cliente.Text;
                    dni_ruc = "";
                }
            }


            //Console.WriteLine(dni_ruc + " cliente " + name_cliente + " tipo " + this.cmbTipoComprobante.SelectedValue.ToString());
            this.generar_busqueda();

        }

        public void generar_busqueda()
        {

            if (this.chkFechas.Checked == true)
            {

                try
                {
                    CLNDocumentTypes cln2 = new CLNDocumentTypes();
                    listaRN = cln2.search_listado_comprobantes_fechas(this.txt_fecha_inicio.Value.ToString("yyyy-MM-dd"), this.txt_fecha_fin.Value.ToString("yyyy-MM-dd"), dni_ruc, name_cliente, this.cmbTipoComprobante.SelectedValue.ToString(), series_number, codigo_venta);
                    this.cargar_datos(listaRN);
                    total_datos = listaRN.Count();

                }
                catch (Exception ex)
                {

                    DialogResult resultadoD = new DialogResult();
                    aviso aviso_form = new aviso("warning", "ERROR EN LA SOLICITUD", ex.Message, false);
                    resultadoD = aviso_form.ShowDialog();
                    return;
                }
            }
            else
            {

                try
                {
                    pagina = 0;

                    //Console.WriteLine("datos=> codigo:"+ codigo_venta.ToString() +" series: "+ series_number.ToString() +" dni_ruc: "+ dni_ruc.ToString() + " cliente:"+name_cliente);
                    CLNDocumentTypes cln2 = new CLNDocumentTypes();
                    listaRN = cln2.search_listado_comprobantes(codigo_venta, series_number, dni_ruc, name_cliente, this.cmbTipoComprobante.SelectedValue.ToString());
                    this.cargar_datos(listaRN);
                    total_datos = listaRN.Count();


                }
                catch (Exception ex)
                {
                    DialogResult resultadoD = new DialogResult();
                    aviso aviso_form = new aviso("warning", "ERROR EN LA SOLICITUD", ex.Message, false);
                    resultadoD = aviso_form.ShowDialog();
                    return;
                }

            }
        }
        private void gestionaResaltados(DataGridView visor)
        {

            for (int i = 0; i < visor.Rows.Count ; i++)
            {
                string compare =(string)visor.Rows[i].Cells[4].Value;
                if (compare == "ACEPTADO")
                {
                    visor.Rows[i].Cells[4].Style.BackColor = Color.Green;
                    visor.Rows[i].Cells[4].Style.ForeColor = Color.White;
                    visor.Rows[i].Cells[4].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
                visor.Rows[i].Cells[5].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                visor.Rows[i].Cells[6].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                visor.Rows[i].Cells[7].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                visor.Rows[i].Cells[8].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
        
        }
        public void cargar_datos(List<CENListadoComprobantes> lista)
        {
            total_datos = lista.Count();
            maximo_paginas = Convert.ToInt32(Math.Ceiling(total_datos / items_por_paginas));
            this.lbl_totalPaginas.Text = maximo_paginas.ToString();

            if (total_datos > 0)
            {
                this.lbl_pagina.Text = (pagina + 1).ToString();
            }
           
            detalleLista.DataSource = null;
            detalleLista.Columns.Clear();
            detalleLista.DataSource = lista.Skip((int)items_por_paginas * pagina).Take((int)items_por_paginas).ToList();
            DataGridViewButtonColumn bcol = new DataGridViewButtonColumn();
            bcol.HeaderText = "ACCION";
            bcol.Text = "PDF";
            bcol.Name = "colAccion";
            bcol.UseColumnTextForButtonValue = true;
            detalleLista.Columns.Add(bcol);

            this.gestionaResaltados(detalleLista);


            this.habilitar_botones();


            this.habilitar_botones();
            if (total_datos == 0)
            {
                this.lblError.Text = "NO SE ENCONTRARON RESULTADOS";
            }
            else
            {
                this.lblError.Text = "";
            }
            //Console.WriteLine("tota " + total_datos + "pagina " + pagina + " maximo paginas " + maximo_paginas + " items_por_paginas " + items_por_paginas);


        }
        public void habilitar_botones()
        {


            if (pagina == 0)
            {
                this.btn_preview.Enabled = false;
            }
            else
            {
                this.btn_preview.Enabled = true;
            }

            if (pagina == ((int)maximo_paginas - 1) || total_datos <= 0)
            {
                this.bnt_next.Enabled = false;
            }
            else
            {
                this.bnt_next.Enabled = true;
            }

        }

        private void txt_codigo_venta_Enter(object sender, EventArgs e)
        {
            if (this.txt_codigo_venta.Text == "CODIGO")
            {
                this.txt_codigo_venta.Text = "";
                this.txt_codigo_venta.ForeColor = Color.Black;
                //this.cmbTipoComprobante.SelectedValue = "0";
            }


        }

        private void txt_codigo_venta_Leave(object sender, EventArgs e)
        {
            if (this.txt_codigo_venta.Text == "")
            {
                this.txt_codigo_venta.Text = "CODIGO";
                this.txt_codigo_venta.ForeColor = Color.Silver;
            }
        }

        private void txt_codigo_venta_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
             (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') >= -1))
            {
                e.Handled = true;
            }


        }

     

        private void txt_serie_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') >= -1))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') >= -1))
            {
                e.Handled = true;
            }
        }

        private void txt_serie_Enter(object sender, EventArgs e)
        {
            if (this.txt_serie.Text == "SERIE")
            {
                this.txt_serie.Text = "";
                this.txt_serie.ForeColor = Color.Black;
            }
        }

        private void txt_serie_Leave(object sender, EventArgs e)
        {
            if (this.txt_serie.Text == "")
            {
                this.txt_serie.Text = "SERIE";
                this.txt_serie.ForeColor = Color.Silver;
            }
        }

        private void txt_number_Enter(object sender, EventArgs e)
        {
            if (this.txt_number.Text == "NUMERO")
            {
                this.txt_number.Text = "";
                this.txt_number.ForeColor = Color.Black;
            }
        }

        private void txt_number_Leave(object sender, EventArgs e)
        {
            if (this.txt_number.Text == "")
            {
                this.txt_number.Text = "NUMERO";
                this.txt_number.ForeColor = Color.Silver;
            }
        }

        private void txt_cliente_Enter(object sender, EventArgs e)
        {
            if (this.txt_cliente.Text == "NUMERO DE DOCUMENTO / NOMBRE")
            {
                this.txt_cliente.Text = "";
                this.txt_cliente.ForeColor = Color.Black;
            }

        }

        private void txt_cliente_Leave(object sender, EventArgs e)
        {
            if (this.txt_cliente.Text == "")
            {
                this.txt_cliente.Text = "NUMERO DE DOCUMENTO / NOMBRE";
                this.txt_cliente.ForeColor = Color.Silver;
            }
        }

        private void txt_cliente_KeyPress(object sender, KeyPressEventArgs e)
        {

        }


        private void txt_number_KeyPress(object sender, KeyPressEventArgs e)
        {
          
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
            (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') >= -1))
            {
                e.Handled = true;
            }
        }

        private void bnt_next_Click(object sender, EventArgs e)
        {

            pagina += 1;

            this.cargar_datos(listaRN);


        }

        private void btn_preview_Click(object sender, EventArgs e)
        {
            pagina -= 1;

            this.cargar_datos(listaRN);
        }

        private void cmbTipoComprobante_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (this.cmbTipoComprobante.SelectedValue.ToString() != "0")            {
                if (this.txt_codigo_venta.Text != "CODIGO")
                {
                    this.txt_codigo_venta.Text = "CODIGO";
                    this.txt_codigo_venta.ForeColor = Color.Silver;
                }

                if (this.txt_serie.Text != "SERIE")
                {
                    this.txt_serie.Text = "SERIE";
                    this.txt_serie.ForeColor = Color.Silver;
                }
                if (this.txt_number.Text != "SERIE")
                {
                    this.txt_number.Text = "NUMERO";
                    this.txt_number.ForeColor = Color.Silver;
                }

         
            }

        


        }

        private void detalleLista_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (this.detalleLista.Columns[e.ColumnIndex].Name == "colAccion")
                {

                  
                    int valor = e.RowIndex;
                     int id_pass = Convert.ToInt32(this.detalleLista.Rows[valor].Cells[0].Value);

                    CLNComprobante clncmp = new CLNComprobante();

                    Boolean resPDf = clncmp.recrearDocumento(id_pass);
                    if (resPDf == false)
                    {

                        DialogResult diagPDF = new DialogResult();
                        aviso aviso_form_error_hash = new aviso("warning", "ERROR AL OBTENER CODIGO HASH", "REVISE QUE FACTURADOR SUNAT GENERE XML  \n"
                                                      + "¿Desea volver a generar?", true);
                        diagPDF = aviso_form_error_hash.ShowDialog();

                        if (diagPDF == DialogResult.OK)
                        {
                            Boolean resPDf2 = clncmp.recrearDocumento(id_pass);

                            if (resPDf == false)
                            {
                                DialogResult diagPDF2 = new DialogResult();
                                aviso aviso_form_error_hash2 = new aviso("warning", "ERROR", "CONTACTESE CON SERVICIO TECNICO", true);
                                diagPDF2 = aviso_form_error_hash2.ShowDialog();
                                return;

                            }


                         }
                        else
                        {
                            return;
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                DialogResult resultadoD = new DialogResult();
                aviso aviso_form = new aviso("warning", "ERROR EN LA SOLICITUD", ex.Message, false);
                resultadoD = aviso_form.ShowDialog();
                return;
            }
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
