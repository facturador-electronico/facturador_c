﻿
namespace ferreteriaApp
{
    partial class searchSaleNote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkFechas = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_fecha_fin = new System.Windows.Forms.DateTimePicker();
            this.txt_fecha_inicio = new System.Windows.Forms.DateTimePicker();
            this.txt_number = new System.Windows.Forms.TextBox();
            this.txt_codigo_venta = new System.Windows.Forms.TextBox();
            this.txt_serie = new System.Windows.Forms.TextBox();
            this.txt_cliente = new System.Windows.Forms.TextBox();
            this.detalleLista = new System.Windows.Forms.DataGridView();
            this.panel_cabecera = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.btn_buscar_sale_note = new System.Windows.Forms.Button();
            this.txtVendedor = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.lbl_pagina = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_totalPaginas = new System.Windows.Forms.Label();
            this.btn_preview = new System.Windows.Forms.Button();
            this.bnt_next = new System.Windows.Forms.Button();
            this.panel_body = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.detalleLista)).BeginInit();
            this.panel_cabecera.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel_body.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkFechas
            // 
            this.chkFechas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkFechas.AutoSize = true;
            this.chkFechas.Location = new System.Drawing.Point(938, 126);
            this.chkFechas.Name = "chkFechas";
            this.chkFechas.Size = new System.Drawing.Size(15, 14);
            this.chkFechas.TabIndex = 42;
            this.chkFechas.UseVisualStyleBackColor = true;
            this.chkFechas.CheckedChanged += new System.EventHandler(this.chkFechas_CheckedChanged);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1104, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(12, 16);
            this.label4.TabIndex = 40;
            this.label4.Text = "-";
            // 
            // txt_fecha_fin
            // 
            this.txt_fecha_fin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_fecha_fin.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.txt_fecha_fin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txt_fecha_fin.Location = new System.Drawing.Point(1121, 126);
            this.txt_fecha_fin.Name = "txt_fecha_fin";
            this.txt_fecha_fin.Size = new System.Drawing.Size(151, 20);
            this.txt_fecha_fin.TabIndex = 5;
            // 
            // txt_fecha_inicio
            // 
            this.txt_fecha_inicio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_fecha_inicio.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.txt_fecha_inicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txt_fecha_inicio.Location = new System.Drawing.Point(954, 126);
            this.txt_fecha_inicio.Name = "txt_fecha_inicio";
            this.txt_fecha_inicio.Size = new System.Drawing.Size(144, 20);
            this.txt_fecha_inicio.TabIndex = 4;
            this.txt_fecha_inicio.Tag = "";
            // 
            // txt_number
            // 
            this.txt_number.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_number.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txt_number.Location = new System.Drawing.Point(1121, 89);
            this.txt_number.MaxLength = 8;
            this.txt_number.Name = "txt_number";
            this.txt_number.Size = new System.Drawing.Size(151, 20);
            this.txt_number.TabIndex = 3;
            this.txt_number.Enter += new System.EventHandler(this.txt_number_Enter);
            this.txt_number.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_number_KeyPress);
            this.txt_number.Leave += new System.EventHandler(this.txt_number_Leave);
            // 
            // txt_codigo_venta
            // 
            this.txt_codigo_venta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_codigo_venta.Location = new System.Drawing.Point(954, 50);
            this.txt_codigo_venta.MaxLength = 11;
            this.txt_codigo_venta.Name = "txt_codigo_venta";
            this.txt_codigo_venta.Size = new System.Drawing.Size(144, 20);
            this.txt_codigo_venta.TabIndex = 1;
            this.txt_codigo_venta.Enter += new System.EventHandler(this.txt_codigo_venta_Enter);
            this.txt_codigo_venta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_codigo_venta_KeyPress);
            this.txt_codigo_venta.Leave += new System.EventHandler(this.txt_codigo_venta_Leave);
            // 
            // txt_serie
            // 
            this.txt_serie.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_serie.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txt_serie.Location = new System.Drawing.Point(954, 89);
            this.txt_serie.MaxLength = 4;
            this.txt_serie.Name = "txt_serie";
            this.txt_serie.Size = new System.Drawing.Size(144, 20);
            this.txt_serie.TabIndex = 2;
            this.txt_serie.Enter += new System.EventHandler(this.txt_serie_Enter);
            this.txt_serie.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_serie_KeyPress);
            this.txt_serie.Leave += new System.EventHandler(this.txt_serie_Leave);
            // 
            // txt_cliente
            // 
            this.txt_cliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_cliente.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txt_cliente.Location = new System.Drawing.Point(954, 166);
            this.txt_cliente.MaxLength = 120;
            this.txt_cliente.Name = "txt_cliente";
            this.txt_cliente.Size = new System.Drawing.Size(319, 20);
            this.txt_cliente.TabIndex = 6;
            this.txt_cliente.Enter += new System.EventHandler(this.txt_cliente_Enter);
            this.txt_cliente.Leave += new System.EventHandler(this.txt_cliente_Leave);
            // 
            // detalleLista
            // 
            this.detalleLista.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.detalleLista.BackgroundColor = System.Drawing.Color.White;
            this.detalleLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.detalleLista.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detalleLista.Location = new System.Drawing.Point(0, 0);
            this.detalleLista.Name = "detalleLista";
            this.detalleLista.Size = new System.Drawing.Size(1302, 430);
            this.detalleLista.TabIndex = 8;
            this.detalleLista.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.detalleProductTable_CellContentClick);
            // 
            // panel_cabecera
            // 
            this.panel_cabecera.AutoSize = true;
            this.panel_cabecera.BackColor = System.Drawing.Color.GhostWhite;
            this.panel_cabecera.Controls.Add(this.label5);
            this.panel_cabecera.Controls.Add(this.txt_serie);
            this.panel_cabecera.Controls.Add(this.panel2);
            this.panel_cabecera.Controls.Add(this.btn_buscar_sale_note);
            this.panel_cabecera.Controls.Add(this.txt_fecha_inicio);
            this.panel_cabecera.Controls.Add(this.txt_cliente);
            this.panel_cabecera.Controls.Add(this.label4);
            this.panel_cabecera.Controls.Add(this.txtVendedor);
            this.panel_cabecera.Controls.Add(this.label11);
            this.panel_cabecera.Controls.Add(this.txt_number);
            this.panel_cabecera.Controls.Add(this.label13);
            this.panel_cabecera.Controls.Add(this.label7);
            this.panel_cabecera.Controls.Add(this.chkFechas);
            this.panel_cabecera.Controls.Add(this.txt_codigo_venta);
            this.panel_cabecera.Controls.Add(this.txt_fecha_fin);
            this.panel_cabecera.Controls.Add(this.label6);
            this.panel_cabecera.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_cabecera.Location = new System.Drawing.Point(0, 0);
            this.panel_cabecera.Name = "panel_cabecera";
            this.panel_cabecera.Size = new System.Drawing.Size(1302, 267);
            this.panel_cabecera.TabIndex = 44;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.DimGray;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(827, 126);
            this.label5.MaximumSize = new System.Drawing.Size(105, 20);
            this.label5.MinimumSize = new System.Drawing.Size(105, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 20);
            this.label5.TabIndex = 43;
            this.label5.Text = "Fechas";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.AutoSize = true;
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1302, 44);
            this.panel2.TabIndex = 50;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.MediumBlue;
            this.label24.Location = new System.Drawing.Point(49, 22);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(100, 16);
            this.label24.TabIndex = 0;
            this.label24.Text = "Buscar Venta";
            // 
            // label22
            // 
            this.label22.Image = global::ferreteriaApp.Properties.Resources.icon_edit_note_321;
            this.label22.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label22.Location = new System.Drawing.Point(6, 13);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(37, 31);
            this.label22.TabIndex = 20;
            // 
            // btn_buscar_sale_note
            // 
            this.btn_buscar_sale_note.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_buscar_sale_note.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btn_buscar_sale_note.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_buscar_sale_note.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_buscar_sale_note.Image = global::ferreteriaApp.Properties.Resources.search_32;
            this.btn_buscar_sale_note.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_buscar_sale_note.Location = new System.Drawing.Point(1178, 225);
            this.btn_buscar_sale_note.Name = "btn_buscar_sale_note";
            this.btn_buscar_sale_note.Size = new System.Drawing.Size(95, 39);
            this.btn_buscar_sale_note.TabIndex = 47;
            this.btn_buscar_sale_note.Text = "Buscar";
            this.btn_buscar_sale_note.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_buscar_sale_note.UseVisualStyleBackColor = true;
            this.btn_buscar_sale_note.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtVendedor
            // 
            this.txtVendedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVendedor.Enabled = false;
            this.txtVendedor.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtVendedor.Location = new System.Drawing.Point(954, 204);
            this.txtVendedor.Name = "txtVendedor";
            this.txtVendedor.Size = new System.Drawing.Size(319, 20);
            this.txtVendedor.TabIndex = 46;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.DimGray;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(827, 89);
            this.label11.MaximumSize = new System.Drawing.Size(105, 20);
            this.label11.MinimumSize = new System.Drawing.Size(105, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(105, 20);
            this.label11.TabIndex = 28;
            this.label11.Text = "Documento";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.DimGray;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(827, 50);
            this.label13.MaximumSize = new System.Drawing.Size(105, 20);
            this.label13.MinimumSize = new System.Drawing.Size(105, 20);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(105, 20);
            this.label13.TabIndex = 26;
            this.label13.Text = "Codigo Venta";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.DimGray;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(827, 201);
            this.label7.MaximumSize = new System.Drawing.Size(105, 20);
            this.label7.MinimumSize = new System.Drawing.Size(105, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 20);
            this.label7.TabIndex = 45;
            this.label7.Text = "Vendedor";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.DimGray;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(827, 166);
            this.label6.MaximumSize = new System.Drawing.Size(105, 20);
            this.label6.MinimumSize = new System.Drawing.Size(105, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 20);
            this.label6.TabIndex = 44;
            this.label6.Text = "Cliente";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(11, 0);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(34, 13);
            this.lblError.TabIndex = 48;
            this.lblError.Text = "Error";
            // 
            // lbl_pagina
            // 
            this.lbl_pagina.AutoSize = true;
            this.lbl_pagina.Location = new System.Drawing.Point(121, 24);
            this.lbl_pagina.Name = "lbl_pagina";
            this.lbl_pagina.Size = new System.Drawing.Size(13, 13);
            this.lbl_pagina.TabIndex = 45;
            this.lbl_pagina.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(148, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(12, 13);
            this.label2.TabIndex = 46;
            this.label2.Text = "/";
            // 
            // lbl_totalPaginas
            // 
            this.lbl_totalPaginas.AutoSize = true;
            this.lbl_totalPaginas.Location = new System.Drawing.Point(174, 24);
            this.lbl_totalPaginas.Name = "lbl_totalPaginas";
            this.lbl_totalPaginas.Size = new System.Drawing.Size(13, 13);
            this.lbl_totalPaginas.TabIndex = 47;
            this.lbl_totalPaginas.Text = "0";
            // 
            // btn_preview
            // 
            this.btn_preview.AutoEllipsis = true;
            this.btn_preview.BackColor = System.Drawing.Color.DodgerBlue;
            this.btn_preview.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_preview.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_preview.ForeColor = System.Drawing.Color.White;
            this.btn_preview.Location = new System.Drawing.Point(69, 18);
            this.btn_preview.Name = "btn_preview";
            this.btn_preview.Size = new System.Drawing.Size(26, 25);
            this.btn_preview.TabIndex = 48;
            this.btn_preview.Text = "<";
            this.btn_preview.UseVisualStyleBackColor = false;
            this.btn_preview.Click += new System.EventHandler(this.btn_preview_Click);
            // 
            // bnt_next
            // 
            this.bnt_next.AutoEllipsis = true;
            this.bnt_next.BackColor = System.Drawing.Color.DodgerBlue;
            this.bnt_next.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bnt_next.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_next.ForeColor = System.Drawing.Color.White;
            this.bnt_next.Location = new System.Drawing.Point(204, 17);
            this.bnt_next.Name = "bnt_next";
            this.bnt_next.Size = new System.Drawing.Size(26, 25);
            this.bnt_next.TabIndex = 49;
            this.bnt_next.Text = ">";
            this.bnt_next.UseVisualStyleBackColor = false;
            this.bnt_next.Click += new System.EventHandler(this.bnt_next_Click);
            // 
            // panel_body
            // 
            this.panel_body.Controls.Add(this.panel6);
            this.panel_body.Controls.Add(this.panel5);
            this.panel_body.Controls.Add(this.panel4);
            this.panel_body.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_body.Location = new System.Drawing.Point(0, 267);
            this.panel_body.Name = "panel_body";
            this.panel_body.Size = new System.Drawing.Size(1302, 458);
            this.panel_body.TabIndex = 50;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.panel3);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 395);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1302, 63);
            this.panel6.TabIndex = 53;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btn_preview);
            this.panel3.Controls.Add(this.lbl_pagina);
            this.panel3.Controls.Add(this.bnt_next);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.lbl_totalPaginas);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(1033, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(269, 63);
            this.panel3.TabIndex = 51;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.detalleLista);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 28);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1302, 430);
            this.panel5.TabIndex = 52;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lblError);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1302, 28);
            this.panel4.TabIndex = 49;
            // 
            // searchSaleNote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1302, 725);
            this.Controls.Add(this.panel_body);
            this.Controls.Add(this.panel_cabecera);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "searchSaleNote";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Buscador de Venta";
            ((System.ComponentModel.ISupportInitialize)(this.detalleLista)).EndInit();
            this.panel_cabecera.ResumeLayout(false);
            this.panel_cabecera.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel_body.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txt_cliente;
        private System.Windows.Forms.TextBox txt_number;
        private System.Windows.Forms.TextBox txt_codigo_venta;
        private System.Windows.Forms.TextBox txt_serie;
        private System.Windows.Forms.DateTimePicker txt_fecha_fin;
        private System.Windows.Forms.DateTimePicker txt_fecha_inicio;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView detalleLista;
        private System.Windows.Forms.CheckBox chkFechas;
        private System.Windows.Forms.Panel panel_cabecera;
        private System.Windows.Forms.TextBox txtVendedor;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btn_buscar_sale_note;
        private System.Windows.Forms.Label lbl_pagina;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_totalPaginas;
        private System.Windows.Forms.Button btn_preview;
        private System.Windows.Forms.Button bnt_next;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel_body;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel6;
    }
}