﻿using ferreteriaApp.CEN;
using ferreteriaApp.DAO;
using ferreteriaApp.HELPERS;
using System;
using System.Collections.Generic;

namespace ferreteriaApp.CLN
{
    public class CLNDocumentTypes
    {

     
        public List<CENDocumentTypes> getTipoDocumentosString(int flag)
        {
            try
            {
                DAODocumentTypes dao = new DAODocumentTypes();
                return dao.getTipoDocumentosString(flag);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        public string getSerieNota(string tipo, string letra)
        {
            try
            {
                DAODocumentTypes dao = new DAODocumentTypes();
                return dao.getSerieNota(tipo,letra);
            }
            catch (Exception ex)
            {
               
                throw ex;
            }
        }
        
      

        public string getNumberSeriesNotes(int flag)
        {
            try
            {
                DAODocumentTypes dao = new DAODocumentTypes();
                return dao.getNumberSeriesNotes(flag);
            }
            catch (Exception ex)
            {
               
                throw ex;
            }
        }

  

        public List<CENBuscarVentasNotas> search_comprobantes(int codigo_venta, String series_number, String dni_ruc, String name_cliente)
        {
            try
            {
                DAODocumentTypes dao = new DAODocumentTypes();
                return dao.search_comprobantes(codigo_venta, series_number, dni_ruc, name_cliente);
            }
            catch (Exception ex)
            {
             
                throw ex;
            }
        }

        public List<CENBuscarVentasNotas> search_comprobantes_fechas(int codigo_venta, String series_number, String fecha_inicio, String fecha_fin, String dni_ruc, String name_cliente)
        {
            try
            {
                DAODocumentTypes dao = new DAODocumentTypes();
                return dao.search_comprobantes_fechas(codigo_venta, series_number,fecha_inicio, fecha_fin, dni_ruc, name_cliente);
            }
            catch (Exception ex)
            {
             
                throw ex;
            }
        }

        public int insertNotes(CenNotes notes)
        {
            try
            {
                DAODocumentTypes dao = new DAODocumentTypes();
                return dao.insertNotes(notes);
            }
            catch (Exception ex)
            {

                throw ex;
            }
           

        }

        public string getSeries(String tipo_comprobante)
        {

            DAODocumentTypes dao = new DAODocumentTypes();
            return dao.getSeries(tipo_comprobante);

        }


        public string getSeriesDocuments(String tipo_comprobante)
        {
            DAODocumentTypes dao = new DAODocumentTypes();
            return dao.getSeriesDocuments(tipo_comprobante);

        }


        public void updateHash(int id, String hash)
        {
            try
            {
                DAODocumentTypes dao = new DAODocumentTypes();
                dao.updateHash(id, hash);
            }
            catch (Exception ex)
            {

                throw ex;
            }
         

        }

        public string getHash(int id)
        {
            try
            {
                DAODocumentTypes dao = new DAODocumentTypes();
                return dao.getHash(id);
            }
            catch (Exception ex)
            {

                throw ex;
            }
           

        }


        public bool getHashXML(string tipocomprobante, string serie, string numero, int id)
        {

            try
            {

                return funciones.Get_XML_Text(tipocomprobante, serie, numero, id); 
            }
            catch (Exception ex)
            {
               
                throw ex;

            }




        }

        public bool getHashXMLResumen(string tipocomprobante, string nombre_archivo, int id)
        {

            try
            {

                return funciones.Get_XML_Text_Resumen(tipocomprobante, nombre_archivo, id);
            }
            catch (Exception ex)
            {

                throw ex;

            }




        }


        //agregado para listado de comprobantes

        public List<CENListadoComprobantes> search_listado_comprobantes(CenSearchComprobantes search)
        {
            try
            {
                DAODocumentTypes dao = new DAODocumentTypes();
                return dao.search_listado_comprobantes(search);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }
        }

        public List<CENListadoComprobantes> search_listado_comprobantes_fechas(CenSearchComprobantes search)
        {
            try
            {
                DAODocumentTypes dao = new DAODocumentTypes();
                return dao.search_listado_comprobantes_fechas( search);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }
        }


        public List<CENDocumentTypes> getDocumentTypes()
        {
            try
            {
                DAODocumentTypes dao = new DAODocumentTypes();
                return dao.getDocumentTypes();
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }


        public CENDocAfectadoNota getDocAfectadoNota(int id)
        {
            try
            {
                DAODocumentTypes dao = new DAODocumentTypes();
                return dao.getDocAfectadoNota(id);
            }
            catch (Exception ex)
            {
               
                throw ex;
            }
        }

        public void setEstadoDocumento(int _id, String _id_estado)
        {
            try
            {
                DAODocumentTypes dao = new DAODocumentTypes();
                dao.setEstadoDocumento(_id,  _id_estado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public List<CENDocumentTypes> getCmbUnidadString(int flag)
        {
            try
            {
                DAODocumentTypes dao = new DAODocumentTypes();
                return dao.getCmbUnidadString(flag);
            }
            catch (Exception ex)
            {
              
                throw ex;
            }
        }
        
    }

}
