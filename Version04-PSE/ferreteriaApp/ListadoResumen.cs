﻿using ferreteriaApp.CEN;
using ferreteriaApp.CLN;
using System;
using System.Collections.Generic;

using System.Drawing;
using System.IO;
using System.Linq;

using System.Windows.Forms;
using WebVentas.util;

namespace ferreteriaApp
{
    public partial class ListadoResumen : Form
    {

        List<CENResumen> _resumen;

        public ListadoResumen()
        {
            InitializeComponent();
          
        }

        private void gestionaResaltados(DataGridView visor)
        {
            int indiceEstado = 7;

            for (int i = 0; i < visor.Rows.Count; i++)
            {

               
                string compare = (string)visor.Rows[i].Cells[0].Value;
                string ticket = (string)visor.Rows[i].Cells[0].Value;

        
                if (compare == CENConstantes.ID_ESTADO)
                {
                    visor.Rows[i].Cells[indiceEstado].Style.BackColor = System.Drawing.ColorTranslator.FromHtml("#28a745");
                    visor.Rows[i].Cells[indiceEstado].Style.ForeColor = Color.White;
                    visor.Rows[i].Cells[indiceEstado].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                        

                }
                if (compare == CENConstantes.ID_ESTADO_REGISTRADO)
                {


                    visor.Rows[i].Cells[indiceEstado].Style.BackColor = Color.Gray;
                    visor.Rows[i].Cells[indiceEstado].Style.ForeColor = Color.White;
                    visor.Rows[i].Cells[indiceEstado].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;


                }
                if (compare == CENConstantes.ID_ESTADO_XMLGENERADO)
                {
                    visor.Rows[i].Cells[indiceEstado].Style.BackColor = System.Drawing.ColorTranslator.FromHtml("#0088cc");
                    visor.Rows[i].Cells[indiceEstado].Style.ForeColor = Color.White;
                    visor.Rows[i].Cells[indiceEstado].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                

                }

                if (compare == CENConstantes.ID_ESTADO_REGISTRADO)
                {
                    visor.Rows[i].Cells[indiceEstado].Style.BackColor = System.Drawing.ColorTranslator.FromHtml("#343a40");
                    visor.Rows[i].Cells[indiceEstado].Style.ForeColor = Color.White;
                    visor.Rows[i].Cells[indiceEstado].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
                if (compare == CENConstantes.ID_ESTADO_OBSERVADO)
                {
                    visor.Rows[i].Cells[indiceEstado].Style.BackColor = Color.Orange;
                    visor.Rows[i].Cells[indiceEstado].Style.ForeColor = Color.White;
                    visor.Rows[i].Cells[indiceEstado].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }



            }
           
            visor.Columns["estado_id"].Visible = false;
            visor.Columns["tipo_estado"].Visible = false;
            visor.Columns["identificador"].Visible = false;


            DataGridViewButtonColumn reenviarCol = new DataGridViewButtonColumn();
            reenviarCol.HeaderText = "SUNAT";
            reenviarCol.Text = "ENVIAR";
            reenviarCol.Name = "colEnviar";
            reenviarCol.UseColumnTextForButtonValue = true;
            visor.Columns.Add(reenviarCol);



            DataGridViewButtonColumn bcol = new DataGridViewButtonColumn();
            bcol.HeaderText = "ACCION";
            bcol.Text = "CONSULTAR";
            bcol.Name = "colConsultar";
            bcol.UseColumnTextForButtonValue = true;
            visor.Columns.Add(bcol);

            visor.AutoResizeColumns();
            visor.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            visor.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            visor.Columns["nombre_archivo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

        

            for (int i = 0; i < visor.Rows.Count; i++)
            {
                string ticket = visor.Rows[i].Cells["ticket"].Value.ToString();

                if (ticket == "" || ticket == null)
                {
                    DataGridViewTextBoxCell oEmptyTextCell = new DataGridViewTextBoxCell();
                    oEmptyTextCell.Value = String.Empty;
                    visor.Rows[i].Cells["colConsultar"] = oEmptyTextCell;
                }
                else
                {
                    DataGridViewTextBoxCell oEmptyTextCell = new DataGridViewTextBoxCell();
                    oEmptyTextCell.Value = String.Empty;
                    visor.Rows[i].Cells["colEnviar"] = oEmptyTextCell;
                }
            }
          




        }


        private void ListadoProductos_Load(object sender, EventArgs e)
        {

            this.cargarResumen();
        }

        private void cargarResumen()
        {
            CLNComprobante clnDoc = new CLNComprobante();
            List<CENResumen> resumen = new List<CENResumen>();

            resumen = clnDoc.buscarResumenDiario(listadoComprobantes.id_pass_resumen_id);
            this.listResumen.DataSource = null;
            listResumen.Columns.Clear();
            this.listResumen.DataSource = resumen;
            _resumen = resumen;

            this.gestionaResaltados(this.listResumen);
        }
      
  
  
    

        private void ListadoProductos_Activated(object sender, EventArgs e)
        {
           
        }

        private void listResumen_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            String datosEmpresa = Helper.ObtenerValorParametro(CENConstantes.CONST_2);
            string RUC = datosEmpresa.Split('|')[0];
            string Ruta = datosEmpresa.Split('|')[1];
            String RutaEndPoint = Ruta + @"VALI\constantes.properties";
            var dataEP = new Dictionary<string, string>();
            dataEP = (Dictionary<string, string>)Helper.ReadDictionaryFile(RutaEndPoint);
            CENEmpresa cenEmp = new CENEmpresa();
            CLNSoap clnsoap = new CLNSoap();
            cenEmp = clnsoap.searchSoap();

            CLNComprobante clnCmp = new CLNComprobante();

            if (this.listResumen.Columns[e.ColumnIndex].Name == "colEnviar")
            {
                Byte[] byteArray;
                String fileZip, fileXml, ArchivoXMLzip, ArchivoCDR, ArchivoCDRruc;
                string rutaXml = "";
                String nombre_archivo, ticket;
                int valor = e.RowIndex;
                int id_pass = listadoComprobantes.id_pass_resumen_id;
                nombre_archivo = this.listResumen.Rows[valor].Cells["nombre_archivo"].Value.ToString();
                ticket = this.listResumen.Rows[valor].Cells["ticket"].Value.ToString();

             

                rutaXml = Ruta + @"FIRMA\";
                //ArchivoCDR = Ruta + @"RPTA\R" + fileZip;
           
                fileXml = rutaXml + nombre_archivo + ".xml";
                fileZip = nombre_archivo + ".zip";
                ArchivoXMLzip = Ruta + @"ENVIO\" +fileZip;

                if (!File.Exists(fileXml))
                {
                    aviso aviso_form00 = new aviso("warning", "ERROR", "El resumen " + nombre_archivo + " no ha sido creado por el SFS SUNAT \n Cree el XML y vuelva a intentar", false);
                    aviso_form00.ShowDialog();
                    return;
                }
                else
                {
                    DialogResult resultadoD2 = new DialogResult();
                    progress progressBar = new progress("ENVIANDO RESUMEN DIARIO " + nombre_archivo,3);
                    resultadoD2 = progressBar.ShowDialog();

                    byteArray = File.ReadAllBytes(ArchivoXMLzip);
                    var wsSunatGeneral = new wsSunatBeta.billServiceClient("BillServicePort1", dataEP["RUTA_SERV_CDP"]);
                    
                    wsSunatGeneral.ClientCredentials.UserName.UserName = cenEmp.usuario_soap;
                    wsSunatGeneral.ClientCredentials.UserName.Password = cenEmp.password_soap;

                    var behavior = new ferreteriaApp.DAO.PasswordDigestBehavior(cenEmp.usuario_soap, cenEmp.password_soap);
                    wsSunatGeneral.Endpoint.EndpointBehaviors.Add(behavior);
                    wsSunatGeneral.Open();

                    dynamic datos = null;
                    datos = wsSunatGeneral.sendSummary(fileZip, byteArray, "");
                    
                    clnCmp.actualizarTicket(listadoComprobantes.id_pass_resumen_id, datos);
                    wsSunatGeneral.Close();
                

                    this.cargarResumen();
                  
                }

            }

            if (this.listResumen.Columns[e.ColumnIndex].Name == "colConsultar")
            {
                String fileZip, fileXml, ArchivoXMLzip, ArchivoCDR, ArchivoCDRruc;
                string rutaXml = "";
                String nombre_archivo, ticket;
                int valor = e.RowIndex;
                int id_pass = listadoComprobantes.id_pass_resumen_id;
                nombre_archivo = this.listResumen.Rows[valor].Cells["nombre_archivo"].Value.ToString();
                ticket = this.listResumen.Rows[valor].Cells["ticket"].Value.ToString();

           
                rutaXml = Ruta + @"FIRMA\";
                //ArchivoCDR = Ruta + @"RPTA\R" + fileZip;

                //revisamos el ticket si existe en la carpeta RPTA
                if (ticket != "" || ticket != null)
                {
                    ArchivoCDR = Ruta + @"RPTA\" + ticket + ".zip";
                    ArchivoCDRruc = Ruta + @"RPTA\" + RUC + @"\" + ticket + ".zip";

                    if (File.Exists(ArchivoCDR) || File.Exists(ArchivoCDRruc))
                    {
                        //revisar 
                        aviso aviso_form00 = new aviso("success", "SUCCESS", "El ticket " + ticket + " ya ha sido enviado anteriormente.", false);
                        aviso_form00.ShowDialog();
                        return;
                    }
                    else
                    {
                        FileStream fs = new FileStream(Ruta + @"RPTA\" + "R-"+nombre_archivo + ".zip", FileMode.Create);
                        try
                        {
                            DialogResult resultadoD2 = new DialogResult();
                            progress progressBar = new progress("CONSULTANDO TICKET " + ticket, 8);
                            resultadoD2 = progressBar.ShowDialog();
                            //obteniendo endpoint
                            dynamic status = null;

                            var wsSunatGeneral = new wsSunatBeta.billServiceClient("BillServicePort1", dataEP["RUTA_SERV_CDP"]);

                            wsSunatGeneral.ClientCredentials.UserName.UserName = cenEmp.usuario_soap;
                            wsSunatGeneral.ClientCredentials.UserName.Password = cenEmp.password_soap;
                            var behavior = new ferreteriaApp.DAO.PasswordDigestBehavior(cenEmp.usuario_soap, cenEmp.password_soap);

                            wsSunatGeneral.Endpoint.EndpointBehaviors.Add(behavior);

                            wsSunatGeneral.Open();
                            //Byte[] returnByte;
                            dynamic datos = null;

                            status = wsSunatGeneral.getStatus(ticket);
                          
                            fs.Write(status.content, 0, status.content.Length);
                            fs.Close();
                           
                            wsSunatGeneral.Close();
                            //ACTUALIZAMOS ESTADOS
                            CENResponseResumen responseRes = new CENResponseResumen();
                            responseRes = Helper.verificarCdrResumenDiario(Ruta + @"RPTA\" + "R-" + nombre_archivo + ".zip", nombre_archivo + ".xml");

                            if (responseRes.code == "0")
                            {
                                
                                //revisar si tiene observaciones
                                try
                                {
                                    String estado = "";
                                    if (responseRes.observacion.Count == 0) estado = CENConstantes.ID_ESTADO; else estado = CENConstantes.ID_ESTADO_OBSERVADO;
                                 
                                    if (responseRes.observacion.Count == 0)
                                    {
                                        deleteBdSFS(Ruta, nombre_archivo);
                                    }
                                    else
                                    {
                                        CENDocumento doc = new CENDocumento();
                                        CLNDocumento daoD = new CLNDocumento();
                                        doc.fec_envi = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                                        doc.des_obse = responseRes.observacion.Count + " - Observacion(es)";
                                        doc.ind_situ = "04";
                                        doc.num_docu = nombre_archivo.Substring(12);

                                        daoD.Update(doc);

                                    }

                                    Cursor.Current = Cursors.Default;
                                    clnCmp.actualizarEstadoResumenDiario(listadoComprobantes.id_pass_resumen_id,estado);
                                    this.cargarResumen();

                                    CLNDocumentTypes clndt = new CLNDocumentTypes();
                                    CENDataComprobante dataCmp; 
                                    foreach (var item in _resumen)
                                    {
                                        foreach (var lstD in item.listaDetalle)
                                        {

                                            dataCmp = clnCmp.buscarDocumento(lstD.documento_id);

                           

                                            //seteamos el hash si esta vacio para poder imprimir pdf
                                            string get_hash = clndt.getHash(lstD.documento_id);

                                            if (get_hash.ToString() == "" || get_hash.ToString() == null)
                                            {
                                                try
                                                {
                                                    bool xml_hash;
                                                    string serie, numero;
                                                    serie = dataCmp.series.Split('-')[0];
                                                    numero = dataCmp.series.Split('-')[1];
                                                    xml_hash = clndt.getHashXMLResumen(dataCmp.tipo_comprobante,nombre_archivo, lstD.documento_id);

                                                }
                                                catch (Exception ex)
                                                {
                                                    //eliminamos de SFS y movemos los archivos
                                                    listadoComprobantes.deleteBdSFS(Ruta, RUC, dataCmp.tipo_comprobante, dataCmp.series, lstD.documento_id);

                                                    aviso aviso_sfs = new aviso("warning", "ERROR AL GRABAR CODIGO HASH DE XML", ex.ToString(), false);
                                                    aviso_sfs.ShowDialog();
                                                    throw ex;

                                                }
                                            }

                                            //eliminamos de SFS y movemos los archivos
                                            listadoComprobantes.deleteBdSFS(Ruta, RUC, dataCmp.tipo_comprobante, dataCmp.series, lstD.documento_id);

                                          
                                        }
                                    }
                                

                                    aviso aviso_form00 = new aviso("success", "Ticket " + ticket + " ha sido consultado exitosamente ",responseRes.descripcion, false);
                                    aviso_form00.ShowDialog();
                                    this.Close();
                                    return;

                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex);
                                    aviso aviso_form2 = new aviso("warning", "ERROR", ex.Message, false);
                                    aviso_form2.ShowDialog();

                                    Cursor.Current = Cursors.Default;
                                    return;

                                }
                                //cambiamos el estado a 05 en comprobante
                            }





                        }
                        catch (Exception ex)
                        {
                            fs.Close();

                            //Del 0100 al 1999 Excepciones
                            //Del 2000 al 3999 Errores que generan rechazo
                            //Del 4000 en adelante Observaciones
                            //if(ex.InnerException.Message.Contains("No se puede resolver el nombre remoto") == true)
                            if (ex.InnerException != null)
                            {
                                Console.WriteLine(ex);
                                aviso aviso_form = new aviso("warning", "Error y/o Revise su conexión a internet", ex.Message, false);
                                aviso_form.ShowDialog();
                                Cursor.Current = Cursors.Default;
                                return;
                            }
                            else
                            {
                                String codeRes0 = (String)((System.ServiceModel.FaultException)ex).Code.Name.ToString();
                                int codeRes = (int)Convert.ToInt32(codeRes0.Split('.')[1]);
                                if (codeRes < 2000)
                                {
                                    //Excepciones
                                    Console.WriteLine(ex);
                                    aviso aviso_form = new aviso("warning", "Error al invocar el servicio de SUNAT", ex.Message, false);
                                    aviso_form.ShowDialog();
                                    Cursor.Current = Cursors.Default;
                                    return;
                                }
                                else if (codeRes < 4000)
                                {
                                    //Rechazo
                                    clnCmp.actualizarEstadoResumenDiario(listadoComprobantes.id_pass_resumen_id, CENConstantes.ID_ESTADO_RECHAZADO);
                                    this.cargarResumen();

                                    CENDocumento doc = new CENDocumento();
                                    CLNDocumento daoD = new CLNDocumento();
                                    String obs = ex.Message.Split('-')[0];
                                    doc.fec_envi = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                                    doc.des_obse = codeRes + " - " + obs;
                                    doc.ind_situ = "05";
                                    doc.num_docu = nombre_archivo.Substring(12);
                                    daoD.Update(doc);
                                }
                                else
                                {

                                    //Observaciones
                                    clnCmp.actualizarEstadoResumenDiario(listadoComprobantes.id_pass_resumen_id, CENConstantes.ID_ESTADO_OBSERVADO);
                                    this.cargarResumen();
                                    String obs = ex.Message.Split('-')[0];
                                    CENDocumento doc = new CENDocumento();
                                    CLNDocumento daoD = new CLNDocumento();
                                    doc.fec_envi = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                                    doc.des_obse = codeRes + " - " + obs;
                                    doc.ind_situ = "04";
                                    doc.num_docu = nombre_archivo.Substring(12);

                                    daoD.Update(doc);
                                }

                            }

                            throw ex;
                        }


                }

            }


            }
        }

        private void deleteBdSFS(String Ruta, String nombre_archivo)
        {
            try
            {

                //seteamos a 03 en bd de factura

                CLNDocumento daoD = new CLNDocumento();

                try
                {
                    var respSFS = daoD.Delete(nombre_archivo.Substring(12));

                    if (respSFS)
                    {
                        //eliminamos en bd sfs y pasamos la data en carpeta
                        String RUC,rutaPlanos, rdi, trd,  rutaNewPlanos, rutaXml, rutaNewXml, rutaEnvio, rutaNewEnvio, fileXml, fileZip
                                            , fileRPTA, rutaRPTA, rutaNewRPTA;
                    
                        rutaPlanos = Ruta + @"DATA\";
                        rdi = nombre_archivo+".RDI";
                        trd = nombre_archivo+ ".TRD";
                       
                        RUC = nombre_archivo.Substring(0, 11);
                        rutaNewPlanos = rutaPlanos + RUC;

                        Helper.CreateIfMissing(rutaNewPlanos);

                        Helper.moveArchivo(rutaPlanos + rdi, rutaNewPlanos + @"\" + rdi);
                        Helper.moveArchivo(rutaPlanos + trd, rutaNewPlanos + @"\" + trd);
                  

                        //antes de mover xml revisamos si existe hash en el comprobante, de no existir, grabamos
                        rutaXml = Ruta + @"FIRMA\";
                        rutaNewXml = rutaXml + RUC;
                        fileXml = nombre_archivo + ".xml";
                        Helper.CreateIfMissing(rutaNewXml);
                        Helper.moveArchivo(rutaXml + fileXml, rutaNewXml + @"\" + fileXml);

                        rutaEnvio = Ruta + @"ENVIO\";
                        rutaNewEnvio = rutaEnvio + RUC;
                        fileZip = nombre_archivo + ".zip"; ;
                        Helper.CreateIfMissing(rutaNewEnvio);
                        Helper.moveArchivo(rutaEnvio + fileZip, rutaNewEnvio + @"\" + fileZip);

                        rutaRPTA = Ruta + @"RPTA\";
                        rutaNewRPTA = rutaRPTA + RUC;
                        fileRPTA = "R-" + nombre_archivo + ".zip";
                        Helper.CreateIfMissing(rutaNewRPTA);
                        Helper.moveArchivo(rutaRPTA + fileRPTA, rutaNewRPTA + @"\" + fileRPTA);

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);

                    aviso aviso_sfs = new aviso("warning", "ERROR", ex.ToString(), false);
                    aviso_sfs.ShowDialog();
                    throw ex;
                }

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex);
                aviso aviso_sfs = new aviso("warning", "ERROR", ex.ToString(), false);
                aviso_sfs.ShowDialog();

                throw ex;

            }
        }

        private void ListadoResumen_FormClosing(object sender, FormClosingEventArgs e)
        {
           
        
        }

        private void btn_close_MouseHover(object sender, EventArgs e)
        {
            this.btn_close.BackColor = Color.Red;
            this.btn_close.ForeColor = Color.White;
        }
        private void EliminarResumen()
        {
            try
            {
                int idresumen = listadoComprobantes.id_pass_resumen_id;
                CLNComprobante cln = new CLNComprobante();
                cln.eliminarResumen(idresumen);

                string datosEmpresa = WebVentas.util.Helper.ObtenerValorParametro(CENConstantes.CONST_2);
                string Ruta = datosEmpresa.Split('|')[1];
                string RUC = datosEmpresa.Split('|')[0];
               
                foreach (var item in _resumen)
                {
                    Helper.eliminarArchivo(Ruta + @"DATA\" + item.nombre_archivo + ".RDI");
                    Helper.eliminarArchivo(Ruta + @"DATA\" + item.nombre_archivo + ".TRD");
                    Helper.eliminarArchivo(Ruta + @"ENVIO\" + item.nombre_archivo + ".zip");
                    Helper.eliminarArchivo(Ruta + @"FIRMA\" + item.nombre_archivo + ".xml");
                    Helper.eliminarArchivo(Ruta + @"PARSE\" + item.nombre_archivo + ".xml");
                    Helper.eliminarArchivo(Ruta + @"TEMP\" + item.nombre_archivo + ".xml");


                    CLNDocumento daoD = new CLNDocumento();
                    var respSFS = daoD.Delete(item.nombre_archivo.Substring(12));

                }

              
            }
            catch (Exception ex)
            {

                throw ex;
            }
       


        }
        private void btn_close_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult diagRes;
                aviso aviso_form_error_hash2 = new aviso("warning", "ANTES DE CERRAR", "Se eliminará resumen diario si cierra ventana. "
                                              + " ¿Desea continuar?", true);
                diagRes = aviso_form_error_hash2.ShowDialog();
                if (diagRes == DialogResult.OK)
                {
                    EliminarResumen();
                    this.Close();
                }
                else
                {
                    return;
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btn_close_MouseLeave(object sender, EventArgs e)
        {
            this.btn_close.BackColor = Color.Transparent;
            this.btn_close.ForeColor = Color.Black;
        }
    }
}
