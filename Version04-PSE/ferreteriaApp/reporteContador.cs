﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using ferreteriaApp.CEN;
using ferreteriaApp.CLN;
using ferreteriaApp.HELPERS;
using WebVentas.util;
using Excel = Microsoft.Office.Interop.Excel;

namespace ferreteriaApp
{
    public partial class reporteContador : Form
    {
        List<CENListadoComprobantes> listaRN = new List<CENListadoComprobantes>();
        int pagina = 0;
        int maximo_paginas = 0;
        decimal items_por_paginas = 10;
        decimal total_datos = 0;

      
        decimal total_datos_excel = 0;

        String _fecha_inicio_mes;
        String _fecha_fin_mes;

        String _fecha_inicio_mes_label;
        String _fecha_fin_mes_label;

        string nombre_excel = "";
        string _ruta, _ruc, _razon = "";

        public reporteContador()
        {
            InitializeComponent();
            this.LlenarComboTipoCmp();
            this.initForm();
            this.getDataEmpresa();
        }

        private void initForm()
        {
            this.labelUbicacion.Text = "";
            this.lblError.Text = "";
            this.txt_fecha.Format = DateTimePickerFormat.Custom;
            this.txt_fecha.CustomFormat = "MMMM  yyyy";
            this.txt_fecha.ShowUpDown = true;
        }

        private void LlenarComboTipoCmp()
        {
            List<CENDocumentTypes> lista;
            this.cmbTipoComprobante.Items.Clear();
            try
            {
                CLNDocumentTypes cln = new CLNDocumentTypes();

                lista = cln.getDocumentTypes();

                this.cmbTipoComprobante.DataSource = lista;
                this.cmbTipoComprobante.DisplayMember = "description";
                this.cmbTipoComprobante.ValueMember = "id";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CENDocumentTypes cen = new CENDocumentTypes();
                cen.id = "0";
                cen.description = "No se encontraron documentos";
                lista = null;
                this.cmbTipoComprobante.DataSource = lista;
                this.cmbTipoComprobante.DisplayMember = "description";
                this.cmbTipoComprobante.ValueMember = "id";
            }
        }

        public void cargar_datos(List<CENListadoComprobantes> lista)
        {
            total_datos = lista.Count;
            maximo_paginas = Convert.ToInt32(Math.Ceiling(total_datos / items_por_paginas));
            this.lbl_totalPaginas.Text = maximo_paginas.ToString();

            if (total_datos > 0)
            {
                this.lbl_pagina.Text = (pagina + 1).ToString();
            }

            detalleLista.DataSource = null;
            detalleLista.Columns.Clear();
            detalleLista.DataSource = lista.ToList();
            this.detalleLista.Columns["ID_TC"].Visible = false;
            this.detalleLista.Columns["DESCUENTO"].Visible = false;



            if (total_datos == 0)
            {
                this.lblError.Text = "NO SE ENCONTRARON RESULTADOS";
                this.labelUbicacion.Text = "";
                return;
            }
            else
            {
                this.lblError.Text = "";
            }

            //ya no recreamos los pdf, solo los copiamos
            //RECREAR PDF PENDIENTES
            //this.RecrearPdfs();

            //Exportamos a excel
            this.toExcel();

            //Copiamos los archivos
            this.copiarTodos();

        }
        private void copiarTodos()
        {
            string Rutapass = _ruta + @"REPO\";
            string Extension = ".pdf";
            string archivo = "";

            //sim no existe creamos el documento 
            string rutaUbicacion = this.labelUbicacion.Text + "\\" + this.txt_fecha.Value.ToString("MMMM") + this.txt_fecha.Value.ToString("yyyy");
            string newFileUb ;
            try
            {
                CreateIfMissing(rutaUbicacion);

                foreach (var item in listaRN)
                {
                    archivo = Rutapass + _ruc + "-" + item.ID_TC.PadLeft(2, '0') + "-" + item.DOCUMENTO + Extension;
                    newFileUb = rutaUbicacion + @"\" + _ruc + "-" + item.ID_TC.PadLeft(2, '0') + "-" + item.DOCUMENTO + Extension;
                    CopyArchivo(archivo, newFileUb);
                }

                CopyArchivo(Rutapass + nombre_excel, rutaUbicacion + @"\" + nombre_excel);
                System.Diagnostics.Process.Start(rutaUbicacion);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        private void RecrearPdfs()
        {
            string Rutapass = _ruta + @"REPO\";
            string Extension = ".pdf";
            string archivo = "";
            CLNComprobante clncmp;

            //RECREAR PDF PENDIENTES
            foreach (var item in listaRN)
            {


                archivo = Rutapass + _ruc + "-" + item.ID_TC.PadLeft(2, '0') + "-" + item.DOCUMENTO + Extension;
                if (!File.Exists(archivo))
                {
                    clncmp = new CLNComprobante();
                    Boolean resPDf = clncmp.recrearDocumento(item.VENTA, false);
                    if (!resPDf)
                    {
                        MessageBox.Show(" Error al recrear PDF el documento " + item.DOCUMENTO + "\n Revise que el SFS haya generado el XML del comprobante");
                    }

                }
            }
        }
        private void CopyArchivo(string rutaA, string rutaB)
        {
            if (File.Exists(rutaA))
            {
                File.Copy(rutaA, rutaB);
            }
        }
        private void CreateIfMissing(string path)
        {
            try
            {
                bool folderExists = Directory.Exists(path);
                if (!folderExists) Directory.CreateDirectory(path);

                DirectoryInfo di = new DirectoryInfo(path);
                FileInfo[] files = di.GetFiles();
                foreach (FileInfo file in files)
                {
                    file.Delete();
                }


            }
            catch (Exception ex)
            {
               
                throw ex;

            }

        }

        public void generar_busqueda()
        {
            DialogResult diagRes;
            CLNDocumentTypes cln2;

            CenSearchComprobantes search = new CenSearchComprobantes
            {
                fecha_inicio = _fecha_inicio_mes,
                fecha_fin = _fecha_fin_mes,
                dni_ruc = "",
                name_cliente = "",
                tipo_comprobante = this.cmbTipoComprobante.SelectedValue.ToString(),
                series_number = "",
                codigo_venta = 0,
                _id_estado = ""
            };
           
            try
            {
                pagina = 0;
                cln2 = new CLNDocumentTypes();
                listaRN = cln2.search_listado_comprobantes_fechas(search);

                int countReg = 0;

                foreach (var item in listaRN)
                {
                    if (item.ESTADO == "REGISTRADO") countReg++;

                }

                if(countReg>0)
                {

                    // sincronizo los comprobantes faltantes
                    funciones.SyncEstadosCpePSE();

                    pagina = 0;
                    cln2 = new CLNDocumentTypes();
                    listaRN = null;
                    listaRN = cln2.search_listado_comprobantes_fechas(search);

                    int countReg2 = 0;

                    foreach (var item in listaRN)
                    {
                        if (item.ESTADO == "REGISTRADO") countReg2++;

                    }
                    if (countReg2 > 0)
                    {
                        aviso aviso_form_error_hash2 = new aviso("warning", countReg + " COMPROBANTE(S) PENDIENTE(S) DE ENVIO",
                       "Hay documento(s) pendiente(s) de envío a SUNAT. Ir a Listado de Comprobantes y sincronizar estados. ¿Desea generar de todas maneras?", true);
                        diagRes = aviso_form_error_hash2.ShowDialog();
                        if (diagRes == DialogResult.OK)
                        {
                            using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
                            {

                                System.Windows.Forms.DialogResult result = dialog.ShowDialog();

                                if (result == System.Windows.Forms.DialogResult.OK)
                                {
                                    var selectedFolder = dialog.SelectedPath;
                                    this.labelUbicacion.Text = selectedFolder;
                                }
                                else
                                {
                                    return;
                                }

                            }

                            if (labelUbicacion.Text == "" || labelUbicacion.Text == null)
                            {
                                MessageBox.Show("Debe seleccionar una ubicación a guardar");
                                return;
                            }
                            else
                            {
                                this.cargar_datos(listaRN);
                                total_datos = listaRN.Count;
                            }


                        }

                    }
                    else
                    {
                        using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
                        {

                            System.Windows.Forms.DialogResult result = dialog.ShowDialog();

                            if (result == System.Windows.Forms.DialogResult.OK)
                            {
                                var selectedFolder = dialog.SelectedPath;
                                this.labelUbicacion.Text = selectedFolder;
                            }
                            else
                            {
                                return;
                            }

                        }

                        if (labelUbicacion.Text == "" || labelUbicacion.Text == null)
                        {
                            MessageBox.Show("Debe seleccionar una ubicación a guardar");
                            return;
                        }
                        else
                        {
                            this.cargar_datos(listaRN);
                            total_datos = listaRN.Count;
                        }


                    }




                }

                else
                {
                    using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
                    {

                        System.Windows.Forms.DialogResult result = dialog.ShowDialog();

                        if (result == System.Windows.Forms.DialogResult.OK)
                        {
                            var selectedFolder = dialog.SelectedPath;
                            this.labelUbicacion.Text = selectedFolder;
                        }
                        else
                        {
                            return;
                        }

                    }

                    if (labelUbicacion.Text == "" || labelUbicacion.Text == null)
                    {
                        MessageBox.Show("Debe seleccionar una ubicación a guardar");
                        return;
                    }
                    else
                    {
                        this.cargar_datos(listaRN);
                        total_datos = listaRN.Count;
                    }


                }


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                aviso aviso_form = new aviso("warning", "ERROR EN LA SOLICITUD", ex.Message, false);
                aviso_form.ShowDialog();
                
                detalleLista.DataSource = null;
                detalleLista.Columns.Clear();
                this.labelUbicacion.Text = "";
                this.lblError.Text = "ERROR: " + ex.Message;
                return;

            }

      
        }

       

        private void button1_Click(object sender, EventArgs e)
        {
            using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
            {

                System.Windows.Forms.DialogResult result = dialog.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    var selectedFolder = dialog.SelectedPath;
                    this.labelUbicacion.Text = selectedFolder;
                }

            }

        }


        private void getDataEmpresa()
        {
            try
            {
                string datosEmpresa = Helper.ObtenerValorParametro(CENConstantes.CONST_2);
                _ruc = datosEmpresa.Split('|')[0];
                _ruta = datosEmpresa.Split('|')[1];
                _razon = datosEmpresa.Split('|')[2];
            }
            catch (Exception ex)
            {

                aviso aviso_form = new aviso("warning", "ERROR", ex.Message, false);
                aviso_form.ShowDialog();
                return;
            }
            
           
        }
        public void hideColumnsToExcel()
        {
            this.listaToExcel.Columns["VENTA"].Visible = false;
            this.listaToExcel.Columns["ID_TC"].Visible = false;
        }

        public Boolean copyAlltoClipboard()
        {

            CLNProductos clnte = new CLNProductos();
          
                if (listaRN.Count == 0)
                {

                    aviso aviso_form = new aviso("warning", "Error", "No hay comprobantes para exportar", false);
                    aviso_form.ShowDialog();
                    return false;

                }
                else
                {
                    listaToExcel.DataSource = null;
                    listaToExcel.Columns.Clear();

                    listaToExcel.DataSource = listaRN;
                    total_datos_excel = listaRN.Count;
                    this.hideColumnsToExcel();


                    listaToExcel.SelectAll();

                    DataObject dataObj = listaToExcel.GetClipboardContent();
                    if (dataObj != null)
                        Clipboard.SetDataObject(dataObj);

                    return true;
                }
        }

        public void toExcel()
        {
                      

            string sufijoPAth = DateTime.Now.ToString("ddMMyyyyHHmmss");

            nombre_excel= "ReporteVentas-" + sufijoPAth + ".xls";
            String SaveFilePath = _ruta + @"REPO\ReporteVentas-" + sufijoPAth + ".xls";
            try
            {
                if (File.Exists(SaveFilePath))
                {

                    File.Delete(SaveFilePath);
                }
            }
            catch (Exception)
            {


                aviso aviso_form = new aviso("warning", "Error", "Cierre el excel ReporteVentas-xxxxx.xls y vuelva a consultar.", false);
                aviso_form.ShowDialog();
                return;
            }

            Cursor.Current = Cursors.WaitCursor;

            Boolean data = copyAlltoClipboard();
            if (!data)
            {
                return;
            }
            Microsoft.Office.Interop.Excel.Application xlexcel;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            xlexcel = new Excel.Application();

            xlexcel.Visible = false;

            xlWorkBook = xlexcel.Workbooks.Add(misValue);

            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            xlWorkSheet.Cells[1, 1] = "VENTAS";

            xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[2, 1]].Merge();


            xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[2, 1]].Cells.VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

            xlWorkSheet.Cells[1, 2] = _razon;
            Excel.Range mergeRazon = xlWorkSheet.Range[xlWorkSheet.Cells[1, 2], xlWorkSheet.Cells[1, 12]];

            string fecha_inicio_p = _fecha_inicio_mes_label;
            string fecha_fin_p = _fecha_fin_mes_label;

            xlWorkSheet.Cells[2, 2] = "REPORTE VENTAS - FECHA: DESDE " + fecha_inicio_p + " HASTA " + fecha_fin_p;



            Excel.Range mergeTitulo = xlWorkSheet.Range[xlWorkSheet.Cells[2, 2], xlWorkSheet.Cells[2, 12]];

            mergeRazon.Merge(true);
            mergeRazon.Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            mergeRazon.Font.Bold = true;

            mergeTitulo.Merge(true);
            mergeTitulo.Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            mergeTitulo.Font.Bold = true;
          
            int anioAc = Convert.ToInt32(DateTime.Now.ToString("yyyy"));
            int mesAC = Convert.ToInt32(DateTime.Now.ToString("MM"));
            int diaAC = Convert.ToInt32(DateTime.Now.ToString("dd"));
            xlWorkSheet.Cells[1, 13].NumberFormat = "dd/MM/yyyy";
            xlWorkSheet.Cells[1, 13].Value = new DateTime(anioAc, mesAC, diaAC).ToOADate();
            xlWorkSheet.Cells[2, 13] = DateTime.Now.ToString("HH:mm:ss");

 
            xlWorkSheet.Cells[1, 13].Font.Bold = true;
            xlWorkSheet.Cells[2, 13].Font.Bold = true;

            Excel.Range boldAll = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[2, 12]];

            boldAll.Font.Bold = true;


            //header text
            xlWorkSheet.Cells[3, 1] = "ITEM";
            xlWorkSheet.Cells[3, 2] = "TIPO DOCUMENTO";
            xlWorkSheet.Cells[3, 3] = "FECHA";
            xlWorkSheet.Cells[3, 4] = "DNI/RUC";
            xlWorkSheet.Cells[3, 5] = "CLIENTE";
            xlWorkSheet.Cells[3, 6] = "NUMERO DOCUMENTO";
            xlWorkSheet.Cells[3, 7] = "ESTADO";
            xlWorkSheet.Cells[3, 8] = "FORMA PAGO";
            xlWorkSheet.Cells[3, 9] = "MONEDA";
            xlWorkSheet.Cells[3, 10] = "GRAVADO";
            xlWorkSheet.Cells[3, 11] = "IGV";
            xlWorkSheet.Cells[3, 12] = "DESCUENTO";
            xlWorkSheet.Cells[3, 13] = "TOTAL";

      

            var columnHeadingsRange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[3, 13]];
            columnHeadingsRange.Interior.Color = Color.Gray;
            columnHeadingsRange.Font.Color = Color.White;
            columnHeadingsRange.Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

            var columnRangeDecimal = xlWorkSheet.Range[xlWorkSheet.Cells[4, 9], xlWorkSheet.Cells[3 + total_datos_excel, 13]];
            columnRangeDecimal.NumberFormat = "##0.00";

            var columnRangeFechas = xlWorkSheet.Range[xlWorkSheet.Cells[4, 3], xlWorkSheet.Cells[3 + total_datos_excel, 3]];
            columnRangeFechas.NumberFormat = "DD/MM/YYYY";

            Excel.Range CR = (Excel.Range)xlWorkSheet.Cells[4, 1];
            CR.Select();
            CR.Columns.AutoFit();
            xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);

            decimal t_datos = 0;
            t_datos = (total_datos_excel == 0) ? 1 : total_datos_excel;
            var allCells = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[3 + total_datos_excel, 13]];
            allCells.Columns.AutoFit();

            Console.WriteLine(total_datos_excel.ToString());
            int indice = 0;
            for (int i = 4; i < 4 + total_datos_excel; i++)
            {
                indice = indice + 1;
                xlWorkSheet.Cells[i, 1] = indice.ToString();
            }
            Excel.Range CR2 = (Excel.Range)xlWorkSheet.Cells[1, 10];

            CR2.Columns.AutoFit();
            xlWorkBook.SaveAs(SaveFilePath, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);

            xlWorkBook.Close(true, misValue, misValue);
            xlexcel.Quit();

            Cursor.Current = Cursors.Default;
        }

        private void button4_Click(object sender, EventArgs e)
        {

            try
            {
              

                var anio = Convert.ToInt32(this.txt_fecha.Value.ToString("yyyy"));
                var mes = Convert.ToInt32(this.txt_fecha.Value.ToString("MM"));

                var month = new DateTime(anio, mes, 1);
                var monthD = new DateTime(anio, mes + 1, 1);
                var inicioDiaMes = month.AddMonths(0).ToString("yyyy-MM-dd");
                var ultimoDiaMes = monthD.AddDays(-1).ToString("yyyy-MM-dd");

                _fecha_inicio_mes = inicioDiaMes;
                _fecha_fin_mes = ultimoDiaMes;

                _fecha_inicio_mes_label = month.AddMonths(0).ToString("dd-MM-yyyy");
                _fecha_fin_mes_label = monthD.AddDays(-1).ToString("dd-MM-yyyy");

                this.generar_busqueda();
            }
            catch (Exception ex)
            {

                aviso aviso_form = new aviso("warning", "ERROR", ex.Message, false);
                aviso_form.ShowDialog();
                return;
            }
           

        }
    }
    }

