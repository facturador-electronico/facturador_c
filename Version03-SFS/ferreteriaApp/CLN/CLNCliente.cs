﻿using ferreteriaApp.CEN;
using ferreteriaApp.DAO;
using System;
using System.Collections.Generic;

namespace ferreteriaApp.CLN
{
    public class CLNCliente
    {



        public bool existeCliente(string num_documento)
        {
            try
            {
                
                DAOCliente db = new DAOCliente();
                return db.existeCliente(num_documento);

            }
            catch (Exception ex)
            {

                return false;
            }

        }

        public CENDataCliente getCliente(string num_documento)
        {
            try
            {
                CENDataCliente cliente = new CENDataCliente();
                DAOCliente db = new DAOCliente();
                cliente= db.getCliente(num_documento);

                return cliente;

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public void insertClients(CENDataCliente cliente)
        {
            try
            {

                DAOCliente dao = new DAOCliente();
                dao.insertClients(cliente);

            }
            catch (Exception ex)
            {

                throw ex;
            }

         

        }

        public bool updateClients(CENDataCliente cliente)
        {
            try
            {

                DAOCliente dao = new DAOCliente();
                dao.updateClients(cliente);
                return true;

            }
            catch (Exception ex)
            {
                
                return false;
              
            }



        }


        public List<CENDataCliente> buscarCliente(String _descripcion, String _codigo_interno)
        {
            try
            {
                DAOCliente dao = new DAOCliente();
                return dao.buscarCliente(_descripcion, _codigo_interno);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}
