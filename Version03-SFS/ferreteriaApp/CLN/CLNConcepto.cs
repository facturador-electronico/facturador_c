﻿using ferreteriaApp.CEN;
using ferreteriaApp.DAO;
using System;
using System.Collections.Generic;

namespace ferreteriaApp.CLN
{
    public class CLNConcepto
    {
        public List<CENTipoDocumento> listaTipoDocumentos(int flag)
        {
            try
            {
                DAOConceptos dao = new DAOConceptos();
                return dao.documentos(flag);
            }
            catch (Exception ex)
            {
              
                throw ex;
            }
        }


        public  string ObtenerValorParametro(int flag)
        {
            try
            {
                DAOConceptos db = new DAOConceptos();
                return db.ObtenerValorParametro(flag);

            }
            catch (Exception ex)
            {

                throw ex;
            }
           
        }


        public void actualizarCheckSunatReniec(int flag)
        {
            try
            {
                DAOConceptos db = new DAOConceptos();
                db.actualizarCheckSunatReniec(flag);

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public CENParametro buscarParametro(int _codigo)
        {
            try
            {
                DAOConceptos dao = new DAOConceptos();
                return dao.buscarParametro(_codigo);
            }
            catch (Exception ex)
            {
               
                throw ex;
            }
        }
    }

}
