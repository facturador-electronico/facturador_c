﻿using System;
using System.Collections.Generic;

using ferreteriaApp.DAO;
using ferreteriaApp.CEN;

namespace ferreteriaApp.CLN
{
    class CLNDocumento
    {
   

        public void Update(CENDocumento doc)
        {
            try
            {
                DAODocumento dao = new DAODocumento();
                
                dao.Update(doc); 
            }
            catch (Exception ex)
            {
         
                throw ex;
            }

        }

        public Boolean Delete(String num_doc)
        {
            try
            {
                DAODocumento dao = new DAODocumento();
                return dao.Delete(num_doc);
            }
            catch (Exception ex)
            {
           
                throw ex;
            }

        }

        public List<CENDocumento> lista()
        {
            try
            {
                DAODocumento dao = new DAODocumento();
                return dao.lista();
            }
            catch (Exception ex)
            {
              
                throw ex;
            }
           
        }
    }
}
