﻿using ferreteriaApp.CEN;
using ferreteriaApp.DAO;
using ferreteriaApp.HELPERS;
using ferreteriaApp.INTERFACES;
using System;
using System.Collections.Generic;
using WebVentas.util;

namespace ferreteriaApp.CLN
{
    public class CLNComprobante : boletaInt
    {

        public string codigoBoleta(string tipo)
        {
            DAOComprobante dao = new DAOComprobante();
            return dao.CodigoBoleta(tipo);
        }

        public string codigoNotas(string tipo, string letra)
        {
            DAOComprobante dao = new DAOComprobante();
            return dao.CodigoNotas(tipo, letra);
        }

        public CENEstructuraComprobante Find(long id)
        {
            DAOComprobante dao = new DAOComprobante();

            try
            {
                return dao.Find(id);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }
        }

        public int GuardarBoleta(CENComprobanteCabecera cabecera, List<CENComprobanteDetalle> detalle)
        {
            DAOComprobante dao = new DAOComprobante();
            return dao.guardarBoleta(cabecera, detalle);
        }

        public List<CENComprobanteCabecera> listarBoletas()
        {
            DAOComprobante dao = new DAOComprobante();
            return dao.All();
        }



        public void CrearArchivos(CENEstructuraComprobante comprobante, string tipocomprobante, string serie, string numero)
        {
            try
            {

                Helper.GenerarArchivoPlanoFacturaBoletaCabecera(comprobante, tipocomprobante, serie, numero);

              
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void GenerarArchivoTRI(string identidicador_tributo,string nombre_tributo,string identificador_tributo_UNEC,string base_imponible, string monto_tributo, string TipoComprobante, string serie, string numero) {
            try
            {
                Helper.GenerarArchivoTRI( identidicador_tributo,nombre_tributo,identificador_tributo_UNEC,base_imponible,monto_tributo, TipoComprobante,  serie,  numero);

            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        public void crearArchivoLey(string number,  string TipoComprobante, string Serie, string Numero) {
            try
            {
                Helper.crearArchivoLey( number,TipoComprobante,Serie,Numero);



            }
            catch (Exception ex) {
                
                throw ex;
            }

        }

        public void CrearArchivosACV(CENComprobanteCabecera comprobante, string tipocomprobante, string serie, string numero)
        {
            try
            {

                Helper.GenerarArchivoPlanoACV(comprobante, tipocomprobante, serie, numero);


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //agregado
        public void CrearArchivosNotas(CENEstructuraComprobante comprobante, string tipocomprobante, string serie, string numero, String codigo_tipo_nota, String descripcion_nota)
            {
                try
                {

                    Helper.GenerarArchivoPlanoNotasCabecera(comprobante, tipocomprobante, serie, numero, codigo_tipo_nota, descripcion_nota);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
       
        public Boolean CrearPDF(List<string> CabeceraComprobante,
           string[] NombresHeaderTable, float[] TamCeldas,
           List<string> BobyTable, List<string> Impuestos,
            string tipocomprobante,
           string serie, string numero, string nombreComprobante, string cantidadEnLetras,
           List<string> subtotal, List<string> datosHash,int id_comprobante)
        {
            try
            {
              return   HELPERS.CrearPDF.crearDocumento(CabeceraComprobante, NombresHeaderTable, TamCeldas,
                    BobyTable, Impuestos, tipocomprobante, serie, numero, nombreComprobante,
                    cantidadEnLetras, subtotal,datosHash, id_comprobante);

            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }


        public Boolean CrearPDFNotas(List<string> CabeceraComprobante,
                                    string[] NombresHeaderTable, float[] TamCeldas,
                                    List<string> BobyTable, List<string> Impuestos,
                                    string tipocomprobante,
                                    string serie, string numero, string nombreComprobante, string cantidadEnLetras,
                                    List<string> subtotal,  int id_comprobante, List<string> datosHash)
        {
            try
            {
                HELPERS.CrearPDF.crearDocumentoNotas(CabeceraComprobante, NombresHeaderTable, TamCeldas,
                    BobyTable, Impuestos, tipocomprobante, serie, numero, nombreComprobante,
                    cantidadEnLetras, subtotal,  id_comprobante, datosHash);

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;

            }
        }
        public Boolean recrearDocumento(int id_comprobante, Boolean openPdf)
        {
            try
            {

                return HELPERS.CrearPDF.RecrearDocumento(id_comprobante, openPdf);
            }
            catch (Exception ex)
            {

                
                return false;
              
            }
        }



        public void crearArchivoPag( string TipoComprobante, string Serie, string Numero, decimal total)
        {
            try
            {
                Helper.crearArchivoPag( TipoComprobante, Serie, Numero, total);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }

        }


        public void deleteComprobantexError(int id)
        {
            try
            {
                DAOComprobante dao = new DAOComprobante();
                dao.deleteComprobantexError(id);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public int buscarResumenCorrelativo(string fecha)
        {
            try
            {
                DAOComprobante dao = new DAOComprobante();
               return dao.buscarResumenCorrelativo(fecha);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public int GenerarArchivoPlanoResumenDiarioCabecera(CENEstructuraComprobante comprobante, string tipocomprobante, string serie, string numero, int id)
        {
            try
            {
                return Helper.GenerarArchivoPlanoResumenDiarioCabecera(comprobante,  tipocomprobante,  serie,  numero, id);



            }
            catch (Exception ex)
            {

                throw ex;
            }

        }


        public int insertarResumenDiario(CENResumen cabecera, List<CENDetalleResumen> detalle)
        {
            try
            {
                DAOComprobante dao = new DAOComprobante();
                return dao.insertarResumenDiario(cabecera, detalle);
            }
            catch (Exception ex)
            {

                throw ex;
            }
           
        }


        public List<CENResumen> buscarResumenDiario(int id)
        {

            try
            {
                DAOComprobante dao = new DAOComprobante();
                return dao.buscarResumenDiario(id);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public void actualizarTicket(int id, string ticket)
        {
            try
            {
                DAOComprobante dao = new DAOComprobante();
                dao.actualizarTicket(id, ticket);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void actualizarEstadoResumenDiario(int id, string ticket)
        {
            try
            {
                DAOComprobante dao = new DAOComprobante();
                dao.actualizarEstadoResumenDiario(id, ticket);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void eliminarResumen(int id)
        {
            try
            {
                DAOComprobante dao = new DAOComprobante();
                dao.eliminarResumen(id);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        public CENDataComprobante buscarDocumento(int id)
        {
            try
            {
                DAOComprobante dao = new DAOComprobante();
                return dao.buscarDocumento(id);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public CenNotes buscarNota(int id)
        {
            try
            {
                DAOComprobante dao = new DAOComprobante();
                return dao.buscarNota(id);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }






    }



}

