﻿using ferreteriaApp.CEN;
using ferreteriaApp.CLN;
using System;
using System.Collections.Generic;

using System.Drawing;
using System.Linq;

using System.Windows.Forms;

namespace ferreteriaApp
{
    public partial class ListadoProductos : Form
    {

        List<CENListadoProducto> productos;

        public ListadoProductos()
        {
            InitializeComponent();
          
        }

        public void buscarProductos()
        {
            string[] pr;
            string l_pr = "";
            int i_p = 1;
            int conteo = 0;
          
            pr = this.txt_producto.Text.Trim().Split(' ');
            conteo = pr.Count();
            foreach (string productos in pr)
            {
                l_pr = l_pr.ToString().Trim() + productos.ToString().Trim();
                if (i_p < conteo) l_pr = l_pr.ToString().Trim() + "|";
                i_p = i_p + 1;
            }


            txt_mensaje.Text = "";

            CLNProductos cln = new CLNProductos();

            if (txt_producto.Text.Trim() == "" && txt_codigo.Text.Trim() == "")
            {
                productos = cln.buscarProducto(l_pr, txt_codigo.Text.Trim(), 0);

                if (productos.Count == 0)
                {
                    txt_mensaje.Visible = true;
                    txt_mensaje.Text = "";

                    txt_mensaje.Text = "No se encontraron productos";
                    listProductos.Rows.Clear();

                }
                else
                {

                    listarProductos(productos);

                }


            }
            else
            {

                productos = cln.buscarProducto(l_pr, txt_codigo.Text.Trim(), 0);

                if (productos.Count == 0)
                {
                    txt_mensaje.Visible = true;
                    txt_mensaje.Text = "";

                    txt_mensaje.Text = "No se encontraron productos con los datos ingresados";
                    listProductos.Rows.Clear();

                }
                else
                {

                    listarProductos(productos);

                }

            }

            this.listProductos.Focus();
            this.listProductos.Rows[0].Selected = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.buscarProductos();



        }

        private void ListadoProductos_Load(object sender, EventArgs e)
        {
            this.txt_producto.Focus();

            listProductos.DataSource = null;

            listProductos.ColumnCount = 5;
            listProductos.ColumnHeadersDefaultCellStyle.BackColor = Color.Navy;
            listProductos.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            listProductos.ColumnHeadersDefaultCellStyle.Font =
                new Font(listProductos.Font, FontStyle.Bold);

            listProductos.Name = "listProductos";
            listProductos.Location = new Point(8, 8);
            listProductos.Size = new Size(950, 250);
            listProductos.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            listProductos.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            listProductos.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            listProductos.GridColor = Color.Black;
            listProductos.RowHeadersVisible = false;

            listProductos.Columns[0].Name = "Codigo";
            listProductos.Columns[1].Name = "Descripción";
            listProductos.Columns[2].Name = "Unidad Medida";
            listProductos.Columns[3].Name = "MONEDA";
            listProductos.Columns[4].Name = "Precio venta";
            listProductos.Columns[4].DefaultCellStyle.Font = new Font(listProductos.DefaultCellStyle.Font, FontStyle.Italic);
            listProductos.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            listProductos.MultiSelect = false;
            listProductos.Dock = DockStyle.Fill;

            foreach (DataGridViewColumn Col in listProductos.Columns)
            {
                Col.SortMode = DataGridViewColumnSortMode.NotSortable;
            }

            
        }
        public void SeleccionarProducto()
        {
            try
            {
                dashboard dash = Owner as dashboard;
                int indice = listProductos.CurrentCell.RowIndex;

                var prod = productos.ToList().Find(item => item.DESCRIPCION == listProductos.Rows[indice].Cells[1].Value.ToString());

                if (prod is null)
                {
                    MessageBox.Show("La fila seleccionada no contiene un producto");
                }
                else
                {
                    dash.Textcodigo.Text = listProductos.Rows[indice].Cells[0].Value.ToString();
                    dash.TextPrdo.Text = listProductos.Rows[indice].Cells[1].Value.ToString();
                    dash.Textunidad.Text = listProductos.Rows[indice].Cells[2].Value.ToString();
                    dash._tipoMoneda = prod.MONEDA_ID;
                    dash.Textcodigo.ReadOnly = true;
                    dash.TextPrecio.ReadOnly = true;
                    dash.TextPrdo.ReadOnly = true;

                    if (prod.CON_IGV == "SI")
                    {
                        dash.TextPrecio.Text = listProductos.Rows[indice].Cells[4].Value.ToString();
                    }
                    else
                    {
                        decimal precio = Convert.ToDecimal(listProductos.Rows[indice].Cells[4].Value.ToString());
                        dash.TextPrecio.Text = Convert.ToString(  Math.Round(precio + Convert.ToDecimal(precio * Convert.ToDecimal(0.18)),2 ));

                    }

                    dash._afectacionIGV = prod.CON_IGV == "SI" ? true : false;
                    dash._busquedaForm = true;
                    dash._codigo_sunat = prod.CODIGO_SUNAT;
                    dash._unidad_medida = prod.UNIDAD_ID;

                    
                    this.Close();

                }




            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("La fila seleccionada no contiene un producto");

            }
        }
   
        private void btn_seleccionar_Click(object sender, EventArgs e)
        {
            this.SeleccionarProducto();
        }

        private void listarProductos(List<CENListadoProducto> productos)
        {

            txt_mensaje.Text = "";

            listProductos.Rows.Clear();

            var prod = productos.ToList().OrderBy(item => item.DESCRIPCION);

            foreach (CENListadoProducto item in prod)
            {

                int table = listProductos.Rows.Add();

                listProductos.Rows[table].Cells[0].Value = item.CODIGO_PRODUCTO;
                listProductos.Rows[table].Cells[1].Value = item.DESCRIPCION;
                listProductos.Rows[table].Cells[2].Value = item.UNIDAD_MEDIDA;
                listProductos.Rows[table].Cells[3].Value = item.MONEDA;
                listProductos.Rows[table].Cells[4].Value = item.PRECIO;
                listProductos.Rows[table].DefaultCellStyle.ForeColor = Color.Black;

            }
        }

        private void txt_producto_KeyUp(object sender, KeyEventArgs e)
        {

            if(e.KeyCode == Keys.Enter)
            {
                this.buscarProductos();
               
            }


        }

        private void ListadoProductos_Activated(object sender, EventArgs e)
        {
            this.txt_producto.Focus();
        }

        private void listProductos_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            this.SeleccionarProducto();
        }

        private void listProductos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.listProductos.CurrentRow.Selected = true;
                e.Handled = true;
                this.SeleccionarProducto();
            }
        }

        private void txt_producto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar.ToString()=="+" 
                || e.KeyChar.ToString() == "*"
                || e.KeyChar.ToString() == "("
                || e.KeyChar.ToString() == ")"
                || e.KeyChar.ToString() == "["
                || e.KeyChar.ToString() == "]"
                )
            {
                e.Handled = true;
                return;
            }

        }
    }
}
