﻿using ferreteriaApp.CEN;
using System.Collections.Generic;

namespace ferreteriaApp.INTERFACES
{
    interface boletaInt
    {

        int GuardarBoleta(CENComprobanteCabecera cabecera, List<CENComprobanteDetalle> detalle);

        List<CENComprobanteCabecera> listarBoletas();

        CENEstructuraComprobante Find(long id);

    }
}
