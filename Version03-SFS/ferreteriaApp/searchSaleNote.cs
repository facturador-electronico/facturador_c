﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using ferreteriaApp.CEN;
using ferreteriaApp.CLN;

namespace ferreteriaApp
{
    public partial class searchSaleNote : Form
    {
        List<CENBuscarVentasNotas> listaRN = new List<CENBuscarVentasNotas>();
        int pagina = 0;
        int maximo_paginas = 0;
        decimal items_por_paginas = 10;
        decimal total_datos = 0;

        String dni_ruc = "";
        String name_cliente = "";
        int codigo_venta;

        String series_number;

        public static int id_pass;
        int dias_antes=0, _dias_antes_boleta=0;


        public searchSaleNote()
        {
            InitializeComponent();
            this.placeHolder();
            this.btn_preview.Enabled = false;
            this.bnt_next.Enabled = false;
            this.lblError.Text = "";
            this.getParamFechas();
            this.txt_fecha_inicio.Enabled = false;
            this.txt_fecha_fin.Enabled = false;
        }



        private void getParamFechas()
        {
            CLNConcepto clnConcepto = new CLNConcepto();

            String param = clnConcepto.ObtenerValorParametro(17);
            string par1;
            par1 = param.Split(',')[0];
           
            dias_antes =Convert.ToInt32(par1);


            //para boletas

            
            CENParametro parFlagRegrear = new CENParametro();
            CLNConcepto clncconcepto = new CLNConcepto();
            parFlagRegrear = clncconcepto.buscarParametro(1009);
            _dias_antes_boleta = parFlagRegrear.par_int1;

        }


        public void placeHolder()
        {
            this.txt_codigo_venta.Text = "CODIGO";
            this.txt_codigo_venta.ForeColor = Color.Silver;

            this.txt_serie.Text = "SERIE";
            this.txt_serie.ForeColor = Color.Silver;

            this.txt_number.Text = "NUMERO";
            this.txt_number.ForeColor = Color.Silver;

            this.txt_cliente.Text = "NUMERO DE DOCUMENTO / NOMBRE";
            this.txt_serie.ForeColor = Color.Silver;

        }

        private void detalleProductTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (this.detalleLista.Columns[e.ColumnIndex].Name == "colAccion")
                {
                    //String documento, letraInicial;
                    //int dias_antes_parseo = 0;
                    //documento = this.detalleLista.Rows[e.RowIndex].Cells["DOCUMENTO"].Value.ToString();
                    //letraInicial = documento.Substring(0, 1);

                    //if (letraInicial == "B") dias_antes_parseo = _dias_antes_boleta; else dias_antes_parseo = dias_antes;

                    //DateTime today = DateTime.Today;
                    //DateTime fecha_doc = Convert.ToDateTime(this.detalleLista.Rows[e.RowIndex].Cells[2].Value);
                    //TimeSpan tSpan = today - fecha_doc;
                    //int dias_pasados = Convert.ToInt32(tSpan.Days);
                    //if (dias_pasados > dias_antes_parseo - 1)
                    //{
                    //    String tituloAviso = "No es posible aplicar Nota";
                    //    String textoAviso = "Fecha de venta (" + fecha_doc.ToString("dd/MM/yyyy") + ") excede a los " + dias_antes_parseo + " días permitidos.";

                    //    aviso aviso_form = new aviso("warning", tituloAviso, textoAviso, false);
                    //    aviso_form.ShowDialog();

                    //    return;
                    //}


                    int valor = e.RowIndex;
                    id_pass = Convert.ToInt32(this.detalleLista.Rows[valor].Cells[0].Value);

                    
                    notes form_notes = new notes();
                    form_notes.ShowDialog();
                    this.generar_busqueda();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
               
                aviso aviso_form = new aviso("warning", "ERROR EN LA SOLICITUD", ex.Message, false);
                aviso_form.ShowDialog();
                
            }
            
        }
        private string agregarCeros(int Values)
        {
            int conteos = Values.ToString().Length;
            int decimalLength = Values.ToString("D").Length + (8-conteos);

            return Values.ToString("D" + decimalLength.ToString());
        }
      


     
        public void button2_Click(object sender, EventArgs e)
        {
            Int64 documento;

            if(this.txt_codigo_venta.Text!= "CODIGO" && !Int64.TryParse(this.txt_codigo_venta.Text, NumberStyles.AllowThousands, CultureInfo.CreateSpecificCulture("es-ES"), out documento))
            {
               
                    aviso aviso_form = new aviso("warning", "Error", "Error codigo solo debe tener números", false);
                    aviso_form.ShowDialog();

                    this.txt_codigo_venta.Focus();
                    return;
                
            }

         
            if ( this.txt_serie.Text!= "SERIE" &&  this.txt_serie.Text.Trim().Length <4)
            {
               
                aviso aviso_form = new aviso("warning", "Error", "Serie debe tener 4 dígitos", false);
                aviso_form.ShowDialog();

                this.txt_serie.Focus();
                return;

            }
           
                if (this.txt_serie.Text != "SERIE"  && this.txt_number.Text == "NUMERO")
                {
                    
                    aviso aviso_form = new aviso("warning", "Error", "Escriba la serie del documento", false);
                    aviso_form.ShowDialog();

                    this.txt_number.Focus();
                    return;
                }
            
            if (this.txt_number.Text != "NUMERO" && this.txt_serie.Text == "SERIE")
            {
                
                aviso aviso_form = new aviso("warning", "Error", "Escriba el número correlativo de la serie del documento", false);
                aviso_form.ShowDialog();

                this.txt_number.Focus();
                return;
            }




            if (this.txt_codigo_venta.Text == "CODIGO")
            {

                codigo_venta = 0;
            }
            else
            {
                if (this.txt_codigo_venta.Text.Length > 11)
                {
                  
                    aviso aviso_form = new aviso("warning", "Error", "11 como máximo de carácteres", false);
                    aviso_form.ShowDialog();

                    this.txt_number.Focus();
                    return;
                }
                else
                {
                    codigo_venta = (int)Convert.ToInt64(this.txt_codigo_venta.Text);
               
                }

            }


            if (this.txt_number.Text.Contains(".") || this.txt_number.Text.Contains(","))
            {
                
                aviso aviso_form = new aviso("warning", "Error", "El campo número de documento, solo debe tener números", false);
                aviso_form.ShowDialog();

                this.txt_number.Focus();
                return;
            }
            else
            {
                if (this.txt_serie.Text == "SERIE" && this.txt_number.Text == "NUMERO")
                {
                    series_number = "";
                }
                else
                {
                    series_number = this.txt_serie.Text + '-' + this.agregarCeros(Convert.ToInt32(this.txt_number.Text));
                }
            }

         

         

            

            //verificar campo dni ruc 
            if (this.txt_cliente.Text == "NUMERO DE DOCUMENTO / NOMBRE")
            {
                dni_ruc = "";
                name_cliente ="";
            }
            else
            {
               if (Char.IsNumber(this.txt_cliente.Text, 1))
                {
                    
                    dni_ruc = this.txt_cliente.Text;
                    name_cliente = "";
                }
                else
                {
                  
                    name_cliente = this.txt_cliente.Text;
                    dni_ruc = "";
                }
            }
           
            this.generar_busqueda();

        }
   
        public void generar_busqueda()
        {

            if (this.chkFechas.Checked)
            {

                try
                {
                    CLNDocumentTypes cln2 = new CLNDocumentTypes();
                    
                    listaRN = cln2.search_comprobantes_fechas(codigo_venta, series_number, this.txt_fecha_inicio.Value.ToString("yyyy-MM-dd"), this.txt_fecha_fin.Value.ToString("yyyy-MM-dd"), dni_ruc, name_cliente);
                    this.cargar_datos(listaRN);
                    total_datos = listaRN.Count;

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    
                    aviso aviso_form = new aviso("warning", "ERROR EN LA SOLICITUD", ex.Message, false);
                    aviso_form.ShowDialog();
                  
                }
            }
            else
            {

                try
                {
                    pagina = 0;

                  
                    CLNDocumentTypes cln2 = new CLNDocumentTypes();
                    listaRN = cln2.search_comprobantes(codigo_venta, series_number, dni_ruc, name_cliente);
                    this.cargar_datos(listaRN);
                    total_datos = listaRN.Count;


                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    aviso aviso_form = new aviso("warning", "ERROR EN LA SOLICITUD", ex.Message, false);
                    aviso_form.ShowDialog();
                 
                }

            }
        }

        private void txt_codigo_venta_Enter(object sender, EventArgs e)
        {
            if(this.txt_codigo_venta.Text == "CODIGO")
            {
                this.txt_codigo_venta.Text = "";
                this.txt_codigo_venta.ForeColor = Color.Black;
            }
         
           
        }

        private void txt_codigo_venta_Leave(object sender, EventArgs e)
        {
            if (this.txt_codigo_venta.Text == "")
            {
                this.txt_codigo_venta.Text = "CODIGO";
                this.txt_codigo_venta.ForeColor = Color.Silver;
            }
        }

        private void txt_codigo_venta_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
             (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') >= -1))
            {
                e.Handled = true;
            }
        }

        private void txt_serie_Enter(object sender, EventArgs e)
        {
            if (this.txt_serie.Text == "SERIE")
            {
                this.txt_serie.Text = "";
                this.txt_serie.ForeColor = Color.Black;
            }
        }

        private void txt_serie_Leave(object sender, EventArgs e)
        {
            if (this.txt_serie.Text == "")
            {
                this.txt_serie.Text = "SERIE";
                this.txt_serie.ForeColor = Color.Silver;
            }
        }

        private void txt_number_Enter(object sender, EventArgs e)
        {
            if (this.txt_number.Text == "NUMERO")
            {
                this.txt_number.Text = "";
                this.txt_number.ForeColor = Color.Black;
            }
        }

        private void txt_number_Leave(object sender, EventArgs e)
        {
            if (this.txt_number.Text == "")
            {
                this.txt_number.Text = "NUMERO";
                this.txt_number.ForeColor = Color.Silver;
            }
        }

        private void txt_number_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
              (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') >= -1))
            {
                e.Handled = true;
            }
        }

        private void txt_cliente_Enter(object sender, EventArgs e)
        {
            if (this.txt_cliente.Text == "NUMERO DE DOCUMENTO / NOMBRE")
            {
                this.txt_cliente.Text = "";
                this.txt_cliente.ForeColor = Color.Black;
            }
           
        }

        private void txt_cliente_Leave(object sender, EventArgs e)
        {
            if (this.txt_cliente.Text == "")
            {
                this.txt_cliente.Text = "NUMERO DE DOCUMENTO / NOMBRE";
                this.txt_cliente.ForeColor = Color.Silver;
            }
        }
    

        public void cargar_datos(List<CENBuscarVentasNotas> lista)
        {
            total_datos = lista.Count;
            maximo_paginas = Convert.ToInt32(Math.Ceiling(total_datos / items_por_paginas));
            this.lbl_totalPaginas.Text = maximo_paginas.ToString();
          
            if (total_datos > 0)
            {
                this.lbl_pagina.Text = (pagina + 1).ToString();
            }


            detalleLista.DataSource = null;
            detalleLista.Columns.Clear();
            detalleLista.DataSource = lista.Skip((int)items_por_paginas * pagina).Take((int)items_por_paginas).ToList();
            DataGridViewButtonColumn bcol = new DataGridViewButtonColumn();
            bcol.HeaderText = "ACCION";
            bcol.Text = "Generar Nota";
            bcol.Name = "colAccion";
            bcol.UseColumnTextForButtonValue = true;
            detalleLista.Columns.Add(bcol);

            this.habilitar_botones();
            if (total_datos == 0)
            {
                this.lblError.Text = "NO SE ENCONTRARON RESULTADOS";
            }
            else
            {
                this.lblError.Text = "";
            }

          


        }
        public void habilitar_botones()
        {
          

            if (pagina == 0)
            {
                this.btn_preview.Enabled = false;
            }
            else
            {
                this.btn_preview.Enabled = true;
            }

            if(pagina== (maximo_paginas - 1) || total_datos <= 0)
            {
                this.bnt_next.Enabled = false;
            }
            else
            {
                this.bnt_next.Enabled = true;
            }
        }
        private void bnt_next_Click(object sender, EventArgs e)
        {
            
            pagina +=1;

            this.cargar_datos(listaRN);
         
           
        }

        private void btn_preview_Click(object sender, EventArgs e)
        {
            pagina -= 1;

            this.cargar_datos(listaRN);
        }

        private void txt_serie_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar == '.' ) && ((sender as TextBox).Text.IndexOf('.') >= -1))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') >= -1))
            {
                e.Handled = true;
            }

      
        }

        private void chkFechas_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chkFechas.Checked)
            {

                this.txt_fecha_inicio.Enabled = true;
                this.txt_fecha_fin.Enabled = true;
            }
            else
            {
                this.txt_fecha_inicio.Enabled = false;
                this.txt_fecha_fin.Enabled = false;
            }
        }
    }
    }

