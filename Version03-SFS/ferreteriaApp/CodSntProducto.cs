﻿using ferreteriaApp.CEN;
using ferreteriaApp.CLN;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ferreteriaApp
{
    public partial class CodSntProducto : Form
    {

        List<CENListadoCodSntProducto> productos;
        private String codigoPass;
        public String CodigoP
        {
            get { return codigoPass; }
            set { codigoPass = value; }
        }
        public CodSntProducto()
        {
            InitializeComponent();

            llenarComboSegmento();
            this.lblError.Visible = false;
        }
     
        private void button1_Click(object sender, EventArgs e)
        {
            int cod_clase = 0;
           if(this.cmbClase.SelectedIndex == -1)
            {
                cod_clase = 0;
                if (this.txt_codigo.Text.Trim() == "")
                {
                    this.lblError.Text = " Digite nombre del producto a buscar";
                    this.lblError.Visible = true;
                    return;
                }
            
            }
            else
            {
                this.lblError.Visible = false;
                cod_clase = (int)this.cmbClase.SelectedValue;
            }

          

            this.listProductos.DataSource = null;
            try
            {
                CLNProductos cln = new CLNProductos();
                productos = cln.buscarCodSntPdt(5, 0, cod_clase, this.txt_codigo.Text.Trim().ToString());
                this.listProductos.DataSource = productos;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CENListadoCodSntProducto cen = new CENListadoCodSntProducto();
                cen.CODIGO = 0;
                cen.DESCRIPCION = "No se encontraron resultados";
                productos = null;
                this.listProductos.DataSource = productos;
            }

        }

        public void llenarComboSegmento()
        {
            List<CENListadoCodSntProducto> lista;
            this.cmbSegmento.Items.Clear();
            try
            {
                CLNProductos cln = new CLNProductos();
                lista = cln.buscarCodSntPdt(1,0,0,"");
                this.cmbSegmento.DataSource = lista;
                this.cmbSegmento.DisplayMember = "DESCRIPCION";
                this.cmbSegmento.ValueMember = "CODIGO";
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CENListadoCodSntProducto cen = new CENListadoCodSntProducto();
                cen.CODIGO =0;
                cen.DESCRIPCION = "No se encontraron resultados";
                lista = null;
                this.cmbSegmento.DataSource = lista;
                this.cmbSegmento.DisplayMember = "DESCRIPCION";
                this.cmbSegmento.ValueMember = "CODIGO";
            }
        }


    

        private void btn_seleccionar_Click(object sender, EventArgs e)
        {

                if (listProductos.CurrentCell != null)
                {
                    int valor = listProductos.CurrentCell.RowIndex;
                    codigoPass = this.listProductos.Rows[valor].Cells[0].Value.ToString();
                    this.Close();
                }
                else
                {
                    
                    aviso aviso_form = new aviso("warning", "Error", "Seleccione un producto de la lista", false);
                    aviso_form.ShowDialog();
                }

     
        }

 

        private void cmbSegmento_SelectedIndexChanged(object sender, EventArgs e)
        {


            this.txt_codigo.Text = "";
            this.lblError.Visible = false;
            List<CENListadoCodSntProducto> lista_familia;
            this.cmbFamilia.DataSource = null;
    
            try
            {
                CLNProductos cln = new CLNProductos();
                lista_familia = cln.buscarCodSntPdt(2, (int)this.cmbSegmento.SelectedValue, 0, "");
                this.cmbFamilia.DataSource = lista_familia;
                this.cmbFamilia.DisplayMember = "DESCRIPCION";
                this.cmbFamilia.ValueMember = "CODIGO";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CENListadoCodSntProducto cen = new CENListadoCodSntProducto();
                cen.CODIGO = 0;
                cen.DESCRIPCION = "No se encontraron resultados";
                lista_familia = null;
                this.cmbFamilia.DataSource = lista_familia;
                this.cmbFamilia.DisplayMember = "DESCRIPCION";
                this.cmbFamilia.ValueMember = "CODIGO";
            }
            



        }

        private void cmbFamilia_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            this.txt_codigo.Text = "";
            this.lblError.Visible = false;
            List<CENListadoCodSntProducto> lista_clase;
            this.cmbClase.DataSource = null;
            try
            {
                CLNProductos cln = new CLNProductos();
                lista_clase = cln.buscarCodSntPdt(3, (int)this.cmbFamilia.SelectedValue, 0, "");
                this.cmbClase.DataSource = lista_clase;
                this.cmbClase.DisplayMember = "DESCRIPCION";
                this.cmbClase.ValueMember = "CODIGO";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CENListadoCodSntProducto cen = new CENListadoCodSntProducto();
                cen.CODIGO =0;
                cen.DESCRIPCION = "No se encontraron resultados";
                lista_clase = null;
                this.cmbClase.DataSource = lista_clase;
                this.cmbClase.DisplayMember = "DESCRIPCION";
                this.cmbClase.ValueMember = "CODIGO";
            }
        }

        private void cmbClase_SelectedIndexChanged(object sender, EventArgs e)
        {

            this.txt_codigo.Text = "";
            this.lblError.Visible = false;
          
              
                this.listProductos.DataSource = null;
                try
                {
                    CLNProductos cln = new CLNProductos();
                    productos = cln.buscarCodSntPdt(4, (int)this.cmbClase.SelectedValue,0, "");
                    this.listProductos.DataSource = productos;
                    this.txt_codigo.Text = "";
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    CENListadoCodSntProducto cen = new CENListadoCodSntProducto();
                    cen.CODIGO = 0;
                    cen.DESCRIPCION = "No se encontraron resultados";
                    productos = null;
                    this.listProductos.DataSource = productos;
                }
            
           
        }
    }
}
