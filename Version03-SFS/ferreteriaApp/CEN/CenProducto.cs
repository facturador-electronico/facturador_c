﻿using System;


namespace ferreteriaApp.CEN
{
    public class CenProducto
    {
        public String descripcion { get; set; }
        public String unidad_medida_id { get; set; }
        public String moneda_id { get; set; }
        public int afectacion_venta_id { get; set; }
        public int afectacion_compra_id { get; set; }

        public decimal precio_venta { get; set; }
        public decimal precio_compra { get; set; }
        public String codigo_interno { get; set; }
        
        public String codigo_sunat { get; set; }
        public String has_igv { get; set; } 
        public DateTime fecha_proceso { get; set; }
        public DateTime fecha_transaccion { get; set; }

     








    }
}
