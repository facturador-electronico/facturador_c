﻿using System.Collections.Generic;

namespace ferreteriaApp.CEN
{
    public class CENComprobanteCabecera
    {
        public int empresa { get; set; }

        public System.DateTime fechaEmision { get; set; }
        public string fechaVencimiento { get; set; }
        public string nombreCliente { get; set; }
        public string documentoCliente { get; set; }
        public string direccionCliente { get; set; }
        public string tipoMoneda { get; set; }
        public string tipoDocumento { get; set; } // 1-> dni 06 ->ruc

        public string estado_comprobante { get; set; }

        //---- campos agregados

        public int tipoOperacion { get; set; } // -> 1 venta

        public string tipoComprobante { get; set; } //-> factura - boleta 


        //--------------------------- ---


        public string codigo { get; set; }
        // datos de producto vendido
        public decimal igv { get; set; }
        public decimal isc { get; set; }
        public decimal otros { get; set; }
        public decimal descuentoGlobal { get; set; }
        public decimal subtotal { get; set; }
        public decimal descuentoTotal { get; set; }
        public decimal anticipos { get; set; }
        public decimal importeTotal { get; set; }


        public List<CENComprobanteDetalle> listDetalle { get; set; }
    }
}
