﻿using System;


namespace ferreteriaApp.CEN
{
    public static class CENConstantes
    {
        
        public const String USER = "vendedor";

        public const String SOLES = "SOLES";
        public const String PEN = "PEN";

        public const int CONST_2 = 2;
        public const int CONST_10 = 10;


        //correlativo de comprobantes
        public const string F001 = "F001";
        public const string B001 = "B001";
        public const string BOLETA = "03";
        public const string FACTURA = "01";

        public const string CODIGO_DNI = "1";
        public const string CODIGO_RUC = "6";

        public const string FACTURA_ELECTRONICA = "FACTURA ELECTRÓNICA";
        public const string BOLETA_ELECTRONICA = "BOLETA DE VENTA ELECTRÓNICA";

        //estados del comprobante

        public const string ID_ESTADO = "05";                           //ESTADO ACEPTADO
        public const string ID_ESTADO_REGISTRADO = "01";                //ESTADO REGISTRADO
        public const string ID_ESTADO_XMLGENERADO = "02";               //ESTADO XMLGENERADO
        public const string ID_ESTADO_ENVIADO = "03";                   //ESTADO ENVIADO - PARA BOLETAS (RESUMEN, BETA)
        public const string ID_ESTADO_FUERA_LIMITE= "04";               //ESTADO FUERA DE LIMITE DE ENVIO ( SI ES BOLETA SI SE PUEDE ENVIAR
        public const string ID_ESTADO_OBSERVADO = "07";                 //ESTADO OBSERVADO
        public const string ID_ESTADO_RECHAZADO = "09";                 //ESTADO RECHAZADO

        public const string DNI_DEFAULT = "00000000";                   //DNI POR DEFECTO PARA BOLETAS
        public const string NOMBRE_DNI_DEFAULT = "CLIENTE VARIOS";      //NOMBRE CLIENTE POR DEFECTO PARA BOLETAS

        //DESCUENTOS POR ITEM
        public const string CODTIPO_DESC_ITEM_AFECTABASE = "00";        //DESCUENTOS QUE AFECTAN LA BASE IMPONIBLE DEL IGV/IVAP
        public const string CODTIPO_DESC_ITEML_NOAFECTABASE = "01";     //DESCUENTOS QUE NO AFECTAN LA BASE IMPONIBLE DEL IGV/IVAP
        //DESCUENTO GLOBALES
        public const string CODTIPO_DESC_GLOBAL_AFECTABASE = "02";      //DESCUENTOS GLOBALES QUE AFECTAN LA BASE IMPONIBLE DEL IGV/IVAP
        public const string CODTIPO_DESC_GLOBAL_NOAFECTABASE = "03";    //DESCUENTOS GLOBALES QUE NO AFECTAN LA BASE IMPONIBLE DEL IGV/IVAP





    }

  
}
