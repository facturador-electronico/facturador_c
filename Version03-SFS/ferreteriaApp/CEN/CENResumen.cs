﻿using System;
using System.Collections.Generic;

namespace ferreteriaApp.CEN
{
    public class CENResumen
    {

        public string estado_id { get; set; }
        public string tipo_estado { get; set; }
        public string fecha_emision { get; set; }
        public string fecha_referencia { get; set; }
        public string identificador { get; set; }
        public string nombre_archivo { get; set; }
        public string ticket { get; set; }
        public string estado { get; set; }

        public List<CENDetalleResumen> listaDetalle { get; set; }

    }

    public class CENDetalleResumen
    {

        public int resumen_id { get; set; }
        public int documento_id { get; set; }

    }
}
