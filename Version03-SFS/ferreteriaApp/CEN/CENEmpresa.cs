﻿namespace ferreteriaApp.CEN
{
    public class CENEmpresa
    {
        public string ruc { get; set; }
        public string razonSocial { get; set; }
        public string direccion { get; set; }
        public string logo { get; set; }
        public string rutaDowland { get; set; }
        public string contacto { get; set; }
        public string usuario_soap { get; set; }
        public string password_soap { get; set; }

    }
}
