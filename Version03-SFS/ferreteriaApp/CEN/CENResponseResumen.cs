﻿using System.Collections.Generic;

namespace ferreteriaApp.CEN
{
    public class CENResponseResumen
    {
        public string descripcion { get; set; }
        public string code  { get;set; }
        public List<string> observacion { get; set; }

    }
}
