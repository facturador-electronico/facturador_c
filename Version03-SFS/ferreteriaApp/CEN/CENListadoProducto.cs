﻿using System;


namespace ferreteriaApp.CEN
{
    public class CENListadoProducto
    {
        public int ID { get; set; }
        public String CODIGO_PRODUCTO { get; set; }
        public String DESCRIPCION { get; set; }
        public String UNIDAD_MEDIDA { get; set; }
       
        public String MONEDA { get; set; }
        public String AFECTACION_IGV { get; set; }
        public decimal PRECIO { get; set; }
        public String CON_IGV { get; set; }

        public String CODIGO_SUNAT { get; set; }

        public String MONEDA_ID { get; set; }
        public String UNIDAD_ID { get; set; }
        public String AFECTACION_ID { get; set; }







    }
}
