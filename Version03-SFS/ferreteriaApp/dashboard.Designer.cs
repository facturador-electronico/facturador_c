﻿namespace ferreteriaApp
{
    partial class dashboard
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dashboard));
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.txtDireccionCliente = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_documento = new System.Windows.Forms.TextBox();
            this.txtCliente = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_cantidad = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_descuento = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_codigo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_precio = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_producto = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txt_titulo_comprobante = new System.Windows.Forms.Label();
            this.txt_codigoBoleta = new System.Windows.Forms.Label();
            this.txt_fecha = new System.Windows.Forms.DateTimePicker();
            this.detalleProductTable = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkDescuentoGlobal = new System.Windows.Forms.CheckBox();
            this.txt_total = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.txt_igv = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txt_descuentoGlobal = new System.Windows.Forms.TextBox();
            this.txt_subtotal = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txt_mensaje = new System.Windows.Forms.Label();
            this.txt_codigo_comprobante = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblCheckSunatReniec = new System.Windows.Forms.Label();
            this.chkSunatReniec = new System.Windows.Forms.CheckBox();
            this.mensaje_documento = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label39 = new System.Windows.Forms.Label();
            this.chkDescuento = new System.Windows.Forms.CheckBox();
            this.cmbUnidad = new System.Windows.Forms.ComboBox();
            this.lbl_modificar = new System.Windows.Forms.Label();
            this.chk_editar_precio = new System.Windows.Forms.CheckBox();
            this.btn_link_products = new System.Windows.Forms.LinkLabel();
            this.txt_id_row = new System.Windows.Forms.Label();
            this.lbl_mensaje = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lbl_descripcion_obligatorios = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.btn_agragr_producto = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.cantidad_productos = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.btn_eliminar = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.detalleProductTable)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.comboBox1.Items.AddRange(new object[] {
            "BOLETA",
            "FACTURA"});
            this.comboBox1.Location = new System.Drawing.Point(13, 61);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(326, 21);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // txtDireccionCliente
            // 
            this.txtDireccionCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDireccionCliente.Location = new System.Drawing.Point(17, 108);
            this.txtDireccionCliente.Name = "txtDireccionCliente";
            this.txtDireccionCliente.Size = new System.Drawing.Size(538, 20);
            this.txtDireccionCliente.TabIndex = 6;
            this.txtDireccionCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDireccionCliente_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(14, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Dirección";
            // 
            // txt_documento
            // 
            this.txt_documento.Enabled = false;
            this.txt_documento.Location = new System.Drawing.Point(337, 60);
            this.txt_documento.MaxLength = 12;
            this.txt_documento.Name = "txt_documento";
            this.txt_documento.Size = new System.Drawing.Size(168, 20);
            this.txt_documento.TabIndex = 4;
            this.txt_documento.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_documento_KeyUp);
            // 
            // txtCliente
            // 
            this.txtCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCliente.Location = new System.Drawing.Point(17, 60);
            this.txtCliente.Name = "txtCliente";
            this.txtCliente.Size = new System.Drawing.Size(305, 20);
            this.txtCliente.TabIndex = 3;
            this.txtCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCliente_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(191, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombres y/o Razón social";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(334, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(164, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Número de documento";
            // 
            // txt_cantidad
            // 
            this.txt_cantidad.Location = new System.Drawing.Point(528, 76);
            this.txt_cantidad.Name = "txt_cantidad";
            this.txt_cantidad.Size = new System.Drawing.Size(69, 20);
            this.txt_cantidad.TabIndex = 10;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(525, 60);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(57, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "Cantidad";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(603, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Uni. Medida";
            // 
            // txt_descuento
            // 
            this.txt_descuento.Location = new System.Drawing.Point(728, 76);
            this.txt_descuento.Name = "txt_descuento";
            this.txt_descuento.ReadOnly = true;
            this.txt_descuento.Size = new System.Drawing.Size(78, 20);
            this.txt_descuento.TabIndex = 12;
            this.txt_descuento.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(725, 60);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Descuento";
            // 
            // txt_codigo
            // 
            this.txt_codigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_codigo.Location = new System.Drawing.Point(293, 76);
            this.txt_codigo.Name = "txt_codigo";
            this.txt_codigo.Size = new System.Drawing.Size(109, 20);
            this.txt_codigo.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(290, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Código";
            // 
            // txt_precio
            // 
            this.txt_precio.Location = new System.Drawing.Point(413, 76);
            this.txt_precio.Name = "txt_precio";
            this.txt_precio.Size = new System.Drawing.Size(108, 20);
            this.txt_precio.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(410, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Precio de venta";
            // 
            // txt_producto
            // 
            this.txt_producto.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_producto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_producto.Location = new System.Drawing.Point(14, 74);
            this.txt_producto.Name = "txt_producto";
            this.txt_producto.Size = new System.Drawing.Size(263, 20);
            this.txt_producto.TabIndex = 7;
            this.txt_producto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_producto_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Producto";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.AutoSize = true;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.txt_titulo_comprobante);
            this.panel2.Controls.Add(this.txt_codigoBoleta);
            this.panel2.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.panel2.Location = new System.Drawing.Point(681, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(298, 61);
            this.panel2.TabIndex = 27;
            // 
            // txt_titulo_comprobante
            // 
            this.txt_titulo_comprobante.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_titulo_comprobante.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.txt_titulo_comprobante.Location = new System.Drawing.Point(3, 6);
            this.txt_titulo_comprobante.Name = "txt_titulo_comprobante";
            this.txt_titulo_comprobante.Size = new System.Drawing.Size(287, 20);
            this.txt_titulo_comprobante.TabIndex = 7;
            this.txt_titulo_comprobante.Text = "FACTURA ELECTRÓNICA";
            this.txt_titulo_comprobante.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_codigoBoleta
            // 
            this.txt_codigoBoleta.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_codigoBoleta.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.txt_codigoBoleta.Location = new System.Drawing.Point(7, 30);
            this.txt_codigoBoleta.Name = "txt_codigoBoleta";
            this.txt_codigoBoleta.Size = new System.Drawing.Size(283, 20);
            this.txt_codigoBoleta.TabIndex = 6;
            this.txt_codigoBoleta.Text = "F001-000";
            this.txt_codigoBoleta.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_fecha
            // 
            this.txt_fecha.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.txt_fecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txt_fecha.Location = new System.Drawing.Point(17, 110);
            this.txt_fecha.Name = "txt_fecha";
            this.txt_fecha.Size = new System.Drawing.Size(322, 20);
            this.txt_fecha.TabIndex = 2;
            // 
            // detalleProductTable
            // 
            this.detalleProductTable.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.detalleProductTable.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.detalleProductTable.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.detalleProductTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.detalleProductTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.detalleProductTable.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.detalleProductTable.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.detalleProductTable.Location = new System.Drawing.Point(4, 10);
            this.detalleProductTable.Name = "detalleProductTable";
            this.detalleProductTable.ReadOnly = true;
            this.detalleProductTable.RowHeadersWidth = 51;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.detalleProductTable.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.detalleProductTable.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.detalleProductTable.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Silver;
            this.detalleProductTable.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.detalleProductTable.Size = new System.Drawing.Size(793, 159);
            this.detalleProductTable.TabIndex = 28;
            this.detalleProductTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.detalleProductTable_CellContentClick_1);
            this.detalleProductTable.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.detalleProductTable_CellPainting);
            this.detalleProductTable.KeyDown += new System.Windows.Forms.KeyEventHandler(this.detalleProductTable_KeyDown);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.chkDescuentoGlobal);
            this.panel1.Controls.Add(this.txt_total);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label40);
            this.panel1.Controls.Add(this.txt_igv);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.txt_descuentoGlobal);
            this.panel1.Controls.Add(this.txt_subtotal);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Location = new System.Drawing.Point(160, 218);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(648, 36);
            this.panel1.TabIndex = 31;
            // 
            // chkDescuentoGlobal
            // 
            this.chkDescuentoGlobal.AutoSize = true;
            this.chkDescuentoGlobal.Location = new System.Drawing.Point(24, 10);
            this.chkDescuentoGlobal.Name = "chkDescuentoGlobal";
            this.chkDescuentoGlobal.Size = new System.Drawing.Size(15, 14);
            this.chkDescuentoGlobal.TabIndex = 34;
            this.chkDescuentoGlobal.UseVisualStyleBackColor = true;
            this.chkDescuentoGlobal.CheckedChanged += new System.EventHandler(this.chkDescuentoGlobal_CheckedChanged);
            // 
            // txt_total
            // 
            this.txt_total.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_total.Location = new System.Drawing.Point(570, 8);
            this.txt_total.Name = "txt_total";
            this.txt_total.ReadOnly = true;
            this.txt_total.Size = new System.Drawing.Size(66, 20);
            this.txt_total.TabIndex = 23;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(527, 11);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 13);
            this.label16.TabIndex = 24;
            this.label16.Text = "TOTAL";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(40, 11);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(119, 13);
            this.label40.TabIndex = 34;
            this.label40.Text = "DESCUENTO GLOBAL";
            // 
            // txt_igv
            // 
            this.txt_igv.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_igv.Location = new System.Drawing.Point(455, 8);
            this.txt_igv.Name = "txt_igv";
            this.txt_igv.ReadOnly = true;
            this.txt_igv.Size = new System.Drawing.Size(66, 20);
            this.txt_igv.TabIndex = 21;
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(424, 11);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(25, 13);
            this.label15.TabIndex = 22;
            this.label15.Text = "IGV";
            // 
            // txt_descuentoGlobal
            // 
            this.txt_descuentoGlobal.Location = new System.Drawing.Point(164, 8);
            this.txt_descuentoGlobal.Name = "txt_descuentoGlobal";
            this.txt_descuentoGlobal.ReadOnly = true;
            this.txt_descuentoGlobal.Size = new System.Drawing.Size(78, 20);
            this.txt_descuentoGlobal.TabIndex = 35;
            this.txt_descuentoGlobal.Text = "0";
            this.txt_descuentoGlobal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_descuentoGlobal.TextChanged += new System.EventHandler(this.txt_descuentoGlobal_TextChanged);
            this.txt_descuentoGlobal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_descuentoGlobal_KeyPress);
            // 
            // txt_subtotal
            // 
            this.txt_subtotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_subtotal.Location = new System.Drawing.Point(343, 8);
            this.txt_subtotal.Name = "txt_subtotal";
            this.txt_subtotal.ReadOnly = true;
            this.txt_subtotal.Size = new System.Drawing.Size(78, 20);
            this.txt_subtotal.TabIndex = 20;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(260, 11);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 13);
            this.label14.TabIndex = 20;
            this.label14.Text = "SUBTOTAL";
            // 
            // txt_mensaje
            // 
            this.txt_mensaje.AutoSize = true;
            this.txt_mensaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_mensaje.ForeColor = System.Drawing.Color.Red;
            this.txt_mensaje.Location = new System.Drawing.Point(27, 602);
            this.txt_mensaje.Name = "txt_mensaje";
            this.txt_mensaje.Size = new System.Drawing.Size(0, 20);
            this.txt_mensaje.TabIndex = 32;
            // 
            // txt_codigo_comprobante
            // 
            this.txt_codigo_comprobante.AutoSize = true;
            this.txt_codigo_comprobante.Location = new System.Drawing.Point(145, 37);
            this.txt_codigo_comprobante.Name = "txt_codigo_comprobante";
            this.txt_codigo_comprobante.Size = new System.Drawing.Size(0, 13);
            this.txt_codigo_comprobante.TabIndex = 33;
            this.txt_codigo_comprobante.Visible = false;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Controls.Add(this.lblCheckSunatReniec);
            this.panel3.Controls.Add(this.chkSunatReniec);
            this.panel3.Controls.Add(this.mensaje_documento);
            this.panel3.Controls.Add(this.label28);
            this.panel3.Controls.Add(this.label27);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.label19);
            this.panel3.Controls.Add(this.txtDireccionCliente);
            this.panel3.Controls.Add(this.txt_documento);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.txtCliente);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Location = new System.Drawing.Point(398, 72);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(581, 141);
            this.panel3.TabIndex = 35;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ferreteriaApp.Properties.Resources.search_32;
            this.pictureBox1.Location = new System.Drawing.Point(298, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(24, 22);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 53;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.pictureBox1.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.pictureBox1.MouseHover += new System.EventHandler(this.pictureBox1_MouseHover);
            // 
            // lblCheckSunatReniec
            // 
            this.lblCheckSunatReniec.AutoSize = true;
            this.lblCheckSunatReniec.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckSunatReniec.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblCheckSunatReniec.Location = new System.Drawing.Point(526, 63);
            this.lblCheckSunatReniec.Name = "lblCheckSunatReniec";
            this.lblCheckSunatReniec.Size = new System.Drawing.Size(49, 13);
            this.lblCheckSunatReniec.TabIndex = 51;
            this.lblCheckSunatReniec.Text = "SUNAT";
            // 
            // chkSunatReniec
            // 
            this.chkSunatReniec.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkSunatReniec.AutoSize = true;
            this.chkSunatReniec.FlatAppearance.BorderColor = System.Drawing.SystemColors.HotTrack;
            this.chkSunatReniec.FlatAppearance.BorderSize = 3;
            this.chkSunatReniec.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.chkSunatReniec.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.chkSunatReniec.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.chkSunatReniec.Location = new System.Drawing.Point(509, 63);
            this.chkSunatReniec.Name = "chkSunatReniec";
            this.chkSunatReniec.Size = new System.Drawing.Size(15, 14);
            this.chkSunatReniec.TabIndex = 5;
            this.chkSunatReniec.UseVisualStyleBackColor = true;
            this.chkSunatReniec.CheckedChanged += new System.EventHandler(this.chkSunatReniec_CheckedChanged);
            // 
            // mensaje_documento
            // 
            this.mensaje_documento.AutoSize = true;
            this.mensaje_documento.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.mensaje_documento.Location = new System.Drawing.Point(353, 84);
            this.mensaje_documento.Name = "mensaje_documento";
            this.mensaje_documento.Size = new System.Drawing.Size(0, 13);
            this.mensaje_documento.TabIndex = 28;
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Red;
            this.label28.Location = new System.Drawing.Point(498, 41);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(18, 13);
            this.label28.TabIndex = 27;
            this.label28.Text = "*";
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Red;
            this.label27.Location = new System.Drawing.Point(202, 42);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(18, 13);
            this.label27.TabIndex = 26;
            this.label27.Text = "*";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(172, 11);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(119, 13);
            this.label22.TabIndex = 21;
            this.label22.Text = "| Campos obligatorios (*)";
            // 
            // label19
            // 
            this.label19.Image = global::ferreteriaApp.Properties.Resources.icons8_client_management_32px;
            this.label19.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label19.Location = new System.Drawing.Point(3, 2);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(37, 31);
            this.label19.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.MediumBlue;
            this.label7.Location = new System.Drawing.Point(41, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(125, 16);
            this.label7.TabIndex = 0;
            this.label7.Text = "Datos del cliente";
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.label30);
            this.panel4.Controls.Add(this.label29);
            this.panel4.Controls.Add(this.label23);
            this.panel4.Controls.Add(this.label21);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.txt_fecha);
            this.panel4.Controls.Add(this.comboBox1);
            this.panel4.Location = new System.Drawing.Point(12, 72);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(365, 141);
            this.panel4.TabIndex = 36;
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Location = new System.Drawing.Point(144, 94);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(18, 13);
            this.label30.TabIndex = 28;
            this.label30.Text = "*";
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Red;
            this.label29.Location = new System.Drawing.Point(258, 42);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(18, 13);
            this.label29.TabIndex = 27;
            this.label29.Text = "*";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(204, 11);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(119, 13);
            this.label23.TabIndex = 22;
            this.label23.Text = "| Campos obligatorios (*)";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(11, 41);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(250, 16);
            this.label21.TabIndex = 21;
            this.label21.Text = "Seleccione el tipo de comprobante";
            // 
            // label20
            // 
            this.label20.Image = global::ferreteriaApp.Properties.Resources.icons8_edit_property_32px;
            this.label20.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label20.Location = new System.Drawing.Point(6, 3);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(34, 31);
            this.label20.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(16, 91);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(131, 16);
            this.label11.TabIndex = 5;
            this.label11.Text = "Fecha de emisión";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.MediumBlue;
            this.label10.Location = new System.Drawing.Point(46, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(157, 16);
            this.label10.TabIndex = 0;
            this.label10.Text = "Tipo de comprobante";
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(252)))), ((int)(((byte)(250)))));
            this.panel5.Controls.Add(this.label39);
            this.panel5.Controls.Add(this.chkDescuento);
            this.panel5.Controls.Add(this.cmbUnidad);
            this.panel5.Controls.Add(this.lbl_modificar);
            this.panel5.Controls.Add(this.chk_editar_precio);
            this.panel5.Controls.Add(this.btn_link_products);
            this.panel5.Controls.Add(this.txt_id_row);
            this.panel5.Controls.Add(this.lbl_mensaje);
            this.panel5.Controls.Add(this.label26);
            this.panel5.Controls.Add(this.label25);
            this.panel5.Controls.Add(this.label24);
            this.panel5.Controls.Add(this.lbl_descripcion_obligatorios);
            this.panel5.Controls.Add(this.label17);
            this.panel5.Controls.Add(this.txt_cantidad);
            this.panel5.Controls.Add(this.btn_agragr_producto);
            this.panel5.Controls.Add(this.label18);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.txt_producto);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.txt_codigo);
            this.panel5.Controls.Add(this.label9);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.txt_descuento);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.txt_precio);
            this.panel5.Location = new System.Drawing.Point(12, 219);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(967, 159);
            this.panel5.TabIndex = 37;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(741, 98);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(44, 13);
            this.label39.TabIndex = 33;
            this.label39.Text = "Agregar";
            this.label39.Visible = false;
            // 
            // chkDescuento
            // 
            this.chkDescuento.AutoSize = true;
            this.chkDescuento.Location = new System.Drawing.Point(791, 99);
            this.chkDescuento.Name = "chkDescuento";
            this.chkDescuento.Size = new System.Drawing.Size(15, 14);
            this.chkDescuento.TabIndex = 32;
            this.chkDescuento.UseVisualStyleBackColor = true;
            this.chkDescuento.Visible = false;
            this.chkDescuento.CheckedChanged += new System.EventHandler(this.chkDescuento_CheckedChanged);
            // 
            // cmbUnidad
            // 
            this.cmbUnidad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUnidad.FormattingEnabled = true;
            this.cmbUnidad.Location = new System.Drawing.Point(603, 76);
            this.cmbUnidad.Name = "cmbUnidad";
            this.cmbUnidad.Size = new System.Drawing.Size(118, 21);
            this.cmbUnidad.TabIndex = 11;
            // 
            // lbl_modificar
            // 
            this.lbl_modificar.AutoSize = true;
            this.lbl_modificar.Location = new System.Drawing.Point(423, 100);
            this.lbl_modificar.Name = "lbl_modificar";
            this.lbl_modificar.Size = new System.Drawing.Size(82, 13);
            this.lbl_modificar.TabIndex = 31;
            this.lbl_modificar.Text = "Modificar precio";
            // 
            // chk_editar_precio
            // 
            this.chk_editar_precio.AutoSize = true;
            this.chk_editar_precio.Location = new System.Drawing.Point(506, 99);
            this.chk_editar_precio.Name = "chk_editar_precio";
            this.chk_editar_precio.Size = new System.Drawing.Size(15, 14);
            this.chk_editar_precio.TabIndex = 30;
            this.chk_editar_precio.UseVisualStyleBackColor = true;
            this.chk_editar_precio.CheckedChanged += new System.EventHandler(this.chk_editar_precio_CheckedChanged);
            // 
            // btn_link_products
            // 
            this.btn_link_products.AutoSize = true;
            this.btn_link_products.Location = new System.Drawing.Point(172, 60);
            this.btn_link_products.Name = "btn_link_products";
            this.btn_link_products.Size = new System.Drawing.Size(105, 13);
            this.btn_link_products.TabIndex = 29;
            this.btn_link_products.TabStop = true;
            this.btn_link_products.Text = "Busca personalizada";
            this.btn_link_products.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btn_link_products_LinkClicked);
            // 
            // txt_id_row
            // 
            this.txt_id_row.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_id_row.ForeColor = System.Drawing.Color.White;
            this.txt_id_row.Location = new System.Drawing.Point(320, 18);
            this.txt_id_row.Name = "txt_id_row";
            this.txt_id_row.Size = new System.Drawing.Size(18, 13);
            this.txt_id_row.TabIndex = 27;
            this.txt_id_row.Visible = false;
            // 
            // lbl_mensaje
            // 
            this.lbl_mensaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_mensaje.ForeColor = System.Drawing.Color.Red;
            this.lbl_mensaje.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_mensaje.Location = new System.Drawing.Point(14, 124);
            this.lbl_mensaje.Name = "lbl_mensaje";
            this.lbl_mensaje.Size = new System.Drawing.Size(784, 22);
            this.lbl_mensaje.TabIndex = 26;
            this.lbl_mensaje.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Location = new System.Drawing.Point(579, 60);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(18, 13);
            this.label26.TabIndex = 25;
            this.label26.Text = "*";
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(503, 60);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(18, 13);
            this.label25.TabIndex = 24;
            this.label25.Text = "*";
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Location = new System.Drawing.Point(71, 60);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(18, 13);
            this.label24.TabIndex = 23;
            this.label24.Text = "*";
            // 
            // lbl_descripcion_obligatorios
            // 
            this.lbl_descripcion_obligatorios.AutoSize = true;
            this.lbl_descripcion_obligatorios.Location = new System.Drawing.Point(181, 17);
            this.lbl_descripcion_obligatorios.Name = "lbl_descripcion_obligatorios";
            this.lbl_descripcion_obligatorios.Size = new System.Drawing.Size(119, 13);
            this.lbl_descripcion_obligatorios.TabIndex = 20;
            this.lbl_descripcion_obligatorios.Text = "| Campos obligatorios (*)";
            this.lbl_descripcion_obligatorios.Click += new System.EventHandler(this.lbl_descripcion_obligatorios_Click);
            // 
            // label17
            // 
            this.label17.Image = global::ferreteriaApp.Properties.Resources.icons8_clear_shopping_cart_32px;
            this.label17.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label17.Location = new System.Drawing.Point(5, 8);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(37, 31);
            this.label17.TabIndex = 18;
            // 
            // btn_agragr_producto
            // 
            this.btn_agragr_producto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_agragr_producto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_agragr_producto.Image = global::ferreteriaApp.Properties.Resources.icons8_plus_32px;
            this.btn_agragr_producto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_agragr_producto.Location = new System.Drawing.Point(810, 60);
            this.btn_agragr_producto.Name = "btn_agragr_producto";
            this.btn_agragr_producto.Size = new System.Drawing.Size(145, 37);
            this.btn_agragr_producto.TabIndex = 13;
            this.btn_agragr_producto.Text = "Agregar producto";
            this.btn_agragr_producto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_agragr_producto.UseVisualStyleBackColor = true;
            this.btn_agragr_producto.Click += new System.EventHandler(this.btn_agragr_producto_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.MediumBlue;
            this.label18.Location = new System.Drawing.Point(46, 15);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(140, 16);
            this.label18.TabIndex = 0;
            this.label18.Text = "Datos del producto";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.MediumBlue;
            this.label13.Location = new System.Drawing.Point(7, 13);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(153, 16);
            this.label13.TabIndex = 19;
            this.label13.Text = "Detalle de productos";
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Controls.Add(this.cantidad_productos);
            this.panel6.Controls.Add(this.label13);
            this.panel6.Controls.Add(this.panel1);
            this.panel6.Location = new System.Drawing.Point(12, 385);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(814, 261);
            this.panel6.TabIndex = 38;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.detalleProductTable);
            this.panel7.Location = new System.Drawing.Point(9, 35);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(800, 177);
            this.panel7.TabIndex = 32;
            // 
            // cantidad_productos
            // 
            this.cantidad_productos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cantidad_productos.ForeColor = System.Drawing.Color.DarkGreen;
            this.cantidad_productos.Location = new System.Drawing.Point(157, 13);
            this.cantidad_productos.Name = "cantidad_productos";
            this.cantidad_productos.Size = new System.Drawing.Size(195, 16);
            this.cantidad_productos.TabIndex = 28;
            this.cantidad_productos.Text = "| Cantidad de productos: 0";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.MediumBlue;
            this.label31.Location = new System.Drawing.Point(135, 20);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(36, 13);
            this.label31.TabIndex = 41;
            this.label31.Text = "[F10]";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Crimson;
            this.label32.Location = new System.Drawing.Point(279, 20);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(36, 13);
            this.label32.TabIndex = 42;
            this.label32.Text = "[F11]";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(412, 20);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(36, 13);
            this.label33.TabIndex = 43;
            this.label33.Text = "[F12]";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(169, 20);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(105, 13);
            this.label34.TabIndex = 44;
            this.label34.Text = "Busca personalizada";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(316, 20);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(90, 13);
            this.label35.TabIndex = 45;
            this.label35.Text = "Agregar Producto";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(448, 20);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(76, 13);
            this.label36.TabIndex = 46;
            this.label36.Text = "Guardar Venta";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(40, 20);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(90, 13);
            this.label37.TabIndex = 48;
            this.label37.Text = "Agregar Producto";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.DarkGreen;
            this.label38.Location = new System.Drawing.Point(11, 20);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(29, 13);
            this.label38.TabIndex = 47;
            this.label38.Text = "[F9]";
            // 
            // btn_eliminar
            // 
            this.btn_eliminar.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.btn_eliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_eliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_eliminar.Image = global::ferreteriaApp.Properties.Resources.icons8_remove_tag_32px;
            this.btn_eliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_eliminar.Location = new System.Drawing.Point(832, 515);
            this.btn_eliminar.Name = "btn_eliminar";
            this.btn_eliminar.Size = new System.Drawing.Size(147, 39);
            this.btn_eliminar.TabIndex = 40;
            this.btn_eliminar.Text = "Eliminar producto";
            this.btn_eliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_eliminar.UseVisualStyleBackColor = true;
            this.btn_eliminar.Click += new System.EventHandler(this.btn_eliminar_Click);
            // 
            // button2
            // 
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = global::ferreteriaApp.Properties.Resources.icons8_save_as_32px_1;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(832, 607);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(147, 39);
            this.button2.TabIndex = 29;
            this.button2.Text = "Guardar venta";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = global::ferreteriaApp.Properties.Resources.icons8_delete_bin_32px_11;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(832, 561);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(147, 39);
            this.button3.TabIndex = 30;
            this.button3.Text = "Cancelar venta";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1013, 682);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.btn_eliminar);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.txt_codigo_comprobante);
            this.Controls.Add(this.txt_mensaje);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "dashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sistema de Ferretaria";
            this.Load += new System.EventHandler(this.dashboard_Load);
            this.Shown += new System.EventHandler(this.dashboard_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dashboard_KeyUp);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.detalleProductTable)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox txtDireccionCliente;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_documento;
        private System.Windows.Forms.TextBox txtCliente;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_cantidad;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btn_agragr_producto;
        private System.Windows.Forms.TextBox txt_descuento;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label txt_titulo_comprobante;
        private System.Windows.Forms.DateTimePicker txt_fecha;
        private System.Windows.Forms.Label txt_codigoBoleta;
        private System.Windows.Forms.DataGridView detalleProductTable;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txt_total;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txt_igv;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txt_subtotal;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label txt_mensaje;
        private System.Windows.Forms.Label txt_codigo_comprobante;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btn_eliminar;
        private System.Windows.Forms.Label lbl_descripcion_obligatorios;
        private System.Windows.Forms.Label mensaje_documento;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label txt_id_row;
        private System.Windows.Forms.Label cantidad_productos;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.LinkLabel btn_link_products;
        private System.Windows.Forms.Label lbl_modificar;
        private System.Windows.Forms.CheckBox chk_editar_precio;
        private System.Windows.Forms.TextBox txt_codigo;
        private System.Windows.Forms.TextBox txt_precio;
        private System.Windows.Forms.TextBox txt_producto;
        private System.Windows.Forms.Label lbl_mensaje;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox cmbUnidad;
        private System.Windows.Forms.Label lblCheckSunatReniec;
        private System.Windows.Forms.CheckBox chkSunatReniec;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.CheckBox chkDescuento;
        private System.Windows.Forms.CheckBox chkDescuentoGlobal;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txt_descuentoGlobal;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

