﻿using ferreteriaApp.CEN;
using ferreteriaApp.CLN;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using WebVentas.util;
using Excel = Microsoft.Office.Interop.Excel;
using ferreteriaApp.HELPERS;
using System.Text.RegularExpressions;

namespace ferreteriaApp
{
    public partial class productos : Form
    {

        List<CENListadoProducto> listaRN = new List<CENListadoProducto>();
        int pagina = 0;
        int maximo_paginas = 0;
        decimal items_por_paginas = 10;
        decimal total_datos = 0;
        decimal total_datos_excel = 0;

        int _id = 0, _actCodSunat;

        public productos()
        {
            InitializeComponent();
            this.llenarComboUnidad();
            this.llenarComboMoneda();
            this.llenarComboAfectacionIgv();
            this.listarProductos();

            this.hideColumns();
            this.getActCodigoSunat();
        
        }
      
        private void getActCodigoSunat()
        {
            CLNConcepto clnConcepto = new CLNConcepto();

            String param = clnConcepto.ObtenerValorParametro(18);

            _actCodSunat = Convert.ToInt32(param);

            if (_actCodSunat == 1) lblOblCodSunat.Visible=true;
            else lblOblCodSunat.Visible = false;
        }
        public void hideColumns()
        {
            this.detalleLista.Columns["ID"].Visible = false;
            this.detalleLista.Columns["CODIGO_SUNAT"].Visible = false;
            this.detalleLista.Columns["UNIDAD_ID"].Visible = false;
            this.detalleLista.Columns["MONEDA_ID"].Visible = false;
            this.detalleLista.Columns["AFECTACION_ID"].Visible = false;
        }

        public void hideColumnsToExcel()
        {
            this.listaToExcel.Columns["ID"].Visible = false;
            this.listaToExcel.Columns["CODIGO_SUNAT"].Visible = false;
            this.listaToExcel.Columns["UNIDAD_ID"].Visible = false;
            this.listaToExcel.Columns["MONEDA_ID"].Visible = false;
            this.listaToExcel.Columns["AFECTACION_ID"].Visible = false;
        }

        public void llenarComboUnidad()
        {
            funciones.llenarComboUnidad(this.cmbUnidad);
        }


        public void llenarComboMoneda()
        {
            List<CENDocumentTypes> lista;
     
            this.cmbMoneda.Items.Clear();
            try
            {
                CLNDocumentTypes cln = new CLNDocumentTypes();
                lista = cln.getTipoDocumentosString(13);
                this.cmbMoneda.DataSource = lista;
                this.cmbMoneda.DisplayMember = "description";
                this.cmbMoneda.ValueMember = "id";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CENDocumentTypes cen = new CENDocumentTypes();
                cen.id = "0";
                cen.description = "No se encontraron resultados";
                lista = null;
                this.cmbMoneda.DataSource = lista;
                this.cmbMoneda.DisplayMember = "description";
                this.cmbMoneda.ValueMember = "id";
            }
        }

        public void llenarComboAfectacionIgv()
        {
            List<CENDocumentTypes> lista;
            this.cmbTipoAfectacionVenta.Items.Clear();
            try
            {
                CLNDocumentTypes cln = new CLNDocumentTypes();
                lista = cln.getTipoDocumentosString(15);
                this.cmbTipoAfectacionVenta.DataSource = lista;
                this.cmbTipoAfectacionVenta.DisplayMember = "description";
                this.cmbTipoAfectacionVenta.ValueMember = "id";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CENDocumentTypes cen = new CENDocumentTypes();
                cen.id = "0";
                cen.description = "No se encontraron resultados";
                lista = null;
                this.cmbTipoAfectacionVenta.DataSource = lista;
                this.cmbTipoAfectacionVenta.DisplayMember = "description";
                this.cmbTipoAfectacionVenta.ValueMember = "id";
            }
        }

        private void txtPrecioVenta_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
       (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
        public void setearCampos()
        {
            this.txtDescripcion.Text = "";
            this.txt_codigo_interno.Text = "";
            this.txtCodigoSunat.Text = "";
            this.txtPrecioVenta.Text = "";
            this.cmbMoneda.SelectedIndex = -1;
            this.cmbMoneda.SelectedIndex = 0;
            this.cmbUnidad.SelectedIndex = -1;
            this.cmbUnidad.SelectedIndex = 0;
            this.cmbTipoAfectacionVenta.SelectedIndex = -1;
            this.cmbTipoAfectacionVenta.SelectedIndex = 0;
            this.chkIgv.Checked = true;
            _id = 0;
            this.detalleLista.Enabled = true;

            this.btnEditar.Enabled = true;
            this.btnEliminar.Enabled = true;
            this.txtCodigoSunat.Enabled = true;

        }
        private Boolean validarCampos()
        {
          
            if (this.txtDescripcion.Text == "")
            {
                aviso aviso_form = new aviso("warning", "Error", "Escriba una descripción", false);
                aviso_form.ShowDialog();
                this.txtDescripcion.Focus();
                return false;
            }
            if (txtDescripcion.Text.Trim().Length == 0)
            {
                
                aviso aviso_form = new aviso("warning", "Error", "Escriba una descripción", false);
               aviso_form.ShowDialog();
                this.txtDescripcion.Focus();
                txtDescripcion.Focus();
                return false;
            }
            if (this.txtPrecioVenta.Text == "")
            {
                aviso aviso_form = new aviso("warning", "Error", "campo precio venta no debe estar vacío", false);
               aviso_form.ShowDialog();
                this.txtPrecioVenta.Focus();
                return false;
            }
            if (Convert.ToDecimal(txtPrecioVenta.Text) <0)
            {
                aviso aviso_form = new aviso("warning", "Error", "precio venta debe ser mayor a cero", false);
               aviso_form.ShowDialog();
                this.txtPrecioVenta.Focus();
                return false;
            }

            if (_actCodSunat == 1 && this.txtCodigoSunat.Text.Trim().Length < 8)
            {
                
                    aviso aviso_form = new aviso("warning", "Error", "ingrese 8 dígitos en codigo de sunat", false);
                    aviso_form.ShowDialog();
                    this.txtCodigoSunat.Focus();
                    return false;
                
   
            }
            else
            {
                if (this.txtCodigoSunat.Text.Trim().Length > 0 && this.txtCodigoSunat.Text.Trim().Length < 8)
                {
                    aviso aviso_form = new aviso("warning", "Error", "ingrese 8 dígitos en codigo de sunat", false);
                    aviso_form.ShowDialog();
                    this.txtCodigoSunat.Focus();
                    return false;
                }

            }

            return true;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            DialogResult diagRes;

            var validacion= this.validarCampos();
            if (validacion)
            {
                String mensaje=(_id== 0)? "Va a guardar un producto \n" :" Va a editar "+ this.txtDescripcion.Text.ToUpper() + " \n";
         


                aviso aviso_form_error_hash2 = new aviso("warning", "ANTES DE SEGUIR", mensaje
                                            + " ¿Desea continuar?", true);
                diagRes = aviso_form_error_hash2.ShowDialog();
                if (diagRes == DialogResult.OK)
                {
                    CLNProductos clnP = new CLNProductos();
                    
                    String _descripcion,_unidad_medida_id,_moneda_id, _codigo_interno, _codigo_sunat, _has_igv;
                    int _afectacion_venta_id, _afectacion_compra_id;
                    decimal _precio_venta, _precio_compra;
                    DateTime _fecha_proceso, _fecha_transaccion;
                    
                    _descripcion = this.txtDescripcion.Text.Trim().ToUpper();
                    _unidad_medida_id = this.cmbUnidad.SelectedValue.ToString();
                    _moneda_id = this.cmbMoneda.SelectedValue.ToString();
                    _afectacion_venta_id = Convert.ToInt32(cmbTipoAfectacionVenta.SelectedValue.ToString());
                    _afectacion_compra_id = 0;
                    _precio_venta = Convert.ToDecimal(this.txtPrecioVenta.Text);
                    _precio_compra = 0;
                    _codigo_interno = this.txt_codigo_interno.Text.Trim();
                    _codigo_sunat = (this.txtCodigoSunat.Text.Trim()=="")?"-": this.txtCodigoSunat.Text.Trim();
                    _has_igv = (this.chkIgv.Checked) ? "1":"0";
                    _fecha_proceso = Convert.ToDateTime( DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                    _fecha_transaccion = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));


                    Boolean instP;
                    CenProducto prod = new CenProducto
                    {
                        descripcion=_descripcion,
                        unidad_medida_id= _unidad_medida_id,
                        moneda_id = _moneda_id,
                        afectacion_venta_id=_afectacion_venta_id,
                        afectacion_compra_id=_afectacion_compra_id,
                        precio_venta = Convert.ToDecimal(_precio_venta),
                        precio_compra=_precio_compra,
                        codigo_interno = _codigo_interno,
                        codigo_sunat=_codigo_sunat,
                        has_igv=_has_igv,
                        fecha_proceso = _fecha_proceso,
                        fecha_transaccion=_fecha_transaccion

                    };

                  

                    if (_id == 0)
                    {
                         instP = clnP.insertarProductos(prod);
                     
                    }
                    else
                    {
                         instP = clnP.updateProducto(prod, _id);
                    }
                     

                    if (instP)
                    {



                        aviso aviso_form = new aviso("success", "SUCCESS", "datos insertados correctamente", false);
                        aviso_form.ShowDialog();
                        this.setearCampos();
                        this.txt_buscar_descripcion.Text = "";
                        this.txt_codigo_interno.Text = "";
                        this.listarProductos();
                        this.panelPaginador.Visible = true;

                        this.txtDescripcion.Focus();

                    }
                    else
                    {
                        aviso aviso_form = new aviso("warning", "ERROR", "problemas al insertar datos", false);
                        aviso_form.ShowDialog();
                    
                    }
                }
          
            }
          
        }

        private void bntCancelar_Click(object sender, EventArgs e)
        {
            this.setearCampos();
            this.panelPaginador.Visible = true;
        }
        public void listarProductos()
        {
            CLNProductos cln2 = new CLNProductos();
            listaRN = cln2.buscarProducto(this.txt_buscar_descripcion.Text.Trim(),this.txt_buscar_codigo_interno.Text.Trim(), _id);
            this.cargar_datos(listaRN);
            total_datos = listaRN.Count;
        }
        public void cargar_datos(List<CENListadoProducto> lista)
        {
            total_datos = lista.Count;
            maximo_paginas = Convert.ToInt32(Math.Ceiling(total_datos / items_por_paginas));
            this.lbl_totalPaginas.Text = maximo_paginas.ToString();

            if (total_datos > 0)
            {
                this.lbl_pagina.Text = (pagina + 1).ToString();
            }

            detalleLista.DataSource = null;
            detalleLista.Columns.Clear();
            detalleLista.DataSource = lista.Skip((int)items_por_paginas * pagina).Take((int)items_por_paginas).ToList();
            this.hideColumns();
       
            this.habilitar_botones();


       
            if (total_datos == 0)
            {
                this.lblError.Text = "NO SE ENCONTRARON RESULTADOS";
            }
            else
            {
                this.lblError.Text = "";
            }
         


        }
        public void habilitar_botones()
        {


            if (pagina == 0)
            {
                this.btn_preview.Enabled = false;
            }
            else
            {
                this.btn_preview.Enabled = true;
            }

            if (pagina == (maximo_paginas - 1) || total_datos <= 0)
            {
                this.bnt_next.Enabled = false;
            }
            else
            {
                this.bnt_next.Enabled = true;
            }

        }

        private void bnt_next_Click(object sender, EventArgs e)
        {
            pagina += 1;

            this.cargar_datos(listaRN);
        }

        private void btn_preview_Click(object sender, EventArgs e)
        {
            pagina -= 1;

            this.cargar_datos(listaRN);
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            
            this.setearCampos();
            this.listarProductos();
            this.panelPaginador.Visible = true;
        }

        private void btnEditar_Click(object sender, EventArgs e )
        {

            if (detalleLista.CurrentCell != null)
            {
                int valor = detalleLista.CurrentCell.RowIndex;


                _id = (int)Convert.ToInt64(this.detalleLista.Rows[valor].Cells[0].Value);

                List<CENListadoProducto> listaProducto;
                CLNProductos cln2 = new CLNProductos();
                listaProducto= cln2.buscarProducto("", "", _id);

                this.txtDescripcion.Text = listaProducto[0].DESCRIPCION.ToString();
                this.txt_codigo_interno.Text = listaProducto[0].CODIGO_PRODUCTO.ToString();
                this.txtCodigoSunat.Text = (listaProducto[0].CODIGO_SUNAT.ToString()=="-")?"": listaProducto[0].CODIGO_SUNAT.ToString();

                this.txtPrecioVenta.Text = listaProducto[0].PRECIO.ToString();
                this.cmbMoneda.SelectedValue = listaProducto[0].MONEDA_ID.ToString();
                this.cmbUnidad.SelectedValue = listaProducto[0].UNIDAD_ID.ToString();
                this.cmbTipoAfectacionVenta.SelectedValue = listaProducto[0].AFECTACION_ID.ToString();
                this.chkIgv.Checked = (listaProducto[0].CON_IGV.ToString() == "SI")?true:false;

                this.panelPaginador.Visible = false;
                this.detalleLista.Enabled = false;

                this.btnEditar.Enabled = false;
                this.btnEliminar.Enabled = false;

            }
            else
            {
               
                aviso aviso_form = new aviso("warning", "Error", "Seleccione un producto de la lista", false);
                aviso_form.ShowDialog();
            }
           
          
        
         
        }



        private void button1_Click(object sender, EventArgs e)
        {
            this.setearCampos();
            _id = 0;
            this.panelPaginador.Visible = true;
        
        }

        private void detalleLista_EnabledChanged(object sender, EventArgs e)
        {
          
            if (!detalleLista.Enabled)
            {
                detalleLista.DefaultCellStyle.BackColor = SystemColors.Control;
                detalleLista.DefaultCellStyle.ForeColor = SystemColors.GrayText;
                detalleLista.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.Control;
                detalleLista.ColumnHeadersDefaultCellStyle.ForeColor = SystemColors.GrayText;
                detalleLista.CurrentCell = null;
                detalleLista.ReadOnly = true;
                detalleLista.EnableHeadersVisualStyles = false;
            }
            else
            {
                detalleLista.DefaultCellStyle.BackColor = SystemColors.Window;
                detalleLista.DefaultCellStyle.ForeColor = SystemColors.ControlText;
                detalleLista.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.Window;
                detalleLista.ColumnHeadersDefaultCellStyle.ForeColor = SystemColors.ControlText;
                detalleLista.ReadOnly = false;
                detalleLista.EnableHeadersVisualStyles = true;
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

            if (detalleLista.CurrentCell != null)
            {

                    DialogResult diagRes;
                    aviso aviso_form_error_hash2 = new aviso("warning", "VA A ELIMINAR UN PRODUCTO", "¿Desea continuar?", true);
                    diagRes = aviso_form_error_hash2.ShowDialog();

                if (diagRes == DialogResult.OK)
                {
                    int valor = detalleLista.CurrentCell.RowIndex;
              

                    _id = (int)Convert.ToInt64(this.detalleLista.Rows[valor].Cells[0].Value);
                    DateTime _fecha_transaccion = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));

                    CLNProductos cln2 = new CLNProductos();
                    Boolean eliminarP = cln2.eliminarProducto( _id, _fecha_transaccion);
                    if (eliminarP)
                    {
                        aviso aviso_form = new aviso("success", "Success", "Producto eliminar correctamente", false);
                        aviso_form.ShowDialog();
                        this.setearCampos();
                        this.listarProductos();
                        this.panelPaginador.Visible = true;
                        
                    }
                    else
                    {
                       
                        aviso aviso_form = new aviso("warning", "Error", "Ocurrio un error al eliminar el producto", false);
                        aviso_form.ShowDialog();
                        this.setearCampos();
                    
                    }

       

                }
                else
                {
                    this.setearCampos();
                    
                }

            }
            else
            {
               
                aviso aviso_form = new aviso("warning", "Error", "Seleccione un producto de la lista", false);
                aviso_form.ShowDialog();
            }
        }
        public Boolean copyAlltoClipboard()
        {
         
            CLNProductos clnte = new CLNProductos();
            List<CENListadoProducto> listTe = new List<CENListadoProducto>();
            
         
            listTe = clnte.buscarProducto("","",0);
            if (listTe.Count == 0)
            {
                
                aviso aviso_form = new aviso("warning", "Error", "No hay produtos para exportar", false);
                aviso_form.ShowDialog();
                return false;

            }
            else
            {
                listaToExcel.DataSource = null;
                listaToExcel.Columns.Clear();
                listaToExcel.DataSource = listTe;
                total_datos_excel = listTe.Count;
                this.hideColumnsToExcel();


                listaToExcel.SelectAll();

                DataObject dataObj = listaToExcel.GetClipboardContent();
                if (dataObj != null)
                    Clipboard.SetDataObject(dataObj);

                return true;
            }
          
        }
        public void toExcel()
        {
            string datosEmpresa = Helper.ObtenerValorParametro(CENConstantes.CONST_2);
            string Ruta = datosEmpresa.Split('|')[1];
            string razon = datosEmpresa.Split('|')[2];
            String SaveFilePath = Ruta + @"REPO\ReporteCataloProductos.xls";
            try
            {
                if (File.Exists(SaveFilePath))
                {

                    File.Delete(SaveFilePath);
                }
            }
            catch (Exception)
            {

                
                aviso aviso_form = new aviso("warning", "Error", "Cierre el excel ReporteCataloProductos.xls y vuelva a consultar.", false);
                aviso_form.ShowDialog();
                return;
            }

            Cursor.Current = Cursors.WaitCursor;

           Boolean data= copyAlltoClipboard();
            if (!data)
            {
                return;
            }
            Microsoft.Office.Interop.Excel.Application xlexcel;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            xlexcel = new Excel.Application();
            
            xlexcel.Visible = false;

            xlWorkBook = xlexcel.Workbooks.Add(misValue);
 
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            xlWorkSheet.Cells[1, 1] = "PRODUCTOS";
    
            xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[2, 1]].Merge();


            xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[2, 1]].Cells.VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

            xlWorkSheet.Cells[1, 2] = razon;
            Excel.Range mergeRazon = xlWorkSheet.Range[xlWorkSheet.Cells[1, 2], xlWorkSheet.Cells[1,7]];

            xlWorkSheet.Cells[2, 2] = "REPORTE CATALOGO DE PRODUCTOS";

            

            Excel.Range mergeTitulo = xlWorkSheet.Range[xlWorkSheet.Cells[2, 2], xlWorkSheet.Cells[2, 7]];

            mergeRazon.Merge(true);
            mergeRazon.Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            mergeRazon.Font.Bold = true;

            mergeTitulo.Merge(true);
            mergeTitulo.Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            mergeTitulo.Font.Bold = true;

            xlWorkSheet.Cells[1, 8] = DateTime.Now.ToString("dd/MM/yyyy");
            xlWorkSheet.Cells[2, 8] = DateTime.Now.ToString("HH:mm:ss");

            Excel.Range boldAll = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[2, 8]];
        
            boldAll.Font.Bold = true;
      

            //header text
            xlWorkSheet.Cells[3, 1] = "ITEM";
            xlWorkSheet.Cells[3, 2] = "CODIGO PRODUCTO";
            xlWorkSheet.Cells[3, 3] = "DESCRIPCION";
            xlWorkSheet.Cells[3, 4] = "UNIDAD";
            xlWorkSheet.Cells[3, 5] = "MONEDA";
            xlWorkSheet.Cells[3, 6] = "TIPO DE AFECTACION";
            xlWorkSheet.Cells[3, 7] = "PRECIO";
            xlWorkSheet.Cells[3, 8] = "CON IGV";

          


            var columnHeadingsRange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[3, 8]];
            columnHeadingsRange.Interior.Color = Color.Gray;
            columnHeadingsRange.Font.Color = Color.White;

            Excel.Range CR = (Excel.Range)xlWorkSheet.Cells[4, 1];
            CR.Select();
            CR.Columns.AutoFit();
            xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);

            decimal t_datos = 0;
            t_datos = (total_datos_excel == 0) ? 1 : total_datos_excel;
            var allCells = xlWorkSheet.Range[xlWorkSheet.Cells[3, 2], xlWorkSheet.Cells[8, 4 + total_datos_excel + 1]];
            allCells.Columns.AutoFit();

            Console.WriteLine(total_datos_excel.ToString());
            int indice = 0;
            for (int i = 4; i < 4 + total_datos_excel; i++)
            {
                indice = indice + 1;
                xlWorkSheet.Cells[i, 1] = indice.ToString();
            }
            Excel.Range CR2 = (Excel.Range)xlWorkSheet.Cells[1, 8];
         
            CR2.Columns.AutoFit();
            xlWorkBook.SaveAs(SaveFilePath, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);

        

            xlWorkBook.Close(true, misValue, misValue);
            xlexcel.Quit();

            System.Diagnostics.Process.Start(SaveFilePath);
            Cursor.Current = Cursors.Default;
        }

        private void txtCodigoSunat_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
         (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') >= -1))
            {
                e.Handled = true;
            }
        }

     
        private void btn_buscar_Click(object sender, EventArgs e)
        {
           
       
            DialogResult diagRes;
            CodSntProducto CodSntProducto = new CodSntProducto();
            diagRes = CodSntProducto.ShowDialog();

            if (diagRes == DialogResult.OK)
            {
                this.txtCodigoSunat.Text=CodSntProducto.CodigoP;
                this.txtCodigoSunat.Enabled = false;
            }
         }

        private void txt_buscar_descripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.ToString() == "+"
               || e.KeyChar.ToString() == "*"
               || e.KeyChar.ToString() == "("
               || e.KeyChar.ToString() == ")"
               || e.KeyChar.ToString() == "["
               || e.KeyChar.ToString() == "]"
               )
            {
                e.Handled = true;
                return;
            }
        }

        private void txtDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.ToString() == "|" 
                || e.KeyChar.ToString() == "'")
            {
                e.Handled = true;
                return;
            }

            //var regex = new Regex(@"|");
            //if (regex.IsMatch(e.KeyChar.ToString()))
            //{
            //    e.Handled = true;
            //}
        }

        private void txt_codigo_interno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.ToString() == "|"
              || e.KeyChar.ToString() == "'")
            {
                e.Handled = true;
                return;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.toExcel();
        }
    }
}

