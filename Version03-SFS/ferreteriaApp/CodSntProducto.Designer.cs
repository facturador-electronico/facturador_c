﻿
namespace ferreteriaApp
{
    partial class CodSntProducto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel7 = new System.Windows.Forms.Panel();
            this.listProductos = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblError = new System.Windows.Forms.Label();
            this.panel_clase = new System.Windows.Forms.Panel();
            this.cmbClase = new System.Windows.Forms.ComboBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel_familia = new System.Windows.Forms.Panel();
            this.cmbFamilia = new System.Windows.Forms.ComboBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel_seg = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbSegmento = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_mensaje = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_codigo = new System.Windows.Forms.TextBox();
            this.btn_buscar = new System.Windows.Forms.Button();
            this.btn_seleccionar = new System.Windows.Forms.Button();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listProductos)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel_clase.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel_familia.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel_seg.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.listProductos);
            this.panel7.Location = new System.Drawing.Point(2, 207);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1120, 267);
            this.panel7.TabIndex = 33;
            // 
            // listProductos
            // 
            this.listProductos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.listProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listProductos.Location = new System.Drawing.Point(0, 3);
            this.listProductos.Name = "listProductos";
            this.listProductos.ReadOnly = true;
            this.listProductos.Size = new System.Drawing.Size(1115, 254);
            this.listProductos.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblError);
            this.panel1.Controls.Add(this.panel_clase);
            this.panel1.Controls.Add(this.panel_familia);
            this.panel1.Controls.Add(this.panel_seg);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txt_mensaje);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txt_codigo);
            this.panel1.Controls.Add(this.btn_buscar);
            this.panel1.Location = new System.Drawing.Point(2, 23);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1120, 178);
            this.panel1.TabIndex = 34;
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblError.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(0, 165);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(34, 13);
            this.lblError.TabIndex = 73;
            this.lblError.Text = "Error";
            // 
            // panel_clase
            // 
            this.panel_clase.Controls.Add(this.cmbClase);
            this.panel_clase.Controls.Add(this.panel5);
            this.panel_clase.Location = new System.Drawing.Point(749, 39);
            this.panel_clase.Name = "panel_clase";
            this.panel_clase.Size = new System.Drawing.Size(366, 66);
            this.panel_clase.TabIndex = 72;
            // 
            // cmbClase
            // 
            this.cmbClase.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.cmbClase.DropDownHeight = 150;
            this.cmbClase.FormattingEnabled = true;
            this.cmbClase.IntegralHeight = false;
            this.cmbClase.Location = new System.Drawing.Point(0, 45);
            this.cmbClase.MinimumSize = new System.Drawing.Size(250, 0);
            this.cmbClase.Name = "cmbClase";
            this.cmbClase.Size = new System.Drawing.Size(366, 21);
            this.cmbClase.TabIndex = 3;
            this.cmbClase.SelectedIndexChanged += new System.EventHandler(this.cmbClase_SelectedIndexChanged);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Goldenrod;
            this.panel5.Controls.Add(this.label5);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(366, 30);
            this.panel5.TabIndex = 71;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(3, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 20);
            this.label5.TabIndex = 1;
            this.label5.Text = "Clase";
            // 
            // panel_familia
            // 
            this.panel_familia.Controls.Add(this.cmbFamilia);
            this.panel_familia.Controls.Add(this.panel4);
            this.panel_familia.Location = new System.Drawing.Point(387, 39);
            this.panel_familia.Name = "panel_familia";
            this.panel_familia.Size = new System.Drawing.Size(356, 66);
            this.panel_familia.TabIndex = 71;
            // 
            // cmbFamilia
            // 
            this.cmbFamilia.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.cmbFamilia.DropDownHeight = 150;
            this.cmbFamilia.FormattingEnabled = true;
            this.cmbFamilia.IntegralHeight = false;
            this.cmbFamilia.Location = new System.Drawing.Point(0, 45);
            this.cmbFamilia.MinimumSize = new System.Drawing.Size(250, 0);
            this.cmbFamilia.Name = "cmbFamilia";
            this.cmbFamilia.Size = new System.Drawing.Size(356, 21);
            this.cmbFamilia.TabIndex = 2;
            this.cmbFamilia.SelectedIndexChanged += new System.EventHandler(this.cmbFamilia_SelectedIndexChanged);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.panel4.Controls.Add(this.label2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(356, 30);
            this.panel4.TabIndex = 71;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Familia";
            // 
            // panel_seg
            // 
            this.panel_seg.Controls.Add(this.panel3);
            this.panel_seg.Controls.Add(this.cmbSegmento);
            this.panel_seg.Location = new System.Drawing.Point(10, 39);
            this.panel_seg.Name = "panel_seg";
            this.panel_seg.Size = new System.Drawing.Size(371, 66);
            this.panel_seg.TabIndex = 70;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.DodgerBlue;
            this.panel3.Controls.Add(this.label1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(371, 30);
            this.panel3.TabIndex = 71;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Segmento";
            // 
            // cmbSegmento
            // 
            this.cmbSegmento.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.cmbSegmento.FormattingEnabled = true;
            this.cmbSegmento.Location = new System.Drawing.Point(0, 45);
            this.cmbSegmento.Name = "cmbSegmento";
            this.cmbSegmento.Size = new System.Drawing.Size(371, 21);
            this.cmbSegmento.TabIndex = 1;
            this.cmbSegmento.SelectedIndexChanged += new System.EventHandler(this.cmbSegmento_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Firebrick;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(619, 140);
            this.label4.MaximumSize = new System.Drawing.Size(175, 20);
            this.label4.MinimumSize = new System.Drawing.Size(175, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(175, 20);
            this.label4.TabIndex = 68;
            this.label4.Text = "Descripción";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_mensaje
            // 
            this.txt_mensaje.AutoSize = true;
            this.txt_mensaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_mensaje.ForeColor = System.Drawing.Color.Red;
            this.txt_mensaje.Location = new System.Drawing.Point(23, 107);
            this.txt_mensaje.Name = "txt_mensaje";
            this.txt_mensaje.Size = new System.Drawing.Size(0, 16);
            this.txt_mensaje.TabIndex = 6;
            this.txt_mensaje.UseMnemonic = false;
            this.txt_mensaje.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(338, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(213, 24);
            this.label3.TabIndex = 5;
            this.label3.Text = "Datos de la búsqueda";
            // 
            // txt_codigo
            // 
            this.txt_codigo.Location = new System.Drawing.Point(800, 140);
            this.txt_codigo.Name = "txt_codigo";
            this.txt_codigo.Size = new System.Drawing.Size(260, 20);
            this.txt_codigo.TabIndex = 4;
            // 
            // btn_buscar
            // 
            this.btn_buscar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btn_buscar.FlatAppearance.BorderSize = 0;
            this.btn_buscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_buscar.Image = global::ferreteriaApp.Properties.Resources.search_32;
            this.btn_buscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_buscar.Location = new System.Drawing.Point(1065, 125);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(45, 43);
            this.btn_buscar.TabIndex = 5;
            this.btn_buscar.UseVisualStyleBackColor = true;
            this.btn_buscar.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_seleccionar
            // 
            this.btn_seleccionar.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btn_seleccionar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btn_seleccionar.FlatAppearance.BorderColor = System.Drawing.Color.Navy;
            this.btn_seleccionar.FlatAppearance.BorderSize = 0;
            this.btn_seleccionar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_seleccionar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_seleccionar.ForeColor = System.Drawing.Color.White;
            this.btn_seleccionar.Location = new System.Drawing.Point(997, 480);
            this.btn_seleccionar.Name = "btn_seleccionar";
            this.btn_seleccionar.Size = new System.Drawing.Size(123, 36);
            this.btn_seleccionar.TabIndex = 7;
            this.btn_seleccionar.Text = "SELECCIONAR";
            this.btn_seleccionar.UseVisualStyleBackColor = false;
            this.btn_seleccionar.Click += new System.EventHandler(this.btn_seleccionar_Click);
            // 
            // CodSntProducto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1134, 534);
            this.Controls.Add(this.btn_seleccionar);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel7);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "CodSntProducto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Listado Códigos Sunat Productos";
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listProductos)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel_clase.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel_familia.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel_seg.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_buscar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_codigo;
        private System.Windows.Forms.Label txt_mensaje;
        private System.Windows.Forms.Button btn_seleccionar;
        private System.Windows.Forms.Panel panel_clase;
        private System.Windows.Forms.ComboBox cmbClase;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel_familia;
        private System.Windows.Forms.ComboBox cmbFamilia;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel_seg;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox cmbSegmento;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView listProductos;
        private System.Windows.Forms.Label lblError;
    }
}