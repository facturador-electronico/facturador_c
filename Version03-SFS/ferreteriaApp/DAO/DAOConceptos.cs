﻿using ferreteriaApp.CEN;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace ferreteriaApp.DAO
{
    public class DAOConceptos
    {
        public List<CENTipoDocumento> documentos(int flag)
        {
            List<CENTipoDocumento> lista = new List<CENTipoDocumento>();
            CENTipoDocumento cen;
            try
            {
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_listar_conceptos", false);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@flag", flag);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    cen = new CENTipoDocumento();
                    cen.codigoDocumento = dr.GetInt16(0);
                    cen.nombreDocumento = dr.GetString(1).ToUpper();
                    lista.Add(cen);
                }

                dr.Close();
                con.cerrarSession();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }

            return lista;
        }


        public string ObtenerValorParametro(int flag)
        {
            
            try
            {
                String response = "";
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_listar_conceptos", false);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@flag", flag);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    response = dr.GetString(0);
                }
                dr.Close();
                con.cerrarSession();
                return response;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }
      
        }


        public void actualizarCheckSunatReniec(int ckeckSunatReniec)
        {

            try
            {
                Conexion con = new Conexion();

                string query = "UPDATE parametro SET par_tipo=@par_tipo WHERE id=45";
                MySqlCommand cmd = con.prepararConsulta(query);
                cmd.CommandText = query;
                cmd.Parameters.AddWithValue("@par_tipo", ckeckSunatReniec);
                cmd.ExecuteNonQuery();
                con.cerrarSession();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }

        }


        public CENParametro buscarParametro(int _codigo)
        {
            List<CENParametro> lista = new List<CENParametro>();
            CENParametro cen = new CENParametro(); ;
            DateTime date = DateTime.MinValue;
            try
            {
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_buscar_parametro", false);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@_codigo", _codigo);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                   
                    cen.parametro = (dr.IsDBNull(0)) ? "" : dr.GetString(0);
                    cen.parametro2 = (dr.IsDBNull(1))?"":dr.GetString(1);
                    cen.par_int1 = (dr.IsDBNull(2)) ? 0 : dr.GetInt32(2);
                    cen.par_int2 = (dr.IsDBNull(3)) ? 0 : dr.GetInt32(3);
                    cen.par_float1 = (dr.IsDBNull(4)) ? 0 : dr.GetDecimal(4);
                    cen.par_float2 = (dr.IsDBNull(5)) ? 0 : dr.GetDecimal(5);
                    cen.par_date1 = (dr.IsDBNull(6)) ? date : dr.GetDateTime(6);
                    cen.par_date2 = (dr.IsDBNull(7)) ? date : dr.GetDateTime(7);
                    cen.par_tipo = (dr.IsDBNull(8)) ? 0 : dr.GetInt32(8);
                   
                }

                dr.Close();
                con.cerrarSession();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }

            return cen;
        }
    }
}
