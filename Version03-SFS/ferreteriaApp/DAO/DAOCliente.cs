﻿using ferreteriaApp.CEN;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SQLite;


namespace ferreteriaApp.DAO
{
    class DAOCliente
    {
        readonly Conexion con;

        private const string SELECT = @"SELECT * FROM DOCUMENTO";
      

     

        public bool existeCliente(string num_documento)
        {

            try
            {
                CENDataCliente datacliente= new CENDataCliente();
                int conteo = 0;
                Conexion con = new Conexion();

                string query = "select count(id) from cliente where numero_documento=@num_documento and marca_baja=0";
                MySqlCommand cmd = con.prepararConsulta(query);
                cmd.CommandText = query;
                cmd.Parameters.AddWithValue("@num_documento", num_documento);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    conteo = dr.GetInt32(0);
                }
                
                dr.Close();
                con.cerrarSession();
                if (conteo == 1) return true; else return false;
                //datacliente;


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public CENDataCliente getCliente(string num_documento )
        {

            try
            {
                CENDataCliente datacliente = new CENDataCliente();
                int conteo = 0;
                Conexion con = new Conexion();

                string query = "select numero_documento, nombres_razonsocial,direccion from cliente where numero_documento=@num_documento and marca_baja=0";
                MySqlCommand cmd = con.prepararConsulta(query);
                cmd.CommandText = query;
                cmd.Parameters.AddWithValue("@num_documento", num_documento);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    datacliente.num_doc = dr.GetString(0);
                    datacliente.nombresCompletos = dr.GetString(1);
                    datacliente.direccion = dr.GetString(2);
                }

                dr.Close();
                con.cerrarSession();

                return datacliente;


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public void insertClients(CENDataCliente cliente)
        {



            try
            {
              
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_insert_cliente", false);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
  
                cmd.Parameters.AddWithValue("@_tipo_documento", cliente.tipo_doc.Trim());
                cmd.Parameters.AddWithValue("@_numero_documento", cliente.num_doc.Trim());
                cmd.Parameters.AddWithValue("@_nombres_razonsocial", cliente.nombresCompletos.Trim());
                cmd.Parameters.AddWithValue("@_direccion", cliente.direccion.Trim());
              

                MySqlDataReader dr = cmd.ExecuteReader();

                dr.Close();
                con.cerrarSession();



            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool updateClients(CENDataCliente cliente)
        {



            try
            {

                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_update_cliente", false);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@_tipo_documento", cliente.tipo_doc.Trim());
                cmd.Parameters.AddWithValue("@_numero_documento", cliente.num_doc.Trim());
                cmd.Parameters.AddWithValue("@_nombres_razonsocial", cliente.nombresCompletos.Trim());
                cmd.Parameters.AddWithValue("@_direccion", cliente.direccion.Trim());


                MySqlDataReader dr = cmd.ExecuteReader();

                dr.Close();
                con.cerrarSession();
                return true;


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public List<CENDataCliente> buscarCliente(String _descripcion, String _codigo_interno)
        {
            List<CENDataCliente> listaSC = new List<CENDataCliente>();
            CENDataCliente censc;
            try
            {
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_buscar_cliente", false);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@_descripcion", _descripcion);
                cmd.Parameters.AddWithValue("@_codigo_interno", _codigo_interno);
                

                MySqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    censc = new CENDataCliente();
                    censc.id = dr.GetInt32(0);
                    censc.num_doc = dr.GetString(1);
                    censc.nombresCompletos = dr.GetString(2);
                    censc.direccion = dr.GetString(3);
                    listaSC.Add(censc);

                }
                dr.Close();
                con.cerrarSession();
                return listaSC;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }




    }
}

