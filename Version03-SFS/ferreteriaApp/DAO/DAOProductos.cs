﻿using ferreteriaApp.CEN;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace ferreteriaApp.DAO
{
    public class DAOProductos
    {

        public Boolean insertarProductos(CenProducto producto)
        {


            try
            {
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_insertar_productos", false);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@_descripcion", producto.descripcion);
                cmd.Parameters.AddWithValue("@_unidad_medida_id", producto.unidad_medida_id);
                cmd.Parameters.AddWithValue("@_moneda_id", producto.moneda_id);
                cmd.Parameters.AddWithValue("@_afectacion_venta_id", producto.afectacion_venta_id);
                cmd.Parameters.AddWithValue("@_afectacion_compra_id", producto.afectacion_compra_id);
                cmd.Parameters.AddWithValue("@_precio_venta", producto.precio_venta);
                cmd.Parameters.AddWithValue("@_precio_compra", producto.precio_compra);
                cmd.Parameters.AddWithValue("@_codigo_interno", producto.codigo_interno);
                cmd.Parameters.AddWithValue("@_codigo_sunat", producto.codigo_sunat);
                cmd.Parameters.AddWithValue("@_has_igv", producto.has_igv);
                cmd.Parameters.AddWithValue("@_fecha_proceso", producto.fecha_proceso);
                cmd.Parameters.AddWithValue("@_fecha_transaccion", producto.fecha_transaccion);

                MySqlDataReader dr = cmd.ExecuteReader();
                dr.Close();
                con.cerrarSession();
                return true;
             }
            catch (Exception ex)
            {
                throw ex;
                return false;
              
            }

        }

        public Boolean updateProducto(CenProducto producto, int _id)
        {


            try
            {
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_update_producto", false);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@_descripcion", producto.descripcion);
                cmd.Parameters.AddWithValue("@_unidad_medida_id", producto.unidad_medida_id);
                cmd.Parameters.AddWithValue("@_moneda_id", producto.moneda_id);
                cmd.Parameters.AddWithValue("@_afectacion_venta_id", producto.afectacion_venta_id);
                cmd.Parameters.AddWithValue("@_afectacion_compra_id", producto.afectacion_compra_id);
                cmd.Parameters.AddWithValue("@_precio_venta", producto.precio_venta);
                cmd.Parameters.AddWithValue("@_precio_compra", producto.precio_compra);
                cmd.Parameters.AddWithValue("@_codigo_interno", producto.codigo_interno);
                cmd.Parameters.AddWithValue("@_codigo_sunat", producto.codigo_sunat);
                cmd.Parameters.AddWithValue("@_has_igv", producto.has_igv);
                cmd.Parameters.AddWithValue("@_id", _id);
                cmd.Parameters.AddWithValue("@_fecha_transaccion", producto.fecha_transaccion);


                MySqlDataReader dr = cmd.ExecuteReader();

                dr.Close();
                con.cerrarSession();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
                Console.WriteLine(ex);
                return false;
              
            }

        }

        public Boolean eliminarProducto(int _id, DateTime _fecha_transaccion)
        {


            try
            {
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_eliminar_producto", false);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@_id", _id);
                cmd.Parameters.AddWithValue("@_fecha_transaccion", _fecha_transaccion);

                MySqlDataReader dr = cmd.ExecuteReader();
                dr.Close();
                con.cerrarSession();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
                return false;
               
            }

        }
        public List<CENListadoProducto> buscarProducto( String _descripcion, String _codigo_interno, int _id)
        {
            List<CENListadoProducto> listaSC = new List<CENListadoProducto>();
            CENListadoProducto censc;
            try
            {
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_buscar_producto", false);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@_descripcion", _descripcion);
                cmd.Parameters.AddWithValue("@_codigo_interno", _codigo_interno);
                cmd.Parameters.AddWithValue("@_id", _id);

                MySqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    censc = new CENListadoProducto();
                    censc.CODIGO_PRODUCTO = dr.GetString(0);
                    censc.DESCRIPCION = dr.GetString(1);
                    censc.UNIDAD_MEDIDA = dr.GetString(2);
                    censc.MONEDA = dr.GetString(3);
                    censc.AFECTACION_IGV = dr.GetString(4);
                    censc.PRECIO = dr.GetDecimal(5);
                    censc.CON_IGV = dr.GetString(6);
                    censc.ID = (int)dr.GetInt64(7);
                    censc.CODIGO_SUNAT = dr.GetString(8);
                    censc.UNIDAD_ID = dr.GetString(9);
                    censc.MONEDA_ID = dr.GetString(10);
                    censc.AFECTACION_ID = dr.GetString(11);
                    listaSC.Add(censc);

                }
                dr.Close();
                con.cerrarSession();
                return listaSC;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        //buscaqueda de codigo sunat productos

        public List<CENListadoCodSntProducto> buscarCodSntPdt(int _flag, int _cod_buscar, int _cod_clase, String _descripcion)
        {
            List<CENListadoCodSntProducto> listaSC = new List<CENListadoCodSntProducto>();
            CENListadoCodSntProducto censc;
            try
            {
                Conexion con = new Conexion();
                MySqlCommand cmd = con.prepararConsulta("pa_buscar_codigo_sunat_producto", false);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@flag", _flag);
                cmd.Parameters.AddWithValue("@_cod_buscar", _cod_buscar);
                cmd.Parameters.AddWithValue("@_cod_clase", _cod_clase);
                cmd.Parameters.AddWithValue("@_descripcion", _descripcion);

       
                MySqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    censc = new CENListadoCodSntProducto();
                    censc.CODIGO =(int)dr.GetInt64(0);
                    censc.DESCRIPCION = dr.GetString(1);
                    listaSC.Add(censc);

                }
                dr.Close();
                con.cerrarSession();
                return listaSC;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}
