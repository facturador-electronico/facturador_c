﻿using ferreteriaApp.CEN;
using ferreteriaApp.CLN;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace ferreteriaApp
{
    public partial class principal : Form
    {

        List<CENListadoComprobantes> listaRN = new List<CENListadoComprobantes>();
        bool _showMensajePendientes = false;
        int _frecuenciaHora = 0;


        public principal()
        {
            InitializeComponent();

            this.WindowState = FormWindowState.Normal;

            this.buscarParametrosDefault();

            this.timer1.Interval = 1000 * 3600 * _frecuenciaHora;
            this.timer1.Start();
           

        }

    

        private void callForm(object objeto)
        {
            this.panelForm.Controls.Clear();
            Form fh = objeto as Form;
            fh.TopLevel = false;
            this.panelForm.Controls.Add(fh);
            panelForm.Tag = fh;
            fh.Dock = DockStyle.Fill;
            fh.Show();
        }

        public void buscarPendientes()
        {
           
            CenSearchComprobantes search = new CenSearchComprobantes
            {
                fecha_inicio = "",
                fecha_fin = "",
                dni_ruc = "",
                name_cliente = "",
                tipo_comprobante = "0",
                series_number = "",
                codigo_venta = 0,
                _id_estado = "01"
            };

            try
            {
                
                CLNDocumentTypes cln2 = new CLNDocumentTypes();
                listaRN = cln2.search_listado_comprobantes(search);

                int countReg = 0;

                foreach (var item in listaRN)
                {
                    if (item.ESTADO == "REGISTRADO") countReg++;
                }

                if (countReg > 0)
                {

                     aviso aviso_form = new aviso("warning", countReg + " COMPROBANTE(S) PENDIENTE(S) DE ENVIO", "Hay documento(s) pendiente(s) de envío a SUNAT. \n Ir a Listado de Comprobantes y sincronizar estados", false);
                    aviso_form.ShowDialog();


                }
                

            }
            catch (Exception ex)
            {
                aviso aviso_form = new aviso("warning", "ERROR EN LA SOLICITUD", ex.Message, false);
                aviso_form.ShowDialog();
                return;

            }


        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                callForm(new dashboard());
            }
            catch (Exception ex)
            {
                aviso aviso_form = new aviso("warning", "ERROR", ex.Message, false);
                aviso_form.ShowDialog();
                return;
               
            }
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                callForm(new searchSaleNote());
            }
            catch (Exception ex)
            {
                aviso aviso_form = new aviso("warning", "ERROR", ex.Message, false);
                aviso_form.ShowDialog();
                return;

            }
           
        }

        private void button3_Click(object sender, EventArgs e)
        {

            try
            {
                callForm(new listadoComprobantes());
            }
            catch (Exception ex)
            {
                aviso aviso_form = new aviso("warning", "ERROR", ex.Message, false);
                aviso_form.ShowDialog();
                return;

            }
           
        }

      

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                callForm(new productos());
            }
            catch (Exception ex)
            {
                aviso aviso_form = new aviso("warning", "ERROR", ex.Message, false);
                aviso_form.ShowDialog();
                return;

            }
           
        }

        private void btn_RepContador_Click(object sender, EventArgs e)
        {
            try
            {
                callForm(new reporteContador());
            }
            catch (Exception ex)
            {
                aviso aviso_form = new aviso("warning", "ERROR", ex.Message, false);
                aviso_form.ShowDialog();
                return;

            }
        }

        private void principal_Shown(object sender, EventArgs e)
        {
            if(_showMensajePendientes)  this.buscarPendientes();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (_showMensajePendientes) this.buscarPendientes();
        }

        private void buscarParametrosDefault()
        {
            try
            {
                CENParametro listaParFlagShow = new CENParametro();
                CENParametro listaParHora = new CENParametro();
                CLNConcepto clncconcepto = new CLNConcepto();

                listaParFlagShow = clncconcepto.buscarParametro(1003);
                listaParHora = clncconcepto.buscarParametro(1004);

                _showMensajePendientes = (listaParFlagShow.par_tipo == 1) ? true : false;
                _frecuenciaHora = listaParHora.par_int1;
            }
            catch (Exception ex)
            {

                aviso aviso_form = new aviso("warning", "ERROR", ex.Message, false);
                aviso_form.ShowDialog();
                return;
            }
          

        }
    }
}
