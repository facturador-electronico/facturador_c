﻿
namespace ferreteriaApp
{
    partial class notes
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(notes));
            this.panel2 = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_numero_nota = new System.Windows.Forms.Label();
            this.cmb_type_note = new System.Windows.Forms.ComboBox();
            this.txt_fecha = new System.Windows.Forms.DateTimePicker();
            this.lbl_serie_note = new System.Windows.Forms.Label();
            this.cmb_reason_notes = new System.Windows.Forms.ComboBox();
            this.txt_descripcion = new System.Windows.Forms.TextBox();
            this.detalleProductTable = new System.Windows.Forms.DataGridView();
            this.colCant = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colVi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colICB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAccion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colValorItem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codigoSunat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label40 = new System.Windows.Forms.Label();
            this.txt_descuentoGlobal = new System.Windows.Forms.TextBox();
            this.txt_total = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txt_igv = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txt_subtotal = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblTipoMonedaPass = new System.Windows.Forms.Label();
            this.txtVendedorVenta = new System.Windows.Forms.TextBox();
            this.txtClienteVenta = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNumeroVenta = new System.Windows.Forms.TextBox();
            this.txtSerieVenta = new System.Windows.Forms.TextBox();
            this.txtTipoCambioVenta = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCodigoVenta = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtImporteNota = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.button3 = new System.Windows.Forms.Button();
            this.btn_Generar = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.detalleProductTable)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label28);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.lbl_numero_nota);
            this.panel2.Controls.Add(this.cmb_type_note);
            this.panel2.Controls.Add(this.txt_fecha);
            this.panel2.Controls.Add(this.lbl_serie_note);
            this.panel2.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.panel2.Location = new System.Drawing.Point(672, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(234, 104);
            this.panel2.TabIndex = 23;
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Red;
            this.label28.Location = new System.Drawing.Point(211, 17);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(18, 13);
            this.label28.TabIndex = 44;
            this.label28.Text = "*";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(76, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 20);
            this.label1.TabIndex = 26;
            this.label1.Text = "-";
            // 
            // lbl_numero_nota
            // 
            this.lbl_numero_nota.AutoSize = true;
            this.lbl_numero_nota.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_numero_nota.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbl_numero_nota.Location = new System.Drawing.Point(100, 38);
            this.lbl_numero_nota.Name = "lbl_numero_nota";
            this.lbl_numero_nota.Size = new System.Drawing.Size(56, 20);
            this.lbl_numero_nota.TabIndex = 25;
            this.lbl_numero_nota.Text = "-xxxxx";
            // 
            // cmb_type_note
            // 
            this.cmb_type_note.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_type_note.Enabled = false;
            this.cmb_type_note.FormattingEnabled = true;
            this.cmb_type_note.Items.AddRange(new object[] {
            "BOLETA",
            "FACTURA"});
            this.cmb_type_note.Location = new System.Drawing.Point(20, 14);
            this.cmb_type_note.Name = "cmb_type_note";
            this.cmb_type_note.Size = new System.Drawing.Size(187, 21);
            this.cmb_type_note.TabIndex = 24;
            this.cmb_type_note.SelectedIndexChanged += new System.EventHandler(this.cmb_type_note_SelectedIndexChanged);
            // 
            // txt_fecha
            // 
            this.txt_fecha.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.txt_fecha.Location = new System.Drawing.Point(17, 68);
            this.txt_fecha.Name = "txt_fecha";
            this.txt_fecha.Size = new System.Drawing.Size(190, 20);
            this.txt_fecha.TabIndex = 4;
            this.txt_fecha.ValueChanged += new System.EventHandler(this.txt_fecha_ValueChanged);
            // 
            // lbl_serie_note
            // 
            this.lbl_serie_note.AutoSize = true;
            this.lbl_serie_note.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_serie_note.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbl_serie_note.Location = new System.Drawing.Point(24, 38);
            this.lbl_serie_note.Name = "lbl_serie_note";
            this.lbl_serie_note.Size = new System.Drawing.Size(46, 20);
            this.lbl_serie_note.TabIndex = 6;
            this.lbl_serie_note.Text = "FC01";
            // 
            // cmb_reason_notes
            // 
            this.cmb_reason_notes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_reason_notes.Enabled = false;
            this.cmb_reason_notes.FormattingEnabled = true;
            this.cmb_reason_notes.Items.AddRange(new object[] {
            "BOLETA",
            "FACTURA"});
            this.cmb_reason_notes.Location = new System.Drawing.Point(78, 56);
            this.cmb_reason_notes.Name = "cmb_reason_notes";
            this.cmb_reason_notes.Size = new System.Drawing.Size(320, 21);
            this.cmb_reason_notes.TabIndex = 25;
            this.cmb_reason_notes.SelectedIndexChanged += new System.EventHandler(this.cmb_reason_notes_SelectedIndexChanged);
            // 
            // txt_descripcion
            // 
            this.txt_descripcion.Location = new System.Drawing.Point(538, 56);
            this.txt_descripcion.Name = "txt_descripcion";
            this.txt_descripcion.Size = new System.Drawing.Size(329, 20);
            this.txt_descripcion.TabIndex = 2;
            this.txt_descripcion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_descripcion_KeyPress);
            // 
            // detalleProductTable
            // 
            this.detalleProductTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.detalleProductTable.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.detalleProductTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.detalleProductTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colCant,
            this.colUM,
            this.colCod,
            this.colDes,
            this.colPU,
            this.colDescu,
            this.colVi,
            this.colICB,
            this.colAccion,
            this.colValorItem,
            this.codigoSunat});
            this.detalleProductTable.Enabled = false;
            this.detalleProductTable.Location = new System.Drawing.Point(18, 428);
            this.detalleProductTable.Name = "detalleProductTable";
            this.detalleProductTable.ReadOnly = true;
            this.detalleProductTable.Size = new System.Drawing.Size(888, 153);
            this.detalleProductTable.TabIndex = 27;
            // 
            // colCant
            // 
            this.colCant.HeaderText = "Cant.";
            this.colCant.Name = "colCant";
            this.colCant.ReadOnly = true;
            // 
            // colUM
            // 
            this.colUM.HeaderText = "Uni Med";
            this.colUM.Name = "colUM";
            this.colUM.ReadOnly = true;
            // 
            // colCod
            // 
            this.colCod.HeaderText = "Código";
            this.colCod.Name = "colCod";
            this.colCod.ReadOnly = true;
            // 
            // colDes
            // 
            this.colDes.HeaderText = "Descripcion";
            this.colDes.Name = "colDes";
            this.colDes.ReadOnly = true;
            // 
            // colPU
            // 
            this.colPU.HeaderText = "P. Unitario";
            this.colPU.Name = "colPU";
            this.colPU.ReadOnly = true;
            // 
            // colDescu
            // 
            this.colDescu.HeaderText = "Desc";
            this.colDescu.Name = "colDescu";
            this.colDescu.ReadOnly = true;
            // 
            // colVi
            // 
            this.colVi.HeaderText = "IGV";
            this.colVi.Name = "colVi";
            this.colVi.ReadOnly = true;
            // 
            // colICB
            // 
            this.colICB.HeaderText = "ICBPER";
            this.colICB.Name = "colICB";
            this.colICB.ReadOnly = true;
            // 
            // colAccion
            // 
            this.colAccion.HeaderText = "Accion";
            this.colAccion.Name = "colAccion";
            this.colAccion.ReadOnly = true;
            this.colAccion.Visible = false;
            // 
            // colValorItem
            // 
            this.colValorItem.HeaderText = "valorItemDisabled";
            this.colValorItem.Name = "colValorItem";
            this.colValorItem.ReadOnly = true;
            this.colValorItem.Visible = false;
            // 
            // codigoSunat
            // 
            this.codigoSunat.HeaderText = "codigoSunat";
            this.codigoSunat.Name = "codigoSunat";
            this.codigoSunat.ReadOnly = true;
            this.codigoSunat.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label40);
            this.panel1.Controls.Add(this.txt_descuentoGlobal);
            this.panel1.Controls.Add(this.txt_total);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.txt_igv);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.txt_subtotal);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Location = new System.Drawing.Point(224, 590);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(682, 36);
            this.panel1.TabIndex = 30;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(32, 11);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(119, 13);
            this.label40.TabIndex = 45;
            this.label40.Text = "DESCUENTO GLOBAL";
            // 
            // txt_descuentoGlobal
            // 
            this.txt_descuentoGlobal.Location = new System.Drawing.Point(156, 8);
            this.txt_descuentoGlobal.Name = "txt_descuentoGlobal";
            this.txt_descuentoGlobal.ReadOnly = true;
            this.txt_descuentoGlobal.Size = new System.Drawing.Size(78, 20);
            this.txt_descuentoGlobal.TabIndex = 46;
            this.txt_descuentoGlobal.Text = "0";
            this.txt_descuentoGlobal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_total
            // 
            this.txt_total.Location = new System.Drawing.Point(577, 8);
            this.txt_total.Name = "txt_total";
            this.txt_total.ReadOnly = true;
            this.txt_total.Size = new System.Drawing.Size(91, 20);
            this.txt_total.TabIndex = 23;
            this.txt_total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(527, 11);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 13);
            this.label16.TabIndex = 24;
            this.label16.Text = "TOTAL";
            // 
            // txt_igv
            // 
            this.txt_igv.Location = new System.Drawing.Point(440, 8);
            this.txt_igv.Name = "txt_igv";
            this.txt_igv.ReadOnly = true;
            this.txt_igv.Size = new System.Drawing.Size(81, 20);
            this.txt_igv.TabIndex = 21;
            this.txt_igv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(409, 11);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(25, 13);
            this.label15.TabIndex = 22;
            this.label15.Text = "IGV";
            // 
            // txt_subtotal
            // 
            this.txt_subtotal.Location = new System.Drawing.Point(322, 8);
            this.txt_subtotal.Name = "txt_subtotal";
            this.txt_subtotal.ReadOnly = true;
            this.txt_subtotal.Size = new System.Drawing.Size(84, 20);
            this.txt_subtotal.TabIndex = 20;
            this.txt_subtotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(245, 11);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 13);
            this.label14.TabIndex = 20;
            this.label14.Text = "SUBTOTAL";
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(15, 613);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(0, 16);
            this.lblError.TabIndex = 40;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.GhostWhite;
            this.panel4.Controls.Add(this.lblTipoMonedaPass);
            this.panel4.Controls.Add(this.txtVendedorVenta);
            this.panel4.Controls.Add(this.txtClienteVenta);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.txtNumeroVenta);
            this.panel4.Controls.Add(this.txtSerieVenta);
            this.panel4.Controls.Add(this.txtTipoCambioVenta);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.txtCodigoVenta);
            this.panel4.Controls.Add(this.label21);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Location = new System.Drawing.Point(18, 131);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(888, 140);
            this.panel4.TabIndex = 42;
            // 
            // lblTipoMonedaPass
            // 
            this.lblTipoMonedaPass.AutoSize = true;
            this.lblTipoMonedaPass.Location = new System.Drawing.Point(821, 49);
            this.lblTipoMonedaPass.Name = "lblTipoMonedaPass";
            this.lblTipoMonedaPass.Size = new System.Drawing.Size(41, 13);
            this.lblTipoMonedaPass.TabIndex = 45;
            this.lblTipoMonedaPass.Text = "label17";
            this.lblTipoMonedaPass.Visible = false;
            // 
            // txtVendedorVenta
            // 
            this.txtVendedorVenta.Enabled = false;
            this.txtVendedorVenta.Location = new System.Drawing.Point(669, 96);
            this.txtVendedorVenta.Name = "txtVendedorVenta";
            this.txtVendedorVenta.Size = new System.Drawing.Size(198, 20);
            this.txtVendedorVenta.TabIndex = 38;
            // 
            // txtClienteVenta
            // 
            this.txtClienteVenta.Enabled = false;
            this.txtClienteVenta.Location = new System.Drawing.Point(78, 95);
            this.txtClienteVenta.Name = "txtClienteVenta";
            this.txtClienteVenta.Size = new System.Drawing.Size(496, 20);
            this.txtClienteVenta.TabIndex = 37;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.DimGray;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(587, 96);
            this.label8.MaximumSize = new System.Drawing.Size(0, 20);
            this.label8.MinimumSize = new System.Drawing.Size(0, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 20);
            this.label8.TabIndex = 36;
            this.label8.Text = "Vendedor";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtNumeroVenta
            // 
            this.txtNumeroVenta.Enabled = false;
            this.txtNumeroVenta.Location = new System.Drawing.Point(669, 41);
            this.txtNumeroVenta.Name = "txtNumeroVenta";
            this.txtNumeroVenta.Size = new System.Drawing.Size(99, 20);
            this.txtNumeroVenta.TabIndex = 35;
            // 
            // txtSerieVenta
            // 
            this.txtSerieVenta.Enabled = false;
            this.txtSerieVenta.Location = new System.Drawing.Point(482, 41);
            this.txtSerieVenta.Name = "txtSerieVenta";
            this.txtSerieVenta.Size = new System.Drawing.Size(92, 20);
            this.txtSerieVenta.TabIndex = 34;
            // 
            // txtTipoCambioVenta
            // 
            this.txtTipoCambioVenta.Enabled = false;
            this.txtTipoCambioVenta.Location = new System.Drawing.Point(284, 42);
            this.txtTipoCambioVenta.Name = "txtTipoCambioVenta";
            this.txtTipoCambioVenta.Size = new System.Drawing.Size(114, 20);
            this.txtTipoCambioVenta.TabIndex = 33;
            this.txtTipoCambioVenta.Text = "0";
            this.txtTipoCambioVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.DimGray;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(587, 41);
            this.label7.MaximumSize = new System.Drawing.Size(65, 20);
            this.label7.MinimumSize = new System.Drawing.Size(65, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 20);
            this.label7.TabIndex = 32;
            this.label7.Text = "Numero";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.DimGray;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(426, 42);
            this.label6.MaximumSize = new System.Drawing.Size(50, 20);
            this.label6.MinimumSize = new System.Drawing.Size(50, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 20);
            this.label6.TabIndex = 31;
            this.label6.Text = "Serie";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.DimGray;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(203, 42);
            this.label5.MaximumSize = new System.Drawing.Size(0, 20);
            this.label5.MinimumSize = new System.Drawing.Size(0, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 20);
            this.label5.TabIndex = 30;
            this.label5.Text = "T.Cambio";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCodigoVenta
            // 
            this.txtCodigoVenta.Enabled = false;
            this.txtCodigoVenta.Location = new System.Drawing.Point(78, 41);
            this.txtCodigoVenta.Name = "txtCodigoVenta";
            this.txtCodigoVenta.Size = new System.Drawing.Size(103, 20);
            this.txtCodigoVenta.TabIndex = 29;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.DimGray;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(10, 42);
            this.label21.MaximumSize = new System.Drawing.Size(60, 20);
            this.label21.MinimumSize = new System.Drawing.Size(60, 20);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(60, 20);
            this.label21.TabIndex = 21;
            this.label21.Text = "Codigo";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.Image = ((System.Drawing.Image)(resources.GetObject("label20.Image")));
            this.label20.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label20.Location = new System.Drawing.Point(3, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(37, 31);
            this.label20.TabIndex = 20;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.DimGray;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(10, 94);
            this.label10.MaximumSize = new System.Drawing.Size(60, 20);
            this.label10.MinimumSize = new System.Drawing.Size(60, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 20);
            this.label10.TabIndex = 5;
            this.label10.Text = "Cliente";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.MediumBlue;
            this.label12.Location = new System.Drawing.Point(46, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(115, 16);
            this.label12.TabIndex = 0;
            this.label12.Text = "Datos de Venta";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.DimGray;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(12, 56);
            this.label2.MaximumSize = new System.Drawing.Size(60, 20);
            this.label2.MinimumSize = new System.Drawing.Size(60, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 20);
            this.label2.TabIndex = 26;
            this.label2.Text = "Motivo";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.DimGray;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(426, 56);
            this.label4.MaximumSize = new System.Drawing.Size(0, 20);
            this.label4.MinimumSize = new System.Drawing.Size(0, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 20);
            this.label4.TabIndex = 27;
            this.label4.Text = "Descripcion";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.DimGray;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(12, 104);
            this.label9.MaximumSize = new System.Drawing.Size(0, 20);
            this.label9.MinimumSize = new System.Drawing.Size(0, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 20);
            this.label9.TabIndex = 28;
            this.label9.Text = "Importe";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtImporteNota
            // 
            this.txtImporteNota.Enabled = false;
            this.txtImporteNota.Location = new System.Drawing.Point(78, 104);
            this.txtImporteNota.Name = "txtImporteNota";
            this.txtImporteNota.Size = new System.Drawing.Size(103, 20);
            this.txtImporteNota.TabIndex = 29;
            this.txtImporteNota.Text = "0.00";
            this.txtImporteNota.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.GhostWhite;
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.txtImporteNota);
            this.panel3.Controls.Add(this.label24);
            this.panel3.Controls.Add(this.cmb_reason_notes);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.txt_descripcion);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Location = new System.Drawing.Point(18, 277);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(888, 140);
            this.panel3.TabIndex = 43;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(867, 59);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(18, 13);
            this.label11.TabIndex = 45;
            this.label11.Text = "*";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(404, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 13);
            this.label3.TabIndex = 44;
            this.label3.Text = "*";
            // 
            // label22
            // 
            this.label22.Image = global::ferreteriaApp.Properties.Resources.icon_edit_note_32;
            this.label22.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label22.Location = new System.Drawing.Point(3, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(37, 31);
            this.label22.TabIndex = 20;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.MediumBlue;
            this.label24.Location = new System.Drawing.Point(46, 9);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(108, 16);
            this.label24.TabIndex = 0;
            this.label24.Text = "Datos de Nota";
            // 
            // button3
            // 
            this.button3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = global::ferreteriaApp.Properties.Resources.icons8_delete_bin_32px_1;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(672, 634);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(103, 33);
            this.button3.TabIndex = 29;
            this.button3.Text = "Cancelar";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btn_Generar
            // 
            this.btn_Generar.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btn_Generar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Generar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Generar.Image = ((System.Drawing.Image)(resources.GetObject("btn_Generar.Image")));
            this.btn_Generar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Generar.Location = new System.Drawing.Point(781, 634);
            this.btn_Generar.Name = "btn_Generar";
            this.btn_Generar.Size = new System.Drawing.Size(125, 33);
            this.btn_Generar.TabIndex = 28;
            this.btn_Generar.Text = "Generar Nota";
            this.btn_Generar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_Generar.UseVisualStyleBackColor = true;
            this.btn_Generar.Click += new System.EventHandler(this.btn_Generar_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 103);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(119, 13);
            this.label13.TabIndex = 44;
            this.label13.Text = "| Campos obligatorios (*)";
            // 
            // notes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(921, 679);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btn_Generar);
            this.Controls.Add(this.detalleProductTable);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "notes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NOTAS";
            this.Activated += new System.EventHandler(this.notes_Activated);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.detalleProductTable)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DateTimePicker txt_fecha;
        private System.Windows.Forms.Label lbl_serie_note;
        private System.Windows.Forms.ComboBox cmb_type_note;
        private System.Windows.Forms.TextBox txt_descripcion;
        private System.Windows.Forms.ComboBox cmb_reason_notes;
        private System.Windows.Forms.DataGridView detalleProductTable;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txt_total;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txt_igv;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txt_subtotal;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btn_Generar;
        private System.Windows.Forms.Label lbl_numero_nota;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtVendedorVenta;
        private System.Windows.Forms.TextBox txtClienteVenta;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNumeroVenta;
        private System.Windows.Forms.TextBox txtSerieVenta;
        private System.Windows.Forms.TextBox txtTipoCambioVenta;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCodigoVenta;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtImporteNota;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblTipoMonedaPass;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCant;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUM;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCod;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDes;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPU;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescu;
        private System.Windows.Forms.DataGridViewTextBoxColumn colVi;
        private System.Windows.Forms.DataGridViewTextBoxColumn colICB;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAccion;
        private System.Windows.Forms.DataGridViewTextBoxColumn colValorItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigoSunat;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txt_descuentoGlobal;
    }
}
