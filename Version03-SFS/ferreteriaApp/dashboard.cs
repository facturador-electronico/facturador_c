﻿using ferreteriaApp.CEN;
using ferreteriaApp.CLN;
using ferreteriaApp.HELPERS;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

namespace ferreteriaApp
{
    public partial class dashboard : Form
    {

        CLNComprobante cln = new CLNComprobante();
        CLNConcepto clnConcepto = new CLNConcepto();

        decimal _totalComparar = 0;

        // encapsulacion de datos para el llenado desde el formulario de producto
        #region campos para llenar producto

        private TextBox _textPrdo;
        private TextBox _textcodigo;
        private TextBox _textPrecio;
        private ComboBox _textunidad;
        public string _tipoMoneda;
        public bool _afectacionIGV;

        public bool _busquedaForm = false;
        public string _codigo_sunat = "-";
        public string _unidad_medida = "";

        public string dias ="";
        public string editarPrecio = "";
        public string activoProdSoloCatalogo = "";
        public string activoBuscarSunatReniec = "";

        public bool _is_dni = false;
        public bool _api_result = false;
        private bool _flag_activar_descuento_global = false;
        private bool _flag_mensaje_dni_default = false;
        private bool _flag_validacion_dni_default = false;
        private decimal _monto_obligatoriedad_dni = 0;

        private int _dias_antes_boleta = 0;
        private int _dias_despues_boleta = 0;

        private string _api_nombres = "";
        private string _api_direccion = "";
        private bool _has_data_bd = false;

        private TextBox _textRucDni;
        private TextBox _textRazonNombres;
        private TextBox _textDireccion;

        public TextBox TextRucDni { get => _textRucDni; set => _textRucDni = value; }
        public TextBox TextRazonNombres { get => _textRazonNombres; set => _textRazonNombres = value; }
        public TextBox TextDireccion { get => _textDireccion; set => _textDireccion = value; }

        public TextBox TextPrdo { get => _textPrdo; set => _textPrdo = value; }
        public TextBox Textcodigo { get => _textcodigo; set => _textcodigo = value; }
        public TextBox TextPrecio { get => _textPrecio; set => _textPrecio = value; }
        public ComboBox Textunidad { get => _textunidad; set => _textunidad = value; }

        public void GetParametro()
        {
            try
            {
                dias = new CLNConcepto().ObtenerValorParametro(17);
                editarPrecio = new CLNConcepto().ObtenerValorParametro(16);
                activoProdSoloCatalogo = new CLNConcepto().ObtenerValorParametro(19);
                activoBuscarSunatReniec = new CLNConcepto().ObtenerValorParametro(20);

                //para boletas
                CENParametro parFlagRegrear;
                CLNConcepto clncconcepto;

                clncconcepto = new CLNConcepto();
                parFlagRegrear = new CENParametro();
                parFlagRegrear = clncconcepto.buscarParametro(1009);
                _dias_antes_boleta = parFlagRegrear.par_int1;

                clncconcepto = new CLNConcepto();
                parFlagRegrear = new CENParametro();
                parFlagRegrear = clncconcepto.buscarParametro(1010);
                _dias_despues_boleta = parFlagRegrear.par_int1;


            

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void buscarFlags()
        {
            CENParametro parametro;
            
            CLNConcepto clncconcepto = new CLNConcepto();

            parametro = new CENParametro();
            parametro = clncconcepto.buscarParametro(1007);
            _flag_mensaje_dni_default = (parametro.par_int1.ToString()=="1")?true:false;
            _flag_validacion_dni_default= (parametro.par_tipo.ToString() == "1") ? true : false;

            parametro = new CENParametro();
            parametro = clncconcepto.buscarParametro(1008);
            _monto_obligatoriedad_dni = Convert.ToDecimal(parametro.par_float1);

            parametro = new CENParametro();
            parametro = clncconcepto.buscarParametro(1011);
            _flag_activar_descuento_global = (parametro.par_tipo.ToString() == "1") ? true : false;


            if (!_flag_activar_descuento_global)
            {
                this.txt_descuento.Enabled = false;
                this.chkDescuentoGlobal.Enabled = false;
            }
        }

        private void init()
        {
            _textPrdo = txt_producto;
            _textcodigo = txt_codigo;
            _textPrecio = txt_precio;
            _textunidad = this.cmbUnidad;

            _textRucDni = txt_documento;
            _textRazonNombres = txtCliente;
            _textDireccion = txtDireccionCliente;
         
            EditarPrecio();
            
            this.cmbUnidad.Enabled = true;

            this._is_dni = false;

            if (activoBuscarSunatReniec == "1") this.chkSunatReniec.Checked = true;  else this.chkSunatReniec.Checked = false;


        }
        public void llenarComboUnidad()
        {
            funciones.llenarComboUnidad(this.cmbUnidad);
        }


        private void EditarPrecio()
        {

            if (editarPrecio == "1")
            {
                chk_editar_precio.Visible = true;
                lbl_modificar.Visible = true;
            }
            else
            {
                chk_editar_precio.Visible = false;
                lbl_modificar.Visible = false;
            }
        }

        #endregion

        public dashboard()
        {
            try
            {
                GetParametro();
                InitializeComponent();
                llenarComboUnidad();
                buscarFlags();
                this._is_dni = false;
                this._api_result = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
           
         
        }

        private void llenarCodigoComprobante()
        {
            string cp;
            comboBox1.SelectedValue = 1;

            if (comboBox1.SelectedValue.ToString() == "1")
            {

                cp = cln.codigoBoleta(CENConstantes.FACTURA);
                txt_documento.MaxLength = 11;
                mensaje_documento.Text = "Cantidad de números 11";
                txt_codigoBoleta.Text = clnConcepto.ObtenerValorParametro(3) + "-" + cp;
            }
            else
            {
                cp = cln.codigoBoleta(CENConstantes.BOLETA);
                txt_documento.MaxLength = 8;
                mensaje_documento.Text = "Cantidad de números 8";
                txt_codigoBoleta.Text = clnConcepto.ObtenerValorParametro(4) + "-" + cp;

            }

           
            txt_codigo_comprobante.Text = cp;

            this.txtCliente.Enabled = true;
            this.txt_documento.Enabled = true;

        }
        private void LlenarCombo()
        {
            List<CENTipoDocumento> lista;
            this.comboBox1.Items.Clear();
            try
            {
                CLNConcepto cln = new CLNConcepto();

                lista = cln.listaTipoDocumentos(1);

                this.comboBox1.DataSource = lista;
                this.comboBox1.DisplayMember = "nombreDocumento";
                this.comboBox1.ValueMember = "codigoDocumento";
            }
            catch
            {
                CENTipoDocumento cen = new CENTipoDocumento();
                cen.codigoDocumento = 0;
                cen.nombreDocumento = "No se encontraron documentos";
                lista = null;
                this.comboBox1.DataSource = lista;
                this.comboBox1.DisplayMember = "nombreDocumento";
                this.comboBox1.ValueMember = "codigoDocumento";
            }


        }

        private void dashboard_Load(object sender, EventArgs e)
        {
            init();
        }

 

        private void btn_agragr_producto_Click(object sender, EventArgs e)
        {




            Boolean estado = true;
            decimal cantidad = 0;
            decimal precio = 0,
                descuento = 0;

            if (txt_producto.Text.Trim() == null || txt_producto.Text.Trim() == "")
            {
                lbl_mensaje.Text = "";
                lbl_mensaje.Text = "Ingrese el nombre del producto";
                estado = false;
                txt_producto.Focus();
                return;
            }

            if (txt_precio.Text.Trim() == null || txt_precio.Text.Trim() == "")
            {
                estado = false;
                lbl_mensaje.Text = "";
                txt_precio.Focus();
                lbl_mensaje.Text = "Ingrese el precio del producto";
                return;
            }
            else
            {


                try
                {
                    precio = Convert.ToDecimal(txt_precio.Text);
                    if (precio <= 0)
                    {
                        estado = false;
                        lbl_mensaje.Text = "";
                        txt_precio.Text = "";
                        txt_precio.Focus();
                        lbl_mensaje.Text = "El precio debe ser mayor a cero, ";
                        return;
                    }
                }
                catch
                {
                    lbl_mensaje.Text = "";
                    txt_precio.Text = "0";
                    estado = false;
                    txt_precio.Text = "";
                    lbl_mensaje.Text = "El precio debe ser expresado en números";
                    txt_precio.Focus();
                    return;
                }

            }

            if (txt_cantidad.Text.Trim() == null || txt_cantidad.Text.Trim() == "")
            {
                estado = false;
                lbl_mensaje.Text = "";
                txt_cantidad.Focus();
                lbl_mensaje.Text = "Ingrese la cantidad del producto";
                return;
            }
            else
            {

                try
                {
                    cantidad = Convert.ToDecimal(txt_cantidad.Text);

                    if (cantidad <= 0)
                    {
                        lbl_mensaje.Text = "";
                        estado = false;
                        txt_cantidad.Text = "";
                        txt_cantidad.Focus();
                        lbl_mensaje.Text = "La cantidad debe ser mayor a cero";
                        return;
                    }
                }
                catch
                {
                    estado = false;
                    lbl_mensaje.Text = "";
                    txt_cantidad.Text = "";
                    txt_cantidad.Focus();
                    lbl_mensaje.Text = "La cantidad debe ser expresada en números";
                    return;
                }

            }

            try
            {
                descuento = Convert.ToDecimal(txt_descuento.Text);
                if (descuento < 0)
                {
                    lbl_mensaje.Text = "";
                    estado = false;
                    txt_descuento.Text = "";
                    txt_descuento.Focus();
                    lbl_mensaje.Text = "El descuento no puede contener signos y debe ser mayor o igual a cero";
                    return;
                }
            }
            catch
            {
                lbl_mensaje.Text = "";
                txt_descuento.Text = "0";
                estado = false;
                lbl_mensaje.Text = "El descuento debe ser expresado en números";
                txt_descuento.Focus();

                return;

            }



            if (estado)
            {
                lbl_mensaje.Text = "";
                int cantidad_prod = detalleProductTable.Rows.Count;
                decimal igv_item;
                String producto = txt_producto.Text;
                String unidadMedida = this.cmbUnidad.Text.ToString();
                String codigo = txt_codigo.Text;
                String codigo_sunat = _codigo_sunat;

                if (!_busquedaForm)
                {

                        List<CENListadoProducto> productos = new CLNProductos().buscarProducto(txt_producto.Text.Trim(), txt_codigo.Text.Trim(), 0);
                        if (productos.Count == 0)
                        {
                            if (activoProdSoloCatalogo == "1")
                            {
                                lbl_mensaje.Text = "Por favor, el producto debe ser registrado en el catálogo de productos, para continuar.";
                                txt_producto.Focus();
                                return;
                            }
                            else
                            {
                                producto = this.txt_producto.Text;
                                unidadMedida = this.cmbUnidad.Text.ToString();
                                precio = Convert.ToDecimal(this.txt_precio.Text);

                                llenarTablaDetalle(cantidad_prod, codigo, codigo_sunat, producto, cantidad, unidadMedida, descuento, precio);
                            }
                               
                        }
                        else if (productos.Count > 1)
                        {
                            lbl_mensaje.Text = "Hay más de un producto con ese nombre en la base de datos";
                            txt_producto.Focus();

                            return;
                        }
                        else
                        {

                            foreach (CENListadoProducto item in productos)
                            {

                                producto = item.DESCRIPCION;
                                codigo = item.CODIGO_PRODUCTO;
                                unidadMedida = item.UNIDAD_MEDIDA;
                                precio = item.PRECIO;
                                codigo_sunat = item.CODIGO_SUNAT;


                                if (item.CON_IGV == "NO")
                                {
                                    precio = precio + precio * Convert.ToDecimal(0.18);

                                }
                            }

                            llenarTablaDetalle(cantidad_prod, codigo, codigo_sunat, producto, cantidad, unidadMedida, descuento, precio);

                        }
                   
                }
                else
                {

                    llenarTablaDetalle(cantidad_prod, codigo, codigo_sunat, producto, cantidad, unidadMedida, descuento, precio);

                }

                _codigo_sunat = "-";
                this.cmbUnidad.SelectedValue = "NIU";
                this.cmbUnidad.Enabled = true;
                _unidad_medida = "";
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {

           

            Int64 documento;
            decimal descuentoTotal = 0;
            decimal totalComp = 0, totalSet = 0 ;
            

            totalSet = (txt_total.Text.Trim() != "") ? Convert.ToDecimal(txt_total.Text.Trim()) : 0;
            //if (Convert.ToDecimal(this.txt_descuentoGlobal.Text) > 0)
            //{
            //    totalComp = totalSet + Convert.ToDecimal(this.txt_descuentoGlobal.Text);
            //}
            //else
            //{
            //    totalComp = totalSet ;
            //}
            totalComp = totalSet;



            CLNComprobante cln = new CLNComprobante();
            DialogResult diagRes;
            
            if (!_is_dni)
            {
                if (txtCliente.Text.Trim().Length == 0)
                {
                    MessageBox.Show("Debe ingresar el nombre del cliente");
                    this.txtCliente.Enabled = true;
                    this.txt_documento.Enabled = true;
                    txtCliente.Focus();
                    return;
                }
                else if (txt_documento.Text.Trim().Length == 0)
                {
                    MessageBox.Show("Debe ingresar documento del cliente");
                    this.txtCliente.Enabled = true;
                    this.txt_documento.Enabled = true;
                    txt_documento.Focus();
                    return;
                }
            }
            else
            {
                if ((txtCliente.Text.Trim().Length == 0) || (txt_documento.Text.Trim().Length == 0))
                {
                    if (_flag_validacion_dni_default)
                    {
                        if (totalComp <= _monto_obligatoriedad_dni)
                        {
                            if(_flag_mensaje_dni_default)
                            {
                                aviso aviso_form_error_hash2 = new aviso("warning", "ANTES DE SEGUIR", "No está ingresando DNI y/o nombres del cliente, se cargará datos por defecto."
                                         + "\n ¿Desea continuar?", true);
                                diagRes = aviso_form_error_hash2.ShowDialog();
                                if (diagRes == DialogResult.OK)
                                {
                                    this.txt_documento.Text = CENConstantes.DNI_DEFAULT;
                                    this.txtCliente.Text = CENConstantes.NOMBRE_DNI_DEFAULT;
                                    this.txtCliente.Enabled = false;
                                    this.txt_documento.Enabled = false;
                                }
                                else
                                {
                                    txt_documento.Focus();
                                    return;
                                }
                            }
                            else
                            {
                                this.txt_documento.Text = CENConstantes.DNI_DEFAULT;
                                this.txtCliente.Text = CENConstantes.NOMBRE_DNI_DEFAULT;
                            }
                        }
                        else
                        {
                          
                        }
                    }
                    else
                    {
                        if (txtCliente.Text.Trim().Length == 0)
                        {
                            MessageBox.Show("Debe ingresar el nombre del cliente");
                            this.txtCliente.Enabled = true;
                            this.txt_documento.Enabled = true;
                            txtCliente.Focus();
                            return;
                        }
                        else if (txt_documento.Text.Trim().Length == 0)
                        {
                            MessageBox.Show("Debe ingresar documento del cliente");
                            this.txtCliente.Enabled = true;
                            this.txt_documento.Enabled = true;
                            txt_documento.Focus();
                            return;
                        }

                    

                    }

                }
                else
                {
                    if (totalComp > _monto_obligatoriedad_dni)
                    {
                        if (this.txt_documento.Text.Trim() == CENConstantes.DNI_DEFAULT || this.txtCliente.Text.Trim() == CENConstantes.NOMBRE_DNI_DEFAULT)
                        {
                            MessageBox.Show("El valor de la venta supera los "+ _monto_obligatoriedad_dni+" soles \n"+"Debe ingresar dni y/o nombres correctos");
                            if (this.txt_documento.Text == CENConstantes.DNI_DEFAULT)
                            {
                                this.txtCliente.Text = "";
                                this.txt_documento.Text = "";
                                mensaje_documento.Text = Convert.ToString(txt_documento.Text.Trim().Length);
                            
                            }
                            this.txtCliente.Enabled = true;
                            this.txt_documento.Enabled = true;
                            this.txt_documento.Focus();
                            return;
                        }

                    }
                }
              
            }



            if (txtCliente.Text.Trim().Length == 0)
            {
                MessageBox.Show("Debe ingresar el nombre del cliente");
                this.txtCliente.Enabled = true;
                this.txt_documento.Enabled = true;
                txtCliente.Focus();
                return;
            }


            if (detalleProductTable.Rows.Count == 1)
            {
                MessageBox.Show("Debe ingresar un producto para guardar la venta");
                return;

            }
            else if ( (comboBox1.SelectedValue.ToString() == "3" &&  txt_documento.Text.Trim().Length < 8) || (comboBox1.SelectedValue.ToString() == "3" && txt_documento.Text.Trim().Length >9  ))
            {
                txt_documento.Focus();
                MessageBox.Show("El dni debe tener 8 digitos");
                this.txtCliente.Enabled = true;
                this.txt_documento.Enabled = true;
                return;
            }
            else if (comboBox1.SelectedValue.ToString() == "1" && txt_documento.Text.Trim().Length < 11)
            {
                txt_documento.Focus();
                MessageBox.Show("El ruc debe tener 11 digitos");
                if (this.txt_documento.Text == CENConstantes.DNI_DEFAULT)
                {
                    this.txtCliente.Text = "";
                    this.txt_documento.Text = "";
                    mensaje_documento.Text = Convert.ToString(txt_documento.Text.Trim().Length);
                 
                }
                this.txtCliente.Enabled = true;
                this.txt_documento.Enabled = true;
                this.txt_documento.Focus();
                return;


            }

            else if (!Int64.TryParse(txt_documento.Text, NumberStyles.AllowThousands, CultureInfo.CreateSpecificCulture("es-ES"), out documento))
            {

                MessageBox.Show("Error el documento solo debe tener números");
                txt_documento.Text = "";
                txt_documento.Focus();
                return;
            }
            else
            {


                DialogResult dialog = MessageBox.Show("¿ Está seguro que desea guardar la venta ?", "Confirmación",
                MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                if (dialog == DialogResult.Yes)
                {



                    //datos del cliente

                    String direccionCliente = txtDireccionCliente.Text;
                    String cliente = txtCliente.Text;

                    decimal anticipo = 0;

                    decimal isc = 0;
                    decimal otrosImpuestos = 0;
                    decimal descuentoGlobal = 0;
                    
                    CENComprobanteDetalle det;
                    CENComprobanteCabecera cab = new CENComprobanteCabecera();
                    List<CENComprobanteDetalle> listDet = new List<CENComprobanteDetalle>();

                    for (int i = 0; i < detalleProductTable.Rows.Count - 1; i++)
                    {

                        det = new CENComprobanteDetalle();

                        det.cantidad = decimal.Parse(detalleProductTable.Rows[i].Cells[2].Value.ToString());
                        det.unidadMedida = detalleProductTable.Rows[i].Cells[3].Value.ToString();
                        det.codigo = detalleProductTable.Rows[i].Cells[0].Value.ToString(); ;
                        det.descripcion = detalleProductTable.Rows[i].Cells[1].Value.ToString();
                        det.precioUnitario = decimal.Parse(detalleProductTable.Rows[i].Cells[4].Value.ToString());
                        det.descuentoUnitario = decimal.Parse(detalleProductTable.Rows[i].Cells[5].Value.ToString());
                        det.valorItem = decimal.Parse(detalleProductTable.Rows[i].Cells[7].Value.ToString());

                        det.codigo_sunat = detalleProductTable.Rows[i].Cells[8].Value.ToString(); ;
                        det.icbper = 0;

                        descuentoTotal = descuentoTotal + Convert.ToDecimal(detalleProductTable.Rows[i].Cells[5].Value);
                        listDet.Add(det);

                    }

                    descuentoGlobal = Convert.ToDecimal(this.txt_descuentoGlobal.Text) / Convert.ToDecimal(1.18);

                    string serie;

                    if (comboBox1.SelectedValue.ToString() == "1")
                    {

                        cab.tipoComprobante = CENConstantes.FACTURA;
                        cab.tipoDocumento = CENConstantes.CODIGO_RUC;
                        serie = clnConcepto.ObtenerValorParametro(3);
                    }
                    else
                    {
                        cab.tipoComprobante = CENConstantes.BOLETA;
                        cab.tipoDocumento = CENConstantes.CODIGO_DNI;
                        serie = clnConcepto.ObtenerValorParametro(4);

                    }
                    decimal igvCpe = 0, subTotalCpe = 0, importaTotalCpe = 0, importPayable=0, importeTotalLetras=0;
                    //if(descuentoGlobal>0)
                    //{
                    //    importeTotalLetras = Math.Round(Convert.ToDecimal(txt_total.Text));
                    //    importaTotalCpe = Math.Round( (Convert.ToDecimal(txt_total.Text) + descuentoGlobal),2);
                    //    subTotalCpe = Math.Round((importaTotalCpe / Convert.ToDecimal(1.18)),2);
                    //    igvCpe = importaTotalCpe - subTotalCpe;

                    //    importPayable = Math.Round(Convert.ToDecimal(txt_total.Text), 2); 

                    //}
                    //else
                    //{
                    //    importaTotalCpe = Convert.ToDecimal(txt_total.Text);
                    //    subTotalCpe     = Convert.ToDecimal(txt_subtotal.Text);
                    //    igvCpe          = Convert.ToDecimal(txt_igv.Text);
                    //    importPayable = importaTotalCpe;
                    //}

                    importeTotalLetras = Convert.ToDecimal(txt_total.Text);
                    importaTotalCpe = Convert.ToDecimal(txt_total.Text);
                    subTotalCpe = Convert.ToDecimal(txt_subtotal.Text);
                    igvCpe = Convert.ToDecimal(txt_igv.Text);
                    importPayable = importaTotalCpe;
                    //************

                    //descuentoTotal = descuentoTotal + (descuentoGlobal/Convert.ToDecimal(1.18));
                    descuentoTotal = descuentoTotal + descuentoGlobal ;

                    cab.tipoOperacion = 1;
                    cab.empresa = 1;
                    cab.codigo = txt_codigoBoleta.Text;
                    cab.fechaEmision = txt_fecha.Value.Date;
                    cab.anticipos = anticipo;
                    cab.descuentoGlobal = descuentoGlobal;
                    cab.descuentoTotal = descuentoTotal;
                    cab.direccionCliente = direccionCliente;
                    cab.nombreCliente = cliente;
                    cab.documentoCliente =this.txt_documento.Text.Trim();
                    cab.tipoMoneda = _tipoMoneda == null ? CENConstantes.PEN : _tipoMoneda;

                    //cab.igv = Convert.ToDecimal(txt_igv.Text);    //cambia con descuento global
                    cab.igv = igvCpe;    //cambia con descuento global

                    cab.isc = isc;
                    cab.otros = otrosImpuestos;

                    //cab.importeTotal = Convert.ToDecimal(txt_total.Text);       // cambia con descuento global
                    //cab.subtotal = Convert.ToDecimal(txt_subtotal.Text); ;      // cambia con descuento global
                    cab.importeTotal = importaTotalCpe;       // cambia con descuento global
                    cab.subtotal = subTotalCpe;      // cambia con descuento global

                    cab.estado_comprobante = CENConstantes.ID_ESTADO;

                    int id_comprobante = cln.GuardarBoleta(cab, listDet);

                    if (id_comprobante >= 1)
                    {
                        try
                        {
                        //guardar cliente si no existe
                        this.guardarClienteAfterSave();

                        txtCliente.Text = "";
                        txt_documento.Text = "";
                        txtDireccionCliente.Text = "";
                        cantidad_productos.Text = "| Cantidad de productos : 0";

                        detalleProductTable.Rows.Clear();
                        txt_subtotal.Text = "";
                        txt_igv.Text = "";
                        txt_total.Text = "";
                        this.txt_descuentoGlobal.Text = "0";
                        this.chkDescuentoGlobal.Checked = false;
                       
                        //llenarCodigoComprobante();

                        CENEstructuraComprobante estructura = cln.Find(id_comprobante);
                       

                      

                            cln.CrearArchivos(estructura, cab.tipoComprobante, serie, txt_codigo_comprobante.Text);
                            cln.GenerarArchivoTRI("1000", "IGV", "VAT", cab.subtotal.ToString(), cab.igv.ToString(), cab.tipoComprobante, serie, txt_codigo_comprobante.Text);
                            cln.crearArchivoLey("1000", cab.tipoComprobante, serie, txt_codigo_comprobante.Text);
                            //creacion de archivos para descuentos globales
                            if(descuentoGlobal > 0)
                            {
                                cln.CrearArchivosACV(cab, cab.tipoComprobante, serie, txt_codigo_comprobante.Text);
                            }


                            if (comboBox1.SelectedValue.ToString() == "1")
                            {
                                cln.crearArchivoPag(this.comboBox1.SelectedValue.ToString(), serie, txt_codigo_comprobante.Text, importPayable);
                            }

                            DialogResult resultadoD = new DialogResult();
                            aviso aviso_form = new aviso("success", "Comprobante con código " + id_comprobante + " generado con éxito", " ¿Desea generar impresión del comprobante?", true);
                            resultadoD = aviso_form.ShowDialog();

                            if (resultadoD == DialogResult.OK)
                            {

                                DialogResult resultadoD2 = new DialogResult();
                                progress progressBar = new progress("GENERANDO PDF DE COMPROBANTE", 7);
                                resultadoD2 = progressBar.ShowDialog();

                                List<string> cabecera = new List<string>();
                                cabecera.Add(string.Format("Fecha Emisión|{0}", cab.fechaEmision.ToString("dd/MM/yyyy")));
                                cabecera.Add(string.Format("Señor(es)|{0}", (cab.documentoCliente == CENConstantes.DNI_DEFAULT) ? "" : cab.nombreCliente));
                                //cabecera.Add(string.Format("R.U.C|{0}", cab.documentoCliente));
                                cabecera.Add(string.Format((cab.tipoDocumento == "1") ? "D.N.I|{0}" : "R.U.C|{0}", (cab.documentoCliente==CENConstantes.DNI_DEFAULT)?"": cab.documentoCliente));
                                cabecera.Add(string.Format("Dirección Cliente|{0}", cab.direccionCliente));
                                cabecera.Add(string.Format("Tipo Moneda|SOLES"));
                                cabecera.Add(string.Format("N° Transacción: |{0}", id_comprobante));


                                List<string> impuestos = new List<string>();

                                impuestos.Add(string.Format("IGV: |{0}", cab.igv));
                                impuestos.Add("ISC: | 0.00");
                                impuestos.Add("OTROS: | 0.00");
                                impuestos.Add(string.Format("DESC.GLOBAL: | {0}", (Math.Round((descuentoGlobal*Convert.ToDecimal(1.18)),2)).ToString("0.00")));

                                ConvertNumberToLetter InLetter = new ConvertNumberToLetter();
                                //string cantidadEnLetras = InLetter.InLetter(cab.importeTotal.ToString());
                                string cantidadEnLetras = InLetter.InLetter(importeTotalLetras.ToString());
                                

                                List<string> subtotal = new List<string>();
                                subtotal.Add(string.Format("Total Valor de venta: |{0}", cab.subtotal.ToString("0.00")));
                                subtotal.Add(string.Format("Descuento Total: |{0}", (Math.Round((descuentoTotal * Convert.ToDecimal(1.18)), 2)).ToString("0.00"))); 
                                subtotal.Add("Anticipos: | 0.00");
                                //subtotal.Add(string.Format("Importe Total: | {0}", cab.importeTotal.ToString("0.00")));  // aplica descuentos
                                subtotal.Add(string.Format("Importe Total: | {0}", importPayable.ToString("0.00")));  // aplica descuentos

                                List<string> body = new List<string>();

                                foreach (CENComprobanteDetalle item in listDet)
                                {
                                    body.Add(string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|", item.cantidad.ToString("0.00"),
                                    item.unidadMedida.ToString(),
                                    item.codigo.ToString()
                                    , item.descripcion.ToString()
                                    , item.precioUnitario.ToString("0.00")
                                    , item.descuentoUnitario.ToString("0.00")
                                    , item.valorItem.ToString("0.00")));
                                }

                                string nombreComprobante = comboBox1.Text;

                                string[] NombHeaderTable = { "Cant.", "Uni. Medida", "Código", "Descripción", "P. Unitario", "Desc", "V. Item" };
                                float[] TamCeldas = { 5, 10, 10, 40, 15, 10, 10 };

                                CLNDocumentTypes cnotes = new CLNDocumentTypes();

                                cnotes.getHashXML(cab.tipoComprobante, serie, txt_codigo_comprobante.Text, id_comprobante);

                                List<string> datosHash = new List<string>();
                                datosHash.Add(Convert.ToString(cab.igv));
                                datosHash.Add(Convert.ToString(cab.subtotal));
                                datosHash.Add(Convert.ToString(cab.fechaEmision.ToString("yyyy-MM-dd")));
                                datosHash.Add(Convert.ToString(cab.tipoDocumento));
                                datosHash.Add(Convert.ToString(cab.documentoCliente));

                                Boolean estado = cln.CrearPDF(cabecera, NombHeaderTable, TamCeldas,
                                         body, impuestos, cab.tipoComprobante, serie, txt_codigo_comprobante.Text, nombreComprobante,
                                         cantidadEnLetras, subtotal, datosHash, id_comprobante
                                     );

                                if (!estado)
                                {
                                    MessageBox.Show("NO SE PUDO IMPRIMIR COMPROBANTE, VERIFIQUE SI EL ENVÍO  DEL COMPROBANTE A SUNAT RE REALIZÓ DE MANERA CORRECTA");
                                }

                                llenarCodigoComprobante();
                                
                                this.txtCliente.Focus();



                            }
                            else
                            {
                                llenarCodigoComprobante();
                            }

                       
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error al crear los archivos del comprobante, vuelva a generar el comprobante \n "+ ex.Message);
                            //eliminamos el comprobante en base de datos
                            cln.deleteComprobantexError(id_comprobante);

                            //actualizamos correlativo
                            llenarCodigoComprobante();
                        }


                    }
                    else
                    {
                        MessageBox.Show("No se pudo guardar la factura");
                    }
                }

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

            DialogResult dialog = MessageBox.Show("¿ Está seguro que desea cancelar la venta ?", "Confirmación",
                MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

            if (dialog == DialogResult.Yes)
            {
                //datos del cliente
                txtCliente.Text = "";
                txtDireccionCliente.Text = "";
                txt_documento.Text = "";

                //datos del producto
                txt_producto.Text = "";
                txt_codigo.Text = "";
                txt_cantidad.Text = "";
                txt_precio.Text = "";

                txt_subtotal.Text = "";
                txt_igv.Text = "";
                txt_total.Text = "";

                this.chkDescuentoGlobal.Checked = false;
                this.txt_descuentoGlobal.Text = "0";


                lbl_mensaje.Text = "";

                this.cmbUnidad.SelectedValue = "NIU";
                this.cmbUnidad.Enabled = true;

                detalleProductTable.Rows.Clear();
            }




        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            string cp = "";

            txt_codigoBoleta.Text = CENConstantes.F001 + "-" + cp;

            txt_documento.Text = "";
            this.txtCliente.Text = "";
            this.txtDireccionCliente.Text = "";

            if (this.chkSunatReniec.Checked == true)   this.txt_documento.Focus();  else  this.txtCliente.Focus();


            if (comboBox1.SelectedValue.ToString() == "1")
            {
                cp = cln.codigoBoleta(CENConstantes.FACTURA);
                txt_documento.Enabled = true;
                txt_codigoBoleta.Text = CENConstantes.F001 + "-" + cp;
                txt_documento.MaxLength = 11;
                mensaje_documento.Text = "Cantidad de números 11";
                _is_dni = false;
                this.lblCheckSunatReniec.Text = "SUNAT";

            }
            else
            {
                cp = cln.codigoBoleta(CENConstantes.BOLETA);
                txt_documento.MaxLength = 8;
                txt_documento.Enabled = true;
                txt_codigoBoleta.Text = CENConstantes.B001 + "-" + cp;
                mensaje_documento.Text = "Cantidad de números 8";
                _is_dni = true;
                this.lblCheckSunatReniec.Text = "RENIEC";


            }
            txt_codigo_comprobante.Text = cp;
            txt_titulo_comprobante.Text = comboBox1.Text;

            setearFechaDiasporCpe(_is_dni);
        }

 
        private void detalleProductTable_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {


        }

        private void detalleProductTable_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {

        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {


            try
            {
                int indice = detalleProductTable.CurrentCell.RowIndex;


                detalleProductTable.Rows.RemoveAt(indice);

                int cantidad_prod = detalleProductTable.Rows.Count - 1;
                cantidad_productos.Text = "| Cantidad de productos: " + cantidad_prod;
                decimal subtotal = 0;
                decimal total = 0;
                decimal igv = 0;

                for (var i = 0; i < detalleProductTable.Rows.Count - 1; i++)
                {
                    subtotal = subtotal + Convert.ToDecimal(detalleProductTable.Rows[i].Cells[7].Value.ToString());
                    total = total + Convert.ToInt32(detalleProductTable.Rows[i].Cells[2].Value.ToString()) * Convert.ToDecimal(detalleProductTable.Rows[i].Cells[4].Value.ToString());
                }

                igv = decimal.Round((total / Convert.ToDecimal(1.18) * Convert.ToDecimal(0.18)), 2); ;


                txt_subtotal.Text = subtotal.ToString();
                txt_igv.Text = igv.ToString();
                txt_total.Text = total.ToString();

                _totalComparar = total;

                aplicarDescuentoGlobal();


                txt_cantidad.Text = "";
                this.cmbUnidad.SelectedValue = "NIU";
                this.cmbUnidad.Enabled = true;
                _unidad_medida = "";
                
                txt_codigo.Text = "";
                txt_producto.Text = "";
                txt_precio.Text = "";
                txt_descuento.Text = "0";
                txt_cantidad.Text = "";
                txt_id_row.Text = "";
                chk_editar_precio.Checked = false;
                txt_precio.ReadOnly = false;
            }
            catch (Exception ex)
            {

                MessageBox.Show("La fila seleccionada no contiene un producto");

            }

        }





        private void lbl_descripcion_obligatorios_Click(object sender, EventArgs e)
        {

        }

     

        private void txt_documento_KeyUp(object sender, KeyEventArgs e)
        {
      
            if (txt_documento.Text.Trim().Length != 0)
            {
                mensaje_documento.Text = Convert.ToString(txt_documento.Text.Trim().Length);
            }

            if (e.KeyCode == Keys.Enter)
            {
                if (txt_documento.Text.Trim().Length == 0)
                {

                    MessageBox.Show("Debe ingresar documento del cliente");
                    return;

                }

                else if (comboBox1.SelectedValue.ToString() == "3" && txt_documento.Text.Trim().Length < 8)
                {
                    txt_documento.Focus();
                    MessageBox.Show("El dni debe tener 8 digitos");
                    return;
                }
                else if (comboBox1.SelectedValue.ToString() == "1" && txt_documento.Text.Trim().Length < 11)
                {
                    txt_documento.Focus();
                    MessageBox.Show("El ruc debe tener 11 digitos");
                    return;
                }

                buscarDocumentoDniRuc();
            }
        }

        private void setearFechaDiasporCpe(bool dni)
        {
            int diasAtras;
            int diasPosteriores;
            if (dni)
            {
                diasAtras = _dias_antes_boleta;
                diasPosteriores = _dias_despues_boleta;

            }
            else
            {
                diasAtras = Convert.ToInt32(dias.Split(',')[0]);
                diasPosteriores = Convert.ToInt32(dias.Split(',')[1]);
            }

            try
            {
                //diasAtras = Convert.ToInt32(dias.Split(',')[0]);

                if (diasAtras <= 0)
                {
                    diasAtras = 1;
                }
                //else
                //{
                //    diasAtras--;
                //}
            }
            catch
            {
                diasAtras = 1;
            }

            try
            {
                //diasPosteriores = Convert.ToInt32(dias.Split(',')[1]);

                if (diasPosteriores <= 0)
                {
                    diasPosteriores = 0;
                }
                //else
                //{
                //    diasPosteriores--;
                //}
            }
            catch
            {
                diasPosteriores = 0;
            }

            txt_fecha.MaxDate = DateTime.Today.AddDays(diasPosteriores);
            txt_fecha.MinDate = DateTime.Today.AddDays(-diasAtras);
        }
        
        private void dashboard_Shown(object sender, EventArgs e)
        {
   
            LlenarCombo();
        
            int diasAtras;
            int diasPosteriores;

            try
            {
                diasAtras = Convert.ToInt32(dias.Split(',')[0]);

                if (diasAtras <= 0)
                {
                    diasAtras = 1;
                }
                //else
                //{
                //    diasAtras--;
                //}
            }
            catch
            {
                diasAtras = 1;
            }

            try
            {
                diasPosteriores = Convert.ToInt32(dias.Split(',')[1]);

                if (diasPosteriores <= 0)
                {
                    diasPosteriores = 0;
                }
                //else
                //{
                //    diasPosteriores--;
                //}
            }
            catch
            {
                diasPosteriores = 0;
            }

            txt_fecha.MaxDate = DateTime.Today.AddDays(diasPosteriores);
            txt_fecha.MinDate = DateTime.Today.AddDays(-diasAtras);

            string CodigoComprobante = cln.codigoBoleta(CENConstantes.FACTURA);
            txt_codigo_comprobante.Text = CodigoComprobante;
            txt_codigoBoleta.Text = clnConcepto.ObtenerValorParametro(3) + "-" + CodigoComprobante;

            txt_documento.MaxLength = 11;
            txt_producto.MaxLength = 250;
            txt_codigo.MaxLength = 10;
            txtDireccionCliente.MaxLength = 100;
            mensaje_documento.Text = "Cantidad de números 11";
            txt_descuento.Text = "0";
            this._is_dni = false;
            this.lblCheckSunatReniec.Text = "SUNAT";

            if (activoBuscarSunatReniec == "1") this.txt_documento.Focus(); else this.txtCliente.Focus();
            txt_titulo_comprobante.Text = CENConstantes.FACTURA_ELECTRONICA;
            detalleProductTable.DataSource = null;

            detalleProductTable.ColumnCount = 9;
            detalleProductTable.ColumnHeadersDefaultCellStyle.BackColor = Color.Navy;
            detalleProductTable.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            detalleProductTable.ColumnHeadersDefaultCellStyle.Font =
                new Font(detalleProductTable.Font, FontStyle.Bold);

            detalleProductTable.Name = "detalleProductTable";
            detalleProductTable.Location = new Point(8, 8);
            detalleProductTable.Size = new Size(950, 250);
            detalleProductTable.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            detalleProductTable.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            detalleProductTable.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            detalleProductTable.GridColor = Color.Black;
            detalleProductTable.RowHeadersVisible = false;

            detalleProductTable.Columns[0].Name = "Cod. Prod";
            detalleProductTable.Columns[1].Name = "Descripción";
            detalleProductTable.Columns[2].Name = "Cantidad";
            detalleProductTable.Columns[3].Name = "Un. Medida";
            detalleProductTable.Columns[4].Name = "Prec. Unitario";
            detalleProductTable.Columns[5].Name = "Desc.";
            detalleProductTable.Columns[6].Name = "IGV";
            detalleProductTable.Columns[7].Name = "Valor Item";
            detalleProductTable.Columns[8].Name = "Cod Sunat";
            detalleProductTable.Columns[8].Visible = false;
            detalleProductTable.Columns[7].DefaultCellStyle.Font = new Font(detalleProductTable.DefaultCellStyle.Font, FontStyle.Italic);
            detalleProductTable.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            detalleProductTable.MultiSelect = false;
            detalleProductTable.Dock = DockStyle.Fill;

            foreach (DataGridViewColumn Col in detalleProductTable.Columns)
            {
                Col.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }

       
        public void showAgregarProductos()
        {
     
            productos list = new productos();
            list.StartPosition = FormStartPosition.CenterParent;
            list.FormBorderStyle = FormBorderStyle.FixedDialog;
            
            AddOwnedForm(list);
            list.ShowDialog();
           
        }
        public void showBuscarProductos()
        {
            lbl_mensaje.Text = "";
            chk_editar_precio.Checked = false;

            ListadoProductos list = new ListadoProductos();
            AddOwnedForm(list);
            list.ShowDialog();
            this.txt_cantidad.Focus();
            if(_unidad_medida=="")
            {

                this.cmbUnidad.SelectedValue = "NIU";
                this.cmbUnidad.Enabled = true;
            }
            else
            {
                this.cmbUnidad.SelectedValue = _unidad_medida.ToString();
                this.cmbUnidad.Enabled = false;
            }
         

        }
        private void btn_link_products_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.showBuscarProductos();
        }

        private void chk_editar_precio_CheckedChanged(object sender, EventArgs e)
        {


            if (this.chk_editar_precio.Checked)
            {
                txt_precio.ReadOnly = false;
                txt_precio.Focus();
            }
            else
            {
                txt_precio.ReadOnly = true;
                txt_cantidad.Focus();
            }
        }

        private void llenarTablaDetalle(int cantidad_prod, string codigo, string codigo_sunat, string producto, decimal cantidad, string unidadMedida, decimal descuento, decimal precio)
        {

            decimal subtotal = 0, total = 0, igv;
            if (cantidad_prod < 15)
            {
                cantidad_productos.Text = "| Cantidad de productos: " + cantidad_prod;

                int table = detalleProductTable.Rows.Add();

                detalleProductTable.Rows[table].Cells[0].Value = codigo;
                detalleProductTable.Rows[table].Cells[1].Value = producto;
                detalleProductTable.Rows[table].Cells[2].Value = cantidad;
                detalleProductTable.Rows[table].Cells[3].Value = unidadMedida;
                detalleProductTable.Rows[table].Cells[5].Value = descuento;
                detalleProductTable.Rows[table].Cells[4].Value = precio;

                detalleProductTable.Rows[table].Cells[8].Value = codigo_sunat.ToString();



                decimal igv_item = decimal.Round(((cantidad * precio) / Convert.ToDecimal(1.18) * Convert.ToDecimal(0.18)), 2);
                //decimal igv_item = decimal.Round(((cantidad * precio) / Convert.ToDecimal(1.18) * Convert.ToDecimal(0.18)), 6);


                detalleProductTable.Rows[table].Cells[6].Value = igv_item;

                decimal valor_item = Convert.ToDecimal((precio * cantidad) - descuento - igv_item);

                detalleProductTable.Rows[table].Cells[7].Value = decimal.Round(valor_item, 2);
                //detalleProductTable.Rows[table].Cells[7].Value = decimal.Round(valor_item, 6);

                detalleProductTable.Rows[table].DefaultCellStyle.ForeColor = Color.Black;


                for (var i = 0; i < detalleProductTable.Rows.Count - 1; i++)
                {
                    subtotal = subtotal + Convert.ToDecimal(detalleProductTable.Rows[i].Cells[7].Value.ToString());
                    total = total + Convert.ToDecimal(detalleProductTable.Rows[i].Cells[2].Value.ToString()) * Convert.ToDecimal(detalleProductTable.Rows[i].Cells[4].Value.ToString())- descuento;
                }

                igv = decimal.Round((total / Convert.ToDecimal(1.18) * Convert.ToDecimal(0.18)), 2);

                txt_subtotal.Text = subtotal.ToString();
                txt_igv.Text = igv.ToString();
                txt_total.Text = total.ToString();

                _totalComparar = total;

                aplicarDescuentoGlobal();


                txt_producto.Text = "";
                txt_codigo.Text = "";
                txt_producto.Text = "";
                txt_precio.Text = "";
                txt_cantidad.Text = "";
                txt_descuento.Text = "0";
                this.cmbUnidad.SelectedValue = "NIU";
                _unidad_medida = "";

                txt_mensaje.Text = "";
                txt_producto.Focus();
                txt_codigo.ReadOnly = false;
                chk_editar_precio.Checked = false;
                txt_producto.ReadOnly = false;
                _busquedaForm = false;
                txt_precio.ReadOnly = false;
            }
            else
            {
                MessageBox.Show("Solo puede agregar 14 productos como máximo");
            }
        }

        private void dashboard_KeyUp(object sender, KeyEventArgs e)
        {
       
            if(e.KeyCode == Keys.F9)
            {
                this.showAgregarProductos();
            }
          
            if (e.KeyCode == Keys.F10)
            {
                this.showBuscarProductos();
            }

            if (e.KeyCode == Keys.F11)
            {
                this.btn_agragr_producto_Click(sender, e);
            }

            if (e.KeyCode == Keys.F12)
            {
                this.button2_Click(sender, e);
            }
        }

        public void buscarDocumentoDniRuc()
        {
            CLNCliente clnCliente = new CLNCliente();
            CENDataCliente cliente = new CENDataCliente();
            bool existeCliente;
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                
                if (this.chkSunatReniec.Checked == true)
                {
                    //Buscamos por Api
                    //if (_is_dni) this.buscarxDni(); else this.buscarxRuc();

                    existeCliente = clnCliente.existeCliente(this.txt_documento.Text);
                    if (existeCliente)
                    {
                        //extrae Datos
                        cliente = clnCliente.getCliente(this.txt_documento.Text);
                        this.txtCliente.Text = cliente.nombresCompletos;
                        this.txtDireccionCliente.Text = cliente.direccion;
                        _has_data_bd = true;
                        if (_is_dni) this.buscarxDni(); else this.buscarxRuc();



                    }
                    else
                    {
                        //cargo del api
                        if (_is_dni) this.buscarxDni(); else this.buscarxRuc();
                    }

                }
                else
                {
                    existeCliente = clnCliente.existeCliente(this.txt_documento.Text);
                    if (existeCliente)
                    {
                        //extrae Datos
                        cliente = clnCliente.getCliente(this.txt_documento.Text);
                        this.txtCliente.Text = cliente.nombresCompletos;
                        this.txtDireccionCliente.Text = cliente.direccion;

                    }
                    else
                    {
                        MessageBox.Show("No se encontraron datos en la base de datos");
                    }
                }

            }
           

            catch (Exception ex)
            {

                aviso aviso_form = new aviso("warning", "ERROR", ex.Message, false);
                aviso_form.ShowDialog();
                return;
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }


        }
        private void guardarClienteAfterSave()
        {
            CENDataCliente cliente ;
            CLNCliente clnCliente = new CLNCliente();
            bool existeCliente;


            try
            {
             
                existeCliente = clnCliente.existeCliente(this.txt_documento.Text);
                if (!existeCliente)
                {
                    cliente = new CENDataCliente();
                    cliente.tipo_doc = (_is_dni == true) ? "6" : "1";
                    cliente.num_doc = this.txt_documento.Text;
                    cliente.nombresCompletos = this.txtCliente.Text;
                    cliente.direccion = this.txtDireccionCliente.Text;
                    //si no existe se inserta
                    clnCliente.insertClients(cliente);

                }
                else
                {
                    cliente = new CENDataCliente();

                    CENDataCliente clienteDB = new CENDataCliente();
                    clienteDB = clnCliente.getCliente(this.txt_documento.Text);

                    if(clienteDB.nombresCompletos.Trim() != this.txtCliente.Text.Trim() || clienteDB.direccion.Trim() != this.txtDireccionCliente.Text.Trim())
                    {
                        DialogResult result = MessageBox.Show("Datos del cliente diferente a base de datos \n ¿Dese guardar los cambios del cliente?", "Mensaje", MessageBoxButtons.YesNo);
                        if (result == DialogResult.Yes)
                        {
                            cliente.tipo_doc = (_is_dni == true) ? "6" : "1";
                            cliente.num_doc = this.txt_documento.Text;
                            cliente.nombresCompletos = this.txtCliente.Text;
                            cliente.direccion = this.txtDireccionCliente.Text;

                            clnCliente.updateClients(cliente);
                            
                        }

                    }

                   

                    
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }
        private void insertarCliente(CENDataCliente cliente)
        {
            CLNCliente clnCliente = new CLNCliente();
            CENDataCliente dataCliente = new CENDataCliente();
            bool existeCliente;
            bool updateCliente = false;
            string  mensaje = "";
            DialogResult diagRes;
            //buscamos si existe cliente en BD
            try
            {
                existeCliente = clnCliente.existeCliente(this.txt_documento.Text);
                if (!existeCliente)
                {
                    //si no existe se inserta
                    clnCliente.insertClients(cliente);

                }
              
            }
            catch (Exception ex)
            {

                throw ex;
            }
           
        }
        public void buscarxDni()
        {
           
           
            CENDataCliente dataCliente = new CENDataCliente();
            CENDataCliente cliente = new CENDataCliente();
            string mensaje = "";
            this.txtDireccionCliente.Focus();

            try
            {
                dataCliente.success = false;
                //dataCliente = await funciones.BuscarDniAsync(this.txt_documento.Text);
                dataCliente = funciones.Consultar(this.txt_documento.Text);

                if (dataCliente.success == true)
                {
                   

                    if(_has_data_bd)
                    {
                       
                        if (dataCliente.nombresCompletos != this.txtCliente.Text.Trim())
                        {

                            mensaje = mensaje + "[DNI/RUC]: " + this.txt_documento.Text.Trim() + " \n";
                            mensaje = mensaje + "[NOMBRES]: " + dataCliente.nombresCompletos.Trim() + " \n";
                            mensaje = mensaje + "[DIRECCION]: " + dataCliente.direccion + " \n";

                            DialogResult result = MessageBox.Show(mensaje, "¿Cargar datos de la web RENIEC/SUNAT?", MessageBoxButtons.YesNo);
                            if (result == DialogResult.Yes)
                            {
                                this.txtCliente.Text = dataCliente.nombresCompletos.Trim();

                            }

                        }

                     
                    }
                    else
                    {
                        this.txtCliente.Text = dataCliente.nombresCompletos.Trim(); 
                        _api_result = true;

                        cliente.tipo_doc = "1";
                        cliente.num_doc = this.txt_documento.Text.Trim();
                        cliente.nombresCompletos = dataCliente.nombresCompletos.Trim();
                        cliente.direccion = dataCliente.direccion.Trim();
                        insertarCliente(cliente);
                    }

                }
                else
                {
                    _api_result = false;
     
                    aviso aviso_form = new aviso("warning", "El DNI " + this.txt_documento.Text + " no existe", dataCliente.mensaje, false);
                    aviso_form.ShowDialog();

                }
            }
            catch (Exception ex)
            {

                aviso aviso_form = new aviso("warning", "ERROR", "REVISE SU CONEXION A INTERNET \n" + ex.Message, false);
                aviso_form.ShowDialog();

                CLNCliente clnCliente = new CLNCliente();
                bool existeCliente;

                existeCliente = clnCliente.existeCliente(this.txt_documento.Text);
                if (existeCliente)
                {
                    //extrae Datos
                    cliente = clnCliente.getCliente(this.txt_documento.Text);
                    this.txtCliente.Text = cliente.nombresCompletos;
                    this.txtDireccionCliente.Text = cliente.direccion;
                    MessageBox.Show("Datos extraídos de la base de datos");

                }
                else
                {
                    MessageBox.Show("No se encontraron datos en la base de datos");
                }

            }
           
          

        }

        public async void buscarxRuc()
        {
         
            CENSunat dataCliente = new CENSunat();
            CENDataCliente cliente = new CENDataCliente();
            string mensaje="";
            this.txtDireccionCliente.Focus();

            try
            {
                dataCliente.TipoRespuesta = 0;
                dataCliente = await funciones.ConsultarRUC(this.txt_documento.Text);


                if (dataCliente.TipoRespuesta == 1)
                {
                   
                    if (_has_data_bd)
                    {

                        if (dataCliente.RazonSocial.Trim() != this.txtCliente.Text.Trim() || dataCliente.DomicilioFiscal.Trim() != this.txtDireccionCliente.Text.Trim())
                        {

                            mensaje = mensaje + "[DNI/RUC]: " + this.txt_documento.Text.Trim() + " \n";
                            mensaje = mensaje + "[NOMBRES]: " + dataCliente.RazonSocial.Trim() + " \n";
                            mensaje = mensaje + "[DIRECCION]: " + dataCliente.DomicilioFiscal + " \n";

                            DialogResult result = MessageBox.Show(mensaje, "¿Cargar datos de la web RENIEC/SUNAT?", MessageBoxButtons.YesNo);
                            if (result == DialogResult.Yes)
                            {
                                this.txtCliente.Text = dataCliente.RazonSocial;
                                this.txtDireccionCliente.Text = dataCliente.DomicilioFiscal;
                            }



                        }


                


                    }
                    else
                    {
                        this.txtCliente.Text = dataCliente.RazonSocial;
                        this.txtDireccionCliente.Text = dataCliente.DomicilioFiscal;
                        _api_result = true;

                        cliente.tipo_doc = "6";
                        cliente.num_doc = this.txt_documento.Text.Trim();
                        cliente.nombresCompletos = dataCliente.RazonSocial.Trim();
                        cliente.direccion = dataCliente.DomicilioFiscal.Trim();
                        insertarCliente(cliente);
                    }
                }
                else
                {
                    _api_result = false;
                    aviso aviso_form = new aviso("warning", "ERROR", dataCliente.MensajeRespuesta, false);
                    aviso_form.ShowDialog();
                }
            }
            catch (Exception ex)
            {

                aviso aviso_form = new aviso("warning", "ERROR","REVISE SU CONEXION A INTERNET \n" + ex.Message, false);
                aviso_form.ShowDialog();

                CLNCliente clnCliente = new CLNCliente();
                bool existeCliente;

                existeCliente = clnCliente.existeCliente(this.txt_documento.Text);
                if (existeCliente)
                {
                    //extrae Datos
                    cliente = clnCliente.getCliente(this.txt_documento.Text);
                    this.txtCliente.Text = cliente.nombresCompletos;
                    this.txtDireccionCliente.Text = cliente.direccion;
                    MessageBox.Show("Datos extraídos de la base de datos");

                }
                else
                {
                    MessageBox.Show("No se encontraron datos en la base de datos");
                }


            }

            
           

        }

     

        private void chkSunatReniec_CheckedChanged(object sender, EventArgs e)
        {
            CLNConcepto concepto = new CLNConcepto();
            if (this.chkSunatReniec.Checked == true)
            {
                this.txt_documento.Focus();
                concepto.actualizarCheckSunatReniec(1);
            }
            else
            {
                //this.txtCliente.Focus();
                this.txt_documento.Focus();
                concepto.actualizarCheckSunatReniec(0);
            }
        }

        private void chkDescuento_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chkDescuento.Checked)
            {
                this.txt_descuento.ReadOnly = false;
                this.txt_descuento.Focus();
            }
            else
            {
                this.txt_descuento.ReadOnly = true;
                this.txt_descuento.Focus();
            }
        }

        private void chkDescuentoGlobal_CheckedChanged(object sender, EventArgs e)
        {
         

            if (this.chkDescuentoGlobal.Checked)
            {
                this.txt_descuentoGlobal.ReadOnly = false;
                this.txt_descuentoGlobal.Focus();
            }
            else
            {
                this.txt_descuentoGlobal.Text = "0";
                this.txt_descuentoGlobal.ReadOnly = true;
                aplicarDescuentoGlobal();
            }
        }

        private void aplicarDescuentoGlobal()
        {
            decimal descuentoGravado = 0, igvDescGrav = 0, descuento = 0, subtotal = 0, total = 0, igv = 0;
            decimal subtotalG = 0, totalG = 0, igvG = 0, txtDescGlobal = 0;

            if (this.txt_descuentoGlobal.Text.Trim() == ".")
            {
                this.txt_descuentoGlobal.Text = "0";
                this.txt_descuentoGlobal.Focus();
                this.txt_descuentoGlobal.SelectAll();
                MessageBox.Show("Ingrese monto mayor a CERO");
                return;
            }
            if (this.txt_descuentoGlobal.Text == "")
            {
                txtDescGlobal = 0;
                this.txt_descuentoGlobal.Text = "0";
                this.txt_descuentoGlobal.Focus();
                this.txt_descuentoGlobal.SelectAll();
            }
            else if(Convert.ToDecimal(this.txt_descuentoGlobal.Text)>= 0)
            {
                if (Convert.ToDecimal(this.txt_descuentoGlobal.Text) > (Convert.ToDecimal(_totalComparar) - Convert.ToDecimal(1)))
                {
                    MessageBox.Show("Descuento no puede exceder al total de la venta");
                    txtDescGlobal = 0;
                    this.txt_descuentoGlobal.Text = "0";
                    this.txt_descuentoGlobal.Focus();
                    this.txt_descuentoGlobal.SelectAll();
                    return;
                }
                else
                {
                    txtDescGlobal = Convert.ToDecimal(this.txt_descuentoGlobal.Text);
                    descuentoGravado = Convert.ToDecimal(this.txt_descuentoGlobal.Text) / Convert.ToDecimal(1.18);

                    igvDescGrav = Convert.ToDecimal(descuentoGravado) * Convert.ToDecimal(0.18);
                }


                for (var i = 0; i < detalleProductTable.Rows.Count - 1; i++)
                {
                    descuento = Convert.ToDecimal(detalleProductTable.Rows[i].Cells[5].Value.ToString());

                    subtotal = subtotal + Convert.ToDecimal(detalleProductTable.Rows[i].Cells[7].Value.ToString());
                    total = total + Convert.ToDecimal(detalleProductTable.Rows[i].Cells[2].Value.ToString()) * Convert.ToDecimal(detalleProductTable.Rows[i].Cells[4].Value.ToString()) - descuento;
                }

                subtotalG = Math.Round(subtotal, 2) - Math.Round(descuentoGravado, 2);
                igvG = Math.Round(((total - subtotal) - igvDescGrav), 2);
                totalG = Math.Round((subtotalG + igvG), 2);


                //totalG = total - Convert.ToDecimal(txtDescGlobal);

                //igvG = decimal.Round((totalG / Convert.ToDecimal(1.18) * Convert.ToDecimal(0.18)), 2); ;
                //subtotalG = totalG - igvG;




                txt_subtotal.Text = subtotalG.ToString();
                txt_igv.Text = igvG.ToString();
                txt_total.Text = totalG.ToString();

            }

           
        }

        private void aplicarDescuentoGlobal_bk()
        {
            decimal descuento = 0, subtotal = 0, total = 0, igv = 0;
            decimal subtotalG = 0, totalG = 0, igvG = 0, txtDescGlobal = 0;

            if (this.txt_descuentoGlobal.Text == "")
            {
                txtDescGlobal = 0;
                this.txt_descuentoGlobal.Text = "0";
                this.txt_descuentoGlobal.Focus();
                this.txt_descuentoGlobal.SelectAll();
            }
            else if (Convert.ToDecimal(this.txt_descuentoGlobal.Text) > Convert.ToDecimal(_totalComparar))
            {
                MessageBox.Show("Descuento no puede exceder al total de la venta");
                txtDescGlobal = 0;
                this.txt_descuentoGlobal.Text = "0";
                this.txt_descuentoGlobal.Focus();
                this.txt_descuentoGlobal.SelectAll();
                return;
            }
            else
            {
                txtDescGlobal = Convert.ToDecimal(this.txt_descuentoGlobal.Text);
               
            }


            for (var i = 0; i < detalleProductTable.Rows.Count - 1; i++)
            {
                descuento = Convert.ToDecimal(detalleProductTable.Rows[i].Cells[5].Value.ToString());

                subtotal = subtotal + Convert.ToDecimal(detalleProductTable.Rows[i].Cells[7].Value.ToString());
                total = total + Convert.ToDecimal(detalleProductTable.Rows[i].Cells[2].Value.ToString()) * Convert.ToDecimal(detalleProductTable.Rows[i].Cells[4].Value.ToString()) - descuento;
            }


            totalG = total - Convert.ToDecimal(txtDescGlobal);

            igvG = decimal.Round((totalG / Convert.ToDecimal(1.18) * Convert.ToDecimal(0.18)), 2); ;
            subtotalG = totalG - igvG;




            txt_subtotal.Text = subtotalG.ToString();
            txt_igv.Text = igvG.ToString();
            txt_total.Text = totalG.ToString();
        }
        private void txt_descuentoGlobal_TextChanged(object sender, EventArgs e)
        {

            aplicarDescuentoGlobal();
        }

        private void detalleProductTable_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Delete)
            {

            }
        }

        private void txt_descuentoGlobal_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!char.IsControl(e.KeyChar))  //bypass control keys
            {
                int dotIndex = txt_descuentoGlobal.Text.IndexOf('.');
                if (char.IsDigit(e.KeyChar))     //ensure it's a digit
                {   //we cannot accept another digit if
                    if (dotIndex != -1 &&  //there is already a dot and
                                           //dot is to the left from the cursor position and
                        dotIndex < txt_descuentoGlobal.SelectionStart &&
                        //there're already 2 symbols to the right from the dot
                        txt_descuentoGlobal.Text.Substring(dotIndex + 1).Length >= 2)
                    {
                        e.Handled = true;
                    }
                }
                else //we cannot accept this char if
                    e.Handled = e.KeyChar != '.' || //it's not a dot or
                                                    //there is already a dot in the text or
                                dotIndex != -1 ||
                                //text is empty or
                                txt_descuentoGlobal.Text.Length == 0 ||
                                //there are more than 2 symbols from cursor position
                                //to the end of the text
                                txt_descuentoGlobal.SelectionStart + 2 < txt_descuentoGlobal.Text.Length;
            }



        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            ListadoClientes list = new ListadoClientes();
            AddOwnedForm(list);
            list.ShowDialog();
            mensaje_documento.Text = Convert.ToString(txt_documento.Text.Trim().Length);
            if (this.txt_documento.Text==CENConstantes.DNI_DEFAULT)
            {
                this.txtCliente.Enabled = false;
                this.txt_documento.Enabled = false;
            }
            else
            {
                this.txtCliente.Enabled = true;
                this.txt_documento.Enabled = true;
            }

         
            this.txt_producto.Focus();
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            this.pictureBox1.Cursor = Cursors.Hand;
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            this.pictureBox1.Cursor = Cursors.Default;
        }

        private void txt_producto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.ToString() == "|"
            || e.KeyChar.ToString() == "'")
            {
                e.Handled = true;
                return;
            }
        }

        private void txtDireccionCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.ToString() == "|")
            {
                e.Handled = true;
                return;
            }
        }

        private void txtCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.ToString() == "|")
            {
                e.Handled = true;
                return;
            }
        }
    }
}
