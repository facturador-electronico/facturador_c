﻿
namespace ferreteriaApp
{
    partial class reporteContador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.detalleLista = new System.Windows.Forms.DataGridView();
            this.panel_cabecera = new System.Windows.Forms.Panel();
            this.cmbTipoComprobante = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.labelUbicacion = new System.Windows.Forms.Label();
            this.txt_fecha = new System.Windows.Forms.DateTimePicker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.lbl_pagina = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_totalPaginas = new System.Windows.Forms.Label();
            this.btn_preview = new System.Windows.Forms.Button();
            this.bnt_next = new System.Windows.Forms.Button();
            this.panel_body = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.listaToExcel = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.detalleLista)).BeginInit();
            this.panel_cabecera.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel_body.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listaToExcel)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // detalleLista
            // 
            this.detalleLista.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.detalleLista.BackgroundColor = System.Drawing.Color.White;
            this.detalleLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.detalleLista.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detalleLista.Location = new System.Drawing.Point(0, 0);
            this.detalleLista.Name = "detalleLista";
            this.detalleLista.Size = new System.Drawing.Size(1050, 393);
            this.detalleLista.TabIndex = 8;
            // 
            // panel_cabecera
            // 
            this.panel_cabecera.AutoSize = true;
            this.panel_cabecera.BackColor = System.Drawing.Color.GhostWhite;
            this.panel_cabecera.Controls.Add(this.cmbTipoComprobante);
            this.panel_cabecera.Controls.Add(this.label3);
            this.panel_cabecera.Controls.Add(this.button4);
            this.panel_cabecera.Controls.Add(this.label1);
            this.panel_cabecera.Controls.Add(this.labelUbicacion);
            this.panel_cabecera.Controls.Add(this.txt_fecha);
            this.panel_cabecera.Controls.Add(this.panel2);
            this.panel_cabecera.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_cabecera.Location = new System.Drawing.Point(0, 0);
            this.panel_cabecera.Name = "panel_cabecera";
            this.panel_cabecera.Size = new System.Drawing.Size(1050, 165);
            this.panel_cabecera.TabIndex = 44;
            // 
            // cmbTipoComprobante
            // 
            this.cmbTipoComprobante.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbTipoComprobante.FormattingEnabled = true;
            this.cmbTipoComprobante.Location = new System.Drawing.Point(765, 91);
            this.cmbTipoComprobante.Name = "cmbTipoComprobante";
            this.cmbTipoComprobante.Size = new System.Drawing.Size(255, 21);
            this.cmbTipoComprobante.TabIndex = 73;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.DimGray;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(572, 90);
            this.label3.MaximumSize = new System.Drawing.Size(150, 20);
            this.label3.MinimumSize = new System.Drawing.Size(150, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(150, 20);
            this.label3.TabIndex = 72;
            this.label3.Text = "Tipo Comprobante";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Image = global::ferreteriaApp.Properties.Resources.carpeta32;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(917, 123);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(103, 39);
            this.button4.TabIndex = 68;
            this.button4.Text = "Exportar";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.DimGray;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(572, 56);
            this.label1.MaximumSize = new System.Drawing.Size(250, 20);
            this.label1.MinimumSize = new System.Drawing.Size(150, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 20);
            this.label1.TabIndex = 53;
            this.label1.Text = "Fecha - Mes";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelUbicacion
            // 
            this.labelUbicacion.AutoSize = true;
            this.labelUbicacion.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelUbicacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUbicacion.ForeColor = System.Drawing.Color.DimGray;
            this.labelUbicacion.Location = new System.Drawing.Point(0, 152);
            this.labelUbicacion.Margin = new System.Windows.Forms.Padding(3, 0, 20, 0);
            this.labelUbicacion.Name = "labelUbicacion";
            this.labelUbicacion.Size = new System.Drawing.Size(64, 13);
            this.labelUbicacion.TabIndex = 52;
            this.labelUbicacion.Text = "Ubicación";
            // 
            // txt_fecha
            // 
            this.txt_fecha.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_fecha.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.txt_fecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txt_fecha.Location = new System.Drawing.Point(765, 54);
            this.txt_fecha.Name = "txt_fecha";
            this.txt_fecha.Size = new System.Drawing.Size(255, 20);
            this.txt_fecha.TabIndex = 51;
            this.txt_fecha.Tag = "";
            // 
            // panel2
            // 
            this.panel2.AutoSize = true;
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1050, 44);
            this.panel2.TabIndex = 50;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.MediumBlue;
            this.label24.Location = new System.Drawing.Point(49, 22);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(131, 16);
            this.label24.TabIndex = 0;
            this.label24.Text = "Reporte Contador";
            // 
            // label22
            // 
            this.label22.Image = global::ferreteriaApp.Properties.Resources.icon_edit_note_321;
            this.label22.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label22.Location = new System.Drawing.Point(6, 13);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(37, 31);
            this.label22.TabIndex = 20;
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(11, 0);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(34, 13);
            this.lblError.TabIndex = 48;
            this.lblError.Text = "Error";
            // 
            // lbl_pagina
            // 
            this.lbl_pagina.AutoSize = true;
            this.lbl_pagina.Location = new System.Drawing.Point(121, 24);
            this.lbl_pagina.Name = "lbl_pagina";
            this.lbl_pagina.Size = new System.Drawing.Size(13, 13);
            this.lbl_pagina.TabIndex = 45;
            this.lbl_pagina.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(148, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(12, 13);
            this.label2.TabIndex = 46;
            this.label2.Text = "/";
            // 
            // lbl_totalPaginas
            // 
            this.lbl_totalPaginas.AutoSize = true;
            this.lbl_totalPaginas.Location = new System.Drawing.Point(174, 24);
            this.lbl_totalPaginas.Name = "lbl_totalPaginas";
            this.lbl_totalPaginas.Size = new System.Drawing.Size(13, 13);
            this.lbl_totalPaginas.TabIndex = 47;
            this.lbl_totalPaginas.Text = "0";
            // 
            // btn_preview
            // 
            this.btn_preview.AutoEllipsis = true;
            this.btn_preview.BackColor = System.Drawing.Color.DodgerBlue;
            this.btn_preview.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_preview.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_preview.ForeColor = System.Drawing.Color.White;
            this.btn_preview.Location = new System.Drawing.Point(69, 18);
            this.btn_preview.Name = "btn_preview";
            this.btn_preview.Size = new System.Drawing.Size(26, 25);
            this.btn_preview.TabIndex = 48;
            this.btn_preview.Text = "<";
            this.btn_preview.UseVisualStyleBackColor = false;
            // 
            // bnt_next
            // 
            this.bnt_next.AutoEllipsis = true;
            this.bnt_next.BackColor = System.Drawing.Color.DodgerBlue;
            this.bnt_next.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bnt_next.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bnt_next.ForeColor = System.Drawing.Color.White;
            this.bnt_next.Location = new System.Drawing.Point(204, 17);
            this.bnt_next.Name = "bnt_next";
            this.bnt_next.Size = new System.Drawing.Size(26, 25);
            this.bnt_next.TabIndex = 49;
            this.bnt_next.Text = ">";
            this.bnt_next.UseVisualStyleBackColor = false;
            // 
            // panel_body
            // 
            this.panel_body.Controls.Add(this.panel6);
            this.panel_body.Controls.Add(this.panel5);
            this.panel_body.Controls.Add(this.panel4);
            this.panel_body.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_body.Location = new System.Drawing.Point(0, 165);
            this.panel_body.Name = "panel_body";
            this.panel_body.Size = new System.Drawing.Size(1050, 421);
            this.panel_body.TabIndex = 50;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.listaToExcel);
            this.panel6.Controls.Add(this.panel3);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 358);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1050, 63);
            this.panel6.TabIndex = 53;
            // 
            // listaToExcel
            // 
            this.listaToExcel.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.listaToExcel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listaToExcel.Location = new System.Drawing.Point(14, 17);
            this.listaToExcel.Name = "listaToExcel";
            this.listaToExcel.ReadOnly = true;
            this.listaToExcel.Size = new System.Drawing.Size(57, 30);
            this.listaToExcel.TabIndex = 76;
            this.listaToExcel.Visible = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btn_preview);
            this.panel3.Controls.Add(this.lbl_pagina);
            this.panel3.Controls.Add(this.bnt_next);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.lbl_totalPaginas);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(781, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(269, 63);
            this.panel3.TabIndex = 51;
            this.panel3.Visible = false;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.detalleLista);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 28);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1050, 393);
            this.panel5.TabIndex = 52;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lblError);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1050, 28);
            this.panel4.TabIndex = 49;
            // 
            // reporteContador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1050, 586);
            this.Controls.Add(this.panel_body);
            this.Controls.Add(this.panel_cabecera);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "reporteContador";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Buscador de Venta";
            ((System.ComponentModel.ISupportInitialize)(this.detalleLista)).EndInit();
            this.panel_cabecera.ResumeLayout(false);
            this.panel_cabecera.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel_body.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listaToExcel)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView detalleLista;
        private System.Windows.Forms.Panel panel_cabecera;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label lbl_pagina;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_totalPaginas;
        private System.Windows.Forms.Button btn_preview;
        private System.Windows.Forms.Button bnt_next;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel_body;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.DateTimePicker txt_fecha;
        private System.Windows.Forms.Label labelUbicacion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ComboBox cmbTipoComprobante;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView listaToExcel;
    }
}