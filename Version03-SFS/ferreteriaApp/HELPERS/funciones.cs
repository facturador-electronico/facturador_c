﻿
using ferreteriaApp.CEN;
using ferreteriaApp.DAO;
using System;

using System.IO;
using System.Xml;
using ferreteriaApp.CLN;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace ferreteriaApp.HELPERS
{
    public static class funciones
    {


        public static string ObtenerValorParametro(int flag)
        {
            try
            {
                DAOConceptos db = new DAOConceptos();
                return db.ObtenerValorParametro(flag);
            }
            catch (Exception ex)
            {

                throw ex;
            }
      
        }
        public static bool Get_XML_Text(string tipocomprobante, string serie, string numero, int id)
        {
          
            try
            {
                String datosEmpresa = ObtenerValorParametro(CENConstantes.CONST_2);

                string RUC = datosEmpresa.Split('|')[0];
                string Ruta = datosEmpresa.Split('|')[1];



                string Tipocomprobante = tipocomprobante;
                string Serie = serie;
                string Numero = numero;
                string Extension = ".xml";

                //cambios ruta
                String Rutapass = Ruta + @"FIRMA\";
                string ArchPlano = Rutapass + RUC + "-" + Tipocomprobante.PadLeft(2, '0') + "-" + Serie + "-" + Numero.PadLeft(8, '0') + Extension;


                if (File.Exists(ArchPlano))
                {

                    XmlTextReader xtr = new XmlTextReader(ArchPlano);
                    string hash = "";
                    while (xtr.Read())
                    {

                        if (xtr.NodeType == XmlNodeType.Element && xtr.Name == "ds:DigestValue")
                        {
                            hash = xtr.ReadElementContentAsString();
                        }
                    }

                    xtr.Close();
                 
                    CLNDocumentTypes clnDT = new CLNDocumentTypes();
                    clnDT.updateHash(id, hash);
                    return true;
                }
                else
                {

                    String  ArchPlanoB = Rutapass + @"\" + RUC+@"\" + RUC + "-" + Tipocomprobante.PadLeft(2, '0') + "-" + Serie + "-" + Numero.PadLeft(8, '0') + Extension;
                    if (File.Exists(ArchPlanoB))
                    {

                        XmlTextReader xtr = new XmlTextReader(ArchPlanoB);
                        string hash = "";
                        while (xtr.Read())
                        {

                            if (xtr.NodeType == XmlNodeType.Element && xtr.Name == "ds:DigestValue")
                            {
                                hash = xtr.ReadElementContentAsString();
                            }
                        }

                        xtr.Close();
                       
                        CLNDocumentTypes clnDT = new CLNDocumentTypes();
                        clnDT.updateHash(id, hash);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

               
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public static bool Get_XML_Text_Resumen(string tipocomprobante, string nombre_archivo, int id)
        {

            try
            {
                String datosEmpresa = ObtenerValorParametro(CENConstantes.CONST_2);

                string RUC = datosEmpresa.Split('|')[0];
                string Ruta = datosEmpresa.Split('|')[1];



                string Tipocomprobante = tipocomprobante;
            
                string Extension = ".xml";

                //cambios ruta
                String Rutapass = Ruta + @"FIRMA\";
                string ArchPlano = Rutapass + nombre_archivo + Extension;


                if (File.Exists(ArchPlano))
                {

                    XmlTextReader xtr = new XmlTextReader(ArchPlano);
                    string hash = "";
                    while (xtr.Read())
                    {

                        if (xtr.NodeType == XmlNodeType.Element && xtr.Name == "ds:DigestValue")
                        {
                            hash = xtr.ReadElementContentAsString();
                        }
                    }

                    xtr.Close();

                    CLNDocumentTypes clnDT = new CLNDocumentTypes();
                    clnDT.updateHash(id, hash);
                    return true;
                }
                else
                {

                    String ArchPlanoB = Rutapass + @"\" + RUC + @"\" + nombre_archivo + Extension;
                    if (File.Exists(ArchPlanoB))
                    {

                        XmlTextReader xtr = new XmlTextReader(ArchPlanoB);
                        string hash = "";
                        while (xtr.Read())
                        {

                            if (xtr.NodeType == XmlNodeType.Element && xtr.Name == "ds:DigestValue")
                            {
                                hash = xtr.ReadElementContentAsString();
                            }
                        }

                        xtr.Close();

                        CLNDocumentTypes clnDT = new CLNDocumentTypes();
                        clnDT.updateHash(id, hash);
                        return true;
                    }
                    else
                    {
                        return false;
                    }


                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }



        public static void llenarComboUnidad(System.Windows.Forms.ComboBox cmbUnidad)
        {
            List<CENDocumentTypes> lista;
            cmbUnidad.Items.Clear();
            try
            {
                CLNDocumentTypes cln = new CLNDocumentTypes();
                lista = cln.getCmbUnidadString(14);
                cmbUnidad.DataSource = lista;
                cmbUnidad.DisplayMember = "description";
                cmbUnidad.ValueMember = "id";
                
            }
            catch (Exception ex)
            {
                aviso aviso_form = new aviso("warning", "ERROR", ex.Message, false);
                aviso_form.ShowDialog();

                CENDocumentTypes cen = new CENDocumentTypes();
                cen.id = "0";
                cen.description = "No se encontraron resultados";
                lista = null;
                cmbUnidad.DataSource = lista;
                cmbUnidad.DisplayMember = "description";
                cmbUnidad.ValueMember = "id";
            }
        }

        // EXTRACCION DE DNI 
        private static string ExtraerContenidoEntreNombreString(string cadena, int posicion, string nombreInicio, string nombreFin, StringComparison reglaComparacion = StringComparison.OrdinalIgnoreCase)
        {
            string respuesta = "";
            int posicionInicio = cadena.IndexOf(nombreInicio, posicion, reglaComparacion);
            if (posicionInicio > -1)
            {
                posicionInicio += nombreInicio.Length;
                int posicionFin = cadena.IndexOf(nombreFin, posicionInicio, reglaComparacion);
                if (posicionFin > -1)
                    respuesta = cadena.Substring(posicionInicio, posicionFin - posicionInicio);
            }

            return respuesta;
        }
        private static string[] ExtraerContenidoEntreNombre(string cadena, int posicion, string nombreInicio, string nombreFin, StringComparison reglaComparacion = StringComparison.OrdinalIgnoreCase)
        {
            string[] arrRespuesta = null;
            int posicionInicio = cadena.IndexOf(nombreInicio, posicion, reglaComparacion);
            if (posicionInicio > -1)
            {
                posicionInicio += nombreInicio.Length;
                int posicionFin = cadena.IndexOf(nombreFin, posicionInicio, reglaComparacion);
                if (posicionFin > -1)
                {
                    posicion = posicionFin + nombreFin.Length;
                    arrRespuesta = new string[2];
                    arrRespuesta[0] = posicion.ToString();
                    arrRespuesta[1] = cadena.Substring(posicionInicio, posicionFin - posicionInicio);
                }
            }

            return arrRespuesta;
        }
        private class DatoSolicitudDNI
        {
            public string _token { get; set; }
            public string dni { get; set; }
        }

        public static async System.Threading.Tasks.Task<CENDataCliente> BuscarDniAsync(string par_dni)
        {
            int tipoRespuesta = 2;
            string mensajeRespuesta = "";

            string nombres_completos = "";
            CENDataCliente dataCliente = new CENDataCliente();
            dataCliente.success = false;
            dataCliente.nombresCompletos = "";
            dataCliente.direccion = "";
            dataCliente.mensaje = "";



            string numeroDNI = par_dni;
            if (string.IsNullOrWhiteSpace(numeroDNI))
                return dataCliente;

            Stopwatch oCronometro = new Stopwatch();
            oCronometro.Start();

            //btnConsultarDNIMediantePaginaExterna.Enabled = false;

            CookieContainer cookies = new CookieContainer();
            HttpClientHandler controladorMensaje = new HttpClientHandler();
            controladorMensaje.CookieContainer = cookies;
            controladorMensaje.UseCookies = true;
            using (System.Net.Http.HttpClient cliente = new HttpClient(controladorMensaje))
            {
                cliente.DefaultRequestHeaders.Add("Host", "eldni.com");
                cliente.DefaultRequestHeaders.Add("sec-ch-ua", "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\", \"Google Chrome\";v=\"90\"");

                cliente.DefaultRequestHeaders.Add("sec-ch-ua-mobile", "?0");
                cliente.DefaultRequestHeaders.Add("Sec-Fetch-Dest", "document");
                cliente.DefaultRequestHeaders.Add("Sec-Fetch-Mode", "navigate");
                cliente.DefaultRequestHeaders.Add("Sec-Fetch-Site", "none");
                cliente.DefaultRequestHeaders.Add("Sec-Fetch-User", "?1");
                cliente.DefaultRequestHeaders.Add("Upgrade-Insecure-Requests", "1");
                cliente.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36");

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |
                                                       SecurityProtocolType.Tls12;

                string url = "https://eldni.com/pe/buscar-por-dni";
                using (HttpResponseMessage resultadoConsultaToken = await cliente.GetAsync(new Uri(url)))
                {
                    if (resultadoConsultaToken.IsSuccessStatusCode)
                    {
                        mensajeRespuesta = await resultadoConsultaToken.Content.ReadAsStringAsync();

                        string token = ExtraerContenidoEntreNombreString(mensajeRespuesta, 0, "name=\"_token\" value=\"", "\">");

                        cliente.DefaultRequestHeaders.Remove("Sec-Fetch-Site");

                        cliente.DefaultRequestHeaders.Add("Origin", "https://eldni.com");
                        cliente.DefaultRequestHeaders.Add("Referer", "https://eldni.com/pe/buscar-por-dni");
                        cliente.DefaultRequestHeaders.Add("Sec-Fetch-Site", "same-origin");

                        DatoSolicitudDNI oDatoSolicitudDNI = new DatoSolicitudDNI();
                        oDatoSolicitudDNI._token = token;
                        oDatoSolicitudDNI.dni = numeroDNI;

                        using (HttpResponseMessage resultadoConsultaDatos = 
                            await cliente.PostAsJsonAsync(url, oDatoSolicitudDNI ))
                        {
                            if (resultadoConsultaDatos.IsSuccessStatusCode)
                            {
                                string contenidoHTML = await resultadoConsultaDatos.Content.ReadAsStringAsync();
                                string nombreInicio = "<table class=\"table table-striped table-scroll\">";
                                string nombreFin = "</table>";
                                string contenidoDNI = ExtraerContenidoEntreNombreString(contenidoHTML, 0, nombreInicio, nombreFin);
                                if (contenidoDNI == "")
                                {
                                    nombreInicio = "<h3 class=\"text-error\">";
                                    nombreFin = "</h3>";
                                    mensajeRespuesta = ExtraerContenidoEntreNombreString(contenidoHTML, 0, nombreInicio, nombreFin);
                                    mensajeRespuesta = mensajeRespuesta == ""
                                        ? string.Format(
                                            "No se pudo realizar la consulta del número de DNI {0}.", numeroDNI)
                                        : string.Format(
                                            "No se pudo realizar la consulta del número de DNI {0}.\r\nDetalle: {1}",
                                            numeroDNI,
                                            mensajeRespuesta);
                                }
                                else
                                {
                                    nombreInicio = "<td>";
                                    nombreFin = "</td>";
                                    string[] arrResultado = ExtraerContenidoEntreNombre(contenidoDNI, 0,
                                        nombreInicio,
                                        nombreFin);
                                    if (arrResultado != null)
                                    {
                                        // Nombres
                                        arrResultado = ExtraerContenidoEntreNombre(contenidoDNI,
                                            Convert.ToInt32(arrResultado[0]),
                                            nombreInicio, nombreFin);
                                        if (arrResultado != null)
                                        {
                                            nombres_completos = arrResultado[1];

                                            // Apellido Paterno
                                            arrResultado = ExtraerContenidoEntreNombre(contenidoDNI,
                                                Convert.ToInt32(arrResultado[0]),
                                                nombreInicio, nombreFin);

                                            if (arrResultado != null)
                                            {
                                                nombres_completos = nombres_completos + " "+ arrResultado[1];

                                                // Apellido Materno
                                                arrResultado = ExtraerContenidoEntreNombre(contenidoDNI,
                                                    Convert.ToInt32(arrResultado[0]),
                                                    nombreInicio, nombreFin);

                                                if (arrResultado != null)
                                                {
                                                    nombres_completos = nombres_completos + " " + arrResultado[1];
                                                    tipoRespuesta = 1;
                                                    mensajeRespuesta = string.Format("Se realizó exitosamente la consulta del número de DNI {0}",
                                                                numeroDNI);
                                                }
                                            }
                                        }
                                    }
                                    dataCliente.success = true;
                                }

                               
                            }
                            else
                            {
                                dataCliente.success = false;
                                mensajeRespuesta = await resultadoConsultaDatos.Content.ReadAsStringAsync();
                                mensajeRespuesta =
                                    string.Format(
                                        "Ocurrió un inconveniente al consultar los datos del DNI {0}.\r\nDetalle:{1}",
                                        numeroDNI, mensajeRespuesta);
                            }
                        }
                    }
                    else
                    {
                        mensajeRespuesta = await resultadoConsultaToken.Content.ReadAsStringAsync();
                        mensajeRespuesta =
                            string.Format(
                                "Ocurrió un inconveniente al consultar el número de DNI {0}.\r\nDetalle:{1}",
                                numeroDNI, mensajeRespuesta);
                    }
                }
            }

            oCronometro.Stop();
         
            dataCliente.nombresCompletos = nombres_completos;
            dataCliente.mensaje = mensajeRespuesta;


            return dataCliente;
        }

        //BUSCAR RUC

        public static string ExtraerContenidoEntreTagString(string cadena, int posicion, string nombreInicio, string nombreFin, StringComparison reglaComparacion = StringComparison.Ordinal)
        {
            string respuesta = "";
            int posicionInicio = cadena.IndexOf(nombreInicio, posicion, reglaComparacion);
            if (posicionInicio > -1)
            {
                posicionInicio += nombreInicio.Length;
                if (nombreFin == null || nombreFin == "")
                    respuesta = cadena.Substring(posicionInicio, cadena.Length - posicionInicio);
                else
                {
                    int posicionFin = cadena.IndexOf(nombreFin, posicionInicio, reglaComparacion);
                    if (posicionFin > -1)
                        respuesta = cadena.Substring(posicionInicio, posicionFin - posicionInicio);
                }

            }

            return respuesta;
        }

        public static string[] ExtraerContenidoEntreTag(string cadena, int posicion, string nombreInicio, string nombreFin, StringComparison reglaComparacion = StringComparison.Ordinal)
        {
            string[] arrRespuesta = null;
            int posicionInicio = cadena.IndexOf(nombreInicio, posicion, reglaComparacion);
            if (posicionInicio > -1)
            {
                posicionInicio += nombreInicio.Length;
                if (nombreFin == null || nombreFin == "")
                {
                    arrRespuesta = new string[2];
                    arrRespuesta[0] = cadena.Length.ToString();
                    arrRespuesta[1] = cadena.Substring(posicionInicio, cadena.Length - posicionInicio);
                }
                else
                {
                    int posicionFin = cadena.IndexOf(nombreFin, posicionInicio, reglaComparacion);
                    if (posicionFin > -1)
                    {
                        posicion = posicionFin + nombreFin.Length;
                        arrRespuesta = new string[2];
                        arrRespuesta[0] = posicion.ToString();
                        arrRespuesta[1] = cadena.Substring(posicionInicio, posicionFin - posicionInicio);
                    }
                }
            }

            return arrRespuesta;
        }

        private static CENSunat ObtenerDatosRuc(string contenidoHTML, bool is_ruc10)
        {

            CENSunat oEnSUNAT = new CENSunat();
            string nombreInicio = "<HEAD><TITLE>";
            string nombreFin = "</TITLE></HEAD>";
            string contenidoBusqueda = ExtraerContenidoEntreTagString(contenidoHTML, 0, nombreInicio, nombreFin);
            if (contenidoBusqueda == ".:: Pagina de Mensajes ::.")
            {
                nombreInicio = "<p class=\"error\">";
                nombreFin = "</p>";
                oEnSUNAT.TipoRespuesta = 2;
                oEnSUNAT.MensajeRespuesta = ExtraerContenidoEntreTagString(contenidoHTML, 0, nombreInicio, nombreFin);
            }
            else if (contenidoBusqueda == ".:: Pagina de Error ::.")
            {
                nombreInicio = "<p class=\"error\">";
                nombreFin = "</p>";
                oEnSUNAT.TipoRespuesta = 3;
                oEnSUNAT.MensajeRespuesta = ExtraerContenidoEntreTagString(contenidoHTML, 0, nombreInicio, nombreFin);
            }
            else
            {
                oEnSUNAT.TipoRespuesta = 2;
                nombreInicio = "<div class=\"list-group\">";
                nombreFin = "<div class=\"panel-footer text-center\">";
                contenidoBusqueda = ExtraerContenidoEntreTagString(contenidoHTML, 0, nombreInicio, nombreFin);
                if (contenidoBusqueda == "")
                {
                    nombreInicio = "<strong>";
                    nombreFin = "</strong>";
                    oEnSUNAT.MensajeRespuesta = ExtraerContenidoEntreTagString(contenidoHTML, 0, nombreInicio, nombreFin);
                    if (oEnSUNAT.MensajeRespuesta == "")
                        oEnSUNAT.MensajeRespuesta = "No se encuentra las cabeceras principales del contenido HTML";
                }
                else
                {
                    contenidoHTML = contenidoBusqueda;
                    oEnSUNAT.MensajeRespuesta = "Mensaje del inconveniente no especificado";
                    nombreInicio = "<h4 class=\"list-group-item-heading\">";
                    nombreFin = "</h4>";
                    int resultadoBusqueda = contenidoHTML.IndexOf(nombreInicio, 0, StringComparison.OrdinalIgnoreCase);
                    if (resultadoBusqueda > -1)
                    {
                        // Modificar cuando el estado del Contribuyente es "BAJA DE OFICIO", porque se agrega un elemento con clase "list-group-item"
                        resultadoBusqueda += nombreInicio.Length;
                        string[] arrResultado = ExtraerContenidoEntreTag(contenidoHTML, resultadoBusqueda,
                            nombreInicio, nombreFin);
                        if (arrResultado != null)
                        {
                            oEnSUNAT.RazonSocial = arrResultado[1];

                            // Tipo Contribuyente
                            nombreInicio = "<p class=\"list-group-item-text\">";
                            nombreFin = "</p>";
                            arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                nombreInicio, nombreFin);
                            if (arrResultado != null)
                            {
                                oEnSUNAT.TipoContribuyente = arrResultado[1];
                                // si es ruc 10 se agrega tipo de documento
                                if(is_ruc10)
                                {
                                    // Tipo de Documento
                                    arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                        nombreInicio, nombreFin);

                                    if (arrResultado != null)
                                    {
                                        oEnSUNAT.TipoDocumento = arrResultado[1].Replace("\r\n", "").Replace("\t", "").Trim();
                                    }
                                }



                                // Nombre Comercial
                                arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                    nombreInicio, nombreFin);
                                if (arrResultado != null)
                                {
                                    oEnSUNAT.NombreComercial = arrResultado[1].Replace("\r\n", "").Replace("\t", "").Trim();

                                    // Fecha de Inscripción
                                    arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                        nombreInicio, nombreFin);
                                    if (arrResultado != null)
                                    {
                                        oEnSUNAT.FechaInscripcion = arrResultado[1];

                                        // Fecha de Inicio de Actividades: 
                                        arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                            nombreInicio, nombreFin);
                                        if (arrResultado != null)
                                        {
                                            oEnSUNAT.FechaInicioActividades = arrResultado[1];

                                            // Estado del Contribuyente
                                            arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                            nombreInicio, nombreFin);
                                            if (arrResultado != null)
                                            {
                                                oEnSUNAT.EstadoContribuyente = arrResultado[1].Trim();

                                                // Condición del Contribuyente
                                                arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                                    nombreInicio, nombreFin);
                                                if (arrResultado != null)
                                                {
                                                    oEnSUNAT.CondicionContribuyente = arrResultado[1].Trim();

                                                    // Domicilio Fiscal
                                                    arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                                        nombreInicio, nombreFin);
                                                    if (arrResultado != null)
                                                    {
                                                        oEnSUNAT.DomicilioFiscal = arrResultado[1].Trim();

                                                        // Actividad(es) Económica(s)
                                                        nombreInicio = "<tbody>";
                                                        nombreFin = "</tbody>";
                                                        arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                                            nombreInicio, nombreFin);
                                                        if (arrResultado != null)
                                                        {
                                                            oEnSUNAT.ActividadesEconomicas = arrResultado[1].Replace("\r\n", "").Replace("\t", "").Trim();

                                                            // Comprobantes de Pago c/aut. de impresión (F. 806 u 816)
                                                            arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                                                nombreInicio, nombreFin);
                                                            if (arrResultado != null)
                                                            {
                                                                oEnSUNAT.ComprobantesPago = arrResultado[1].Replace("\r\n", "").Replace("\t", "").Trim();

                                                                // Sistema de Emisión Electrónica
                                                                arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                                                    nombreInicio, nombreFin);
                                                                if (arrResultado != null)
                                                                {
                                                                    oEnSUNAT.SistemaEmisionComprobante = arrResultado[1].Replace("\r\n", "").Replace("\t", "").Trim();

                                                                    // Afiliado al PLE desde
                                                                    nombreInicio = "<p class=\"list-group-item-text\">";
                                                                    nombreFin = "</p>";
                                                                    arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                                                        nombreInicio, nombreFin);
                                                                    if (arrResultado != null)
                                                                    {
                                                                        oEnSUNAT.AfiliadoPLEDesde = arrResultado[1];

                                                                        // Padrones 
                                                                        nombreInicio = "<tbody>";
                                                                        nombreFin = "</tbody>";
                                                                        arrResultado = ExtraerContenidoEntreTag(contenidoHTML, Convert.ToInt32(arrResultado[0]),
                                                                            nombreInicio, nombreFin);
                                                                        if (arrResultado != null)
                                                                        {
                                                                            oEnSUNAT.Padrones = arrResultado[1].Replace("\r\n", "").Replace("\t", "").Trim();
                                                                        }
                                                                    }

                                                                    oEnSUNAT.TipoRespuesta = 1;
                                                                    oEnSUNAT.MensajeRespuesta = "Ok";
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return oEnSUNAT;
        }

        public static async System.Threading.Tasks.Task<CENSunat>  ConsultarRUC(string pa_ruc)
        {

            int tipoRespuesta = 2;
            string mensajeRespuesta = "";

            CENSunat oEnSUNAT = new CENSunat();
            bool is_ruc_10 = false;
            string min_ruc = "";
            min_ruc = pa_ruc.Substring(0, 2);
            if (min_ruc == "10")
            {
                is_ruc_10 = true;
            }
            else
            {
                is_ruc_10 = false;
            }

            string ruc = pa_ruc;
            if (string.IsNullOrWhiteSpace(ruc))
                return oEnSUNAT;


          

            try
            {
                Stopwatch oCronometro = new Stopwatch();
                oCronometro.Start();

                //btnConsultarRUCMedianteNumeroRandom.Enabled = false;
                CookieContainer cookies = new CookieContainer();
                HttpClientHandler controladorMensaje = new HttpClientHandler();
                controladorMensaje.CookieContainer = cookies;
                controladorMensaje.UseCookies = true;
                using (HttpClient cliente = new HttpClient(controladorMensaje))
                {
                    cliente.DefaultRequestHeaders.Add("Host", "e-consultaruc.sunat.gob.pe");
                    cliente.DefaultRequestHeaders.Add("sec-ch-ua",
                        " \" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\", \"Google Chrome\";v=\"90\"");
                    cliente.DefaultRequestHeaders.Add("sec-ch-ua-mobile", "?0");
                    cliente.DefaultRequestHeaders.Add("Sec-Fetch-Dest", "document");
                    cliente.DefaultRequestHeaders.Add("Sec-Fetch-Mode", "navigate");
                    cliente.DefaultRequestHeaders.Add("Sec-Fetch-Site", "none");
                    cliente.DefaultRequestHeaders.Add("Sec-Fetch-User", "?1");
                    cliente.DefaultRequestHeaders.Add("Upgrade-Insecure-Requests", "1");
                    cliente.DefaultRequestHeaders.Add("User-Agent",
                        "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36");
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |
                                                           SecurityProtocolType.Tls12;
                    await Task.Delay(100);

                    string url =
                        "https://e-consultaruc.sunat.gob.pe/cl-ti-itmrconsruc/jcrS00Alias";
                    using (HttpResponseMessage resultadoConsulta = await cliente.GetAsync(new Uri(url)))
                    {
                        if (resultadoConsulta.IsSuccessStatusCode)
                        {
                            await Task.Delay(100);
                            cliente.DefaultRequestHeaders.Remove("Sec-Fetch-Site");

                            cliente.DefaultRequestHeaders.Add("Origin", "https://e-consultaruc.sunat.gob.pe");
                            cliente.DefaultRequestHeaders.Add("Referer", url);
                            cliente.DefaultRequestHeaders.Add("Sec-Fetch-Site", "same-origin");

                            string numeroDNI = "12345678"; // cualquier número DNI pero que exista en SUNAT.
                            var lClaveValor = new List<KeyValuePair<string, string>>
                        {
                            new KeyValuePair<string, string>("accion", "consPorTipdoc"),
                            new KeyValuePair<string, string>("razSoc", ""),
                            new KeyValuePair<string, string>("nroRuc", ""),
                            new KeyValuePair<string, string>("nrodoc", numeroDNI),
                            new KeyValuePair<string, string>("contexto", "ti-it"),
                            new KeyValuePair<string, string>("modo", "1"),
                            new KeyValuePair<string, string>("search1", ""),
                            new KeyValuePair<string, string>("rbtnTipo", "2"),
                            new KeyValuePair<string, string>("tipdoc", "1"),
                            new KeyValuePair<string, string>("search2", numeroDNI),
                            new KeyValuePair<string, string>("search3", ""),
                            new KeyValuePair<string, string>("codigo", ""),
                        };
                            FormUrlEncodedContent contenido = new FormUrlEncodedContent(lClaveValor);

                            url = "https://e-consultaruc.sunat.gob.pe/cl-ti-itmrconsruc/jcrS00Alias";
                            using (HttpResponseMessage resultadoConsultaRandom = await cliente.PostAsync(url, contenido))
                            {
                                if (resultadoConsultaRandom.IsSuccessStatusCode)
                                {
                                    await Task.Delay(100);
                                    string contenidoHTML = await resultadoConsultaRandom.Content.ReadAsStringAsync();
                                    string numeroRandom = ExtraerContenidoEntreTagString(contenidoHTML, 0, "name=\"numRnd\" value=\"", "\">");

                                    lClaveValor = new List<KeyValuePair<string, string>>
                                {
                                    new KeyValuePair<string, string>("accion", "consPorRuc"),
                                    new KeyValuePair<string, string>("actReturn", "1"),
                                    new KeyValuePair<string, string>("nroRuc", ruc),
                                    new KeyValuePair<string, string>("numRnd", numeroRandom),
                                    new KeyValuePair<string, string>("modo", "1")
                                };

                                    int cConsulta = 0;
                                    int nConsulta = 3;
                                    HttpStatusCode codigoEstado = HttpStatusCode.Unauthorized;
                                    while (cConsulta < nConsulta && codigoEstado == HttpStatusCode.Unauthorized)
                                    {
                                        contenido = new FormUrlEncodedContent(lClaveValor);
                                        using (HttpResponseMessage resultadoConsultaDatos =
                                        await cliente.PostAsync(url, contenido))
                                        {
                                            codigoEstado = resultadoConsultaDatos.StatusCode;
                                            if (resultadoConsultaDatos.IsSuccessStatusCode)
                                            {
                                                contenidoHTML = await resultadoConsultaDatos.Content.ReadAsStringAsync();
                                                contenidoHTML = WebUtility.HtmlDecode(contenidoHTML);

                                                #region Obtener los datos del RUC
                                                oEnSUNAT = ObtenerDatosRuc(contenidoHTML, is_ruc_10);
                                                if (oEnSUNAT.TipoRespuesta == 1)
                                                {
                                                    string[] nom_com;
                                                    string direccion_com;
                                                    nom_com = oEnSUNAT.RazonSocial.Split('-');
                                                    direccion_com = oEnSUNAT.DomicilioFiscal;
                                                    oEnSUNAT.RazonSocial = nom_com[1].Trim();
                                                    oEnSUNAT.DomicilioFiscal= GetDireccion(direccion_com);
                                                    //txtRUC.Text = oEnSUNAT.RazonSocial;
                                                    //txtTipoContribuyente.Text = oEnSUNAT.TipoContribuyente;
                                                    //txtNombreComercial.Text = oEnSUNAT.NombreComercial;
                                                    //txtFechaInscripcion.Text = oEnSUNAT.FechaInscripcion;
                                                    //txtFechaInicioActividades.Text = oEnSUNAT.FechaInicioActividades;
                                                    //txtEstadoContribuyente.Text = oEnSUNAT.EstadoContribuyente;
                                                    //txtCondicionContribuyente.Text = oEnSUNAT.CondicionContribuyente;
                                                    //txtDomicilioFiscal.Text = oEnSUNAT.DomicilioFiscal;
                                                    //txtSistemaEmisionComprobante.Text = oEnSUNAT.SistemaEmisionComprobante;
                                                    //txtActividadesEconomicas.Text = oEnSUNAT.ActividadesEconomicas;
                                                    //txtComprobantesPago.Text = oEnSUNAT.ComprobantesPago;
                                                    //txtAfiliadoPLE.Text = oEnSUNAT.AfiliadoPLEDesde;
                                                    //txtPadrones.Text = oEnSUNAT.Padrones;

                                                    tipoRespuesta = 1;
                                                    mensajeRespuesta =
                                                        string.Format("Se realizó exitosamente la consulta del número de RUC {0}",
                                                            ruc);
                                                }
                                                else
                                                {
                                                    tipoRespuesta = oEnSUNAT.TipoRespuesta;
                                                    mensajeRespuesta = string.Format(
                                                        "No se pudo realizar la consulta del número de RUC {0}.\r\nDetalle: {1}",
                                                        ruc,
                                                        oEnSUNAT.MensajeRespuesta);
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                mensajeRespuesta = await resultadoConsultaDatos.Content.ReadAsStringAsync();
                                                mensajeRespuesta =
                                                    string.Format(
                                                        "Ocurrió un inconveniente al consultar los datos del RUC {0}.\r\nDetalle:{1}",
                                                        ruc, mensajeRespuesta);
                                            }
                                        }

                                        cConsulta++;
                                    }



                                }
                                else
                                {
                                    mensajeRespuesta = await resultadoConsultaRandom.Content.ReadAsStringAsync();
                                    mensajeRespuesta =
                                        string.Format(
                                            "Ocurrió un inconveniente al consultar el número random del RUC {0}.\r\nDetalle:{1}",
                                            ruc, mensajeRespuesta);
                                }
                            }
                        }
                        else
                        {
                            mensajeRespuesta = await resultadoConsulta.Content.ReadAsStringAsync();
                            mensajeRespuesta =
                                string.Format(
                                    "Ocurrió un inconveniente al consultar la página principal {0}.\r\nDetalle:{1}",
                                    ruc, mensajeRespuesta);
                        }
                    }
                }

                oEnSUNAT.MensajeRespuesta = mensajeRespuesta;
                return oEnSUNAT;

                oCronometro.Stop();
            }
            catch (Exception ex)
            {

                throw ex;
            }

           


        }

        private static string GetDireccion(string direccion)
        {
            string[] dire;
            string newDir = "";
            dire = direccion.Split('-');
            int count = 0;
            foreach (string der in dire)
            {
                if (count == 0)
                {

                    newDir = der.Trim();
                }
                else
                {
                    newDir = newDir + " - " + der.Trim();
                }
                count++;

            }
            return newDir;

        }


        //CONSULTA DE DNI POR APIPERU

        public static CENDataCliente Consultar(string DNI)
        {
            var Result = new CENDataCliente();
            try
            {

                string urlPar = "", tokenPar = "";
                string html = string.Empty;

                CENParametro parFlagRegrear;
                CLNConcepto clncconcepto;

                clncconcepto = new CLNConcepto();
                parFlagRegrear = new CENParametro();
                parFlagRegrear = clncconcepto.buscarParametro(1000);
                urlPar = parFlagRegrear.parametro;
                tokenPar = parFlagRegrear.parametro2;

                string url = $"{urlPar}{DNI}";

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Headers.Add("Authorization", "Bearer " + tokenPar);
                //request.Headers.Add("Authorization", "Bearer 8dddb831c0f5145ddc683e1f42b2fb5d788ff2997a210c9b544f202ddd2f2b1a"); //token de prueba
                //request.Headers.Add("Authorization", "Bearer 26d9db9ff8972239c3e19a967ee2dc337fd19e3b444be2b6a6fe680c14e21c95"); // token expirado


                request.AutomaticDecompression = DecompressionMethods.GZip;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    html = reader.ReadToEnd();
                }

                var objE= JsonConvert.DeserializeObject<CENTransactionApidni>(html);

                if(objE.success)
                {
                    Result.success = objE.success;
                    Result.nombresCompletos = objE.data.apellido_paterno + " " + objE.data.apellido_materno + " " + objE.data.nombres;
                    Result.direccion = "";
                }
                else
                {
                    Result.success = objE.success;
                    Result.mensaje = "Si el problema persiste revisar Token de " + urlPar+
                        " \n Comunicarse con el área de sistemas";
                }
            

           }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }



    }
}
