﻿
using ferreteriaApp.CEN;
using ferreteriaApp.CLN;
using iText.Barcodes;

using iText.Kernel.Colors;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Xobject;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using System;
using System.Collections.Generic;
using System.IO;

using WebVentas.util;

namespace ferreteriaApp.HELPERS
{
    public static class CrearPDF
    {


        public static Boolean crearDocumento(List<string> CabeceraComprobante,
            string[] NombresHeaderTable, float[] TamCeldas,
            List<string> BobyTable, List<string> Impuestos,
             string tipocomprobante,
            string serie, string numero, string nombreComprobante,
            string cantidadEnLetras, List<string> subtotal, List<string> datosHash, int id_comprobante)
        {

            CLNDocumentTypes clnDT = new CLNDocumentTypes();
            string get_hash = clnDT.getHash(id_comprobante);

            string logo = "";
            bool has_logo = false;
            CLNConcepto clncconcepto = new CLNConcepto();
            CENParametro listaPar;

            //buscamos logo
            listaPar = clncconcepto.buscarParametro(1002);
            logo = @"" + listaPar.parametro;
            has_logo = (listaPar.par_tipo == 1) ? true : false;


            if (get_hash == null || get_hash == "")
            {
                return false;  
            }
            else {

                string datosEmpresa = Helper.ObtenerValorParametro(CENConstantes.CONST_2);

                string Ruta = datosEmpresa.Split('|')[1];

                string RUC = datosEmpresa.Split('|')[0];
                string razon = datosEmpresa.Split('|')[2];
                string telefono = datosEmpresa.Split('|')[3];
                string celular = datosEmpresa.Split('|')[4];
                string direccion = datosEmpresa.Split('|')[5];

                string lineaDataEmpresa = razon + "\n" + ((direccion == "") ? "" : direccion) + "\n" + ((telefono == "") ? "" : "Telefono: " + telefono) + "\n" + ((celular == "") ? "" : "Celular: " + celular);

                string Tipocomprobante = tipocomprobante;
                string Serie = serie;
                string Numero = numero;
                string Extension = ".pdf";


            string Rutapass = Ruta + @"REPO\";
            string ArchPlano = Rutapass + RUC + "-" + Tipocomprobante.PadLeft(2, '0') + "-" + Serie + "-" + Numero.PadLeft(8, '0') + Extension;

                string linea = RUC + "\r\n" + Tipocomprobante + "\r\n" + serie + "\r\n" + numero + "\r\n" + datosHash[0]
                + "|" + datosHash[1] + "\r\n" + datosHash[2] + "\r\n" + datosHash[3] + "\r\n" + datosHash[4] + "\r\n" + get_hash.ToString();

                FileInfo file = new FileInfo(Ruta);
                file.Directory.Create();

                PdfDocument pdfDoc = new PdfDocument(new PdfWriter(ArchPlano));

                Document Doc = new Document(pdfDoc);
                Doc.SetMargins(60, 20, 50, 20);


                Table TabInfoEmpresa;
              

                Cell cellEmpresaData = new Cell();
                cellEmpresaData.Add(new Paragraph(lineaDataEmpresa));
                cellEmpresaData.SetFontSize(8);
                cellEmpresaData.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));

                Cell cellQr = new Cell();
                cellQr.Add(CreateBarcode(linea, pdfDoc));
                cellQr.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));

                Cell cellDataComprobante = new Cell();
                cellDataComprobante.SetBorder(new SolidBorder(ColorConstants.BLACK, 1));

                Cell celNombreComprobante = new Cell();
                Cell celRuc = new Cell();
                Cell celSerie = new Cell();

                celSerie.SetFontSize(8);
                celRuc.SetFontSize(8);
                celNombreComprobante.SetFontSize(8);

                celNombreComprobante.Add(new Paragraph(nombreComprobante));
                celRuc.Add(new Paragraph("RUC: " + RUC));
                celSerie.Add(new Paragraph(Serie + "-" + Numero.PadLeft(8, '0')));

                cellDataComprobante.Add(celNombreComprobante);
                cellDataComprobante.Add(celRuc);
                cellDataComprobante.Add(celSerie);

                cellDataComprobante.SetTextAlignment(TextAlignment.CENTER);

                cellDataComprobante.SetFontSize(8);

                //LOGO
                if (has_logo && File.Exists(logo))
                {

                    //Add a image  
                    iText.IO.Image.ImageData imageData;
                    imageData = iText.IO.Image.ImageDataFactory.Create(logo);
                    Image image = new Image(imageData);
                    image.SetWidth(100);
                    image.SetHeight(100);

                    Cell cellLogo = new Cell();
                    cellLogo.Add(image);
                    cellLogo.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));

                    TabInfoEmpresa = new Table(UnitValue.CreatePercentArray(new float[] { 25, 50, 20, 30 }));
                    TabInfoEmpresa.AddCell(cellLogo);

                }
                else
                {
                    TabInfoEmpresa = new Table(UnitValue.CreatePercentArray(new float[] { 50, 20, 30 }));
                }
                //End Logo

                TabInfoEmpresa.SetWidth(UnitValue.CreatePercentValue(100));
                TabInfoEmpresa.SetTextAlignment(TextAlignment.LEFT);
                TabInfoEmpresa.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));



                TabInfoEmpresa.AddCell(cellEmpresaData);
                TabInfoEmpresa.AddCell(cellQr);
                TabInfoEmpresa.AddCell(cellDataComprobante);
                TabInfoEmpresa.SetMarginBottom(20);
                Doc.Add(TabInfoEmpresa);



                Table tabInfo = new Table(UnitValue.CreatePercentArray(new float[] { 5, 10 }));
                tabInfo.SetWidth(UnitValue.CreatePercentValue(80));
                tabInfo.SetTextAlignment(TextAlignment.LEFT);

                // crea la cebera del comprobante

                foreach (string item in CabeceraComprobante)
                {

                    string[] dataCabecera = item.Split('|');

                    for (int i = 0; i < dataCabecera.Length; i++)
                    {
                        Cell celda = new Cell();
                        celda.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));
                        celda.SetFontSize(8);
                        celda.Add(new Paragraph(dataCabecera[i]));
                        tabInfo.AddCell(celda);
                    }

                }
                tabInfo.SetMarginBottom(20);
                Doc.Add(tabInfo);

                Table tab = new Table(UnitValue.CreatePercentArray(TamCeldas));
                tab.SetWidth(UnitValue.CreatePercentValue(100));

                //se crea los encabezado de la tabla

                foreach (string columna in NombresHeaderTable)
                {
                    Cell celda = new Cell();
                    celda.Add(new Paragraph(columna));
                    celda.SetBackgroundColor(ColorConstants.WHITE);
                    celda.SetBorder(new SolidBorder(ColorConstants.BLACK, 1));
                    celda.SetFontColor(ColorConstants.BLACK);
                    celda.SetFontSize(8);
                    celda.SetTextAlignment(TextAlignment.CENTER);

                    tab.AddHeaderCell(celda);
                }

                Doc.Add(tab);

                // se crea el contenido de la tabla

                Table tabBody = new Table(UnitValue.CreatePercentArray(TamCeldas));
                tab.SetWidth(UnitValue.CreatePercentValue(100));

                foreach (string item in BobyTable)
                {
                    string[] dataBody = item.Split('|');

                    for (int i = 0; i < dataBody.Length - 1; i++)
                    {
                        Cell celda = new Cell();
                        celda.SetBackgroundColor(ColorConstants.WHITE);
                        celda.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));
                        celda.SetFontSize(8);
                        if (i == 1 || i == 2 || i == 3) celda.SetTextAlignment(TextAlignment.JUSTIFIED); else celda.SetTextAlignment(TextAlignment.RIGHT);
                        celda.SetWidth(25f);
                        celda.Add(new Paragraph(dataBody[i]));
                        tabBody.AddCell(celda);
                    }

                }


                tabBody.SetMarginBottom(20);

                Doc.Add(tabBody);


                //tabla de resumen de impuestos

                Table tabImpuestos = new Table(UnitValue.CreatePercentArray(new float[] { 10, 5 }));
                tabImpuestos.SetWidth(UnitValue.CreatePercentValue(100));
                tabImpuestos.SetTextAlignment(TextAlignment.RIGHT);
                tabImpuestos.SetMarginBottom(20);
                foreach (string item in Impuestos)
                {

                    string[] dataImpuestos = item.Split('|');

                    for (int i = 0; i < dataImpuestos.Length; i++)
                    {
                        Cell celda = new Cell();
                        celda.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));
                        celda.SetFontSize(8);
                        celda.Add(new Paragraph(dataImpuestos[i]));
                        tabImpuestos.AddCell(celda);
                    }

                }


                Doc.Add(tabImpuestos);


                Table TabResumenVenta = new Table(UnitValue.CreatePercentArray(new float[] { 50, 50 }));
                TabResumenVenta.SetWidth(UnitValue.CreatePercentValue(100));

                TabResumenVenta.SetTextAlignment(TextAlignment.LEFT);
                TabResumenVenta.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));

                Cell cellCntidadLetter = new Cell();
                cellCntidadLetter.SetPadding(3f);
                cellCntidadLetter.Add(new Paragraph("SON:" + cantidadEnLetras + " SOLES"));
                if (Tipocomprobante == "01") cellCntidadLetter.Add(new Paragraph("FORMA DE PAGO: CONTADO"));
                cellCntidadLetter.SetFontSize(8);
                cellCntidadLetter.SetBorder(new SolidBorder(ColorConstants.BLACK, 1));
                cellCntidadLetter.SetWidth(30f);

                Cell cellSubtotal = new Cell();
                cellSubtotal.SetBorder(new SolidBorder(ColorConstants.BLACK, 1));
                cellSubtotal.SetWidth(40f);

                Table tabSubtotal = new Table(UnitValue.CreatePercentArray(new float[] { 10, 5 }));
                tabSubtotal.SetWidth(UnitValue.CreatePercentValue(100));
                tabSubtotal.SetTextAlignment(TextAlignment.RIGHT);

                foreach (string item in subtotal)
                {

                    string[] dataSubtotal = item.Split('|');

                    for (int i = 0; i < dataSubtotal.Length; i++)
                    {
                        Cell celda = new Cell();
                        celda.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));
                        celda.SetFontSize(8);
                        celda.Add(new Paragraph(dataSubtotal[i]));
                        tabSubtotal.AddCell(celda);
                    }

                }

                cellSubtotal.Add(tabSubtotal);
                cellSubtotal.SetTextAlignment(TextAlignment.RIGHT);

                cellSubtotal.SetFontSize(8);


                TabResumenVenta.AddCell(cellCntidadLetter);
                TabResumenVenta.AddCell(cellSubtotal);
                TabResumenVenta.SetMarginBottom(10);
                Doc.Add(TabResumenVenta);


                Table tabFooter = new Table(UnitValue.CreatePercentArray(new float[] { 70 }));
                tabFooter.SetWidth(UnitValue.CreatePercentValue(70));
                tabFooter.SetHorizontalAlignment(HorizontalAlignment.CENTER);
                tabFooter.SetVerticalAlignment(VerticalAlignment.BOTTOM);
                tabFooter.SetTextAlignment(TextAlignment.CENTER);

                Cell celdaHash = new Cell();
                celdaHash.Add(new Paragraph(get_hash));
                celdaHash.SetFontSize(7);
                celdaHash.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));

                Cell celdaDescripcionHash = new Cell();
                celdaDescripcionHash.Add(new Paragraph("Representación impresa de la " + nombreComprobante + " generada desde el sistema facturador SUNAT. Puede verficarla utilizando su clave SOL"));
                celdaDescripcionHash.SetFontSize(7);


                tabFooter.AddCell(celdaHash);
                tabFooter.AddCell(celdaDescripcionHash);
                Doc.Add(tabFooter);

                Doc.Close();

                System.Diagnostics.Process.Start(ArchPlano);

                return true;

            }

        }


        public static Cell CreateBarcode(string code, PdfDocument pdfDoc)
        {
            BarcodeQRCode barcodeQRcode = new BarcodeQRCode(code);
            PdfFormXObject barcodeObject = barcodeQRcode.CreateFormXObject(null, pdfDoc);
            Cell cell = new Cell().Add(new Image(barcodeObject));
            cell.SetPaddingTop(10);
            cell.SetPaddingRight(10);
            cell.SetPaddingBottom(10);
            cell.SetPaddingLeft(10);

            return cell;
        }

     
        public static Boolean crearDocumentoNotas(List<string> CabeceraComprobante,
                           string[] NombresHeaderTable, float[] TamCeldas,
                           List<string> BobyTable, List<string> Impuestos,
                           string tipocomprobante,
                           string serie, string numero, string nombreComprobante,
                           string cantidadEnLetras, List<string> subtotal, int id_comprobante, List<string> datosHash)
        {

            string logo = "";
            bool has_logo = false;
            CLNConcepto clncconcepto = new CLNConcepto();
            CENParametro listaPar;

            //buscamos logo
            listaPar = clncconcepto.buscarParametro(1002);
            logo = @"" + listaPar.parametro;
            has_logo = (listaPar.par_tipo == 1) ? true : false;

            string datosEmpresa = Helper.ObtenerValorParametro(CENConstantes.CONST_2);


            string Ruta = datosEmpresa.Split('|')[1];

            string RUC = datosEmpresa.Split('|')[0];
            string razon = datosEmpresa.Split('|')[2];
            string telefono = datosEmpresa.Split('|')[3];
            string celular = datosEmpresa.Split('|')[4];
            string direccion = datosEmpresa.Split('|')[5];

            string lineaDataEmpresa = razon + "\n" + ((direccion == "") ? "" : direccion) + "\n" + ((telefono == "") ? "" : "Telefono: " + telefono) + "\n" + ((celular == "") ? "" : "Celular: " + celular);


            string Tipocomprobante = tipocomprobante;
            string Serie = serie;
            string Numero = numero;
            string Extension = ".pdf";
            string Rutapass = Ruta + @"REPO\";
            string ArchPlano = Rutapass + RUC + "-" + Tipocomprobante.PadLeft(2, '0') + "-" + serie + "-" + Numero.PadLeft(8, '0') + Extension;

            FileInfo file = new FileInfo(Ruta);
            file.Directory.Create();


            PdfDocument pdfDoc = new PdfDocument(new PdfWriter(ArchPlano));

            Document Doc = new Document(pdfDoc);

            try
            {


                Doc.SetMargins(60, 20, 50, 20);


                Table TabInfoEmpresa ;
       

                Cell cellEmpresaData = new Cell();
                cellEmpresaData.Add(new Paragraph(lineaDataEmpresa));
                cellEmpresaData.SetFontSize(8);
                cellEmpresaData.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));





                CLNDocumentTypes clnDT = new CLNDocumentTypes();
                string get_hash = clnDT.getHash(id_comprobante);


                string linea = RUC + "\r\n" + Tipocomprobante + "\r\n" + serie + "\r\n" + numero + "\r\n" + datosHash[0]
                + "\r\n" + datosHash[1] + "\r\n" + datosHash[2] + "\r\n" + datosHash[3] + "\r\n" + datosHash[4] + "\r\n" + get_hash.ToString();


                Cell cellQr = new Cell();
                cellQr.Add(CreateBarcode(linea, pdfDoc));
                cellQr.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));

                Cell cellDataComprobante = new Cell();
                cellDataComprobante.SetBorder(new SolidBorder(ColorConstants.BLACK, 1));
                cellDataComprobante.Add(new Paragraph(nombreComprobante + "\n" + "RUC: " + RUC + "\n" + Serie + "-" + Numero.PadLeft(8, '0')));
                cellDataComprobante.SetTextAlignment(TextAlignment.CENTER);
              
                cellDataComprobante.SetFontSize(8);


                //LOGO

                if (has_logo && File.Exists(logo))
                {

                    //Add a image  
                    iText.IO.Image.ImageData imageData;
                    imageData = iText.IO.Image.ImageDataFactory.Create(logo);
                    Image image = new Image(imageData);
                    image.SetWidth(100);
                    image.SetHeight(100);

                    Cell cellLogo = new Cell();
                    cellLogo.Add(image);
                    cellLogo.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));

                    TabInfoEmpresa = new Table(UnitValue.CreatePercentArray(new float[] { 25, 50, 20, 30 }));
                    TabInfoEmpresa.AddCell(cellLogo);

                }
                else
                {
                    TabInfoEmpresa = new Table(UnitValue.CreatePercentArray(new float[] { 50, 20, 30 }));
                }
                //End Logo

                TabInfoEmpresa.SetWidth(UnitValue.CreatePercentValue(100));
                TabInfoEmpresa.SetTextAlignment(TextAlignment.LEFT);
                TabInfoEmpresa.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));


                TabInfoEmpresa.AddCell(cellEmpresaData);
                TabInfoEmpresa.AddCell(cellQr);
                TabInfoEmpresa.AddCell(cellDataComprobante);
                Doc.Add(TabInfoEmpresa);

                Table Separador = new Table(UnitValue.CreatePercentArray(1));
                Separador.SetWidth(UnitValue.CreatePercentValue(100));
                Cell Cel = new Cell();
                Cel.Add(new Paragraph(""));
                Cel.SetHeight(UnitValue.CreatePercentValue(50));
                Cel.SetBorder(new SolidBorder(ColorConstants.WHITE, 1));
                Separador.AddCell(Cel);

                Doc.Add(Separador);
                Doc.Add(Separador);
                Doc.Add(Separador);

                Table tabInfo = new Table(UnitValue.CreatePercentArray(new float[] { 10, 20, 5, 10 }));
                tabInfo.SetWidth(UnitValue.CreatePercentValue(80));
                tabInfo.SetTextAlignment(TextAlignment.LEFT);

                // crea la cebera del comprobante
                foreach (string item in CabeceraComprobante)
                {

                    string[] dataCabecera = item.Split('|');

                    for (int i = 0; i < dataCabecera.Length; i++)
                    {
                        Cell celda = new Cell();
                        celda.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));
                        celda.SetFontSize(8);
                        celda.Add(new Paragraph(dataCabecera[i]));
                        tabInfo.AddCell(celda);
                    }

                }
                Doc.Add(tabInfo);



                Doc.Add(Separador);
                Doc.Add(Separador);
                Doc.Add(Separador);

                Table tab = new Table(UnitValue.CreatePercentArray(TamCeldas));
                tab.SetWidth(UnitValue.CreatePercentValue(100));

                //se crea los encabezado de la tabla

                foreach (string columna in NombresHeaderTable)
                {
                    Cell celda = new Cell();
                    celda.Add(new Paragraph(columna));
                    celda.SetBackgroundColor(ColorConstants.WHITE);
                    celda.SetBorder(new SolidBorder(ColorConstants.BLACK, 1));
                    celda.SetFontColor(ColorConstants.BLACK);
                    celda.SetFontSize(8);
                    celda.SetTextAlignment(TextAlignment.CENTER);

                    tab.AddHeaderCell(celda);
                }

                Doc.Add(tab);

                // se crea el contenido de la tabla

                Table tabBody = new Table(UnitValue.CreatePercentArray(TamCeldas));
                tab.SetWidth(UnitValue.CreatePercentValue(100));

                foreach (string item in BobyTable)
                {
                    string[] dataBody = item.Split('|');

                    for (int i = 0; i < dataBody.Length - 1; i++)
                    {
                        Cell celda = new Cell();
                        celda.SetBackgroundColor(ColorConstants.WHITE);
                        celda.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));
                        celda.SetFontSize(8);
                        //celda.SetTextAlignment(TextAlignment.JUSTIFIED);
                        if (i == 1 || i == 2 || i == 3) celda.SetTextAlignment(TextAlignment.JUSTIFIED); else celda.SetTextAlignment(TextAlignment.RIGHT);
                        celda.SetWidth(25f);
                        celda.Add(new Paragraph(dataBody[i]));
                        tabBody.AddCell(celda);
                    }

                }


                Doc.Add(tabBody);

                Doc.Add(Separador);
                Doc.Add(Separador);
                Doc.Add(Separador);

                //tabla de resumen de impuestos

                Table tabImpuestos = new Table(UnitValue.CreatePercentArray(new float[] { 10, 5 }));
                tabImpuestos.SetWidth(UnitValue.CreatePercentValue(100));
                tabImpuestos.SetTextAlignment(TextAlignment.RIGHT);
                tabImpuestos.SetMarginBottom(20);
                foreach (string item in Impuestos)
                {

                    string[] dataImpuestos = item.Split('|');

                    for (int i = 0; i < dataImpuestos.Length; i++)
                    {
                        Cell celda = new Cell();
                        celda.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));
                        celda.SetFontSize(8);
                        celda.Add(new Paragraph(dataImpuestos[i]));
                        tabImpuestos.AddCell(celda);
                    }

                }


                Doc.Add(tabImpuestos);


                Table TabResumenVenta = new Table(UnitValue.CreatePercentArray(new float[] { 50, 50 }));
                TabResumenVenta.SetWidth(UnitValue.CreatePercentValue(100));

                TabResumenVenta.SetTextAlignment(TextAlignment.LEFT);
                TabResumenVenta.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));

                Cell cellCntidadLetter = new Cell();
                cellCntidadLetter.SetPadding(3f);
                cellCntidadLetter.Add(new Paragraph("SON: " + cantidadEnLetras + " SOLES"));
                cellCntidadLetter.SetFontSize(8);
                cellCntidadLetter.SetBorder(new SolidBorder(ColorConstants.BLACK, 1));
                cellCntidadLetter.SetWidth(30f);

                Cell cellSubtotal = new Cell();
                cellSubtotal.SetBorder(new SolidBorder(ColorConstants.BLACK, 1));
                cellSubtotal.SetWidth(40f);

                Table tabSubtotal = new Table(UnitValue.CreatePercentArray(new float[] { 10, 5 }));
                tabSubtotal.SetWidth(UnitValue.CreatePercentValue(100));
                tabSubtotal.SetTextAlignment(TextAlignment.RIGHT);

                foreach (string item in subtotal)
                {

                    string[] dataSubtotal = item.Split('|');

                    for (int i = 0; i < dataSubtotal.Length; i++)
                    {
                        Cell celda = new Cell();
                        celda.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));
                        celda.SetFontSize(8);
                        celda.Add(new Paragraph(dataSubtotal[i]));
                        tabSubtotal.AddCell(celda);
                    }

                }

                cellSubtotal.Add(tabSubtotal);
                cellSubtotal.SetTextAlignment(TextAlignment.RIGHT);

                cellSubtotal.SetFontSize(8);


                TabResumenVenta.AddCell(cellCntidadLetter);
                TabResumenVenta.AddCell(cellSubtotal);
                Doc.Add(TabResumenVenta);


                Table tabFooter = new Table(UnitValue.CreatePercentArray(new float[] { 70 }));
                tabFooter.SetWidth(UnitValue.CreatePercentValue(70));
                tabFooter.SetHorizontalAlignment(HorizontalAlignment.CENTER);
                tabFooter.SetVerticalAlignment(VerticalAlignment.BOTTOM);
                tabFooter.SetTextAlignment(TextAlignment.CENTER);


                Doc.Add(Separador);
                Doc.Add(Separador);
                Doc.Add(Separador);
                //hash




                Cell celdaHash = new Cell();
                celdaHash.Add(new Paragraph(get_hash));
                celdaHash.SetFontSize(7);
                celdaHash.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));



                Cell celdaDescripcionHash = new Cell();
                celdaDescripcionHash.Add(new Paragraph("Representación impresa de la " + nombreComprobante + " generada desde el sistema facturador SUNAT. Puede verficarla utilizando su clave SOL"));
                celdaDescripcionHash.SetFontSize(7);


                tabFooter.AddCell(celdaHash);
                tabFooter.AddCell(celdaDescripcionHash);
                Doc.Add(tabFooter);

                Doc.Close();

                System.Diagnostics.Process.Start(ArchPlano);

                return true;
            }
            catch (System.Exception)
            {

                Doc.Close();
                return false;
            }
        }





        public static Boolean RecrearDocumento(int id_comprobante, Boolean openPdf)
        {

            string ArchPlano = "";
            string logo = "";
            bool has_logo = false;
            CLNConcepto clncconcepto = new CLNConcepto();
      
            CENParametro listaPar;
            CENEstructuraComprobante cen ;
            try
            {
                //buscamos logo
                listaPar = clncconcepto.buscarParametro(1002);
                logo = @""+listaPar.parametro;
                has_logo = (listaPar.par_tipo == 1) ? true : false;


                CLNComprobante cln = new CLNComprobante();
                CENComprobanteCabecera cab = new CENComprobanteCabecera();
               
                cen = cln.Find(id_comprobante);
                string serie_numero = cen.serieDocumento;
                string Tipocomprobante = cen.tipo_comprobante;
                string Extension = ".pdf";



                string datosEmpresa = Helper.ObtenerValorParametro(CENConstantes.CONST_2);
                string Ruta = datosEmpresa.Split('|')[1];

                string RUC = datosEmpresa.Split('|')[0];
                string razon = datosEmpresa.Split('|')[2];
                string telefono = datosEmpresa.Split('|')[3];
                string celular = datosEmpresa.Split('|')[4];
                string direccion = datosEmpresa.Split('|')[5];
                string Rutapass = Ruta + @"REPO\";


                ArchPlano = Rutapass + RUC + "-" + Tipocomprobante.PadLeft(2, '0') + "-" + serie_numero + Extension;
                //vemos si el archivo existe, si existe solo lo imprimimos directo
                if (File.Exists(ArchPlano))
                {
                    if(openPdf)
                    {
                        System.Diagnostics.Process.Start(ArchPlano);
                       
                    }
                    return true;


                }
                else
                {
                    //sim no existe creamos el documento 
                    FileInfo file = new FileInfo(Ruta);
                    file.Directory.Create();
                    PdfDocument pdfDoc = new PdfDocument(new PdfWriter(ArchPlano));
                    Document Doc = new Document(pdfDoc);

                    try
                    {


                        Doc.SetMargins(60, 20, 50, 20);
                        //Table TabInfoEmpresa = new Table(UnitValue.CreatePercentArray(new float[] { 50, 20, 30 }));
                        Table TabInfoEmpresa;
                       
                        string lineaDataEmpresa = razon + "\n" + ((direccion == "") ? "" : direccion) + "\n" + ((telefono == "") ? "" : "Telefono: " + telefono) + "\n" + ((celular == "") ? "" : "Celular: " + celular);

                        Cell cellEmpresaData = new Cell();
                        cellEmpresaData.Add(new Paragraph(lineaDataEmpresa));
                        cellEmpresaData.SetFontSize(8);
                  
                        cellEmpresaData.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));


                        string[] arraySeriesVenta = cen.serieDocumento.Split('-');

                        string serie = arraySeriesVenta[0];
                        string numero = arraySeriesVenta[1];


                        CLNDocumentTypes clnDT = new CLNDocumentTypes();
                        string get_hash = "";
                        get_hash = clnDT.getHash(id_comprobante);
                        if (get_hash.ToString() == "" || get_hash.ToString() == null)
                        {
                            try
                            {
                                bool xml_hash;
                                xml_hash = clnDT.getHashXML(Tipocomprobante, serie, numero, id_comprobante); 
                                if (!xml_hash)
                                {

                                    Doc.Close();
                                    if (File.Exists(ArchPlano))
                                    {
                                        File.Delete(ArchPlano);
                                    }
                                    return false;
                                }
                                else
                                {
                                    get_hash = clnDT.getHash(id_comprobante);
                                }
                            }
                            catch (Exception)
                            {
                                Doc.Close();
                                if (File.Exists(ArchPlano))
                                {
                                    File.Delete(ArchPlano);
                                }
                                return false;
                            }
                        }


                        decimal anticipo = 0;
                        decimal isc = 0;
                        decimal otrosImpuestos = 0;
                        //decimal descuentoGlobal = 0;
                        //int descuentoTotal = 0;
                        cab.empresa = 1;
                        cab.codigo = serie_numero;
                        cab.fechaEmision = Convert.ToDateTime(cen.Cab_Fac_Fecha);

                        cab.anticipos = anticipo;
                        //cab.descuentoGlobal = descuentoGlobal;
                        //cab.descuentoTotal = descuentoTotal;
                        cab.descuentoGlobal = cen.descuento_global;
                        cab.descuentoTotal = cen.descuento_total;

                        cab.direccionCliente = cen.direccionCustomer;
                        cab.nombreCliente = cen.nomCustomer;
                        cab.documentoCliente = cen.docCustomer;
                        cab.tipoMoneda = CENConstantes.SOLES;
                        cab.igv = (Convert.ToDecimal(cen.SumTotalTributos.ToString()) < 0) ? Convert.ToDecimal((cen.SumTotalTributos * -1).ToString()) : Convert.ToDecimal(cen.SumTotalTributos.ToString());
                        cab.isc = isc;
                        cab.otros = otrosImpuestos;
                        cab.importeTotal = (Convert.ToDecimal(cen.TotalPrecioVenta.ToString()) < 0) ? Convert.ToDecimal((cen.TotalPrecioVenta * -1).ToString()) : Convert.ToDecimal(cen.TotalPrecioVenta.ToString());
                        cab.subtotal = (Convert.ToDecimal(cen.TotalValorVenta.ToString()) < 0) ? Convert.ToDecimal((cen.TotalValorVenta * -1).ToString()) : Convert.ToDecimal(cen.TotalValorVenta.ToString());
                        cab.tipoDocumento = cen.typeDocCustomer;
                        cab.tipoOperacion = 1;
                        cab.empresa = 1;
                        cab.tipoComprobante = Tipocomprobante;





                        List<string> cabecera = new List<string>();

                        cabecera.Add(string.Format("Fecha Emisión|" + cab.fechaEmision.ToString("dd/MM/yyyy") + "||"));
                        //cabecera.Add(string.Format("Señor(es)|" + cab.nombreCliente + "||"));
                        //cabecera.Add(string.Format("R.U.C|" + cab.documentoCliente + "||"));
                        cabecera.Add(string.Format("Señor(es)|" + ((cab.documentoCliente == CENConstantes.DNI_DEFAULT) ? "" : cab.nombreCliente) + "||"));
                        cabecera.Add(string.Format((cab.tipoDocumento == "1") ? "D.N.I|{0}" + "||" : "R.U.C|{0}" + "||", (cab.documentoCliente == CENConstantes.DNI_DEFAULT) ? "" : cab.documentoCliente));
                        //cabecera.Add(string.Format("R.U.C|" + ((cab.documentoCliente == CENConstantes.DNI_DEFAULT) ? "" : cab.documentoCliente) + "||"));

                        cabecera.Add(string.Format("Dirección Cliente|" + cab.direccionCliente + "||"));
                        String nombreComprobante = "";

                        if (Tipocomprobante == "07" || Tipocomprobante == "08")
                        {

                            nombreComprobante = (Tipocomprobante == "07") ? "NOTA DE CREDITO" : "NOTA DE DEBITO";

                            CENDocAfectadoNota listaDocAfN;
                            listaDocAfN = clnDT.getDocAfectadoNota(id_comprobante);
                            string reason_notes = "";
                            if (Tipocomprobante == "07")
                            {
                                reason_notes = listaDocAfN.reason_credit;
                            }
                            else
                            {
                                reason_notes = listaDocAfN.reason_debit;
                            }
                            //aca tenemos que buscar la serie_numero del documento afectado, estoy haciendo el procedimiento almacenado
                            cabecera.Add(string.Format("Tipo Moneda|SOLES" + "|APLICA|" + listaDocAfN.codigo));
                            cabecera.Add(string.Format("N° Transacción|" + id_comprobante + "|MOTIVO|" + reason_notes));
                        }
                        else
                        {

                            nombreComprobante = (Tipocomprobante == "01") ? "FACTURA ELECTRÓNICA" : "BOLETA DE VENTA ELECTRÓNICA";
                            cabecera.Add("Tipo Moneda|SOLES||");
                            cabecera.Add(string.Format("N° Transacción|" + id_comprobante));
                        }


                        //seguimos llenando la cabecera y detalle
                        //crearPDF
                        List<string> datosHash = new List<string>();
                        datosHash.Add(Convert.ToString(cab.igv));
                        datosHash.Add(Convert.ToString(cab.subtotal));
                        datosHash.Add(Convert.ToString(cab.fechaEmision.ToString("yyyy-MM-dd")));
                        datosHash.Add(Convert.ToString(cen.typeDocCustomer));
                        datosHash.Add(Convert.ToString(cen.docCustomer));

                        string linea = RUC + "\r\n" + Tipocomprobante + "\r\n" + serie + "\r\n" + numero + "\r\n" + datosHash[0]
                        + "\r\n" + datosHash[1] + "\r\n" + datosHash[2] + "\r\n" + datosHash[3] + "\r\n" + datosHash[4] + "\r\n" + get_hash.ToString();


                        Cell cellQr = new Cell();
                        cellQr.Add(CreateBarcode(linea, pdfDoc));
                        cellQr.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));



                        Cell cellDataComprobante = new Cell();
                        cellDataComprobante.SetBorder(new SolidBorder(ColorConstants.BLACK, 1));
                        cellDataComprobante.Add(new Paragraph(nombreComprobante + "\n" + "RUC: " + RUC + "\n" + serie + "-" + numero.PadLeft(8, '0')));
                        cellDataComprobante.SetTextAlignment(TextAlignment.CENTER);
                    
                        cellDataComprobante.SetFontSize(8);


                        //LOGO

                        if (has_logo && File.Exists(logo))
                        {

                            //Add a image  
                            iText.IO.Image.ImageData imageData;
                            imageData = iText.IO.Image.ImageDataFactory.Create(logo);
                            Image image = new Image(imageData);
                            image.SetWidth(100);
                            image.SetHeight(100);

                            Cell cellLogo = new Cell();
                            cellLogo.Add(image);
                            cellLogo.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));

                            TabInfoEmpresa = new Table(UnitValue.CreatePercentArray(new float[] { 22, 50, 20, 30 }));
                            TabInfoEmpresa.AddCell(cellLogo);

                        }
                        else
                        {
                            TabInfoEmpresa = new Table(UnitValue.CreatePercentArray(new float[] { 50, 20, 30 }));
                        }
                        //End Logo
                       
                        TabInfoEmpresa.SetWidth(UnitValue.CreatePercentValue(100));
                        TabInfoEmpresa.SetTextAlignment(TextAlignment.LEFT);
                        TabInfoEmpresa.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));

                        TabInfoEmpresa.AddCell(cellEmpresaData);
                        TabInfoEmpresa.AddCell(cellQr);
                        TabInfoEmpresa.AddCell(cellDataComprobante);
                        Doc.Add(TabInfoEmpresa);

                        Table Separador = new Table(UnitValue.CreatePercentArray(1));
                        Separador.SetWidth(UnitValue.CreatePercentValue(100));
                        Cell Cel = new Cell();
                        Cel.Add(new Paragraph(""));
                        Cel.SetHeight(UnitValue.CreatePercentValue(50));
                        Cel.SetBorder(new SolidBorder(ColorConstants.WHITE, 1));
                        Separador.AddCell(Cel);

                        Doc.Add(Separador);
                        Doc.Add(Separador);
                        Doc.Add(Separador);

                        Table tabInfo = new Table(UnitValue.CreatePercentArray(new float[] { 10, 20, 5, 10 }));
                        tabInfo.SetWidth(UnitValue.CreatePercentValue(80));
                        tabInfo.SetTextAlignment(TextAlignment.LEFT);




                        // crea la cebera del comprobante
                        foreach (string item in cabecera)
                        {

                            string[] dataCabecera = item.Split('|');

                            for (int i = 0; i < dataCabecera.Length; i++)
                            {
                                Cell celda = new Cell();
                                celda.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));
                                celda.SetFontSize(8);
                                celda.Add(new Paragraph(dataCabecera[i]));
                                tabInfo.AddCell(celda);
                            }

                        }
                        Doc.Add(tabInfo);



                        Doc.Add(Separador);
                        Doc.Add(Separador);
                        Doc.Add(Separador);

                        string[] NombresHeaderTable = { "Cant.", "Uni. Medida", "Código", "Descripción", "P. Unitario", "Desc", "V. Item" };
                        float[] TamCeldas = { 5, 10, 10, 40, 15, 10, 10 };

                        Table tab = new Table(UnitValue.CreatePercentArray(TamCeldas));
                        tab.SetWidth(UnitValue.CreatePercentValue(100));

                        //se crea los encabezado de la tabla

                        foreach (string columna in NombresHeaderTable)
                        {
                            Cell celda = new Cell();
                            celda.Add(new Paragraph(columna));
                            celda.SetBackgroundColor(ColorConstants.WHITE);
                            celda.SetBorder(new SolidBorder(ColorConstants.BLACK, 1));
                            celda.SetFontColor(ColorConstants.BLACK);
                            celda.SetFontSize(8);
                            celda.SetTextAlignment(TextAlignment.CENTER);

                            tab.AddHeaderCell(celda);
                        }

                        Doc.Add(tab);

                        // se crea el contenido de la tabla

                        Table tabBody = new Table(UnitValue.CreatePercentArray(TamCeldas));
                        tab.SetWidth(UnitValue.CreatePercentValue(100));


                        List<string> body = new List<string>();


                        //listado de detalle 

                        CENComprobanteDetalle det;
                        List<CENComprobanteDetalle> listDet = new List<CENComprobanteDetalle>();
                        foreach (var listR in cen.listaDetalle)
                        {
                            det = new CENComprobanteDetalle();

                            det.cantidad = listR.cantidad;
                            det.unidadMedida = listR.unidadMedidaNota;
                            det.codigo = listR.codigoProductoNota;
                            det.descripcion = listR.Producto;
                            //det.precioUnitario = listR.ValorUnitario;
                            det.precioUnitario = listR.PrecioVtaUnitario;

                            det.descuentoUnitario = Convert.ToDecimal(0);
                            //det.valorItem = listR.SumTribxItem;
                            det.valorItem = listR.ValorUnitario;
                            det.icbper = 0;

                            listDet.Add(det);



                        }
                        List<string> BobyTable = new List<string>();

                        foreach (CENComprobanteDetalle item in listDet)
                        {
                            BobyTable.Add(string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|",
                                (item.cantidad < 0 ? (item.cantidad * -1).ToString("0.00") : item.cantidad.ToString("0.00")),

                            item.unidadMedida.ToString(),
                            item.codigo.ToString()
                            , item.descripcion.ToString()
                            , (item.precioUnitario < 0 ? (item.precioUnitario * -1).ToString("0.00") : item.precioUnitario.ToString("0.00"))

                            , item.descuentoUnitario.ToString("0.00")
                            , (item.valorItem < 0 ? (item.valorItem * -1).ToString("0.00") : item.valorItem.ToString("0.00"))));
                        }

                        foreach (string item in BobyTable)
                        {
                            string[] dataBody = item.Split('|');

                            for (int i = 0; i < dataBody.Length - 1; i++)
                            {
                                Cell celda = new Cell();
                                celda.SetBackgroundColor(ColorConstants.WHITE);
                                celda.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));
                                celda.SetFontSize(8);
                                if(i==1 || i == 2 || i ==3) celda.SetTextAlignment(TextAlignment.JUSTIFIED);  else celda.SetTextAlignment(TextAlignment.RIGHT);
                                celda.SetWidth(25f);
                                celda.Add(new Paragraph(dataBody[i]));
                                tabBody.AddCell(celda);
                            }

                        }


                        Doc.Add(tabBody);

                        Doc.Add(Separador);
                        Doc.Add(Separador);
                        Doc.Add(Separador);

                        ////tabla de resumen de impuestos
                        List<string> impuestos = new List<string>();

                        impuestos.Add(string.Format("IGV: |{0}", cab.igv));
                        impuestos.Add("ISC: | 0.00");
                        impuestos.Add("OTROS: | 0.00");
                        //impuestos.Add("DESC.GLOBAL: | 0.00");   //aplica descuentos
                        impuestos.Add(string.Format("DESC.GLOBAL: | {0}", Math.Round((cab.descuentoGlobal * Convert.ToDecimal(1.18)), 2)));   //aplica descuentos


                        Table tabImpuestos = new Table(UnitValue.CreatePercentArray(new float[] { 10, 5 }));
                        tabImpuestos.SetWidth(UnitValue.CreatePercentValue(100));
                        tabImpuestos.SetTextAlignment(TextAlignment.RIGHT);
                        tabImpuestos.SetMarginBottom(20);


                        foreach (string item in impuestos)
                        {

                            string[] dataImpuestos = item.Split('|');

                            for (int i = 0; i < dataImpuestos.Length; i++)
                            {
                                Cell celda = new Cell();
                                celda.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));
                                celda.SetFontSize(8);
                                celda.Add(new Paragraph(dataImpuestos[i]));
                                tabImpuestos.AddCell(celda);
                            }

                        }


                        Doc.Add(tabImpuestos);


                        Table TabResumenVenta = new Table(UnitValue.CreatePercentArray(new float[] { 50, 50 }));
                        TabResumenVenta.SetWidth(UnitValue.CreatePercentValue(100));

                        TabResumenVenta.SetTextAlignment(TextAlignment.LEFT);
                        TabResumenVenta.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));

                        decimal importeTotalLetras = 0;

                        //importeTotalLetras = (cab.descuentoTotal > 0) ?Math.Round((cab.importeTotal-cab.descuentoTotal),2): Math.Round(cab.importeTotal,2);
                        importeTotalLetras = Math.Round(cab.importeTotal, 2);
                        ConvertNumberToLetter InLetter = new ConvertNumberToLetter();
                        //string cantidadEnLetras = InLetter.InLetter(cab.importeTotal.ToString());
                        string cantidadEnLetras = InLetter.InLetter(importeTotalLetras.ToString());


                        Cell cellCntidadLetter = new Cell();
                        cellCntidadLetter.SetPadding(3f);
                        cellCntidadLetter.Add(new Paragraph("SON: " + cantidadEnLetras + " SOLES"));
                        if(Tipocomprobante == "01") cellCntidadLetter.Add(new Paragraph("FORMA DE PAGO: CONTADO"));
                        cellCntidadLetter.SetFontSize(8);
                        cellCntidadLetter.SetBorder(new SolidBorder(ColorConstants.BLACK, 1));
                        cellCntidadLetter.SetWidth(30f);

                        Cell cellSubtotal = new Cell();
                        cellSubtotal.SetBorder(new SolidBorder(ColorConstants.BLACK, 1));
                        cellSubtotal.SetWidth(40f);

                        Table tabSubtotal = new Table(UnitValue.CreatePercentArray(new float[] { 10, 5 }));
                        tabSubtotal.SetWidth(UnitValue.CreatePercentValue(100));
                        tabSubtotal.SetTextAlignment(TextAlignment.RIGHT);

                        //cuando aplica descuentos
                        decimal importPayable = 0;
                        //if (cab.descuentoTotal > 0)
                        //{
                        //    importPayable = cab.importeTotal - cab.descuentoTotal;
                        //}
                        //else
                        //{
                        //    importPayable = cab.importeTotal;
                        //}

                        importeTotalLetras = cab.importeTotal;
                      
                        importPayable = cab.importeTotal;

                        List<string> subtotal = new List<string>();
                        subtotal.Add(string.Format("Total Valor de venta: |{0}", cab.subtotal));
                        subtotal.Add(string.Format("Descuento Total: | {0}", Math.Round((cab.descuentoTotal*Convert.ToDecimal(1.18)),2)));
                        subtotal.Add("Anticipos: | 0.00");
                        subtotal.Add(string.Format("Importe Total: | {0}", importPayable));


                        foreach (string item in subtotal)
                        {

                            string[] dataSubtotal = item.Split('|');

                            for (int i = 0; i < dataSubtotal.Length; i++)
                            {
                                Cell celda = new Cell();
                                celda.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));
                                celda.SetFontSize(8);
                                celda.Add(new Paragraph(dataSubtotal[i]));
                                tabSubtotal.AddCell(celda);
                            }

                        }

                      
                        cellSubtotal.Add(tabSubtotal);
                        cellSubtotal.SetTextAlignment(TextAlignment.RIGHT);

                        cellSubtotal.SetFontSize(8);


                        TabResumenVenta.AddCell(cellCntidadLetter);
                        TabResumenVenta.AddCell(cellSubtotal);
                        Doc.Add(TabResumenVenta);


                        Table tabFooter = new Table(UnitValue.CreatePercentArray(new float[] { 70 }));
                        tabFooter.SetWidth(UnitValue.CreatePercentValue(70));
                        tabFooter.SetHorizontalAlignment(HorizontalAlignment.CENTER);
                        tabFooter.SetVerticalAlignment(VerticalAlignment.BOTTOM);
                        tabFooter.SetTextAlignment(TextAlignment.CENTER);


                        Doc.Add(Separador);
                        Doc.Add(Separador);
                        Doc.Add(Separador);
                        //hash




                        Cell celdaHash = new Cell();
                        celdaHash.Add(new Paragraph(get_hash));
                        celdaHash.SetFontSize(7);
                        celdaHash.SetBorder(new SolidBorder(ColorConstants.WHITE, 0));


                        //crearDocumento QR

                        Cell celdaDescripcionHash = new Cell();
                        celdaDescripcionHash.Add(new Paragraph("Representación impresa de la " + nombreComprobante + " generada desde el sistema facturador SUNAT. Puede verficarla utilizando su clave SOL"));
                        celdaDescripcionHash.SetFontSize(7);


                        tabFooter.AddCell(celdaHash);
                        tabFooter.AddCell(celdaDescripcionHash);
                        Doc.Add(tabFooter);

                        Doc.Close();

                        if (openPdf)
                        {
                            System.Diagnostics.Process.Start(ArchPlano);
                        }
                        return true;
                    }
                    catch (System.Exception ex)
                    {

                        Doc.Close();
                        if (File.Exists(ArchPlano))
                        {
                            File.Delete(ArchPlano);
                        }
                        return false;
                    }
                    //return true;
                }


            }
            catch (Exception ex)
            {
                if (File.Exists(ArchPlano))
                {
                    File.Delete(ArchPlano);
                }
                return false;
            }


        }
    }
}
