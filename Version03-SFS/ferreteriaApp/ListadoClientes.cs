﻿using ferreteriaApp.CEN;
using ferreteriaApp.CLN;
using System;
using System.Collections.Generic;

using System.Drawing;
using System.Linq;

using System.Windows.Forms;

namespace ferreteriaApp
{
    public partial class ListadoClientes : Form
    {

        List<CENDataCliente> clientes;

        public ListadoClientes()
        {
            InitializeComponent();
          
        }

        public void buscarClientes()
        {
            string[] pr;
            string l_pr = "";
            int i_p = 1;
            int conteo = 0;
          
            pr = this.txt_cliente.Text.Trim().Split(' ');
            conteo = pr.Count();
            foreach (string productos in pr)
            {
                l_pr = l_pr.ToString().Trim() + productos.ToString().Trim();
                if (i_p < conteo) l_pr = l_pr.ToString().Trim() + "|";
                i_p = i_p + 1;
            }


            txt_mensaje.Text = "";

            CLNCliente cln = new CLNCliente();

            if (txt_cliente.Text.Trim() == "" && txt_codigo.Text.Trim() == "")
            {
                clientes = cln.buscarCliente(l_pr, txt_codigo.Text.Trim());

                if (clientes.Count == 0)
                {
                    txt_mensaje.Visible = true;
                    txt_mensaje.Text = "";

                    txt_mensaje.Text = "No se encontraron resultados";
                    listClientes.Rows.Clear();

                }
                else
                {

                    listarProductos(clientes);

                }


            }
            else
            {

                clientes = cln.buscarCliente(l_pr, txt_codigo.Text.Trim());

                if (clientes.Count == 0)
                {
                    txt_mensaje.Visible = true;
                    txt_mensaje.Text = "";

                    txt_mensaje.Text = "No se encontraron resultados con los datos ingresados";
                    listClientes.Rows.Clear();

                }
                else
                {

                    listarProductos(clientes);

                }

            }

            this.listClientes.Focus();
            this.listClientes.Rows[0].Selected = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.buscarClientes();



        }

        private void ListadoProductos_Load(object sender, EventArgs e)
        {
            this.txt_cliente.Focus();

            listClientes.DataSource = null;

            listClientes.ColumnCount = 5;
            listClientes.ColumnHeadersDefaultCellStyle.BackColor = Color.Navy;
            listClientes.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            listClientes.ColumnHeadersDefaultCellStyle.Font =
                new Font(listClientes.Font, FontStyle.Bold);

            listClientes.Name = "listProductos";
            listClientes.Location = new Point(8, 8);
            listClientes.Size = new Size(950, 250);
            listClientes.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            listClientes.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            listClientes.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            listClientes.GridColor = Color.Black;
            listClientes.RowHeadersVisible = false;

            listClientes.Columns[0].Name = "ID";
            listClientes.Columns[1].Name = "DOCUMENTO";
            listClientes.Columns[2].Name = "NOMBRES O RAZON SOCIAL";
            listClientes.Columns[3].Name = "DIRECCION";
            listClientes.Columns[4].DefaultCellStyle.Font = new Font(listClientes.DefaultCellStyle.Font, FontStyle.Italic);
            listClientes.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            listClientes.MultiSelect = false;
            listClientes.Dock = DockStyle.Fill;

            listClientes.Columns[0].Visible = false;
            listClientes.Columns[4].Visible = false;

            foreach (DataGridViewColumn Col in listClientes.Columns)
            {
                Col.SortMode = DataGridViewColumnSortMode.NotSortable;
            }

            
        }
        public void SeleccionarCliente()
        {
            try
            {
                dashboard dash = Owner as dashboard;
                int indice = listClientes.CurrentCell.RowIndex;

                var prod = clientes.ToList().Find(item => item.id == Convert.ToInt32(listClientes.Rows[indice].Cells[0].Value));

                if (prod is null)
                {
                    MessageBox.Show("La fila seleccionada no contiene un cliente");
                }
                else
                {
                    dash.TextRucDni.Text = listClientes.Rows[indice].Cells["DOCUMENTO"].Value.ToString();
                    dash.TextRazonNombres.Text = listClientes.Rows[indice].Cells[2].Value.ToString();
                    dash.TextDireccion.Text = listClientes.Rows[indice].Cells["DIRECCION"].Value.ToString();
                  
                    this.Close();

                }




            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("La fila seleccionada no contiene un cliente");

            }
        }
   
        private void btn_seleccionar_Click(object sender, EventArgs e)
        {
            this.SeleccionarCliente();
        }

        private void listarProductos(List<CENDataCliente> productos)
        {

            txt_mensaje.Text = "";

            listClientes.Rows.Clear();

            var prod = productos.ToList().OrderBy(item => item.nombresCompletos);

            foreach (CENDataCliente item in prod)
            {

                int table = listClientes.Rows.Add();

                listClientes.Rows[table].Cells[0].Value = item.id;
                listClientes.Rows[table].Cells[1].Value = item.num_doc;
                listClientes.Rows[table].Cells[2].Value = item.nombresCompletos;
                listClientes.Rows[table].Cells[3].Value = item.direccion;
                listClientes.Rows[table].DefaultCellStyle.ForeColor = Color.Black;

            }

            


        }

        private void txt_producto_KeyUp(object sender, KeyEventArgs e)
        {

            if(e.KeyCode == Keys.Enter)
            {
                this.buscarClientes();
               
            }


        }

        private void ListadoProductos_Activated(object sender, EventArgs e)
        {
            this.txt_cliente.Focus();
        }

        private void listProductos_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            this.SeleccionarCliente();
        }

        private void listProductos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.listClientes.CurrentRow.Selected = true;
                e.Handled = true;
                SeleccionarCliente();
            }
        }

        private void txt_producto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar.ToString()=="+" 
                || e.KeyChar.ToString() == "*"
                || e.KeyChar.ToString() == "("
                || e.KeyChar.ToString() == ")"
                || e.KeyChar.ToString() == "["
                || e.KeyChar.ToString() == "]"
                )
            {
                e.Handled = true;
                return;
            }

        }

        private void txt_codigo_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.buscarClientes();

            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_close_MouseHover(object sender, EventArgs e)
        {
            this.btn_close.BackColor = Color.Red;
            this.btn_close.ForeColor = Color.White;
        }

        private void btn_close_MouseLeave(object sender, EventArgs e)
        {
            this.btn_close.BackColor = Color.Transparent;
            this.btn_close.ForeColor = Color.Black;
        }
    }
}
